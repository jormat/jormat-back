# Jormat Sandbox Api

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 3.2.0.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7
- [Grunt](http://gruntjs.com/) (`npm install --global grunt-cli`)
- [Grunt](https://gulpjs.com//) (`npm install --global gulp`)
- [MariaDB](https://mariadb.org/)

### Developing

1. Run `npm install` to install server dependencies.

2. In  `./server/config/`  filepath replace the  filename from  `local.env.sample.js to local.env.js`

3. Fill the required details in the `local.env.js` file. (configuration settings to run the sandbox-api)

4. Create the development database with the credentials set inside `local.env.js`.

5. Install sequelize: `npm install -g sequelize-cli`

6. Install sequelize: `npm install -g gulp`

6. Run `grunt build --force` to build the development server. 
      - `grant build --force` CMD  calls the task which injects the router url and intialize all project files. 
         (each time when the new basepath router files  is created  we need to run  `grant build --force`  )

7. Run `grunt serve` or `gulp`  to run the server. (recommend to use gulp)

## Build & development

Run `grunt build --force` for building the router paths
run `grunt serve` or `gulp`  for preview.