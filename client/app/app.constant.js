(function(angular, undefined) {
'use strict';

angular.module('cmmApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);