/**
 * Main application routes
 */
'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
	// Insert routes below
	// Insert routerFilepath below
	// injector:routerFilepath
	app.use("/api/ballots", require("./api/ballots"));
	app.use("/api/billing", require("./api/billing"));
	app.use("/api/categories", require("./api/categories"));
	app.use("/api/clients", require("./api/clients"));
	app.use("/api/demo", require("./api/demo"));
	app.use("/api/documents", require("./api/documents"));
	app.use("/api/finance", require("./api/finance"));
	app.use("/api/guides", require("./api/guides"));
	app.use("/api/invoices", require("./api/invoices"));
	app.use("/api/items", require("./api/items"));
	app.use("/api/locations", require("./api/locations"));
	app.use("/api/notes", require("./api/notes"));
	app.use("/api/orders", require("./api/orders"));
	app.use("/api/payments", require("./api/payments"));
	app.use("/api/providers", require("./api/providers"));
	app.use("/api/purchases", require("./api/purchases"));
	app.use("/api/quotations", require("./api/quotations"));
	app.use("/api/reports", require("./api/reports"));
	app.use("/api/image", require("./api/up-image"));
	app.use("/api/users", require("./api/users"));
	app.use("/api/warehouseDeliveries", require("./api/warehouseDeliveries"));
	app.use("/api/warehouses", require("./api/warehouses"));
	// endinjector
	// app.use('/auth', require('./auth'));


	// All undefined asset or api routes should return a 404 api|
	//app.route('/:url(auth|components|bower_components|assets)/*').get(errors[404]);
}


