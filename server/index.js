'use strict';

// Set default node environment to development
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (env === 'development' || env === 'test') {
	// Register the Babel require hook
	require('babel-core/register');
}

    var environment = require('gulp-env'),
    localenv=require('./config/local.env.js');
	environment({
	     // file: './server/config/local.env.js',
	     // type: '.js',
	     vars: localenv
	});
	

// Export the application
exports = module.exports = require('./app');
