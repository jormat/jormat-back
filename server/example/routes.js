/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';
import expres1 from 'express';

export default function(app,express) {
  // Insert routes below
  
// Insert routerFilepath below
// injector:routerFilepath
app.use("/api/thing", require("./api/thing"));
app.use("/api/user-api", require("./api/user-api"));
app.use("/api/user-api/seeds", require("./api/user-api/seeds"));
app.use("/api/u_forecast_api", require("./api/u_forecast_api"));
app.use("/api/u_forecast_api/routes/academic_programming", require("./api/u_forecast_api/routes/academic_programming"));
app.use("/api/u_forecast_api/routes/file_load", require("./api/u_forecast_api/routes/file_load"));
app.use("/api/u_forecast_api/routes", require("./api/u_forecast_api/routes"));
app.use("/api/u_forecast_api/routes/protobuf", require("./api/u_forecast_api/routes/protobuf"));
app.use("/api/u_forecast_api/routes/schedule_range_administrator", require("./api/u_forecast_api/routes/schedule_range_administrator"));
app.use("/api/u_forecast_api/routes/simulation", require("./api/u_forecast_api/routes/simulation"));
app.use("/api/u_forecast_api/shell", require("./api/u_forecast_api/shell"));
app.use("/api/u_retention_api", require("./api/u_retention_api"));
app.use("/api/u_retention_api/routes", require("./api/u_retention_api/routes"));


// endinjector
  app.use('/auth', require('./auth'));
  // All undefined asset or api routes should return a 404 api|
  app.route('/:url(auth|components|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });

   module.exports=app;
   
}
