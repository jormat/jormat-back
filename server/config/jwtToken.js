/**
 * Configurations of logger.
 */
const nJwt = require('njwt')
const localEnv = require('../config/local.env')
const JwtSecret = localEnv.JWT_TOKEN_SECRET || 'JormatKey2023'

const JwtTokenIntialize = (token, details, callback) => {
	if (token) {
		const tokenBearer = token.replace('Bearer ', '')

		nJwt.verify(tokenBearer, JwtSecret, 'HS256', (err, verifiedJwt) => {
			if (err) {
				callback({
					success: false,
					message: 'Failed to authenticate token. ',
					error  : err,
					data   : false,
				})
			} else {
				callback({
					success: true,
					message: 'authenticate token Success. ',
					data   : verifiedJwt,
				})
			}
		})
	} else {
		callback({
			success: false,
			message: 'Failed to authenticate token. ',
			data   : '',
		})
	}
}

module.exports = {
	JwtTokenIntialize,
}
