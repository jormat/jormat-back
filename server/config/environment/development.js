'use strict';

// Development specific configuration
// ==================================

module.exports = {
	seedDB: true
};

if (process.env.GENERIC_DB==1){
    module.exports.sequelize2=1;
    module.exports.sequelize= {
		database: process.env.GENERIC_DB_DEVELOPMENT_DATABASE,
		username: process.env.GENERIC_DB_USERNAME,
		password: process.env.GENERIC_DB_PASSWORD,		
		options: {
			host: ((process.env.GENERIC_DB_HOST)?process.env.GENERIC_DB_HOST:'localhost'),
			dialect: process.env.GENERIC_DB_DIALECT,
			port: ((process.env.GENERIC_DB_PORT)?process.env.GENERIC_DB_PORT:'3306'),
			logging: ((process.env.GENERIC_DB_LOGGING)?((process.env.GENERIC_DB_LOGGING==0)?false:true):true),
			pool: {
				max: 5,
				min: 0,
				idle: 10000
			},
			dialectOptions: {
				multipleStatements: true
			}
		}
    }
}else if (process.env.GENERIC_DB==3){
    module.exports.sequelize3=3;
    module.exports.mssql= {
		database: process.env.GENERIC_DB_DEVELOPMENT_DATABASE,
		user: process.env.GENERIC_DB_USERNAME,
		password: process.env.GENERIC_DB_PASSWORD,	
		server: process.env.GENERIC_DB_DEVELOPMENT_SERVER,	
		options: {
			port: ((process.env.GENERIC_DB_PORT)?process.env.GENERIC_DB_PORT:'1433'),
			connectTimeout  : 300000,
			requestTimeout  : 300000
		}
    }
}else{  		
    module.exports.sequelize2=0;
    module.exports.sequelize= {
		database: process.env.MARIADB_DEVELOPMENT_DATABASE,
		username: process.env.MARIADB_USERNAME,
		password: process.env.MARIADB_PASSWORD,		
		options: {
			host: ((process.env.MARIADB_HOST)?process.env.MARIADB_HOST:'localhost'),
			dialect: 'mariadb',
			port: ((process.env.MARIADB_PORT)?process.env.MARIADB_PORT:'3306'),
			logging: ((process.env.MARIADB_LOGGING)?((process.env.MARIADB_LOGGING==0)?false:true):true),
			timezone: process.env.MARIADB_TIMEZONE,						
			pool: {
				max: 5,
				min: 0,
				idle: 10000				
			},
			dialectOptions: {
				multipleStatements: true
			}
		}
	}
}