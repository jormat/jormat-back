'use strict';

var config = require('./local.env.js');

module.exports = {
    development: {
        database: config.MARIADB_DEVELOPMENT_DATABASE,
        username: config.MARIADB_USERNAME,
        password: config.MARIADB_PASSWORD,
        options: {
            host: 'localhost',
            dialect: 'mariadb',

            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    }
}
