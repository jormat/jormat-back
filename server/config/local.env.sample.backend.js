'use strict';
// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.
module.exports = {
    DOMAIN:                       'http://localhost:9000',
    PORT :                         9000,
    SESSION_SECRET:               'cmm-secret',
    DEBUG_PORT:                    5000,
    // Control debug level for modules using visionmedia/debug
    DEBUG:                          '',
    MARIADB_DEVELOPMENT_DATABASE:   '_db_name',
    MARIADB_TEST_DATABASE:          '_db_name',
    MARIADB_USERNAME:               '_db_user',
    MARIADB_PASSWORD:               '_db_pass',
    MARIADB_PORT:                   '3306',
    MARIADB_HOST:                   '_db_ip',
    PROTOCOL:                       'http',
    MAX_OLD_SPACE_SIZE:1024
    
};