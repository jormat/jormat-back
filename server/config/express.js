/**
 * Express configuration
 */
'use strict';

import express from 'express';
import favicon from 'serve-favicon';
import morgan from 'morgan';
import compression from 'compression';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import cookieParser from 'cookie-parser';
import errorHandler from 'errorhandler';
import path from 'path';
import lusca from 'lusca';
import config from './environment';
import passport from 'passport';
import session from 'express-session';
// import cookieSession from 'cookie-session';
import sqldb from '../sqldb';
import expressSequelizeSession from 'express-sequelize-session';
const JwtToken = require('./jwtToken')

var Store=null,storeValue=null
if(process.env.GENERIC_DB!=3){
     var Store = expressSequelizeSession(session.Store);
     var storeValue= new Store(sqldb.sequelize)
}

var v8 = require('v8')

let totalHeapSize=v8.getHeapStatistics().total_available_size
let totalHeapSizeInGB=(totalHeapSize/1024/1024/1024).toFixed(2)
console.log(`total heap size (bytes) ${totalHeapSize}, (GB ${totalHeapSizeInGB})`)





export default function(app) {
    const env = app.get('env');
    // app.use(cookieSession({
    //     name: 'session',
    //     keys: ['key1', 'key2'],
    // }))
    app.set('views', config.root + '/server/views');
    app.set('view engine', 'jade');
    app.use(compression());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json({limit: '50mb'}));
    // app.use(bodyParser.raw({type: function(){return true;}, limit: '5mb'}));
    app.use(methodOverride());
    app.use(cookieParser());
    app.use(passport.initialize());
    app.use('/', express.static(__dirname));

    // Persist sessions with mongoStore / sequelizeStore
    // We need to enable sessions for passport-twitter because it's an
    // oauth 1.0 strategy, and Lusca depends on sessions
    app.use(session({
        secret: config.secrets.session,
        saveUninitialized: true,
        resave: false,
        store: storeValue
    }));



    /**
    * Lusca - express server security
    * https://github.com/krakenjs/lusca
    */
    if ('test' !== env) {
        // app.use(lusca({
        //     csrf: {
        //         angular: true
        //     },
        //     xframe: 'SAMEORIGIN',
        //     hsts: {
        //         maxAge: 31536000, //1 year, in seconds
        //         includeSubDomains: true,
        //         preload: true
        //     },
        //     xssProtection: true
        // }));
    }

    app.set('appPath', path.join(config.root, 'client'));

    if ('production' === env) {
        app.use(favicon(path.join(config.root, 'client', 'favicon.ico')));
        app.use(express.static(app.get('appPath')));
        app.use(morgan('dev'));
    }

	app.use(require('connect-livereload')({ ignore: ['xport','.pdf','csv','get','down','valid','.xlsx','.xml'] }));
	app.use('/', express.static(path.join(config.root, '/temp')));
	app.use('/', express.static(path.join(config.root, '/webservice')));

    if ('development' === env) {
        app.use(require('connect-livereload')({ ignore: ['xport','.pdf','csv','get','down','valid','.xlsx','.xml'] }));
    }

    if ('development' === env || 'test' === env) {
        app.use(express.static(path.join(config.root, '.tmp')));
        app.use(express.static(app.get('appPath')));
        app.use(morgan('dev'));
        app.use(errorHandler()); // Error handler - has to be last
    }

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,x-access-token");
        const isValidRequest = ([ 'POST', 'PUT', 'DELETE', 'GET', 'PATCH' ].indexOf(req.method) >= 0)

		if ((process.env.JWT_TOKEN_ACTIVATE == 'true' || process.env.JWT_TOKEN_ACTIVATE == true) && isValidRequest) {
            const token = req.headers['x-access-token'] || req.headers['authorization']
			if (routeRequireTokenValidation(req.url)) {
				JwtToken.JwtTokenIntialize(token, null, function (result) {
					if (result.success == true) {
						req.jwt = result.data.body
						next()
					} else {
						res.status(401).send({
							status : false,
							message: 'authenticate token Failed',
						})
					}
				})
			} else {
                next()
            }
        } else {
            req.jwt = {}
			next()
        }
    })

    const routeRequireTokenValidation = (url) => {
        return (
            !RegExp(/\/api\/users\/login/gi).test(url) &&
			!RegExp(/\/api\/image/gi).test(url) &&
			!RegExp(/\/api\/purchases\/multiple/gi).test(url) &&
            !RegExp(/\/api\/users\/uploadFiles/gi).test(url) &&
			!RegExp(/\/api\/users\/userCreateMobile/gi).test(url) &&
			!RegExp(/\/api\/users\/access/gi).test(url) &&
            !RegExp(/\/api\/users\/file/gi).test(url) &&
            !RegExp(/\/api\/purchases\/file/gi).test(url) &&
            !RegExp(/\/api\/guides\/uploadGuides/gi).test(url) &&
            !RegExp(/\/api\/quotations\/uploadQuotations/gi).test(url) &&
            !RegExp(/\/api\/items\/uploadImports/gi).test(url) &&
            !RegExp(/\/api\/items\/uploadItems/gi).test(url) &&
            !RegExp(/\/api\/orders\/uploadOrders/gi).test(url) &&
            !RegExp(/\/api\/items\/downloadItemsPDF/gi).test(url) &&
            !RegExp(/\/api\/pbi/gi).test(url)
        )
    }
}
