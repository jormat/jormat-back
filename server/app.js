/**
 * Main application file
 */

'use strict';

import express from 'express';
import config from './config/environment';
import fs from 'fs';



var  http =  require('http');
  
import sqldb from './sqldb';


// Populate databases with sample data
if (config.seedDB) { require('./config/seed'); }


// Setup server
var app = express();

var server = http.createServer(app);

// Insert app below
// Insert helperFilepath below
// injector:helperFilepath
require("./api/demo/helper/demo.helper.js");
require("./api/helpers/fileConverter.helper.js");
require("./api/items/export.helper.js");
// endinjector


require('./config/express')(app);
require('./routes')(app);

// Start server


		
		 
		server.listen(config.port, config.ip, function() {
			
			console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
		});



// Expose app
exports = module.exports = app;
