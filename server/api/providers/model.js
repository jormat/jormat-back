
'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');


function _getList(userId){

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                    P.id as "providerId",
                    P.dsName as "providerName",
                    P.dsPhoneNumber as "phone",
                    P.dsMobileNumber as "mobile",
                    P.dsCode as  "rut",
                    P.dsEmail as "email",
                    if(p.type=1, 'Nacional','Internacional') AS "type"
                    FROM provider P WHERE inStatus = 1
                    ORDER BY    P.id DESC  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getProvidersDetails(providerId){

    var deferred = Q.defer()

    let sql = `
            SELECT DISTINCT
            P.id as "providerId",
            P.dsName as "providerName",
            P.dsCode as  "rut",
            P.dsPhoneNumber as "phone",
            P.dsMobileNumber as "mobile",
            P.dsEmail as "email",
            P.dsAddress as   "address",
            P.dsCity as "cityName",
            P.dsUrl as "web",
            P.dsDesc as "commercialBusiness",
            P.dsAccount AS "bankAccount",
            P.inStatus as "active",
            p.type AS "type"
            FROM provider p
            WHERE       P.id=:providerId;`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            providerId:providerId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createProvider(data){

    var deferred = Q.defer()
    if(data.mobile==undefined) data.mobile="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.web==undefined) data.web="www."
    if(data.bankAccount==undefined) data.bankAccount="no definido"
    if(data.commercialBusiness==undefined) data.commercialBusiness="Giro no definido"
    
    let sql = ` INSERT INTO provider (
                                dsName,
                                dsCode,
                                dsAddress,
                                dsAccount,
                                dsPhoneNumber,
                                dsMobileNumber,
                                dsEmail,
                                dsUrl,
                                dsCity,
                                dsDesc,
                                type,
                                inStatus) 
                      VALUES (  :providerName,
                                :rut,
                                :address,
                                :bankAccount,
                                :phone,
                                :mobile,
                                :email,
                                :web,
                                :cityName,
                                :commercialBusiness,
                                :type,
                                 1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerName: data.providerName,
            rut: data.rut,
            address: data.address,
            bankAccount: data.bankAccount,
            phone: data.phone,
            mobile: data.mobile,
            email: data.email,
            web: data.web,
            cityName: data.cityName,
            commercialBusiness: data.commercialBusiness,
            type: data.type

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise

}

function _updateProvider(data){

    var deferred = Q.defer()
    if(data.mobile=='') data.mobile="s/n"
    if(data.address=='') data.address='Sin dirección'
    if(data.web=='') data.web="www."
    if(data.bankAccount=='') data.bankAccount="no definido"
    if(data.commercialBusiness=='') data.commercialBusiness="Giro no definido"
    
    let sql = `UPDATE  provider SET 
                                 dsName=:providerName,
                                 dsCode=:rut,
                                 dsAddress=:address,
                                 dsAccount=:bankAccount,
                                 dsPhoneNumber=:phone,
                                 dsMobileNumber=:mobile,
                                 dsEmail=:email,
                                 dsUrl=:web,
                                 dsCity=:cityName,
                                 dsDesc=:commercialBusiness,
                                 type=:type
                          WHERE  provider.id=:providerId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId: data.providerId,
            providerName: data.providerName,
            rut: data.rut,
            address: data.address,
            bankAccount: data.bankAccount,
            phone: data.phone,
            mobile: data.mobile,
            email: data.email,
            web: data.web,
            cityName: data.cityName,
            commercialBusiness: data.commercialBusiness,
            type: data.type


        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledProvider(providerId){

    var deferred = Q.defer()
    let sql = ` UPDATE provider SET inStatus=0 where id=:providerId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            providerId:providerId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getList: _getList,
    _getProvidersDetails:_getProvidersDetails,
    _createProvider:_createProvider,
    _updateProvider:_updateProvider,
    _disabledProvider:_disabledProvider,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}