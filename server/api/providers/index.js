var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/', service.getProvidersDetails);
router.post('/', service.createProvider);
router.put('/', service.updateProvider);
router.delete('/', service.disabledProvider);
router.get('/list', service.getProviders);

module.exports = router;