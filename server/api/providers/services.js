'use strict'

var model = require('./model');


function getProviders(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getProvidersDetails(req, res) {

    var query = [];
    var providerId = req.query['providerId'];
    query.push(model._getProvidersDetails(providerId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createProvider(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._createProvider(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the provider is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateProvider(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._updateProvider(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the provider is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledProvider(req, res) {

    var query = [];
    var providerId = req.query['providerId'];
    var userId = req.query['userId'];
    query.push(model._disabledProvider(providerId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the provider is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}
module.exports = {
    getProviders: getProviders,
    getProvidersDetails:getProvidersDetails,
    createProvider:createProvider,
    updateProvider:updateProvider,
    disabledProvider:disabledProvider,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}