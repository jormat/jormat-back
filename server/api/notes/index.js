const express = require('express');

// view engine setup
const router = express.Router();
const service = require('./services');


//credit Notes services
router.get('/credit/list', service.getCreditNotes);
router.get('/credit/', service.getCreditNotesDetails);
router.post('/credit/', service.createCreditNote);
router.post('/credit/failure', service.createCreditNoteFailure);
router.put('/credit/', service.updateCreditNote);
router.delete('/credit/', service.disabledCreditNote);
router.get('/credit/items', service.getCreditNotesItems);
router.put('/credit/validate', service.validateCreditNote);
router.get('/credit/report', service.getReportNotes);

//sale Notes services
router.get('/sale/list', service.getSaleNotes);
router.get('/sale/', service.getSaleNotesDetails);
router.get('/sale/items', service.getSaleNotesItems);
router.post('/sale/', service.saleNoteCreate);
router.put('/sale/update', service.saleNoteUpdate);
router.post('/sale/changeStatu', service.setSaleNotes);

//supplier Return (credit Notes providers)
router.get('/returns/list', service.getSupplierReturn);
router.get('/returns/', service.getSupplierReturnDetails);
router.get('/returns/items', service.getSupplierReturnItems);
router.post('/returns/', service.createSupplierReturn);

module.exports = router;