'use strict'

var model = require('./model');

// credit notes 

function getCreditNotes(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getCreditNotes(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCreditNotesDetails(req, res) {

    var query = [];
    var creditNoteId = req.query['creditNoteId'];
    query.push(model._getCreditNotesDetails(creditNoteId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createCreditNote(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createCreditNote(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsCreditNote(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create credit note",
            status: 0
        });
    }
}

function validateCreditNote(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._validateCreditNote(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the note is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createCreditNoteFailure(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createCreditNote(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsCreditNoteFailure(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create credit note",
            status: 0
        });
    }
}

function updateCreditNote(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._updateCreditNote(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledCreditNote(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledCreditNote(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCreditNotesItems(req, res) {

    var query = [];
    var creditNoteId = req.query['creditNoteId'];
    query.push(model._getCreditNotesItems(creditNoteId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getReportNotes(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getReportNotes(startDate,endDate))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSaleNotes(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getSaleNotes(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSaleNotesDetails(req, res) {

    var query = [];
    var saleNoteId = req.query['saleNoteId'];
    query.push(model._getSaleNotesDetails(saleNoteId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSaleNotesItems(req, res) {

    var query = [];
    var saleNoteId = req.query['saleNoteId'];
    query.push(model._getSaleNotesItems(saleNoteId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function saleNoteCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._saleNoteCreate(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createSaleNoteItems(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create sale note",
            status: 0
        });
    }
}

function saleNoteUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._saleNoteUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the note is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

const setSaleNotes = (req, res) => {
	const data = req.body;
	var query = [];
	query.push(model._setSaleNotes(data))
	if(req.body){
		Promise.all(query).then((result) => {
			res.json({
				data: result[0][1][0],
				status: 1
			});
		}).catch((error) => {
			res.json({
				data: error,
				status: 0
			});
		})
	}else{
		res.json({
			data: [],
			messagge:"not data this services setSaleNotes",
			status: 0
		});
	}
}

function getSupplierReturn(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getSupplierReturn(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSupplierReturnDetails(req, res) {

    var query = [];
    var returnId = req.query['returnId'];
    query.push(model._getSupplierReturnDetails(returnId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSupplierReturnItems(req, res) {

    var query = [];
    var returnId = req.query['returnId'];
    query.push(model._getSupplierReturnItems(returnId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createSupplierReturn(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createSupplierReturn(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createSupplierReturnItems(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create supplier return",
            status: 0
        });
    }
}

module.exports = {
    getCreditNotes,
    getCreditNotesDetails,
    createCreditNote,
    validateCreditNote,
    createCreditNoteFailure,
    updateCreditNote,
    disabledCreditNote,
    getCreditNotesItems,
    getReportNotes,
    getSaleNotes,
    getSaleNotesDetails,
    getSaleNotesItems,
    saleNoteCreate,
    saleNoteUpdate,
    setSaleNotes,
    getSupplierReturn,
    getSupplierReturnDetails,
    getSupplierReturnItems,
    createSupplierReturn,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}