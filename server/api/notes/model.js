'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

// credit notes
function _getCreditNotes(){

     var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        n.noteId as creditNoteId,
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        n.total,
                        IFNULL(n.total - SUM(pn.total),n.total) AS pending,
                        w.dsCode AS "origin",
                        c.dsCode AS "rut",
                        n.invoiceId,
                        DATE_FORMAT(n.date, '%d/%m/%y') as date,
                        n.noteId,
                        u.usName AS "userCreation",
                        if(n.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(n.type=1, 'Devolución','Falla') AS "type"
                        
                    FROM creditnotes n
                    inner JOIN  clients c
                    ON  n.clientId = c.id
                    left JOIN  warehouse w
                    ON  n.origin = w.id
                    left JOIN  users u
                    ON  n.usId = u.usId
                    left  JOIN   paymentsnotes pn 
                    ON    n.noteId = pn.noteId
                    WHERE n.inStatus = 1
                    GROUP BY       n.noteId
                    order BY       n.noteId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCreditNotesDetails(creditNoteId){

    
    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        n.noteId as creditNoteId,
                        c.dsName AS clientName,
                        c.dsCode AS rut,
                        n.total,
                        ROUND(n.total/1.19,0) as "netPrice",
                        w.dsCode AS "origin",
                        n.invoiceId,
                        n.total - ROUND(n.total/1.19)  as "priceVAT",
                        DATE_FORMAT(n.date, '%d/%m/%y') as date,
                        DATE_FORMAT(n.date, '%H:%i:%S') as time,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(n.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(n.type=1, 'Devolución','Falla') AS "type",
                        n.commentary AS "comment"
                        
                    FROM creditNotes n
                    inner JOIN  clients c
                    ON  n.clientId = c.id
                    left JOIN  warehouse w
                    ON  n.origin = w.id
                    left JOIN  users u
                    ON  n.usId = u.usId
                    left JOIN  users us
                    ON  n.usModifierId = us.usId
                    WHERE n.noteId =  :creditNoteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            creditNoteId:creditNoteId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _createCreditNote(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"

    let sql = `INSERT INTO creditnotes(date,clientId,invoiceId,status,total,inStatus,origin,commentary,usId,type)
                      VALUES (NOW(),:clientId,:invoiceId,0,:total,1,:originId,:observation,:userId,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            invoiceId:data.invoiceId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            type:data.type,
            observation:data.comment
        }
    }).then(function (creditNoteId) {
        deferred.resolve(creditNoteId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _validateCreditNote(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE creditnotes SET status=1, usModifierId =:userId WHERE noteId=:creditNoteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            creditNoteId:data.creditNoteId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsCreditNote(items,creditNoteId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO creditnotesitems (noteId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:creditNoteId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty+:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            creditNoteId:creditNoteId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsCreditNoteFailure(items,creditNoteId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO creditnotesitems (noteId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:creditNoteId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            creditNoteId:creditNoteId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateCreditNote(model){

    var data = [
       {
          "messagge": "the credit note is update succesfully",
        }
    ]

    return data;
}

function _disabledCreditNote(model){

    var data = [
       {
          "messagge": "the credit note is disabled succesfully",
        }
    ]

    return data;
}

function _getCreditNotesItems(creditNoteId){

    var deferred = Q.defer()

    let sql = `SELECT   itemId,
                        i.dsName AS itemDescription,
                        cni.nmUnities as quantityItems,
                        cni.nmPrice AS price,
                        cni.total
                FROM creditnotesitems cni
                INNER JOIN items i
                ON  i.id = cni.itemId
                WHERE noteId = :creditNoteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            creditNoteId:creditNoteId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getReportNotes(startDate,endDate){

    var deferred = Q.defer()

    let sql = `  SELECT DISTINCT
                        n.noteId as creditNoteId,
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        n.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(n.date, '%d/%m/%y') as date,
                        n.invoiceId,
                        u.usName AS "userCreation",
                        if(n.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(n.type=1, 'Devolución','Falla') AS "type"
                        
                    FROM creditnotes n
                    inner JOIN  clients c
                    ON  n.clientId = c.id
                    left JOIN  warehouse w
                    ON  n.origin = w.id
                    left JOIN  users u
                    ON  n.usId = u.usId
                    WHERE n.date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND n.inStatus = 1
                    order BY n.noteId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getSaleNotes(){

    var deferred = Q.defer()

    let sql = ` SELECT DISTINCT
                        sn.saleNoteId as "saleNoteId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        sn.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(sn.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        sn.discount as "discount",
                        sn.isOut,
                        if(sn.status=1, 'Pendiente','Facturado') AS "statusName",
                        sn.isOut,
                        CASE 
                            WHEN sn.isOut IS NULL THEN 'Por Entregar' 
                            WHEN sn.isOut = 1 THEN 'Entregado' 
                            WHEN sn.isOut = 2 THEN 'Incompleto' 
                        END AS dsOut,
                        CASE 
                            WHEN sn.isOut IS NULL THEN 'danger' 
                            WHEN sn.isOut = 1 THEN 'success' 
                            WHEN sn.isOut = 2 THEN 'warning' 
                        END AS styleOut
                    FROM salenote sn
                    inner JOIN  clients c
                    ON  sn.clientId = c.id
                    left JOIN  warehouse w
                    ON  sn.origin = w.id
                    left JOIN  users u
                    ON  sn.usId = u.usId
                    WHERE sn.inStatus = 1
                    order BY sn.saleNoteId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

const _setSaleNotes = (data) => {
	const deferred = Q.defer()

	let sql = `	UPDATE	salenote
				SET		status = 2,
						usModifierId = ${data.userId}
				WHERE	saleNoteId = ${data.documentId};
				SELECT		DISTINCT
							sn.saleNoteId as "saleNoteId",
							c.id AS "clientId",
							c.dsName AS "clientName",
							sn.total,
							w.dsCode AS "origin",
							DATE_FORMAT(sn.date, '%d/%m/%y') as date,
							u.usName AS "userCreation",
							sn.discount as "discount",
							if(sn.status=1, 'Pendiente','Facturado') AS "statusName",
                            CASE WHEN sn.isOut IS NULL THEN 'Por Entregar' ELSE 'Entregado' END AS dsOut
				FROM 		salenote sn
				INNER JOIN	clients c
				ON			sn.clientId = c.id
				LEFT JOIN	warehouse w
				ON			sn.origin = w.id
				LEFT JOIN	users u
				ON			sn.usId = u.usId
				WHERE		sn.inStatus = 1 
				AND			sn.saleNoteId = ${data.documentId}`
	sequelize.query(sql, {
		type: Sequelize.QueryTypes.SELECT,
	}).then((result) => {
		deferred.resolve(result)
	}).catch((error) => {
		deferred.reject(error)
	})

	return deferred.promise
}


function _getSaleNotesDetails(saleNoteId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        sn.saleNoteId as "saleNoteId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        sn.total,
                        ROUND(sn.total/1.19,0) as "netPrice",
                        sn.total - ROUND(sn.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        DATE_FORMAT(sn.date, '%d/%m/%y') as date,
                        DATE_FORMAT(sn.date, '%H:%i:%S') as time,
                        u.usName AS "userCreation",
                        u2.usName AS "userOut",
                        sn.isOut,
                        if(sn.status=1, 'Pendiente','Facturado') AS "statusName",
                        CASE WHEN sn.isOut IS NULL THEN 'Por Entregar' ELSE 'Entregado' END AS dsOut,
                        sn.discount AS "discount",
                        sn.nameDescription,
                        sn.phone,
                        sn.commentary AS "comment",
                        td.code AS typeName,
						td.id AS typeId
                    FROM salenote sn
                    inner JOIN  clients c
                    ON  sn.clientId = c.id
                    left JOIN  warehouse w
                    ON  sn.origin = w.id
                    left JOIN  users u
                    ON  sn.usId = u.usId
                    left JOIN  users u2
                    ON  sn.userOutId = u2.usId
                    LEFT JOIN	typedocument td
			        ON			td.id = sn.typeDoc
                    WHERE sn.saleNoteId =:saleNoteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            saleNoteId:saleNoteId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getSaleNotesItems(saleNoteId){

    var deferred = Q.defer()

    let sql = `SELECT   i.id AS itemId,
                        i.dsName AS itemDescription,
                        i.netPrice AS netPurchaseValue,
                        i.dsMaxDiscount AS maxDiscount,
                        i.oil AS oil,
                        i.webType as webType,
                        ITR.dsReference as referenceKey,
                        sni.nmUnities as quantityItems,
                        sni.nmPrice AS price,
                        sni.discount AS discount,
                        sni.total
                FROM salenoteitems sni
                INNER JOIN items i
                ON  i.id = sni.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE saleNoteId =:saleNoteId
                GROUP BY    sni.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            saleNoteId:saleNoteId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _saleNoteCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.clientId == undefined) data.clientId = 4934
    if(data.name == undefined) data.name="s/n"
    if(data.phone == undefined) data.phone = "s/n"

    let sql = `INSERT INTO salenote(date,clientId,status,total,inStatus,origin,commentary,discount,usId,nameDescription,phone)
                      VALUES (NOW(),:clientId,1,:total,1,:originId,:observation,:discount,:userId,:name,:phone)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        replacements:{
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            name:data.name,
            phone:data.phone,
            observation:data.comment
        }
    }).then(function (quotationId) {
        deferred.resolve(quotationId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createSaleNoteItems(items,saleNoteId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO salenoteitems (saleNoteId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:saleNoteId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            saleNoteId:saleNoteId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _saleNoteUpdate(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE salenote SET status=2, usModifierId =:userId WHERE saleNoteId=:saleNoteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            saleNoteId:data.saleNoteId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getSupplierReturn(){

     var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        s.returnId as returnId,
                        p.id AS "providerId",
                        p.dsName AS "providerName",
                        s.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(s.date, '%d/%m/%y') as date,
                        s.invoiceId,
                        u.usName AS "userCreation",
                        if(s.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(s.type=1, 'Devolución','Falla') AS "type"
                        
                    FROM supplierreturn s
                    inner JOIN  provider p
                    ON  s.providerId = p.id
                    left JOIN  warehouse w
                    ON  s.origin = w.id
                    left JOIN  users u
                    ON  s.usId = u.usId
                    WHERE s.inStatus = 1
                    order BY s.returnId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getSupplierReturnDetails(returnId){

    
    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        s.returnId as returnId,
                        p.dsName AS providerName,
                        p.dsCode AS rut,
                        s.total,
                        ROUND(s.total/1.19,0) as "netPrice",
                        w.dsCode AS "origin",
                        s.invoiceId,
                        s.total - ROUND(s.total/1.19)  as "priceVAT",
                        DATE_FORMAT(s.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(s.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(s.type=1, 'Devolución','Falla') AS "type",
                        s.commentary AS "comment"
                        
                    FROM supplierreturn s
                    inner JOIN  provider p
                    ON  s.providerId = p.id
                    left JOIN  warehouse w
                    ON  s.origin = w.id
                    left JOIN  users u
                    ON  s.usId = u.usId
                    left JOIN  users us
                    ON  s.usModifierId = us.usId
                    WHERE s.returnId =  :returnId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            returnId:returnId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getSupplierReturnItems(returnId){

    var deferred = Q.defer()

    let sql = `SELECT   itemId,
                        i.dsName AS itemDescription,
                        si.nmUnities as quantityItems,
                        si.nmPrice AS price,
                        si.total
                FROM supplierreturnitems si
                INNER JOIN items i
                ON  i.id = si.itemId
                WHERE returnId = :returnId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            returnId:returnId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _createSupplierReturn(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"

    let sql = `INSERT INTO supplierreturn(date,providerId,invoiceId,status,total,inStatus,origin,commentary,usId,type)
                      VALUES (NOW(),:providerId,:invoiceId,0,:total,1,:originId,:observation,:userId,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            invoiceId:data.invoiceId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            type:data.type,
            observation:data.comment
        }
    }).then(function (returnId) {
        deferred.resolve(returnId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createSupplierReturnItems(items,returnId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO supplierreturnitems (returnId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:returnId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            returnId:returnId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getCreditNotes,
    _getCreditNotesDetails,
    _createCreditNote,
    _validateCreditNote,
    _createItemsCreditNote,
    _createItemsCreditNoteFailure,
    _updateCreditNote,
    _disabledCreditNote,
    _getCreditNotesItems,
    _getReportNotes,
    _getSaleNotes,
    _getSaleNotesDetails,
    _getSaleNotesItems,
    _saleNoteCreate,
    _createSaleNoteItems,
    _saleNoteUpdate,
    _setSaleNotes,
    _getSupplierReturn,
    _getSupplierReturnDetails,
    _getSupplierReturnItems,
    _createSupplierReturn,
    _createSupplierReturnItems,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}