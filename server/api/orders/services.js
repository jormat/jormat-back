'use strict'

var model = require('./model');

const fs = require('fs')
const csv = require('csv-express')
const Parse = require('csv-parse')

//Ballots clients

function uploadOrders(req, res) {
    const fileUpload = req.file

    if (!fileUpload) {
        const error = {
            parent : { errno: 1 },
            message: 'archivo omitido',
            sql    : 'no sql',
        }

        res.json({
            data: [],
            status: 0,
            error
        });

        return
    }

    const folderPath = fileUpload.path.toString()
    const source = fs.createReadStream(folderPath)
    const parser = Parse({ delimiter: ';', columns: true })
    let linesRead = 0
    let output = []

    parser.on('readable', function () {
        let record = parser.read()

        while (record) {
            if ((Object.keys(record).length == 1)) {
                if (record[Object.keys(record)[0]].length != 0) {
                    errorLectura = "Delimitador debe ser ';' "

                    return false
                }
            } else {
                linesRead++
                if(record.codigo != undefined && record.codigo != null && record.codigo != '') {
                    output.push({
                        itemId : record.codigo,
                        itemDescription : record.nombre,
                        quantity : (record.cantidad == undefined || record.cantidad == null || record.cantidad == '') ? 0 : record.cantidad,
                        disscount : (record.desc == undefined || record.desc == null || record.desc == '') ? 0 : record.desc,
                        netPrice : (record.precio == undefined || record.precio == null || record.precio == '') ? 0 : record.precio,
                        referenceKey : (record.referencia == undefined || record.referencia == null || record.referencia == '') ? 0 : record.referencia
                    })
                }
            }

            record = parser.read()
        }
    })

    parser.on('end', function () {
        if (linesRead == 0) {
            res.status(500).json({ status: false, error: 'Archivo Vacio' })

            return false
        }

        res.json({
            data    : output,
            status     : 1,
        })
    })

    parser.on('error', function () {
        res.status(500).json({ status: false, error: 'Error de Formato' })

        return false
    })

    source.pipe(parser)
}

function getOrders(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getOrders(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOrderDetails(req, res) {

    var query = [];
    var orderId = req.query['orderId'];
    query.push(model._getOrderDetails(orderId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOrderItems(req, res) {

    var query = [];
    var orderId = req.query['orderId'];
    var originId = req.query['originId'];
    var destinationId = req.query['destinationId'];
    query.push(model._getOrderItems(orderId,originId,destinationId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createOrder(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createOrder(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsOrder(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create ballot",
            status: 0
        });
    }
}

function updateOrder(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._updateBallot(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function validateOrder(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._validateOrder(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPreOrders(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getPreOrders(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPreOrderDetails(req, res) {

    var query = [];
    var orderId = req.query['orderId'];
    query.push(model._getPreOrderDetails(orderId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPreOrdersItems(req, res) {

    var query = [];
    var orderId = req.query['orderId'];
    query.push(model._getPreOrdersItems(orderId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function preOrdersCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._preOrdersCreate(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._preOrdersItemsCreate(data.itemsInvoices[i],result[0]))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    console.log('error result',error)
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            console.log('error push services',error)
            res.json({
                data: error,
                status: 0,
                
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create order",
            status: 0
        });
    }
}

module.exports = {
    getOrders: getOrders,
    getOrderDetails:getOrderDetails,
    getOrderItems:getOrderItems,
    createOrder:createOrder,
    updateOrder:updateOrder,
    validateOrder:validateOrder,
    getPreOrders:getPreOrders,
    getPreOrderDetails:getPreOrderDetails,
    getPreOrdersItems:getPreOrdersItems,
    preOrdersCreate:preOrdersCreate,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
    uploadOrders

}