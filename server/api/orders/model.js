'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//Order providers
function _getOrders(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        o.id as "orderId",
                        p.dsName AS "providerName",
                        o.total,
                        s.name AS "statusName",
                        w.dsCode AS "origin",
                        wd.dsCode AS "destination",
                        w.id AS "originId",
                        wd.id AS "destinationId",
                        DATE_FORMAT(o.date, '%d/%m/%y') as date,
                        t.code as "type",
                        o.currency AS "currency",
                        u.usName AS "userCreation"
                        
                    FROM orders o
                    INNER JOIN  provider p
                    ON  o.providerId = p.id
                    INNER JOIN  warehouse w
                    ON  o.origin = w.id
                    left JOIN  warehouse wd
                    ON  o.destination = wd.id
                    INNER JOIN  users u
                    ON  o.usId = u.usId
                    left JOIN  status s
                    ON  o.status = s.id
                    INNER JOIN  types t
                    ON  o.type = t.id
                    WHERE o.inStatus = 1
                    order BY o.id DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getOrderDetails(orderId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    o.id as "orderId",
                    p.dsName AS "providerName",
                    p.dsCode AS "rut",
                    o.total,
                    s.name AS "statusName",
                    ROUND(o.total/1.19) AS "netPrice",
                    o.total - ROUND(o.total/1.19)  as "priceVAT",
                    o.discount,
                    w.dsCode AS "origin",
                    wd.dsCode AS "destination",
                    w.id AS "originId",
                    wd.id AS "destinationId",
                    o.date,
                    u.usName AS "userCreation",
                    us.usName AS "userUpdate",
                    o.commentary AS "comment",
                    t.name as "type",
                    o.currency,
                    o.currencyValue
                    
                FROM orders o
                INNER JOIN  provider p
                ON  o.providerId = p.id
                INNER JOIN  warehouse w
                ON  o.origin = w.id
                left JOIN  warehouse wd
                ON  o.destination = wd.id
                INNER JOIN  users u
                ON  o.usId = u.usId
                left JOIN  users us
                ON  o.usModifierId = us.usId
                left JOIN  status s
                ON  o.status = s.id
                INNER JOIN  types t
                ON  o.type = t.id
                WHERE o.id = :orderId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            orderId:orderId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getOrderItems(orderId,originId,destinationId){

    var deferred = Q.defer()

    if(destinationId == undefined) destinationId=originId

    let sql = `SELECT      STB.*,
                                SUM(ITW.itemQty) as "stockGeneral"
                                FROM        (SELECT  i.id AS itemId,
                                                     i.dsName AS itemDescription,
                                                     ITR.dsReference as referenceKey,
                                                     i.brand AS brand,
                                                     i.brandCode AS brandCode,
                                                     iw.itemQty AS stock,
                                                     oi.nmUnities as quantityItems,
                                                     oi.nmPrice AS price,
                                                     GROUP_CONCAT(distinct l.dsName) as "locations",
                                                     oi.total
                                                     FROM ordersitems oi
                                                     INNER JOIN items i
                                                     ON  i.id = oi.itemId
                                                     INNER JOIN  itemreferences ITR
                                                     ON ITR.itemId = i.id
                                                     INNER JOIN itemwarehouse iw
                                                     ON  i.id = iw.itemId AND iw.warehouseId = :originId
                                                     INNER JOIN  itemwarehouselocation iwl
                                                     ON i.id = iwl.itemId and iwl.warehouseId = :destinationId
                                                     INNER JOIN  location l
                                                     ON l.id = iwl.locationId
                                                     WHERE orderId= :orderId 
                                                     GROUP BY    oi.id) STB
                                LEFT JOIN   itemwarehouse ITW
                                ON          ITW.itemId = STB.itemId
                                GROUP BY    STB.itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            orderId:orderId,
            originId:originId,
            destinationId:destinationId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createOrder(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación";
    if(data.currency==undefined) data.currency='Peso Chileno'
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.discount == undefined) data.discount=0

    let sql = `INSERT INTO orders (providerId,date,total,status,inStatus,usId,origin,destination,commentary,type,currency,currencyValue)
                                    VALUES (:providerId,NOW(),:total,1,1,:userId,:originId,:destinationId,:observation,:type,:currency,:valueCurrency)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            date:data.date,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            destinationId:data.destinationId,
            type:data.type,
            observation:data.comment,
            currency:data.currency,
            valueCurrency:data.valueCurrency
        }
    }).then(function (invoiceId) {
        deferred.resolve(invoiceId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsOrder(items,orderId,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO ordersitems (   id,
                                                    orderId,
                                                    itemId, 
                                                    nmPrice,
                                                    nmUnities,
                                                    discount,
                                                    total) 
                                            VALUES (null,
                                                    :orderId,
                                                    :itemId,
                                                    :price,
                                                    :quantity,
                                                    :disscount,
                                                    (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            orderId:orderId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateOrder(model){

    var data = [
       {
          "messagge": "the order is update succesfully",
        }
    ]

    return data;
}



function _validateOrder(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE orders SET status=:status, usModifierId =:userId WHERE id=:orderId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            status:data.statusValue,
            userId:data.userId,
            orderId:data.orderId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPreOrders(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        o.id as "orderId",
                        p.dsName AS "providerName",
                        o.total,
                        s.name AS "statusName",
                        w.dsCode AS "origin",
                        w.id AS "originId",
                        DATE_FORMAT(o.date, '%d/%m/%y') as date,
                        t.code as "type",
                        o.currency AS "currency",
                        u.usName AS "userCreation"
                        
                    FROM preorders o
                    INNER JOIN  provider p
                    ON  o.providerId = p.id
                    INNER JOIN  warehouse w
                    ON  o.origin = w.id
                    INNER JOIN  users u
                    ON  o.usId = u.usId
                    left JOIN  status s
                    ON  o.status = s.id
                    INNER JOIN  types t
                    ON  o.type = t.id
                    WHERE o.inStatus = 1
                    order BY o.id DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPreOrderDetails(orderId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    o.id as "orderId",
                    p.dsName AS "providerName",
                    p.dsCode AS "rut",
                    s.name AS "statusName",
                    w.dsCode AS "origin",
                    o.date,
                    u.usName AS "userCreation",
                    us.usName AS "userUpdate",
                    o.commentary AS "comment",
                    t.name as "type"
                    
                FROM preorders o
                INNER JOIN  provider p
                ON  o.providerId = p.id
                INNER JOIN  warehouse w
                ON  o.origin = w.id
                INNER JOIN  users u
                ON  o.usId = u.usId
                left JOIN  users us
                ON  o.usModifierId = us.usId
                left JOIN  status s
                ON  o.status = s.id
                INNER JOIN  types t
                ON  o.type = t.id
                WHERE o.id = :orderId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            orderId:orderId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getPreOrdersItems(orderId){

    var deferred = Q.defer()

    let sql = `SELECT                  
                        pre.wishListId AS wishListId,
                        pre.dsName AS itemDescription,
                        pre.nmUnities as quantityItems,
                        group_concat(distinct ITR.dsReference) AS reference,
                        w.brand,
                        pre.itemId,
                        pre.brandCode
                    FROM preordersitems pre
                    INNER JOIN wishlist w
                    ON  w.wishListId = pre.wishListId
                    INNER JOIN  wishlistreferences ITR
                    ON          ITR.wishListId = w.wishListId
                    WHERE pre.orderId = :orderId
                    GROUP BY    w.wishListId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            orderId:orderId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _preOrdersCreate(data){
    
    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación";
    if(data.providerId == undefined) data.providerId = 165

    let sql = `INSERT INTO preorders (providerId,date,status,inStatus,usId,origin,commentary,type)
                                    VALUES (:providerId,NOW(),1,1,:userId,:originId,:observation,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            userId:data.userId,
            originId:data.originId,
            type:data.type,
            observation:data.comment
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _preOrdersItemsCreate(items,orderId){ 

    var deferred = Q.defer()

    let sql = `   INSERT INTO preordersitems (      id,
                                                    orderId,
                                                    itemId, 
                                                    brandCode,
                                                    nmUnities,
                                                    dsName,
                                                    wishListId) 
                                            VALUES ( null,
                                                    :orderId,
                                                    :itemId,
                                                    :brandCode,
                                                    :quantity,
                                                    :itemDescription,
                                                    :wishListId);
                                                    
                                UPDATE wishlist SET granted=1
                                       WHERE wishListId=:wishListId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            orderId:orderId,
            quantity:items.quantity,
            itemId:items.jormatId,
            brandCode:items.brandCode,
            wishListId:items.wishListId,
            itemDescription:items.itemDescription
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
        
    })

    return deferred.promise
}

module.exports = {
    _getOrders: _getOrders,
    _getOrderDetails:_getOrderDetails,
    _getOrderItems:_getOrderItems,
    _createOrder:_createOrder,
    _createItemsOrder:_createItemsOrder,
    _updatOrder:_updateOrder,
    _validateOrder:_validateOrder,
    _getPreOrders:_getPreOrders,
    _getPreOrderDetails:_getPreOrderDetails,
    _getPreOrdersItems:_getPreOrdersItems,
    _preOrdersCreate:_preOrdersCreate,
    _preOrdersItemsCreate:_preOrdersItemsCreate,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}