var express = require('express');
const multer = require('multer')
// view engine setup
var router = express.Router();
var service = require('./services');
const upload = multer({ dest: 'files/' })

//orders clients
router.get('/list', service.getOrders);
router.get('/', service.getOrderDetails);
router.get('/items', service.getOrderItems);
router.post('/', service.createOrder);
router.put('/', service.updateOrder);
router.put('/validate', service.validateOrder);
router.post('/uploadOrders', upload.single('uploadOrders'), service.uploadOrders);

//pre-orders providers
router.get('/providers', service.getPreOrders);
router.get('/providers-details', service.getPreOrderDetails);
router.get('/providers-items', service.getPreOrdersItems); 
router.post('/providers', service.preOrdersCreate); 


module.exports = router;