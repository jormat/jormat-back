'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//invoices clients jormat
function _getInvoicesPages(clientName, filters, offset, limit, count){
	let whereCount = ` 	WHERE i.inStatus = 1 `
	let where = ` 	WHERE i.inStatus = 1 `
	let sql = ``
	const conditionLimit = limit ? `LIMIT :limit OFFSET :offset` : ''
	// let conditionRut = (typeof dsCode !== 'undefined' && typeof dsCode !== '') ? ` AND c.dsCode = '${dsCode}' ` : ``
	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'invoiceId':
					where += "	AND i.invoiceId like '%" + filter[i] + "%'"
					whereCount += "	AND i.invoiceId like '%" + filter[i] + "%'"
					break
				case 'clientName':
					where += "	AND c.dsName like '%" + filter[i] + "%'"
					whereCount += "	AND c.dsName like '%" + filter[i] + "%'"
					break
				case 'rut':
					where += "	AND c.dsCode like '%" + filter[i] + "%'"
					whereCount += "	AND c.dsCode like '%" + filter[i] + "%'"
					break
				case 'total':
					where += "	AND i.total like '%" + filter[i] + "%'"
					whereCount += "	AND i.total like '%" + filter[i] + "%'"
					break
				case 'origin':
					where += "	AND w.dsCode like '%" + filter[i] + "%'"
					whereCount += "	AND w.dsCode like '%" + filter[i] + "%'"
					break
				case 'date':
					where += "	AND DATE_FORMAT(i.date, '%d/%m/%y') like '%" + filter[i] + "%'"
					whereCount += "	AND DATE_FORMAT(i.date, '%d/%m/%y') like '%" + filter[i] + "%'"
					break
				case 'purchaseOrder':
					where += "	AND i.nmOrder like '%" + filter[i] + "%'"
					whereCount += "	AND i.nmOrder like '%" + filter[i] + "%'"
					break
				case 'statusName':
					where += "	AND s.otherName like '%" + filter[i] + "%'"
					whereCount += "	AND s.otherName like '%" + filter[i] + "%'"
					break
			}
		}
	}

	if (clientName){
		where += "	AND c.dsName like '%" + clientName + "%'"
		whereCount += "	AND c.dsName like '%" + clientName + "%'"
	}

	if (!count) {
		sql += `	SELECT 	DISTINCT i.invoiceId as invoiceId,
							c.id AS clientId,
							c.dsName AS clientName,
							c.dsCode as rut,
							i.total,
							w.dsCode AS origin,
							DATE_FORMAT(i.date, '%d/%m/%y') as date,
							i.nmOrder AS purchaseOrder,
							u.usName AS "userCreation",
							i.discount AS "discount",
							if(i.packOff=0, 'No','Si') AS "packOff",
							i.isOut,
							s.otherName AS statusName,
							s.style AS style,
							i.isCredit AS isCredit
					FROM invoices i
					INNER JOIN  clients c
					ON  i.clientId = c.id
					LEFT  JOIN  warehouse w
					ON  i.origin = w.id
					LEFT  JOIN  users u
					ON  i.usId = u.usId
					left JOIN  status s
					ON  i.status = s.id
					${where}
					ORDER BY i.invoiceId DESC
					${conditionLimit} `
	} else {
		sql += `	SELECT 	COUNT(DISTINCT i.invoiceId) AS total
					FROM invoices i
					INNER JOIN  clients c
					ON  i.clientId = c.id
					LEFT  JOIN  warehouse w
					ON  i.origin = w.id
					LEFT  JOIN  users u
					ON  i.usId = u.usId
					left JOIN  status s
					ON  i.status = s.id
					${where} `
	}

	return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		},
	})
}

function _getInvoices(dsCode){

    const deferred = Q.defer()
    let conditionRut = (typeof dsCode !== 'undefined') ? ` AND c.dsCode = '${dsCode}' ` : ``

    let sql = `SELECT DISTINCT
                       i.invoiceId as "invoiceId",
                        c.id AS "clientId",
                       c.dsName AS "clientName",
                        c.dsCode as "rut",
                        i.total,
                        w.dsCode AS "origin",
                       DATE_FORMAT(i.date, '%d/%m/%y') as date,
                       i.nmOrder AS "purchaseOrder",
                        u.usName AS "userCreation",
                       i.discount AS "discount",
                        if(i.packOff=0, 'No','Si') AS "packOff",
                        i.isOut,
                        s.otherName AS "statusName",
                       s.style AS style,
                        i.isCredit AS isCredit

                   FROM invoices i
                    INNER JOIN  clients c
                    ON  i.clientId = c.id
                    LEFT  JOIN  warehouse w
                   ON  i.origin = w.id
                   LEFT  JOIN  users u
                   ON  i.usId = u.usId
                   left JOIN  status s
                   ON  i.status = s.id
                   WHERE i.inStatus = 1
                   ${conditionRut}
                   ORDER BY i.invoiceId DESC`
   sequelize.query(sql, {
       type: Sequelize.QueryTypes.SELECT,
       replacements:{
           userId:1
       }
   }).then(function (result) {
       deferred.resolve(result)
   }).catch(function (error){
       deferred.reject(error)
   })

    return deferred.promise
}

function _getInvoicesDetails(invoiceId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.invoiceId as "invoiceId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        i.total,
                        ROUND(i.total/1.19,0) as "netPrice",
                        i.total - ROUND(i.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        i.origin AS "originId",
                        i.nmOrder AS "purchaseOrder",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.date, '%H:%i:%S') as time,
                        u.usName AS "userCreation",
                        u2.usName AS "userOut",
                        u2.usName AS "userOut",
                        i.transportationName AS "transportationName",
                        i.isOut,
                        s.otherName AS "statusName",
                        i.discount AS "discount",
                        if(i.packOff=0, 'No','Si') AS "packOff",
                        c.dsPhoneNumber AS "phone",
                        c.dsAddress AS "address",
                        c.dsCity AS "city",
                        c.customerManager AS "customerManager",
                        i.commentary AS "comment",
                        td.code AS typeName,
                        pm.dsName AS paymentForm,
                        i.receiver AS receiver,
                        i.isCredit AS isCredit,
                        DATE_FORMAT(i.expirationDate, '%d/%m/%y') AS expirationDate,
						td.id AS typeId,
                        s.style AS style

                    FROM invoices i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  users u2
                    ON  i.userOutId = u2.usId
                    LEFT JOIN	typedocument td
			        ON			td.id = i.typeDoc
                    left JOIN  status s
                    ON  i.status = s.id
                    INNER JOIN paymentmethod pm
                    ON  pm.paymentmethodId = i.paymentmethodId
                    WHERE i.invoiceId =:invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createInvoice(data){
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.packOff == true) data.packOff=1
    if(data.paymentmethodId == undefined) data.paymentmethodId=0
    if(data.receiver == undefined) data.receiver="undefined"
    if(data.packOff == false) data.packOff=0 
    if(data.transportationId == undefined) data.transportationId=null 
    if(data.transportationName == undefined) data.transportationName=null 

    let sql = `INSERT INTO invoices(date,clientId,nmOrder,status,total,netTotal,inStatus,origin,commentary,discount,packOff,usId,paymentmethodId,receiver,expirationDate,transportationId,transportationName)
                      VALUES (NOW(),:clientId,:purchaseOrder,1,:total,:netTotal,1,:originId,:observation,:discount,:packOff,:userId,:paymentmethodId,:receiver,NOW(),:transportationId,:transportationName)`

    return 	sequelize.query(sql, {
			type: Sequelize.QueryTypes.INSERT,
			replacements:{
				clientId:data.clientId,
				purchaseOrder:data.purchaseOrder,
				total:data.total,
				netTotal:data.netTotal,
				userId:data.userId,
				originId:data.originId,
				discount:data.discount,
				packOff:data.packOff,
				paymentmethodId: data.paymentmethodId,
				receiver: data.receiver,
				observation:data.comment,
                transportationId: data.transportationId,
				transportationName:data.transportationName
			}
		})
}

function _validationInvoice(data) {
	const sql = `SELECT * FROM invoices WHERE date =:date AND origin =:originId AND clientId =:clientId AND nmOrder =:purchaseOrder AND status =1 AND total =:total AND netTotal =:netTotal AND inStatus = 1 ORDER BY invoiceId DESC LIMIT 1;`

    return 	sequelize.query(sql, {
			type: Sequelize.QueryTypes.SELECT,
			replacements:{
				clientId: data.clientId,
				purchaseOrder: data.purchaseOrder,
				total: data.total,
				netTotal: data.netTotal,
				originId: data.originId,
				date: data.date
			}
		})

}

function _createItemsInvoiceListClients(items,invoiceId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO invoicesitems (invoiceId,
                                            itemId,
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total)
                                    VALUES (:invoiceId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));

                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity)
                                       WHERE itemId=:itemId
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:invoiceId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateInvoice(model){

    var data = [
       {
          "messagge": "the Invoice is update succesfully",
        }
    ]

    return data;
}

function _disabledInvoice(model){

    var data = [
       {
          "messagge": "the Invoice is disabled succesfully",
        }
    ]

    return data;
}


function _getInvoicesItems(invoiceId,originId){

    var deferred = Q.defer()

    let sql = `SELECT   i.id AS itemId,
                        i.dsName AS itemDescription,
                        i.oil AS oil,
                        i.webType as webType,
                        i.dsMaxDiscount AS maxDiscount,
                        i.netPrice AS netPurchaseValue,
                        ITR.dsReference as referenceKey,
                        i.brandCode AS brandCode,
                        pi.nmUnities as quantityItems,
                        pi.nmPrice AS price,
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        pi.discount AS discount,
                        pi.total
                FROM invoicesitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                INNER JOIN  itemwarehouselocation iwl
                ON i.id = iwl.itemId and iwl.warehouseId = :originId
                INNER JOIN  location l
                ON l.id = iwl.locationId
                WHERE invoiceId =:invoiceId
                GROUP BY    pi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

//invoices providers
function _getInvoicesProvider(userId){

    var deferred = Q.defer()

    let sql = `   SELECT DISTINCT
                        i.id as "invoiceId",
                        i.dsCode as  "invoiceCode",
                        p.dsName AS "providerName",
                        i.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        if(i.type=1, 'NAC','INTER') AS "type",
                        s.name AS "statusName",
                        s.style AS style,
                        i.currency AS "currency",
                        i.importId,
                        u.usName AS "userCreation"

                    FROM providerinvoices i
                    INNER JOIN  provider p
                    ON  i.providerId = p.id
                    INNER JOIN  warehouse w
                    ON  i.origin = w.id
                    INNER JOIN  users u
                    ON  i.usId = u.usId
                    INNER JOIN  statusothers s
                    ON  i.status = s.id
                    WHERE i.inStatus = 1
                    order BY i.id DESC  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInvoicesProviderDetails(invoiceId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    i.id as "invoiceId",
                    i.dsCode as  "invoiceCode",
                    p.dsName AS "providerName",
                    p.dsCode AS "rut",
                    i.total,
                    ROUND(i.total/1.19) AS "netPrice",
                    i.total - ROUND(i.total/1.19)  as "priceVAT",
                    i.discount,
                    w.dsCode AS "origin",
                    i.origin AS "originId",
                    i.date,
                    u.usName AS "userCreation",
                    i.commentary AS "comment",
                    if(i.type=1, 'Nacional','Internacional') AS "type",
                    s.name AS "statusName",
                    s.style AS style,
                    i.currency,
                    i.currencyValue
                FROM providerinvoices i
                INNER JOIN  provider p
                ON  i.providerId = p.id
                INNER JOIN  warehouse w
                ON  i.origin = w.id
                INNER JOIN  users u
                ON  i.usId = u.usId
                INNER JOIN  statusothers s
                ON  i.status = s.id
                WHERE i.id =:invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createInvoiceProvider(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación";
    if(data.currency==undefined) data.currency='Peso Chileno'
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.discount == undefined) data.discount=0
    if(data.importId==undefined) data.importId=null

    let sql = `INSERT INTO providerinvoices (dsCode,providerId,date,total,netTotal,inStatus,usId,origin,commentary,discount,type,currency,currencyValue,importId)
                                    VALUES (:invoiceCode,:providerId,NOW(),:total,:netTotal,1,:userId,:originId,:observation,:discount,:type,:currency,:valueCurrency,:importId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceCode:data.invoiceCode,
            providerId:data.providerId,
            date:data.date,
            total:data.total,
            netTotal:data.netTotal,
            userId:data.userId,
            importId:data.importId,
            originId:data.originId,
            discount:data.discount,
            type:data.type,
            observation:data.comment,
            currency:data.currency,
            valueCurrency:data.valueCurrency
        }
    }).then(function (invoiceId) {
        deferred.resolve(invoiceId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise

}

function _createItemsInvoiceList(items,invoiceCode,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO providerinvoicesitems (id,
                                                    invoiceId,
                                                    itemId,
                                                    nmPrice,
                                                    nmUnities,
                                                    discount,
                                                    total)
                                            VALUES (null,
                                                    :invoiceCode,
                                                    :itemId,
                                                    :price,
                                                    :quantity,
                                                    :disscount,
                                                    (:price * :quantity - (:disscount * (:price * :quantity)) / 100));

                  UPDATE itemwarehouse SET itemQty=(itemQty+:quantity)
                                       WHERE itemId=:itemId
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceCode:invoiceCode,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _disabledInvoiceProvider(invoiceId,userId){

    var deferred = Q.defer()

    let sql = `UPDATE providerinvoices SET inStatus=0 WHERE id=:invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:invoiceId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInvoicesProviderItems(invoiceId){

    var deferred = Q.defer()

    let sql = `SELECT   i.id AS itemId,
                        i.dsName AS itemDescription,
                        ITR.dsReference as referenceKey,
                        i.brandCode AS brandCode,
                        pi.nmUnities as quantityItems,
                        pi.nmPrice AS price,
                        pi.discount AS discount,
                        pi.total
                FROM providerinvoicesitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE invoiceId =:invoiceId
                GROUP BY    pi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getReportInvoices(startDate,endDate){

    var deferred = Q.defer()

    let sql = `  SELECT DISTINCT
                        i.invoiceId as "invoiceId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        i.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.nmOrder AS "purchaseOrder",
                        u.usName AS "userCreation",
                        i.discount AS "discount",
                        s.otherName AS "statusName",
                        s.style AS style

                    FROM invoices i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    WHERE I.date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND i.inStatus = 1
                    order BY i.invoiceId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInvoicesByClient(clientId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `  SELECT DISTINCT
                        i.invoiceId as "invoiceId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        i.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.nmOrder AS "purchaseOrder",
                        u.usName AS "userCreation",
                        i.discount AS "discount",
                        s.otherName AS "statusName",
                        s.style AS style

                    FROM invoices i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    WHERE I.date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND i.inStatus = 1 AND c.id = :clientId
                    order BY i.invoiceId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getTotalByClient(clientId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `  SELECT DISTINCT
                        i.invoiceId as "invoiceId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        sum(i.total) AS total

                    FROM invoices i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    WHERE I.date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND i.inStatus = 1 AND c.id = :clientId
                    order BY i.invoiceId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _updatePackOff(invoiceId){

    var deferred = Q.defer()

    let sql = `UPDATE invoices SET packOff=1 WHERE invoiceId=:invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:invoiceId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _setCreditStatus(data){

    var deferred = Q.defer()
    let sql = `UPDATE invoices SET isCredit=1, expirationDate = date_add(date, interval :days DAY) WHERE invoiceId=:invoiceId`


    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:data.invoiceId,
            days:data.days

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getExpirationInvoices(dsCode){

    const deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.invoiceId as "invoiceId",
                        i.clientId as "clientId",
                        i.status,
                        i.total,
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.date, '%Y-%m-%d')as dateInvoice,
                        i.expirationDate as expirationDate,
                        i.isCredit AS isCredit

                    FROM invoices i
                    WHERE i.inStatus = 1 AND i.status = 1 AND i.isCredit = 1
                    ORDER BY i.invoiceId DESC`
    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _setStatusInvoices(data){

    var deferred = Q.defer()

    let sql = `UPDATE invoices SET status=:value, dateUpdate=NOW(),usModifier=:userId WHERE invoiceId=:invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            value:data.value,
            userId:data.userId,
            invoiceId:data.invoiceId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createInvoiceProviderDraft(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="sin comentario";
    if(data.currency==undefined) data.currency='----'
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.discount == undefined) data.discount=0
    if(data.importId==undefined) data.importId=null

    let sql = `INSERT INTO providerinvoices (dsCode,providerId,date,total,netTotal,inStatus,usId,origin,commentary,discount,type,currency,currencyValue,importId,status)
                                    VALUES (:invoiceCode,:providerId,NOW(),:total,:netTotal,1,:userId,:originId,:observation,:discount,:type,:currency,:valueCurrency,:importId,2)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceCode:data.invoiceCode,
            providerId:data.providerId,
            date:data.date,
            total:data.total,
            netTotal:data.netTotal,
            userId:data.userId,
            importId:data.importId,
            originId:data.originId,
            discount:data.discount,
            type:data.type,
            observation:data.comment,
            currency:data.currency,
            valueCurrency:data.valueCurrency
        }
    }).then(function (invoiceId) {
        deferred.resolve(invoiceId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise

}

function _createItemsInvoiceListDraft(items,invoiceCode){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO providerinvoicesitems (id,
                                                    invoiceId,
                                                    itemId,
                                                    nmPrice,
                                                    nmUnities,
                                                    discount,
                                                    draft,
                                                    total)
                                            VALUES (null,
                                                    :invoiceCode,
                                                    :itemId,
                                                    :price,
                                                    :quantity,
                                                    :disscount,
                                                    1,
                                                    (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceCode:invoiceCode,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateInvoiceProviderDraft(data){

    var deferred = Q.defer()
    let where = ` `
    if(data.comment==undefined) data.comment="sin comentario";
    if(data.currency==undefined) data.currency='Peso Chileno'
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.discount == undefined) data.discount=0
    if(data.importId==undefined) data.importId=null 

    var deferred = Q.defer()

    if (data.value==0){
            where += ", date = NOW()"
        }

    let sql = `DELETE FROM providerinvoicesitems WHERE  invoiceId =:invoiceId;

               UPDATE  providerinvoices SET  
                                       total =:total,          
                                       dsCode =:invoiceCode, 
                                       netTotal =:netTotal, 
                                       currency =:currency,
                                       currencyValue =:valueCurrency, 
                                       usModifierId =:userId, 
                                       commentary =:observation, 
                                       discount =:discount,
                                       status =:value
                                       ${where}
                                  WHERE id =:invoiceId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:data.invoiceId,
            invoiceCode:data.invoiceCode,
            userId:data.userId,
            discount:data.discount,
            observation:data.comment,
            total:data.total,
            netTotal:data.netTotal,
            valueCurrency:data.valueCurrency,
            value:data.value,
            currency:data.currency
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _createInvoiceProviderDraftItems(items,invoiceId){

    var deferred = Q.defer()
    console.log('look 3',invoiceId)
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO providerinvoicesitems (invoiceId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            draft,
                                            total) 
                                    VALUES (:invoiceId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                             1,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:invoiceId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateInvoiceProviderItems(items,invoiceCode,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO providerinvoicesitems (id,
                                                    invoiceId,
                                                    itemId,
                                                    nmPrice,
                                                    nmUnities,
                                                    discount,
                                                    draft,
                                                    total)
                                            VALUES (null,
                                                    :invoiceCode,
                                                    :itemId,
                                                    :price,
                                                    :quantity,
                                                    :disscount,
                                                     0,
                                                    (:price * :quantity - (:disscount * (:price * :quantity)) / 100));

                  UPDATE itemwarehouse SET itemQty=(itemQty+:quantity)
                                       WHERE itemId=:itemId
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceCode:invoiceCode,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInvoicesItemsLocation(invoiceId,originId){

    var deferred = Q.defer()

    let sql = ` SELECT     distinct STB.*,
                        SUM(ITW.itemQty) as "generalStock"
                FROM (SELECT   
					         @i := @i + 1 AS id,
					         i.id as itemId ,
                        i.dsName AS itemDescription,
                        ii.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        i.brandCode AS brandCode,
                        i.netPrice AS netPurchaseValue,
                        ii.nmPrice AS price,
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        ii.discount AS discount,
                        ii.total
                FROM providerinvoicesitems ii
                INNER JOIN items i
                ON  i.id = ii.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                
                INNER JOIN  itemwarehouse iw
                ON i.id = iw.itemId and iw.warehouseId = :originId
                INNER JOIN  itemwarehouselocation iwl
                ON i.id = iwl.itemId and iwl.warehouseId = :originId
                INNER JOIN  location l
                ON l.id = iwl.locationId
                cross join (select @i := 0) r        
                        
                WHERE invoiceId = :invoiceId
                GROUP BY    ii.id)STB
                INNER JOIN  itemwarehouse ITW
                        ON ITW.itemId =  STB.itemId
                     GROUP BY    STB.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

module.exports = {
    _getInvoices: _getInvoices,
    _getInvoicesDetails:_getInvoicesDetails,
    _createInvoice:_createInvoice,
    _createItemsInvoiceListClients:_createItemsInvoiceListClients,
    _updateInvoice:_updateInvoice,
    _disabledInvoice:_disabledInvoice,
    _getInvoicesItems:_getInvoicesItems,
    _getInvoicesProvider: _getInvoicesProvider,
    _getInvoicesProviderDetails:_getInvoicesProviderDetails,
    _createInvoiceProvider:_createInvoiceProvider,
    _createItemsInvoiceList:_createItemsInvoiceList,
    _disabledInvoiceProvider:_disabledInvoiceProvider,
    _getInvoicesProviderItems:_getInvoicesProviderItems,
    _getReportInvoices:_getReportInvoices,
    _getInvoicesByClient:_getInvoicesByClient,
    _getTotalByClient:_getTotalByClient,
    _updatePackOff:_updatePackOff,
    _setCreditStatus:_setCreditStatus,
    _getExpirationInvoices:_getExpirationInvoices,
    _setStatusInvoices:_setStatusInvoices,
    _getInvoicesPages:_getInvoicesPages,
    _createInvoiceProviderDraft:_createInvoiceProviderDraft,
    _createItemsInvoiceListDraft:_createItemsInvoiceListDraft,
    _updateInvoiceProviderDraft:_updateInvoiceProviderDraft,
    _createInvoiceProviderDraftItems:_createInvoiceProviderDraftItems,
    _updateInvoiceProviderItems:_updateInvoiceProviderItems,
    _getInvoicesItemsLocation:_getInvoicesItemsLocation,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
	_validationInvoice
}

