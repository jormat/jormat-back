'use strict'

var model = require('./model');

//invoices clients

function getInvoicesPages(req, res) {
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = req.query['pageSize'] ? parseInt(req.query['pageSize']) : false
    const clientName = req.query.searchText
	const {
		filters = {},
	} = req.query

	const promises = [
		model._getInvoicesPages(clientName,filters, offset, limit, false),
		model._getInvoicesPages(clientName,filters, offset, limit, true)
	]

	Promise.all(promises).then((result) => {
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}

function getInvoices(req, res) {

    var query = [];
    const dsRut = req.query.dsCode
    query.push(model._getInvoices(dsRut))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesDetails(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._getInvoicesDetails(invoiceId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createInvoice(req, res) {
    const data = req.body

    if(req.body){
		model._validationInvoice(data).then((response) => {
			if (response.length > 0) {
				res.json({
					data: response[0].invoiceId,
					status: 1
				});
			} else {
				model._createInvoice(data).then((result) => {
					if(data.itemsInvoices.length > 0){} {
						const query = []

						for(let i in data.itemsInvoices){
							query.push(model._createItemsInvoiceListClients(data.itemsInvoices[i],result,data.originId))
						}

						Promise.all(query).then((results) => {
							res.json({
								data: result,
								status: 1
							})

						}).catch(function(error) {
							res.json({
								data: error,
								status: 0
							});
						})
					}

					res.json({
						data: result,
						status: 1
					})
				}).catch(function(error) {
					res.json({
						data: error,
						status: 0
					})
				})
			}
		}).catch(function(error) {
			res.json({
				data: error,
				status: 0
			})
		})
    }else{
        res.json({
            data: [],
            messagge:"not data this services _createInvoice",
            status: 0
        });
    }
}

function updateInvoice(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._updateInvoice(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledInvoice(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledInvoice(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesItems(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    var originId = req.query['originId'];
    query.push(model._getInvoicesItems(invoiceId,originId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

//invoices providers

function getInvoicesProvider(req, res) {

    var query = [];
    query.push(model._getInvoicesProvider(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesProviderDetails(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._getInvoicesProviderDetails(invoiceId))
    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createInvoiceProvider(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createInvoiceProvider(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsInvoiceList(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {

                    res.json({
                        data: result[0],
                        status: 1
                    });

                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services createInvoiceProvider",
            status: 0
        });
    }
}


function disabledInvoiceProvider(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    var userId = req.query['userId'];
    query.push(model._disabledInvoiceProvider(invoiceId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Invoice provider is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesProviderItems(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._getInvoicesProviderItems(invoiceId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getReportInvoices(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getReportInvoices(startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesByClient(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getInvoicesByClient(clientId,startDate,endDate))
    query.push(model._getTotalByClient(clientId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            total: result[1],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updatePackOff(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._updatePackOff(invoiceId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Invoice is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function setCreditStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._setCreditStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Invoice credit is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getExpirationInvoices(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getExpirationInvoices())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function setStatusInvoices(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._setStatusInvoices(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Invoice status is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createInvoiceProviderDraft(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createInvoiceProviderDraft(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsInvoiceListDraft(data.itemsInvoices[i],result[0]))
              }
                Promise.all(query).then((results) => {

                    res.json({
                        data: result[0],
                        status: 1
                    });

                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services createInvoiceProvider",
            status: 0
        });
    }
}

function updateInvoiceProviderDraft(req, res) {

    var query = [];
    var data = req.body;

    if(req.body){
        Promise.all([model._updateInvoiceProviderDraft(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createInvoiceProviderDraftItems(data.itemsInvoices[i],data.invoiceId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services update quotation",
            status: 0
        });
    }
}

function updateInvoiceProvider(req, res) {

    var query = [];
    var data = req.body;

    if(req.body){
        Promise.all([model._updateInvoiceProviderDraft(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._updateInvoiceProviderItems(data.itemsInvoices[i],data.invoiceId,data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services update quotation",
            status: 0
        });
    }
}

function getInvoicesItemsLocation(req, res){

    var query = [];
    var invoiceId = req.query['invoiceId'];
    var originId = req.query['originId'];
    query.push(model._getInvoicesItemsLocation(invoiceId,originId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getInvoices: getInvoices,
    getInvoicesDetails:getInvoicesDetails,
    getInvoicesPages:getInvoicesPages,
    createInvoice:createInvoice,
    updateInvoice:updateInvoice,
    disabledInvoice:disabledInvoice,
    getInvoicesItems:getInvoicesItems,
    getInvoicesProvider: getInvoicesProvider,
    getInvoicesProviderDetails:getInvoicesProviderDetails,
    createInvoiceProvider:createInvoiceProvider,
    disabledInvoiceProvider:disabledInvoiceProvider,
    getInvoicesProviderItems:getInvoicesProviderItems,
    getReportInvoices:getReportInvoices,
    getInvoicesByClient:getInvoicesByClient,
    updatePackOff:updatePackOff,
    setCreditStatus:setCreditStatus,
    getExpirationInvoices:getExpirationInvoices,
    setStatusInvoices:setStatusInvoices,
    createInvoiceProviderDraft:createInvoiceProviderDraft,
    updateInvoiceProviderDraft:updateInvoiceProviderDraft,
    updateInvoiceProvider:updateInvoiceProvider,
    getInvoicesItemsLocation:getInvoicesItemsLocation,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}
