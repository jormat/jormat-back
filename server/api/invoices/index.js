var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');


//invoices clients
router.get('/list', service.getInvoices);
router.get('/list-pages', service.getInvoicesPages);
router.get('/', service.getInvoicesDetails);
router.post('/', service.createInvoice);
router.put('/', service.updateInvoice);
router.put('/credit', service.setCreditStatus);
router.delete('/', service.disabledInvoice);
router.get('/items', service.getInvoicesItems);
router.get('/report', service.getReportInvoices);
router.get('/clients', service.getInvoicesByClient);
router.delete('/packoff', service.updatePackOff);
router.get('/expiration', service.getExpirationInvoices);
router.put('/status', service.setStatusInvoices);

//invoices Providers
router.get('/providers/list', service.getInvoicesProvider);
router.get('/providers/', service.getInvoicesProviderDetails);
router.post('/providers/', service.createInvoiceProvider);
router.delete('/providers/', service.disabledInvoiceProvider);
router.get('/providers/items', service.getInvoicesProviderItems);
router.post('/providers/draft', service.createInvoiceProviderDraft);
router.put('/providers/draft', service.updateInvoiceProviderDraft);
router.put('/providers/', service.updateInvoiceProvider);
router.get('/providers/items-location', service.getInvoicesItemsLocation);


module.exports = router;