var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');


//Documents clients
router.get('/nn/list', service.getDocuments);
router.get('/nn', service.getDocumentsDetails);
router.post('/nn', service.createDocument);
router.put('/nn', service.updateDocument);
router.delete('/nn', service.disabledDocument);
router.get('/nn/items', service.getDocumentItems);
router.put('/nn/pay', service.payDocument);
router.put('/nn/packoff', service.updatePackOff);


//Documents imports
router.get('/import/list', service.getImports);
router.get('/import', service.getImportsDetails);
router.get('/import/items', service.getImportsItems);
router.post('/import', service.importCreate);
router.put('/import', service.updateImport);
router.post('/import/draft', service.importCreateDraft);
router.put('/import/draft', service.updateImportDraft);
router.put('/import/update', service.updateImportPostDraft);
router.get('/import/registration/list', service.getImportsRegistration);
router.get('/import/registration', service.getImportsRegistrationDetails);
router.post('/import/registration', service.importRegistrationCreate);
router.put('/import/registration', service.updateImportRegistration);
router.get('/import/registration/payments', service.importRegistrationPaymentList);
router.post('/import/registration/payments', service.importRegistrationCreatePayment);
router.delete('/import/registration/payments', service.disabledPayment);
router.put('/import/registration/status', service.updateImportRegistrationStatus);
router.put('/import/registration/close', service.closeImportRegistration);
router.get('/import/items-location', service.getImportsItemsLocation);

//after sale Documents
router.get('/after-sale/list', service.getAfterSale);
router.get('/after-sale/', service.getAfterSaleDetails);
router.get('/after-sale/items', service.getAfterSaleItems);
router.post('/after-sale/', service.afterSaleCreate);
// router.put('/sale/update', service.saleNoteUpdate);

//Void Documents
router.get('/void/list', service.getVoidDocuments);
router.get('/void/', service.getVoidDocumentDetails);
router.get('/void/items', service.getVoidDocumenItems);
router.post('/void/', service.createVoidDocument);
router.post('/void/failure', service.createVoidDocumentFailure);
router.put('/void/status', service.voidDocumentStatus);

//budgets 
router.get('/budgets/list', service.getBudgets);
router.get('/budgets/', service.getBudgetsDetails);
router.get('/budgets/items', service.getBudgetsItems);
router.post('/budgets', service.budgetsCreate);
router.put('/budgets', service.updateBudget);

//exempt invoices
router.get('/exempt/list', service.getExemptInvoices);
router.get('/exempt', service.getExemptInvoicesDetails);
router.get('/exempt/items', service.getExemptInvoicesItems);
router.post('/exempt', service.exemptInvoicesCreate);
router.put('/exempt/pay', service.exemptInvoicesPayment);


module.exports = router;