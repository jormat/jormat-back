'use strict'

var model = require('./model');

//Documents NN clients

function getDocuments(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getDocuments(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getDocumentsDetails(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getDocumentsDetails(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getDocumentItems(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    var originId = req.query['originId'];
    query.push(model._getDocumentItems(documentId,originId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createDocument(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createDocument(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsDocuments(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services _createDocument",
            status: 0
        });
    }
}

function payDocument(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._payDocument(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the document is payment succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateDocument(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateDocument(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the document is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledDocument(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledDocument(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImports(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getImports())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImportsDetails(req, res) {

    var query = [];
    var importId = req.query['importId'];
    query.push(model._getImportDetails(importId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImportsItems(req, res){

    var query = [];
    var importId = req.query['importId'];
    query.push(model._getImportsItems(importId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImportsItemsLocation(req, res){

    var query = [];
    var importId = req.query['importId'];
    var originId = req.query['originId'];
    query.push(model._getImportsItemsLocation(importId,originId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function importCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._importCreate(data)]).then((result) => {
            query=[]
            for(var i in data.importItems){
                query.push(model._importCreateItems(data.importItems[i],result[0]))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create import",
            status: 0
        });
    }
}

function updateImport(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateImport(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAfterSale(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getAfterSale(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAfterSaleDetails(req, res) {

    var query = [];
    var afterSaleId = req.query['afterSaleId'];
    query.push(model._getAfterSaleDetails(afterSaleId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAfterSaleItems(req, res) {

    var query = [];
    var afterSaleId = req.query['afterSaleId'];
    query.push(model._getAfterSaleItems(afterSaleId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function afterSaleCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._afterSaleCreate(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._afterSaleItemsCreate(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create after sale",
            status: 0
        });
    }
}


function getImportsRegistration(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getImportsRegistration())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function importRegistrationCreate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._importRegistrationCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the import registration is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImportsRegistrationDetails(req, res) {

    var query = [];
    var importId = req.query['importId'];
    query.push(model._getImportsRegistrationDetails(importId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function importRegistrationPaymentList(req, res) {

    var query = [];
    var recordId = req.query['recordId'];
    query.push(model._importRegistrationPaymentList(recordId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function importRegistrationCreatePayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._importRegistrationCreatePayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the payment importa registration is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledPayment(req, res) {

    var query = [];
    var paymentId = req.query['paymentId'];
    var recordId = req.query['recordId'];
    var userId = req.query['userId'];
    query.push(model._disabledPayment(paymentId,recordId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateImportRegistrationStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateImportRegistrationStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateImportRegistration(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateImportRegistration(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function closeImportRegistration(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._closeImportRegistration(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


// Void Documents 

function getVoidDocuments(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getVoidDocuments(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVoidDocumentDetails(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getVoidDocumentDetails(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVoidDocumenItems(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getVoidDocumenItems(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createVoidDocument(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createVoidDocument(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsVoidDocument(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create void document",
            status: 0
        });
    }
}

function createVoidDocumentFailure(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createVoidDocument(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsVoidDocumentFailure(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create void document",
            status: 0
        });
    }
}

function voidDocumentStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._voidDocumentStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the document is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBudgets(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getBudgets(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBudgetsDetails(req, res) {

    var query = [];
    var budgetId = req.query['budgetId'];
    query.push(model._getBudgetsDetails(budgetId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBudgetsItems(req, res) {

    var query = [];
    var budgetId = req.query['budgetId'];
    query.push(model._getBudgetsItems(budgetId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function budgetsCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._budgetsCreate(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsBudgets(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create budgets",
            status: 0
        });
    }
}


function updateBudget(req, res) {


    var query = [];
    var data = req.body;
    query.push(model._updateBudget(data))
    
    for(var i in data.itemsInvoices){
            query.push(model._createItemsBudgets(data.itemsInvoices[i],data.budgetId))
          }

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "budget is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getExemptInvoices(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getExemptInvoices(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function exemptInvoicesCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._exemptInvoicesCreate(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._exemptInvoicesItemsAdd(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create Document exempt",
            status: 0
        });
    }
}


function getExemptInvoicesDetails(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getExemptInvoicesDetails(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getExemptInvoicesItems(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getExemptInvoicesItems(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function exemptInvoicesPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._exemptInvoicesPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the exempt document is payment succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updatePackOff(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updatePackOff(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the packoff is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function importCreateDraft(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._importCreateDraft(data)]).then((result) => {
            query=[]
            for(var i in data.importItems){
                query.push(model._itemsImportsCreateDraft(data.importItems[i],result[0]))
              }
                Promise.all(query).then((results) => {

                    res.json({
                        data: result[0],
                        status: 1
                    });

                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create draft imports",
            status: 0
        });
    }
}

function updateImportDraft(req, res) {

    var query = [];
    var data = req.body;

    if(req.body){
        Promise.all([model._updateImportDraft(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._itemsImportsCreateDraft(data.itemsInvoices[i],data.importId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services update import",
            status: 0
        });
    }
}

function updateImportPostDraft(req, res) {

    var query = [];
    var data = req.body;

    if(req.body){
        Promise.all([model._updateImportDraft(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._importCreateItems(data.itemsInvoices[i],data.importId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services update quotation",
            status: 0
        });
    }
}

module.exports = {
    getDocuments: getDocuments,
    getDocumentsDetails:getDocumentsDetails,
    getDocumentItems:getDocumentItems,
    createDocument:createDocument,
    payDocument:payDocument,
    updateDocument:updateDocument,
    disabledDocument:disabledDocument,
    getImports:getImports,
    getImportsDetails:getImportsDetails,
    getImportsItems:getImportsItems,
    getImportsItemsLocation:getImportsItemsLocation,
    importCreate:importCreate,
    updateImport:updateImport,
    getAfterSale:getAfterSale,
    getAfterSaleDetails:getAfterSaleDetails,
    getAfterSaleItems:getAfterSaleItems,
    afterSaleCreate:afterSaleCreate,
    getImportsRegistration:getImportsRegistration,
    importRegistrationCreate:importRegistrationCreate,
    getImportsRegistrationDetails:getImportsRegistrationDetails,
    importRegistrationPaymentList:importRegistrationPaymentList,
    importRegistrationCreatePayment:importRegistrationCreatePayment,
    disabledPayment:disabledPayment,
    updateImportRegistrationStatus:updateImportRegistrationStatus,
    updateImportRegistration:updateImportRegistration,
    closeImportRegistration:closeImportRegistration,
    getVoidDocuments:getVoidDocuments,
    getVoidDocumentDetails:getVoidDocumentDetails,
    getVoidDocumenItems:getVoidDocumenItems,
    createVoidDocument:createVoidDocument,
    createVoidDocumentFailure:createVoidDocumentFailure,
    voidDocumentStatus:voidDocumentStatus,
    getBudgets:getBudgets,
    getBudgetsDetails:getBudgetsDetails,
    getBudgetsItems:getBudgetsItems,
    budgetsCreate:budgetsCreate,
    updateBudget:updateBudget,
    getExemptInvoices:getExemptInvoices,
    exemptInvoicesCreate:exemptInvoicesCreate,
    getExemptInvoicesDetails:getExemptInvoicesDetails,
    getExemptInvoicesItems:getExemptInvoicesItems,
    exemptInvoicesPayment:exemptInvoicesPayment,
    updatePackOff:updatePackOff,
    importCreateDraft:importCreateDraft,
    updateImportDraft:updateImportDraft,
    updateImportPostDraft:updateImportPostDraft,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}