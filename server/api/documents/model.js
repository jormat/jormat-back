'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//Documents NN
function _getDocuments(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.documentId as "documentId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        i.total,
                        w.dsCode AS "origin",
                        i.origin AS "originId",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.date, '%H:%i:%S') as time,
                        IFNULL(i.total - SUM(p.total),i.total) AS pending,
                        i.nmOrder AS "purchaseOrder",
                        u.usName AS "userCreation",
                        i.isOut,
                        if(i.status=1, 'Pagado','Sin Pagar') AS "statusName"
                        
                    FROM documentsnn i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left  JOIN      paymentsnn p
                    ON  i.documentId=p.documentId
                    WHERE i.inStatus = 1
                    GROUP BY       i.documentId
                    order BY i.documentId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getDocumentsDetails(documentId){

   var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.documentId as "documentId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        c.dsPhoneNumber AS "phone",
                        c.dsAddress AS "address",
                        c.dsCode AS "rut",
                        c.dsCity AS "city",
                        i.transportationName as "transportationName",
                        i.total,
                        ROUND(i.total/1.19,0) as "netPrice",
                        i.total - ROUND(i.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        i.origin AS "originId",
                        i.nmOrder AS "purchaseOrder",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.date, '%H:%i:%S') as time,
                        u.usName AS "userCreation",
                        u2.usName AS "userOut",
                        i.isOut,
                        us.usName AS "userModifier",
                        if(i.status=1, 'Pagado','Sin Pagar') AS "statusName",
                        pm.dsName AS paymentForm,
                        DATE_FORMAT(i.datePayment, '%d/%m/%y') as datePayment,
                        i.discount AS "discount",
                        if(i.packOff=0, 'No','Si') AS "packOff",
                        i.commentary AS "comment",
                        td.code AS typeName,
						td.id AS typeId
                        
                    FROM documentsnn i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  users us
                    ON  i.usModifierId = us.usId
                    left JOIN  users u2
                    ON  i.userOutId = u2.usId
                    INNER JOIN paymentmethod pm
                    ON  pm.paymentMethodId = i.paymentFormId
                    LEFT JOIN	typedocument td
			        ON			td.id = i.typeDoc
                    WHERE i.documentId = :documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise 
}

function _getDocumentItems(documentId,originId){

    var deferred = Q.defer()
    if(originId == undefined) originId=1
    let sql = `SELECT      i.id as itemId ,
                        i.dsName AS itemDescription,
                        i.oil AS oil,
                        i.dsMaxDiscount AS maxDiscount,
                        i.webType as webType,
                        pi.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        pi.nmPrice AS price,
                        pi.discount AS discount,
                        pi.total
                FROM documentsnnitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                INNER JOIN  itemwarehouselocation iwl
                 ON i.id = iwl.itemId and iwl.warehouseId = :originId
                 INNER JOIN  location l
                 ON l.id = iwl.locationId
                WHERE documentId = :documentId
                GROUP BY    pi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createDocument(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.clientId == undefined) data.clientId = 4934
    if(data.packOff == false) data.packOff=0 
    if(data.transportationName == undefined) data.transportationName=null 
    if(data.transportationId == undefined) data.transportationId=null 

    let sql = `INSERT INTO documentsnn(date,clientId,status,total,netTotal,inStatus,origin,commentary,discount,usId,packOff,transportationName,transportationId,paymentFormId)
                      VALUES (NOW(),:clientId,0,:total,:netTotal,1,:originId,:observation,:discount,:userId,:packOff,:transportationName,:transportationId,:paymentmethodId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            total:data.total,
            netTotal:data.netTotal,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            observation:data.comment,
            transportationId:data.transportationId,
            transportationName:data.transportationName,
            paymentmethodId:data.paymentmethodId,
            packOff:data.packOff
        }
    }).then(function (documentId) {
        deferred.resolve(documentId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _payDocument(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE documentsnn SET status=1, 
                                      usModifierId =:userId,
                                      paymentFormId = :paymentMethodId,
                                      datePayment = NOW() 
                                      WHERE documentId=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            documentId:data.documentId,
            paymentMethodId: data.paymentMethodId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsDocuments(items,documentId,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
        
    let sql = `   INSERT INTO documentsnnitems (documentId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:documentId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:documentId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateDocument(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE documentsnn SET status=0, 
                                      usModifierId =:userId
                                      WHERE documentId=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            documentId:data.documentId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledDocument(model){

    var data = [
       {
          "messagge": "the Document NN is disabled succesfully",
        }
    ]

    return data;
}

function _getImports(model){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.importId,
                        p.dsName AS "providerName",
                        i.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        i.currency,
                        s.name AS "statusName",
                        s.style AS style
                        
                    FROM imports i
                    inner JOIN  provider p
                    ON  i.providerId = p.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    WHERE i.inStatus = 1
                    order BY i.importId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getImportDetails(importId){

   var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.importId as "importId",
                        p.dsName AS "providerName",
                        p.dsCode AS "rut",
                        i.total,
                        ROUND(i.total/1.19,0) as "netPrice",
                        i.total - ROUND(i.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        i.origin AS "originId",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        i.currency AS "currency",
                        i.currencyValue AS "currencyValue",
                        if(i.type=1, 'Nacional','Internacional') AS "type",
                        s.name AS "statusName",
                        s.style AS style,
                        i.discount AS "discount",
                        i.commentary AS "comment"
                        
                    FROM imports i
                    inner JOIN  provider p
                    ON  i.providerId = p.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  users us
                    ON  i.usModifierId = us.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    WHERE i.importId =:importId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            importId:importId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise 
}

function _getImportsItems(importId){

    var deferred = Q.defer()

    let sql = `SELECT   i.id as itemId ,
                        i.dsName AS itemDescription,
                        ii.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        i.brandCode AS brandCode,
                        i.netPrice AS netPurchaseValue,
                        ii.nmPrice AS price,
                        ii.discount AS discount,
                        ii.total
                FROM importsitems ii
                INNER JOIN items i
                ON  i.id = ii.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE importId = :importId
                GROUP BY    ii.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            importId:importId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _getImportsItemsLocation(importId,originId){

    var deferred = Q.defer()

    let sql = `SELECT     distinct STB.*,
                        SUM(ITW.itemQty) as "generalStock"
                FROM (SELECT   
					         @i := @i + 1 AS id,
					         i.id as itemId ,
                        i.dsName AS itemDescription,
                        ii.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        i.brandCode AS brandCode,
                        i.netPrice AS netPurchaseValue,
                        ii.nmPrice AS price,
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        ii.discount AS discount,
                        ii.total
                FROM importsitems ii
                INNER JOIN items i
                ON  i.id = ii.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                
                INNER JOIN  itemwarehouse iw
                ON i.id = iw.itemId and iw.warehouseId = :originId
                INNER JOIN  itemwarehouselocation iwl
                ON i.id = iwl.itemId and iwl.warehouseId = :originId
                INNER JOIN  location l
                ON l.id = iwl.locationId
                cross join (select @i := 0) r        
                        
                WHERE importId = :importId
                GROUP BY    ii.id)STB
                INNER JOIN  itemwarehouse ITW
                        ON ITW.itemId =  STB.itemId
                     GROUP BY    STB.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            importId:importId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _importCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0

    let sql = `INSERT INTO imports(date,providerId,status,total,inStatus,origin,commentary,discount,usId,type,currency,currencyValue)
                      VALUES (NOW(),:providerId,1,:total,1,:originId,:observation,:discount,:userId,:type,:currency,:currencyValue)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            total:data.total,
            userId:data.userId,
            type:data.type,
            originId:data.originId,
            discount:data.discount,
            currency:data.currency,
            currencyValue:data.currencyValue,
            observation:data.comment
        }
    }).then(function (importId) {
        deferred.resolve(importId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _importCreateItems(items,importId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
        
    let sql = `   INSERT INTO importsitems (importId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:importId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            importId:importId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateImport(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE imports SET status=:status, usModifierId =:userId WHERE importId=:importId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            status:data.statusValue,
            userId:data.userId,
            importId:data.importId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getAfterSale(){

     var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        af.afterSaleId as afterSaleId,
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        af.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(af.date, '%d/%m/%y') as date,
                        af.documentId,
                        u.usName AS "userCreation",
                        s.name AS "statusName",
                        t.name AS type
                        
                    FROM aftersale af
                    inner JOIN  clients c
                    ON  af.clientId = c.id
                    left JOIN  warehouse w
                    ON  af.origin = w.id
                    left JOIN  users u
                    ON  af.usId = u.usId
                    left JOIN  typeDocument t
                    ON  t.id = af.type
                    left JOIN  status s
                    ON  af.status = s.id
                    WHERE af.inStatus = 1
                    order BY af.afterSaleId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getAfterSaleDetails(afterSaleId){

    
    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        af.afterSaleId as afterSaleId,
                        c.dsName AS clientName,
                        c.dsCode AS rut,
                        af.total,
                        ROUND(af.total/1.19,0) as "netPrice",
                        w.dsCode AS "origin",
                        af.documentId,
                        af.total - ROUND(af.total/1.19)  as "priceVAT",
                        DATE_FORMAT(af.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        "Reportado" AS "statusName",
                        af.discount AS "discount",
                        t.name AS type,
                        af.commentary AS "comment"
                        
                    FROM aftersale af
                    inner JOIN  clients c
                    ON  af.clientId = c.id
                    left JOIN  warehouse w
                    ON  af.origin = w.id
                    left JOIN  users u
                    ON  af.usId = u.usId
                    left JOIN  users us
                    ON  af.usModifierId = us.usId
                    left JOIN  typeDocument t
                    ON  t.id = af.type
                    WHERE af.afterSaleId =  :afterSaleId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            afterSaleId:afterSaleId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getAfterSaleItems(afterSaleId){

    var deferred = Q.defer()

    let sql = `SELECT   itemId,
                        i.dsName AS itemDescription,
                        afi.nmUnities as quantityItems,
                        afi.nmPrice AS price,
                        afi.discount AS discount,
                        afi.total
                FROM aftersaleitems afi
                INNER JOIN items i
                ON  i.id = afi.itemId
                WHERE afterSaleId = :afterSaleId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            afterSaleId:afterSaleId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _afterSaleCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Baja por falla"
    if(data.discount == undefined) data.discount=0

    let sql = `INSERT INTO aftersale(date,clientId,documentId,status,discount,total,inStatus,origin,commentary,usId,type)
                      VALUES (NOW(),:clientId,:documentId,6,:discount,:total,1,:originId,:observation,:userId,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            documentId:data.documentId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            type:data.type,
            observation:data.comment
        }
    }).then(function (afterSaleId) {
        deferred.resolve(afterSaleId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _afterSaleItemsCreate(items,afterSaleId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO aftersaleitems (afterSaleId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:afterSaleId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            afterSaleId:afterSaleId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getImportsRegistration(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.importId,
                        p.dsName AS "providerName",
                        i.codeInvoice AS "codeInvoice",
                        i.shipmentNumber AS "shipmentNumber",
                        i.total,
                        i.packing AS "packing",
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.receptionDate, '%d/%m/%y') as receptionDate,
                        u.usName AS "userCreation",
                        i.currency,
                        sa.name AS "arrivalStatus",
                        s.code AS "statusName"
                        
                    FROM importregistration i
                    inner JOIN  provider p
                    ON  i.providerId = p.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    left JOIN  status sa
                    ON  i.arrivalStatus = sa.id
                    WHERE i.inStatus = 1
                    order BY i.importId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _importRegistrationCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.currencyValue==undefined) data.currencyValue=0

    let sql = `INSERT INTO importregistration(date,providerId,status,total,inStatus,origin,commentary,codeInvoice,usId,shipmentNumber,currency,currencyValue,packing,packingQuantity,arrivalDate,receptionDate,customsTax)
                      VALUES (NOW(),:providerId,1,:total,1,:originId,:observation,:codeInvoice,:userId,:shipmentNumber,:currency,:currencyValue,:packing,:packingQuantity,:arrivalDate,:receptionDate,:customsTax)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            total:data.total,
            userId:data.userId,
            shipmentNumber:data.shipmentNumber,
            originId:data.originId,
            codeInvoice:data.codeInvoice,
            currency:data.currency,
            currencyValue:data.currencyValue,
            packing:data.packing,
            packingQuantity:data.packingQuantity,
            arrivalDate:data.arrivalDate,
            receptionDate:data.receptionDate,
            customsTax:data.customsTax,
            observation:data.comment
        }
    }).then(function (importId) {
        deferred.resolve(importId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getImportsRegistrationDetails(importId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.importId,
                        p.dsName AS "providerName",
                        p.id AS "providerId",
                        i.codeInvoice AS "codeInvoice",
                        i.shipmentNumber AS "shipmentNumber",
                        i.total,
                        i.packing AS "packing",
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        DATE_FORMAT(i.receptionDate, '%d/%m/%y') as receptionDate,
                        DATE_FORMAT(i.arrivalDate, '%d/%m/%y') as arrivalDate,
                        DATE_FORMAT(i.payDateTax, '%d/%m/%y') as payDateTax,
                        DATE_FORMAT(i.closeDate, '%d/%m/%y') as closeDate,
                        u.usName AS "userCreation",
                        i.packingQuantity AS "packingQuantity",
                        i.currencyValue AS "valueCurrency",
                        i.customsTax AS "customsTax",
                        i.currency,
                        i.commentary AS "commentary",
                        sa.name AS "arrivalStatus",
                        s.name AS "statusName"
                        
                    FROM importregistration i
                    inner JOIN  provider p
                    ON  i.providerId = p.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    left JOIN  status s
                    ON  i.status = s.id
                    left JOIN  status sa
                    ON  i.arrivalStatus = sa.id
                    WHERE importId = :importId
                    order BY i.importId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            importId:importId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _importRegistrationCreatePayment(data){

    var deferred = Q.defer()
    if(data.numberDocument == undefined) data.numberDocument = 0
 
    let sql = `   INSERT INTO importregistrationpayments (recordId,total,paymentMethodId,docNumber,date,observation,usId,currencyValue) 
                         VALUES        (:recordId,:total,:paymentFormId,:numberDocument,NOW(),:comment,:userId,:currencyValue)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            recordId:data.recordId,
            total:data.total,
            paymentFormId:data.paymentFormId,
            numberDocument:data.numberDocument,
            currencyValue:data.currencyValue,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _importRegistrationPaymentList(recordId){

    var deferred = Q.defer()

    let sql = `SELECT    irp.id AS id,
                         DATE_FORMAT(irp.date, '%d/%m/%y') as date,
                         irp.total,
                         irp.observation AS comment,
                         u.usName AS userCreation,
                         irp.docNumber AS document,
                         irp.currencyValue AS currencyValue,
                         pm.dsName AS paymentForm
                         
                FROM importregistrationpayments irp
                inner JOIN  users u
                ON  u.usId = irp.usId
                INNER JOIN  paymentMethod  pm 
                ON pm.paymentMethodId = irp.paymentMethodId 
                WHERE recordId = :recordId AND irp.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            recordId:recordId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _disabledPayment(paymentId,recordId,userId){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM importregistrationpayments where id = :paymentId;
                 UPDATE  importregistration SET  status = 1 WHERE importId = :recordId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: paymentId,
            recordId: recordId,
            userId: userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateImportRegistrationStatus(data){

    var deferred = Q.defer()
 
    let sql = `UPDATE  importregistration SET  status = 7, usModifierId =:userId WHERE importId = :recordId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            recordId: data.recordId,
            userId: data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateImportRegistration(data){

    var deferred = Q.defer()

    if(data.currencyValue==undefined) data.currencyValue=0
    
    let sql = `UPDATE importregistration SET codeInvoice=:codeInvoice, shipmentNumber=:shipmentNumber, packingQuantity=:packingQuantity, total=:total, customsTax=:customsTax, currencyValue=:currencyValue, commentary=:comment, payDateTax=:payDateTaxCustom, usModifierId =:userId WHERE importId=:recordId` 

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            recordId:data.recordId,
            userId:data.userId,
            codeInvoice:data.codeInvoice,
            shipmentNumber:data.shipmentNumber,
            packingQuantity:data.packingQuantity,
            total:data.total, 
            currencyValue:data.currencyValue,
            customsTax:data.customsTax,
            payDateTaxCustom:data.payDateTaxCustom,
            comment:data.comment

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _closeImportRegistration(data){

    var deferred = Q.defer()
 
    let sql = `UPDATE  importregistration SET  arrivalStatus = :value, closeDate=:closeDate, usModifierId =:userId WHERE importId = :recordId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            recordId: data.recordId,
            closeDate: data.closeDate,
            value: data.value,
            userId: data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

// Void Documents
function _getVoidDocuments(){

    var deferred = Q.defer()
    let sql = `SELECT DISTINCT
                        v.id as id,
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        v.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(v.date, '%d/%m/%y') as date,
                        v.documentId,
                        u.usName AS "userCreation",
                        if(v.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(v.reason=1, 'Devolución','Falla') AS "reason",
                        t.code AS "typeDoc"
                        
                    FROM voiddocuments v
                    inner JOIN  clients c
                    ON  v.clientId = c.id
                    left JOIN  warehouse w
                    ON  v.origin = w.id
                    left JOIN  users u
                    ON  v.usId = u.usId
                    left JOIN  typedocument t
                    ON  v.codeDoc = t.id
                    WHERE v.inStatus = 1
                    order BY v.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getVoidDocumentDetails(documentId){

    
    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        v.id as id,
                        c.dsName AS clientName,
                        c.dsCode AS rut,
                        v.total,
                        ROUND(v.total/1.19,0) as "netPrice",
                        w.dsCode AS "origin",
                        v.documentId,
                        v.total - ROUND(v.total/1.19)  as "priceVAT",
                        DATE_FORMAT(v.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(v.status=1, 'Aplicada','No Aplicada') AS "statusName",
                        if(v.reason=1, 'Devolución','Falla') AS "reason",
                        v.commentary AS "comment",
                        t.code AS "typeDoc"
                        
                    FROM voiddocuments v
                    inner JOIN  clients c
                    ON  v.clientId = c.id
                    left JOIN  warehouse w
                    ON  v.origin = w.id
                    left JOIN  users u
                    ON  v.usId = u.usId
                    left JOIN  users us
                    ON  v.usModifierId = us.usId
                    left JOIN  typedocument t
                    ON  v.codeDoc = t.id
                    WHERE v.id = :documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getVoidDocumenItems(documentId){

    var deferred = Q.defer()

    let sql = `SELECT   cni.itemId,
                        i.dsName AS itemDescription,
                        cni.nmUnities as quantityItems,
                        cni.nmPrice AS price,
                        cni.total
                FROM voiddocumentsitems cni
                INNER JOIN items i
                ON  i.id = cni.itemId
                WHERE documentId = :documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createVoidDocument(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"

    let sql = `INSERT INTO voiddocuments(date,clientId,documentId,status,total,inStatus,origin,commentary,usId,reason,codeDoc)
                      VALUES (NOW(),:clientId,:documentSaleId,0,:total,1,:originId,:observation,:userId,:reason,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            documentSaleId:data.documentSaleId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            reason:data.reason,
            type:data.type,
            observation:data.comment
        }
    }).then(function (documentId) {
        deferred.resolve(documentId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsVoidDocument(items,documentId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO voiddocumentsitems (documentId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:documentId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty+:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:documentId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsVoidDocumentFailure(items,documentId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO voiddocumentsitems (documentId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:documentId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:documentId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _voidDocumentStatus(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE voiddocuments SET status=1, usModifierId =:userId WHERE id=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            documentId:data.documentId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getBudgets(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        b.budgetId as "budgetId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        b.total,
                        w.dsCode AS "origin",
                        w.id AS "warehouseId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        b.discount as "discount",
                        if(b.status=1, 'Pagado','Sin pagar') AS "statusName"
                        
                    FROM budgets b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    WHERE b.inStatus = 1
                    order BY b.budgetId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getBudgetsDetails(budgetId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        b.budgetId as "budgetId",
                        c.dsName AS "clientName",
                        c.id AS "clientId",
                        c.dsCode AS "rut",
                        b.total,
                        ROUND(b.total/1.19,0) as "netPrice",
                        b.total - ROUND(b.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        w.id AS "warehouseId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(b.status=1, 'Pagado','Sin Pagar') AS "statusName",
                        b.discount AS "discount",
                        b.nameDescription,
                        b.phone,
                        b.commentary AS "comment"
                        
                    FROM budgets b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    left JOIN  users us
                    ON  b.usModifierId = us.usId
                    WHERE b.budgetId =:budgetId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            budgetId:budgetId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getBudgetsItems(budgetId){

    var deferred = Q.defer()

    let sql = `SELECT   i.id AS itemId,
                        i.dsName AS itemDescription,
                        i.netPrice AS netPurchaseValue,
                        i.dsMaxDiscount AS maxDiscount,
                        i.oil AS oil,
                        i.webType AS webType,
                        ITR.dsReference as referenceKey,
                        bi.nmUnities as quantityItems,
                        bi.nmPrice AS price,
                        bi.discount AS discount,
                        bi.total
                FROM budgetsitems bi
                INNER JOIN items i
                ON  i.id = bi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE budgetId =:budgetId
                GROUP BY    bi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            budgetId:budgetId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}


function _budgetsCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.clientId == undefined) data.clientId = 4934
    if(data.name == undefined) data.name="s/n"
    if(data.phone == undefined) data.phone = "s/n"

    let sql = `INSERT INTO budgets(date,clientId,status,total,inStatus,origin,commentary,discount,usId,nameDescription,phone)
                      VALUES (NOW(),:clientId,1,:total,1,:originId,:observation,:discount,:userId,:name,:phone)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        replacements:{
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            name:data.name,
            phone:data.phone,
            observation:data.comment
        }
    }).then(function (quotationId) {
        deferred.resolve(quotationId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsBudgets(items,budgetId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO budgetsitems (budgetId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:budgetId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            budgetId:budgetId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _updateBudget(data){

    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.name == undefined) data.name="s/n"
    if(data.phone == undefined) data.phone = "s/n"

    var deferred = Q.defer()
    let sql = `DELETE FROM budgetsitems WHERE  budgetId = :budgetId;

               UPDATE  budgets SET  total =:total, 
                                       nameDescription =:name, 
                                       phone =:phone, 
                                       usModifierId =:userId, 
                                       commentary =:observation, 
                                       discount =:discount
                                  WHERE budgetId =:budgetId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            budgetId:data.budgetId,
            userId:data.userId,
            discount:data.discount,
            observation:data.comment,
            total:data.total,
            name:data.name,
            phone:data.phone
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getExemptInvoices(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.documentId as "documentId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        i.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.nmOrder AS "purchaseOrder",
                        u.usName AS "userCreation",
                        i.isOut,
                        if(i.status=1, 'Pagado','Sin Pagar') AS "statusName"
                        
                    FROM exemptinvoices i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    WHERE i.inStatus = 1
                    order BY i.documentId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _exemptInvoicesCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.purchaseOrder == undefined) data.purchaseOrder=0
    if(data.clientId == undefined) data.clientId = 4934

    let sql = `INSERT INTO exemptinvoices(date,clientId,status,total,netTotal,inStatus,origin,commentary,discount,usId,nmOrder)
                      VALUES (NOW(),:clientId,0,:total,:netTotal,1,:originId,:observation,:discount,:userId,:purchaseOrder)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            total:data.total,
            netTotal:data.netTotal,
            purchaseOrder:data.purchaseOrder,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            observation:data.comment
        }
    }).then(function (documentId) {
        deferred.resolve(documentId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _exemptInvoicesItemsAdd(items,documentId,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
        
    let sql = `   INSERT INTO exemptinvoicesitems (documentId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:documentId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:documentId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getExemptInvoicesDetails(documentId){

    var deferred = Q.defer()
 
     let sql = `SELECT DISTINCT
                         i.documentId as "documentId",
                         c.id AS "clientId",
                         c.dsName AS "clientName",
                         c.dsCode AS "rut",
                         i.total,
                         ROUND(i.total/1.19,0) as "netPrice",
                         i.total - ROUND(i.total/1.19)  as "priceVAT",
                         w.dsCode AS "origin",
                         i.nmOrder AS "purchaseOrder",
                         DATE_FORMAT(i.date, '%d/%m/%y') as date,
                         u.usName AS "userCreation",
                         u2.usName AS "userOut",
                         i.isOut,
                         us.usName AS "userModifier",
                         if(i.status=1, 'Pagado','Sin Pagar') AS "statusName",
                         pm.dsName AS paymentForm,
                         DATE_FORMAT(i.datePayment, '%d/%m/%y') as datePayment,
                         i.discount AS "discount",
                         i.commentary AS "comment",
                         td.code AS typeName,
                         td.id AS typeId
                         
                     FROM exemptinvoices i
                     inner JOIN  clients c
                     ON  i.clientId = c.id
                     left JOIN  warehouse w
                     ON  i.origin = w.id
                     left JOIN  users u
                     ON  i.usId = u.usId
                     left JOIN  users us
                     ON  i.usModifierId = us.usId
                     left JOIN  users u2
                     ON  i.userOutId = u2.usId
                     INNER JOIN paymentmethod pm
                     ON  pm.paymentMethodId = i.paymentFormId
                     LEFT JOIN	typedocument td
                     ON			td.id = i.typeDoc
                     WHERE i.documentId = :documentId`
 
     sequelize.query(sql, {
         type: Sequelize.QueryTypes.SELECT,
         replacements:{
             documentId:documentId
         }
     }).then(function (result) {
         deferred.resolve(result)
     }).catch(function (error){
         deferred.reject(error)
     })
 
     return deferred.promise 
 }

 function _getExemptInvoicesItems(documentId){

    var deferred = Q.defer()

    let sql = `SELECT      i.id as itemId ,
                        i.dsName AS itemDescription,
                        i.oil AS oil,
                        pi.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        pi.nmPrice AS price,
                        pi.discount AS discount,
                        pi.total
                FROM exemptinvoicesitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE documentId = :documentId
                GROUP BY    pi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _exemptInvoicesPayment(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE exemptInvoices SET status=1, 
                                      usModifierId =:userId,
                                      paymentFormId = :paymentMethodId,
                                      datePayment = NOW() 
                                      WHERE documentId=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            documentId:data.documentId,
            paymentMethodId: data.paymentMethodId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updatePackOff(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE documentsnn SET packOff=1 WHERE documentId=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:data.documentId
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _importCreateDraft(data){

    var deferred = Q.defer()
    if(data.currency==undefined) data.currency='----'
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0

    let sql = `INSERT INTO imports(date,providerId,status,total,inStatus,origin,commentary,discount,usId,type,currency,currencyValue)
                      VALUES (NOW(),:providerId,11,:total,1,:originId,:observation,:discount,:userId,:type,:currency,:currencyValue)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            providerId:data.providerId,
            total:data.total,
            userId:data.userId,
            type:data.type,
            originId:data.originId,
            discount:data.discount,
            currency:data.currency,
            currencyValue:data.currencyValue,
            observation:data.comment
        }
    }).then(function (importId) {
        deferred.resolve(importId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise

}

function _itemsImportsCreateDraft(items,importId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0
    let sql = `   INSERT INTO importsitems (importId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            draft,
                                            total) 
                                    VALUES (:importId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            1,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            importId:importId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateImportDraft(data){

    var deferred = Q.defer()
    let where = ` `
    if(data.comment==undefined) data.comment="sin comentario";
    if(data.valueCurrency==undefined) data.valueCurrency=0
    if(data.discount == undefined) data.discount=0
    if(data.importId==undefined) data.importId=null 

    var deferred = Q.defer()

    if (data.value==0){
            where += ", date = NOW()"
        }

    let sql = `DELETE FROM importsitems WHERE  importId =:importId;

               UPDATE  imports SET  
                                       total =:total,
                                       currency =:currency,
                                       currencyValue =:valueCurrency, 
                                       usModifierId =:userId, 
                                       commentary =:observation, 
                                       discount =:discount,
                                       status =:value
                                       ${where}
                                  WHERE importId =:importId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            importId:data.importId,
            userId:data.userId,
            discount:data.discount,
            observation:data.comment,
            total:data.total,
            valueCurrency:data.valueCurrency,
            value:data.value,
            currency:data.currency
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

module.exports = {
    _getDocuments: _getDocuments,
    _getDocumentsDetails:_getDocumentsDetails,
    _getDocumentItems:_getDocumentItems,
    _createDocument:_createDocument,
    _payDocument:_payDocument,
    _createItemsDocuments:_createItemsDocuments,
    _updateDocument:_updateDocument,
    _disabledDocument:_disabledDocument,
    _getImports:_getImports,
    _getImportDetails:_getImportDetails,
    _getImportsItems:_getImportsItems,
    _getImportsItemsLocation:_getImportsItemsLocation,
    _importCreate:_importCreate,
    _importCreateItems:_importCreateItems,
    _updateImport:_updateImport,
    _getAfterSale:_getAfterSale,
    _getAfterSaleDetails:_getAfterSaleDetails,
    _getAfterSaleItems:_getAfterSaleItems,
    _afterSaleCreate:_afterSaleCreate,
    _afterSaleItemsCreate:_afterSaleItemsCreate,
    _getImportsRegistration:_getImportsRegistration,
    _importRegistrationCreate:_importRegistrationCreate,
    _getImportsRegistrationDetails:_getImportsRegistrationDetails,
    _importRegistrationPaymentList:_importRegistrationPaymentList,
    _importRegistrationCreatePayment:_importRegistrationCreatePayment,
    _disabledPayment:_disabledPayment,
    _updateImportRegistrationStatus:_updateImportRegistrationStatus,
    _updateImportRegistration:_updateImportRegistration,
    _closeImportRegistration:_closeImportRegistration,
    _getVoidDocuments:_getVoidDocuments,
    _getVoidDocumentDetails:_getVoidDocumentDetails,
    _getVoidDocumenItems:_getVoidDocumenItems,
    _createVoidDocument:_createVoidDocument,
    _createItemsVoidDocument:_createItemsVoidDocument,
    _createItemsVoidDocumentFailure:_createItemsVoidDocumentFailure,
    _voidDocumentStatus:_voidDocumentStatus,
    _getBudgets:_getBudgets,
    _getBudgetsDetails:_getBudgetsDetails,
    _getBudgetsItems:_getBudgetsItems,
    _budgetsCreate:_budgetsCreate,
    _createItemsBudgets:_createItemsBudgets,
    _updateBudget:_updateBudget,
    _getExemptInvoices:_getExemptInvoices,
    _exemptInvoicesCreate:_exemptInvoicesCreate,
    _exemptInvoicesItemsAdd:_exemptInvoicesItemsAdd,
    _getExemptInvoicesDetails:_getExemptInvoicesDetails,
    _getExemptInvoicesItems:_getExemptInvoicesItems,
    _exemptInvoicesPayment:_exemptInvoicesPayment,
    _updatePackOff:_updatePackOff,
    _importCreateDraft:_importCreateDraft,
    _itemsImportsCreateDraft:_itemsImportsCreateDraft,
    _updateImportDraft:_updateImportDraft,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}

