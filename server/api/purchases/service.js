const fs = require('fs');
const path = require('path')
const localenv = require('../../config/local.env.js');

function getFile(req, res) {
    const files = fs.readdirSync(`files/${req.params.importId}`);
    const fileUrls = files.map(file => ({
        name: file,
        url: path.join(__dirname, `../../../files/${req.params.importId}/`, file),
        url2: localenv.DOMAIN + `/api/purchases/file/${req.params.importId}?fileName=${file}`
      }));
      res.json(fileUrls);
}

function getFilePath(fileName, id) {
    const filePath = path.join(__dirname, `../../../files/${id}/`, fileName)
    return filePath;
}

async function uploadFiles(req, res) {
    const importId = req.params.importId;
    const files = req.files;

    //crear carpeta con id si no existiese
    const dir = `files/${importId}`;
    if (!fs.existsSync(dir)) fs.mkdirSync(dir);

    //Renombrar archivos y mover a la carpeta
    files.map(f => {
        const name = `${f.originalname}`;
        const dest = `${dir}/${name}`;
        if (fs.existsSync(dest)) {
        } else {
          fs.renameSync(f.path, dest);
        }
      });

    res.status(200).send({ status:1 })


}

function getFileUrl(req, res) {
	const fileName = req.query.fileName;
	const id = req.params.id;
    const filePath = getFilePath(fileName, id);
    if (filePath) {
        res.sendFile(filePath);
        res.status(200);
    } else {
        console.log('File no existe');
        res.json({
            data: [],
            status: 0,
        });
        res.status(400);
        return
    }
}

function deleteFile(req, res) {
	const fileName = req.query.fileName;
	const id = req.query.id;
    const filePath = getFilePath(fileName, id);
    if (filePath) {
        fs.unlink(filePath, (err) => {
			if (err && err.errno) {
				res.status(500).send('Error al eliminar el archivo.');
			}

			res.status(200).send('Archivo eliminado correctamente.');
		})
    } else {
        res.status(400).send('Archivo no encontrado.');
    }
}

module.exports = {
    getFile: getFile,
    uploadFiles: uploadFiles,
	getFileUrl,
	deleteFile
};
