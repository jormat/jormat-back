const express = require('express');
var router = express.Router();
const bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const MAX_SIZE = 10 * 1024 * 1024;  // 10 MB
const service = require('./service'),
    multer = require('multer')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (file.size > MAX_SIZE) {
            cb(new Error('El archivo debe pesar a lo mas 10mb'));
        } else {
            cb(null, 'files/');
        }

    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
}),
    upload = multer({
        storage,
        limit: {
            fileSize: MAX_SIZE
        }
    })
router.get('/file/:id', service.getFileUrl);
router.delete('/file', service.deleteFile);

router.get('/files/:importId', (req, res) => {
    try {
        service.getFile(req, res);
    } catch (error) {
        res.status(500).send(err);
    }
});



router.post('/multiple/:importId', upload.array('files', 4), (req, res) => {

    try {
        service.uploadFiles(req, res);
    } catch (error) {
        res.status(500).send(err);
    }

})



module.exports = router;
