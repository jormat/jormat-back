'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');




function _getExpenseControl(){

    var deferred = Q.defer()

    let sql = `SELECT    ec.id AS expenseControlId,
                         ec.documentId AS documentId,
                         DATE_FORMAT(ec.date, '%d/%m/%y') as date,
                         w.dsCode AS origin,
                         ec.rut AS rut,
                         ec.providerName AS providerName,
                         DATE_FORMAT(ec.acceptanceDate, '%d/%m/%y') as acceptanceDate,
                         DATE_FORMAT(ec.admissionDate, '%d/%m/%y') as admissionDate,
                         ec.details AS details,
                         ec.shareValue AS shareValue,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
						 a.id AS accountId,
                         a.dsName AS accountName,
                         s.code AS statusName,
                         s.style AS styleStatus,
                         td.code AS documentType

                FROM expensecontrol ec
                left JOIN  warehouse w
                ON  ec.origin = w.id
                left JOIN  typedocument td
                ON  ec.typeDoc = td.id
                left JOIN  users u
                ON  ec.usId = u.usId
                left JOIN  users us
                ON  ec.usModifierId = us.usId
                left JOIN  accounts a
                ON  a.id = ec.accountId
                left JOIN  status s
                ON  ec.statusId = s.id
                WHERE ec.inStatus = 1
                GROUP BY    ec.id
                order BY ec.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getExpenseControlDetails(expenseControlId){

    var deferred = Q.defer()

    let sql = `SELECT    ec.id AS expenseControlId,
                         ec.documentId AS documentId,
                         DATE_FORMAT(ec.date, '%d/%m/%y') as date,
                         w.dsCode AS origin,
                         ec.rut AS rut,
                         ec.providerName AS providerName,
                         DATE_FORMAT(ec.acceptanceDate, '%d/%m/%y') as acceptanceDate,
                         DATE_FORMAT(ec.admissionDate, '%d/%m/%y') as admissionDate,
                         ec.details AS details,
                         ec.shareValue AS shareValue,
                         ec.responsableName AS responsableName,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
                         a.dsName AS accountName,
                         s.code AS statusName,
                         s.maleName AS status,
                         s.style AS styleStatus,
                         td.code AS documentType,
                         td.name AS documentTypeName,
                         if(ec.isArchived=0, 'No','Si') AS "isArchived"

                FROM expensecontrol ec
                left JOIN  warehouse w
                ON  ec.origin = w.id
                left JOIN  typedocument td
                ON  ec.typeDoc = td.id
                left JOIN  users u
                ON  ec.usId = u.usId
                left JOIN  users us
                ON  ec.usModifierId = us.usId
                left JOIN  accounts a
                ON  a.id = ec.accountId
                left JOIN  status s
                ON  ec.statusId = s.id
                WHERE ec.id = :expenseControlId
                GROUP BY    ec.id
                order BY ec.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            expenseControlId:expenseControlId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlCreate(data){

    var deferred = Q.defer()

    if(data.responsable==undefined) data.responsable='';
    if(data.details == undefined) data.details = ''

    let sql = `   INSERT INTO expensecontrol (documentId,accountId,typeDoc,date,admissionDate,origin,usId,shareValue,netValue,details,providerName,rut,responsableName,statusId)
                                 VALUES      (:folio,:accountId,:documentType,:day,NOW(),:origin,:userId,:shareValue,:netValue,:details,:providerName,:rut,:responsable,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            folio:data.folio,
            accountId:data.accountId,
            documentType:data.documentType,
            origin:data.origin,
            userId:data.userId,
            shareValue:data.shareValue,
            netValue : data.netValue,
            details:data.details,
            providerName:data.providerName,
            day:data.day,
            rut:data.rut,
            responsable:data.responsable

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlUpdate(data){

    var deferred = Q.defer()

    if(data.details == undefined) data.details = ''

    let sql = `  UPDATE  expensecontrol SET  documentId = :folio,
                   responsableName = :responsable,
                   providerName = :providerName,
                   shareValue = :shareValue,
                   netValue = :netValue,
                   details = :details,
                   usModifierId =:userId
                   WHERE id = :expenseControlId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            expenseControlId:data.expenseControlId,
            userId:data.userId,
            folio:data.folio,
            responsable:data.responsable,
            providerName:data.providerName,
            shareValue:data.shareValue,
            netValue:data.netValue,
            details:data.details

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlUpdateStatus(data){

    var deferred = Q.defer()

    let sql = `UPDATE  expensecontrol SET  statusId = :value,
                                   usModifierId =:userId,
                                   acceptanceDate = NOW()
                              WHERE id = :expenseControlId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            expenseControlId:data.expenseControlId,
            userId:data.userId,
            value:data.value

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlRejected(data){

    var deferred = Q.defer()

    let sql = `UPDATE  expensecontrol SET  shareValue = 0,
                                           netValue = 0
                              WHERE id = :expenseControlId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            expenseControlId:data.expenseControlId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlArchived(data){

    var deferred = Q.defer()

    let sql = `UPDATE  expensecontrol SET  isArchived = 1,
                                   userArchived =:userId
                              WHERE id = :expenseControlId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            expenseControlId:data.expenseControlId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _expenseControlDisabled(data){

    var deferred = Q.defer()

    let sql = `  UPDATE  expensecontrol SET  inStatus = 0,
                   usModifierId =:userId
                   WHERE id = :expenseControlId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            expenseControlId:data.expenseControlId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getHistoricExpenseControl(startDate,endDate){

    var deferred = Q.defer()

    let sql = `SELECT    ec.id AS expenseControlId,
                         ec.documentId AS documentId,
                         DATE_FORMAT(ec.date, '%d/%m/%y') as date,
                         w.dsCode AS origin,
                         ec.rut AS rut,
                         ec.providerName AS providerName,
                         DATE_FORMAT(ec.acceptanceDate, '%d/%m/%y') as acceptanceDate,
                         DATE_FORMAT(ec.admissionDate, '%d/%m/%y') as admissionDate,
                         ec.details AS details,
                         ec.shareValue AS shareValue,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
                         a.dsName AS accountName,
                         s.code AS statusName,
                         s.style AS styleStatus,
                         td.code AS documentType

                FROM expensecontrol ec
                left JOIN  warehouse w
                ON  ec.origin = w.id
                left JOIN  typedocument td
                ON  ec.typeDoc = td.id
                left JOIN  users u
                ON  ec.usId = u.usId
                left JOIN  users us
                ON  ec.usModifierId = us.usId
                left JOIN  accounts a
                ON  a.id = ec.accountId
                left JOIN  status s
                ON  ec.statusId = s.id
                WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND ec.inStatus = 1
                GROUP BY    ec.id
                order BY ec.date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getAccounts(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    id as "accountId",
                    dsName as "accountName",
                    code as  "accountCode"
                    FROM accounts where inStatus = 1
                    order by id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getAccountsDetails(accountId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT id AS accountId,
                               code,
                               dsName AS description
                FROM           accounts WHERE id = :accountId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            accountId:accountId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _accountsCreate(data){

    var deferred = Q.defer()


    let sql = `   INSERT INTO accounts (dsName,code,inStatus)
                                 VALUES      (:accountName,:accountCode,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            accountName:data.accountName,
            accountCode:data.accountCode
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _accountsUpdate(data){

    var deferred = Q.defer()


    let sql = `UPDATE  accounts SET  dsName = :accountName,
                                     code =:accountCode
                              WHERE  id = :accountId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            accountId:data.accountId,
            accountName:data.accountName,
            accountCode:data.accountCode
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _accountsDisabled(data){

    var deferred = Q.defer()

    let sql = `  UPDATE  accounts SET  inStatus = 0,
                   usModifierId =:userId
                   WHERE id = :accountId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            accountId:data.accountId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

// function _getVouchers(){

//     var deferred = Q.defer()

//     let sql = `     SELECT DISTINCT
//                         v.voucherId as "voucherId",
//                         v.total,
//                         w.dsName AS "origin",
//                         DATE_FORMAT(v.date, '%d/%m/%y') as date,
//                         u.usLastName AS "userCreation",
//                         s.name AS "statusName",
//                         s.style AS "style"

//                     FROM vouchers v
//                     left JOIN  warehouse w
//                     ON  v.origin = w.id
//                     left JOIN  users u
//                     ON  v.usId = u.usId
//                     left JOIN  status s
//                     ON  v.status = s.id
//                     WHERE v.inStatus = 1
//                     order BY v.voucherId DESC`

//     sequelize.query(sql, {
//         type: Sequelize.QueryTypes.SELECT,
//         replacements:{
//             userId:1
//         }
//     }).then(function (result) {
//         deferred.resolve(result)
//     }).catch(function (error){
//         deferred.reject(error)
//     })

//     return deferred.promise
// }

function _getVouchers(){

    var deferred = Q.defer()

    let sql = ` SELECT  id,documentId,paymentForm,payDate,clientName,total,observation,docNumber,typeDoc,userCreation

                FROM (  SELECT      p.id,
                                    p.invoiceId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                                    p.date AS date,
                                    c.dsName as clientName,
                                    p.total,
                                    p.observation,
                                    p.docNumber,
                                    'Factura' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM payments p
                                    INNER JOIN clients c
                                    ON  c.id = p.clientId
                                    INNER JOIN  paymentMethod  pm
                                    ON pm.paymentMethodId = p.paymentMethodId
                                    left JOIN  users u
                                    ON  p.usId = u.usId
                                    WHERE p.inStatus = 1

                        UNION ALL

                        SELECT      p.id,
                                    p.documentId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                                    p.date AS date,
                                    c.dsName as clientName,
                                    p.total,
                                    p.observation,
                                    p.docNumber,
                                    'NN' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM paymentsnn p
                                    INNER JOIN documentsnn d
                                    ON  d.documentId = p.documentId
                                    INNER JOIN clients c
                                    ON  c.id = d.clientId
                                    INNER JOIN  paymentMethod  pm
                                    ON pm.paymentMethodId = p.paymentMethodId
                                    left JOIN  users u
                                    ON  p.usId = u.usId
                                    WHERE p.inStatus = 1

                        UNION ALL

                        SELECT      pb.id as id,
                                    pb.ballotId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(pb.date, '%d/%m/%y') as payDate,
                                    pb.date AS date,
                                    c.dsName AS "clientName",
                                    pb.total,
                                    'sin obervacion' as observation,
                                    'no define' as docNumber,
                                    'Boleta' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM paymentsballots pb
                                    INNER JOIN ballots b
                                    ON  b.ballotId = pb.ballotId
                                    inner JOIN  clients c
                                    ON  b.clientId = c.id
                                    left JOIN  users u
                                    ON  pb.usId = u.usId
                                    INNER JOIN paymentmethod pm
                                    ON  pm.paymentMethodId = pb.paymentMethodId
                                    WHERE pb.inStatus = 1
                        )consulta

                order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getVouchersDetails(voucherId){

    var deferred = Q.defer()

    let sql = `SELECT   p.id,
                        p.invoiceId as documentId,
                        pm.dsName AS paymentForm,
                        DATE_FORMAT(p.date, '%d/%m/%y') as date,
                        p.total,
                        c.dsName as clientName,
                        p.observation,
                        p.docNumber,
                        u.usName AS "userCreation"

                FROM payments p
                INNER JOIN clients c
                ON  c.id = p.clientId
                INNER JOIN  paymentMethod  pm
                ON pm.paymentMethodId = p.paymentMethodId
                INNER JOIN  users u
                ON  p.usId = u.usId
                WHERE p.id = :voucherId and p.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            voucherId:voucherId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getVouchersReport(startDate,endDate){

    var deferred = Q.defer()

    let sql = `  SELECT  id,documentId,paymentForm,payDate,clientName,total,observation,docNumber,typeDoc,userCreation

                FROM (  SELECT      p.id,
                                    p.invoiceId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                                    p.date AS date,
                                    C.dsName as clientName,
                                    p.total,
                                    p.observation,
                                    p.docNumber,
                                    'Factura' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM payments p
                                    INNER JOIN clients c
                                    ON  c.id = p.clientId
                                    INNER JOIN  paymentMethod  pm
                                    ON pm.paymentMethodId = p.paymentMethodId
                                    left JOIN  users u
                                    ON  p.usId = u.usId
                                    WHERE p.inStatus = 1

                        UNION ALL

                        SELECT      i.documentId as id,
                                    i.documentId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(i.datePayment, '%d/%m/%y') as payDate,
                                    i.datePayment AS date,
                                    c.dsName AS "clientName",
                                    i.total,
                                    'sin obervacion' as observation,
                                    'no define' as docNumber,
                                    'NN' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM documentsnn i
                                    inner JOIN  clients c
                                    ON  i.clientId = c.id
                                    left JOIN  users u
                                    ON  i.usModifierId = u.usId
                                    INNER JOIN paymentmethod pm
                                    ON  pm.paymentMethodId = i.paymentFormId
                                    WHERE i.inStatus = 1 AND i.status=1

                        UNION ALL

                        SELECT      b.ballotId as id,
                                    b.ballotId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(b.datePayment, '%d/%m/%y') as payDate,
                                    b.datePayment AS date,
                                    c.dsName AS "clientName",
                                    b.total,
                                    'sin obervacion' as observation,
                                    'no define' as docNumber,
                                    'Boleta' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM ballots b
                                    inner JOIN  clients c
                                    ON  b.clientId = c.id
                                    left JOIN  users u
                                    ON  b.usModifierId = u.usId
                                    INNER JOIN paymentmethod pm
                                    ON  pm.paymentMethodId = b.paymentFormId
                                    WHERE b.inStatus = 1 AND b.status=2
                        )consulta

            WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
            order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getVouchersItems(voucherId){

    var deferred = Q.defer()

    let sql = `SELECT  DISTINCT
                        vi.id,
                        vi.documentId AS documentId,
                        i.clientId AS clientId,
                        c.dsName AS clientName,
                        td.name AS type,
                        vi.date as date,
                        pm.dsName AS paymentForm,
                        vi.paymentForm AS paymentFormId,
                        vi.pay,
                        vi.total,
                        IFNULL(vi.total - SUM(p.total),vi.total) AS pending,
                        if(i.status=1, 1,0) AS "status",
                        vi.isClick,
                        'false' as disabled

                        FROM voucheritems vi
                        INNER JOIN typedocument td
                        ON  td.id = vi.type
                        INNER JOIN invoices i
                        ON  i.invoiceId = vi.documentId
                        left JOIN payments p
                            ON  i.invoiceId = p.invoiceId
                        INNER JOIN clients c
                        ON  c.id = i.clientId
                        INNER JOIN paymentmethod pm
                        ON  pm.paymentMethodId = vi.paymentForm
                        WHERE vi.voucherId = :voucherId
                        GROUP BY       i.invoiceId

                    UNION ALL

                    SELECT  DISTINCT
                        vi.id,
                        vi.documentId AS documentId,
                        b.clientId AS clientId,
                        c.dsName AS clientName,
                        td.name AS type,
                        vi.date as date,
                        pm.dsName AS paymentForm,
                        vi.paymentForm AS paymentFormId,
                        vi.pay,
                        vi.total,
                        vi.total AS pending,
                        if(b.status=1, 1,0) AS "status",
                        vi.isClick,
                        'false' as disabled

                        FROM voucheritems vi
                        INNER JOIN typedocument td
                        ON  td.id = vi.type
                        INNER JOIN ballots b
                        ON  b.ballotId = vi.documentId
                        INNER JOIN clients c
                        ON  c.id = b.clientId
                        INNER JOIN paymentmethod pm
                        ON  pm.paymentMethodId = vi.paymentForm
                        WHERE vi.voucherId = :voucherId
                        GROUP BY    vi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            voucherId:voucherId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _vouchersCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"

    let sql = `INSERT INTO vouchers(date,status,total,inStatus,origin,commentary,usId)
                      VALUES (NOW(),1,:total,1,:originId,:observation,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            observation:data.comment
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _vouchersItemsAssociated(items,voucherId){

    var deferred = Q.defer()
    let sql = `   INSERT INTO voucheritems (voucherId,
                                            documentId,
                                            pay,
                                            total,
                                            type,
                                            date,
                                            paymentForm)
                                    VALUES (:voucherId,
                                            :documentId,
                                            :pay,
                                            :totalDocument,
                                            :typeId,
                                            :date,
                                            :paymentMethod)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            voucherId:voucherId,
            documentId:items.documentId,
            totalDocument:items.totalDocument,
            pay:items.pay,
            date:items.date,
            typeId:items.typeId,
            paymentMethod:parseInt(items.paymentMethod)

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _vouchersUpdate(data){

    var deferred = Q.defer()

    let sql = `UPDATE vouchers SET status=:status, usModifierId =:userId WHERE voucherId=:voucherId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            status:data.statusValue,
            userId:data.userId,
            voucherId:data.voucherId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _statusClickUpdate(data){

    var deferred = Q.defer()

    let sql = `UPDATE voucheritems SET isClick=1, usModifierId =:userId WHERE id=:paymentItemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            paymentItemId:data.paymentItemId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

const _getVoucherPages=(filters, offset, limit, count) =>{

    let whereCount = ` 	WHERE 1=1 `
	let where = ` 	WHERE 1=1 `
	let sql = ``

	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'documentId':
					where += "	AND documentId =" + parseInt(filter[i])
					whereCount += "	AND documentId =" + parseInt(filter[i])
					break
				case 'clientName':
					where += "	AND clientName like '%" + filter[i] + "%'"
					whereCount += "	AND clientName like '%" + filter[i] + "%'"
					break
				case 'p.docNumber':
					where += "	AND p.docNumber like '%" + filter[i] + "%'"
					whereCount += "	AND p.docNumber like '%" + filter[i] + "%'"
					break
				
				case 'paymentForm':
					where += "	AND paymentForm like '%" + filter[i] + "%'"
					whereCount += "	AND paymentForm like '%" + filter[i] + "%'"
					break
				case 'dateOut':
					where += "	AND dateOut like '%" + filter[i] + "%'"
					whereCount += "	AND dateOut like '%" + filter[i] + "%'"
					break
				case 'userCreation':
					where += "	AND userCreation like '%" + filter[i] + "%'"
					whereCount += "	AND userCreation like '%" + filter[i] + "%'"
					break

			}
		}
	}


    if (!count) {
        
        sql = ` SELECT  id,documentId,paymentForm,payDate,clientName,total,observation,docNumber,typeDoc,userCreation

                FROM (  SELECT      p.id,
                                    p.invoiceId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                                    p.date AS date,
                                    c.dsName as clientName,
                                    p.total,
                                    p.observation,
                                    p.docNumber,
                                    'Factura' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM payments p
                                    INNER JOIN clients c
                                    ON  c.id = p.clientId
                                    INNER JOIN  paymentMethod  pm
                                    ON pm.paymentMethodId = p.paymentMethodId
                                    left JOIN  users u
                                    ON  p.usId = u.usId
                                    WHERE p.inStatus = 1

                        UNION ALL

                        SELECT      p.id,
                                    p.documentId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                                    p.date AS date,
                                    c.dsName as clientName,
                                    p.total,
                                    p.observation,
                                    p.docNumber,
                                    'NN' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM paymentsnn p
                                    INNER JOIN documentsnn d
                                    ON  d.documentId = p.documentId
                                    INNER JOIN clients c
                                    ON  c.id = d.clientId
                                    INNER JOIN  paymentMethod  pm
                                    ON pm.paymentMethodId = p.paymentMethodId
                                    left JOIN  users u
                                    ON  p.usId = u.usId
                                    WHERE p.inStatus = 1

                        UNION ALL

                        SELECT      pb.id as id,
                                    pb.ballotId as documentId,
                                    pm.dsName AS paymentForm,
                                    DATE_FORMAT(pb.date, '%d/%m/%y') as payDate,
                                    pb.date AS date,
                                    c.dsName AS "clientName",
                                    pb.total,
                                    'sin obervacion' as observation,
                                    'no define' as docNumber,
                                    'Boleta' AS typeDoc,
                                    u.usName AS "userCreation"

                                    FROM paymentsballots pb
                                    INNER JOIN ballots b
                                    ON  b.ballotId = pb.ballotId
                                    inner JOIN  clients c
                                    ON  b.clientId = c.id
                                    left JOIN  users u
                                    ON  pb.usId = u.usId
                                    INNER JOIN paymentmethod pm
                                    ON  pm.paymentMethodId = pb.paymentMethodId
                                    WHERE pb.inStatus = 1
                        )consulta
                         ${where}
            order BY date DESC
            LIMIT :limit OFFSET :offset;`

    }else{
        
        sql = ` SELECT COUNT(documentId) AS total

        FROM (  SELECT      p.id,
                            p.invoiceId as documentId,
                            pm.dsName AS paymentForm,
                            DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                            p.date AS date,
                            c.dsName as clientName,
                            p.total,
                            p.observation,
                            p.docNumber,
                            'Factura' AS typeDoc,
                            u.usName AS "userCreation"

                            FROM payments p
                            INNER JOIN clients c
                            ON  c.id = p.clientId
                            INNER JOIN  paymentMethod  pm
                            ON pm.paymentMethodId = p.paymentMethodId
                            left JOIN  users u
                            ON  p.usId = u.usId
                            WHERE p.inStatus = 1

                UNION ALL

                SELECT      p.id,
                            p.documentId as documentId,
                            pm.dsName AS paymentForm,
                            DATE_FORMAT(p.date, '%d/%m/%y') as payDate,
                            p.date AS date,
                            c.dsName as clientName,
                            p.total,
                            p.observation,
                            p.docNumber,
                            'NN' AS typeDoc,
                            u.usName AS "userCreation"

                            FROM paymentsnn p
                            INNER JOIN documentsnn d
                            ON  d.documentId = p.documentId
                            INNER JOIN clients c
                            ON  c.id = d.clientId
                            INNER JOIN  paymentMethod  pm
                            ON pm.paymentMethodId = p.paymentMethodId
                            left JOIN  users u
                            ON  p.usId = u.usId
                            WHERE p.inStatus = 1

                UNION ALL

                SELECT      pb.id as id,
                            pb.ballotId as documentId,
                            pm.dsName AS paymentForm,
                            DATE_FORMAT(pb.date, '%d/%m/%y') as payDate,
                            pb.date AS date,
                            c.dsName AS "clientName",
                            pb.total,
                            'sin obervacion' as observation,
                            'no define' as docNumber,
                            'Boleta' AS typeDoc,
                            u.usName AS "userCreation"

                            FROM paymentsballots pb
                            INNER JOIN ballots b
                            ON  b.ballotId = pb.ballotId
                            inner JOIN  clients c
                            ON  b.clientId = c.id
                            left JOIN  users u
                            ON  pb.usId = u.usId
                            INNER JOIN paymentmethod pm
                            ON  pm.paymentMethodId = pb.paymentMethodId
                            WHERE pb.inStatus = 1
                )consulta
                ${whereCount};`
        

    }
    return sequelize.query(sql, {
		type: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		}
	})
}

module.exports = {
    _getExpenseControl:_getExpenseControl,
    _getExpenseControlDetails:_getExpenseControlDetails,
    _expenseControlCreate:_expenseControlCreate,
    _expenseControlUpdate:_expenseControlUpdate,
    _expenseControlUpdateStatus:_expenseControlUpdateStatus,
    _expenseControlRejected:_expenseControlRejected,
    _expenseControlArchived:_expenseControlArchived,
    _expenseControlDisabled:_expenseControlDisabled,
    _getHistoricExpenseControl:_getHistoricExpenseControl,
    _getAccounts:_getAccounts,
    _getAccountsDetails:_getAccountsDetails,
    _accountsCreate:_accountsCreate,
    _accountsUpdate:_accountsUpdate,
    _accountsDisabled:_accountsDisabled,
    _getVouchers:_getVouchers,
    _getVouchersDetails:_getVouchersDetails,
    _getVouchersReport:_getVouchersReport,
    _getVouchersItems:_getVouchersItems,
    _vouchersCreate:_vouchersCreate,
    _vouchersItemsAssociated:_vouchersItemsAssociated,
    _vouchersUpdate:_vouchersUpdate,
    _statusClickUpdate:_statusClickUpdate,
    _getVoucherPages:_getVoucherPages,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}
