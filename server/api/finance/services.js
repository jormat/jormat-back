'use strict'

var model = require('./model');

//Payments clients

function getExpenseControl(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getExpenseControl())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getExpenseControlDetails(req, res) {

    var query = [];
    var expenseControlId = req.query['expenseControlId'];
    query.push(model._getExpenseControlDetails(expenseControlId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlCreate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "el costo ha sido creado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "el costo ha sido actualizado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlUpdateStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlUpdateStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "El costo ha sido actualizado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlArchived(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlArchived(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "El costo ha sido archivado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlRejected(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlRejected(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "el costo ha sido rechazado exitosamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function expenseControlDisabled(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._expenseControlDisabled(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "el costo ha sido desabilitado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getHistoricExpenseControl(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getHistoricExpenseControl(startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAccounts(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getAccounts())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAccountsDetails(req, res) {

    var query = [];
    var accountId = req.query['accountId'];
    query.push(model._getAccountsDetails(accountId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function accountsCreate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._accountsCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "la cuenta ha sido creado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function accountsUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._accountsUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "la cuenta ha sido actualizada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function accountsDisabled(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._accountsDisabled(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "la cuenta ha sido desabilitado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVouchers(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getVouchers())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVouchersDetails(req, res) {

    var query = [];
    var voucherId = req.query['voucherId'];
    query.push(model._getVouchersDetails(voucherId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVouchersReport(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getVouchersReport(startDate,endDate))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getVouchersItems(req, res) {

    var query = [];
    var voucherId = req.query['voucherId'];
    query.push(model._getVouchersItems(voucherId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function vouchersCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._vouchersCreate(data)]).then((result) => {
            query=[]
            for(var i in data.paymentsItems){
                query.push(model._vouchersItemsAssociated(data.paymentsItems[i],result[0]))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services vouchersCreate",
            status: 0
        });
    }
}

function vouchersUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._vouchersUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function statusClickUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._statusClickUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

/*function getVoucherPages(req, res) {
    //console.log("paramss", req.query['newPage'],req.query['newPage'])
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = req.query['pageSize'] ? parseInt(req.query['pageSize']) : false
    const clientName = req.query.searchText
	const {
		filters = {},
	} = req.query

	const promises = [
		model._getVoucherPages(clientName,filters, offset, limit, false),
		model._getVoucherPages(clientName,filters, offset, limit, true)
	]

	Promise.all(promises).then((result) => {
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}*/

const getVoucherPages = (req, res) => {
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = parseInt(req.query['pageSize'])
	const {
		filters = {}
	} = req.query

	const promises = [
		model._getVoucherPages(filters, offset, limit, false),
		model._getVoucherPages(filters, offset, limit, true)
	]

	Promise.all(promises).then((result) => {
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
	}).catch((error) => {
		res.json({
			data: [],
			message: error,
			status: 0
		});
	})
}

module.exports = {
    getExpenseControl:getExpenseControl,
    getExpenseControlDetails:getExpenseControlDetails,
    expenseControlCreate:expenseControlCreate,
    expenseControlUpdateStatus:expenseControlUpdateStatus,
    expenseControlRejected:expenseControlRejected,
    expenseControlUpdate:expenseControlUpdate,
    expenseControlArchived:expenseControlArchived,
    expenseControlDisabled:expenseControlDisabled,
    getHistoricExpenseControl:getHistoricExpenseControl,
    getAccounts:getAccounts,
    getAccountsDetails:getAccountsDetails,
    accountsCreate:accountsCreate,
    accountsUpdate:accountsUpdate,
    accountsDisabled:accountsDisabled,
    getVouchers:getVouchers,
    getVouchersDetails:getVouchersDetails,
    getVouchersReport:getVouchersReport,
    getVouchersItems:getVouchersItems,
    vouchersCreate:vouchersCreate,
    vouchersUpdate:vouchersUpdate,
    statusClickUpdate:statusClickUpdate,
    getVoucherPages:getVoucherPages,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}