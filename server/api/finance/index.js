var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');


//expenseControl
router.get('/expenseControl/list', service.getExpenseControl);
router.get('/expenseControl', service.getExpenseControlDetails);
router.post('/expenseControl', service.expenseControlCreate);
router.put('/expenseControl', service.expenseControlUpdate);
router.put('/expenseControl/status', service.expenseControlUpdateStatus);
router.put('/expenseControl/archived', service.expenseControlArchived);
router.put('/expenseControl/rejected', service.expenseControlRejected);
router.put('/expenseControl/disabled', service.expenseControlDisabled);
router.get('/expenseControl/report', service.getHistoricExpenseControl);


//accounts
router.get('/accounts/list', service.getAccounts);
router.get('/accounts', service.getAccountsDetails);
router.put('/accounts', service.accountsUpdate);
router.post('/accounts', service.accountsCreate);
router.put('/accounts/disabled', service.accountsDisabled);

//vouchers
router.get('/vouchers/list', service.getVouchers);
router.get('/vouchers', service.getVouchersDetails);
router.post('/vouchers', service.vouchersCreate);
router.get('/vouchers/report', service.getVouchersReport);
router.get('/vouchers/payments', service.getVouchersItems);
router.put('/vouchers/validate', service.vouchersUpdate);
router.put('/vouchers/click', service.statusClickUpdate);
router.get('/vouchers/Pagination', service.getVoucherPages);

module.exports = router;