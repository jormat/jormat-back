const bluebird = require('bluebird')
const path = require('path')

const localenv = require('../../config/local.env.js')
const unoconvConfig = localenv.UNOCONV || { host: '127.0.0.1', port: 9000 }

const execAsync = (command) => new bluebird((resolve, reject) => {
	exec(command, (status, response) => {
		if (status === 1 || status === 0) {
			return resolve({
				status,
				response,
			})
		}

		reject(new Error(`Command: "${command}" - Code: "${status}" - Response: "${response}"`))
	})
})

const _getConversionPath = (pathToJoin) => {
	const runInK8SInstance = localenv.K8S || false
	let rootPath

	if (runInK8SInstance) {
		rootPath = '/uploadfiles'
	} else {
		rootPath = path.join('/unoconv', localenv.UPLOADFILE_FOR_DB)
	}

	return path.join(rootPath, pathToJoin)
}

const _libreOffice = (inputFormat, inputFileName, outputFormat, outputFileName, conversionFolder) => new bluebird((resolve, reject) => {
	// Documentation with bash command: man unoconv
	const conversionPath = _getConversionPath(conversionFolder)
	const outputFilePath = path.join(conversionPath, outputFileName)
	const inputFilePath = path.join(conversionPath, inputFileName)

	// Ignore output with '2> /dev/null' because when is used a remote server doesn't recognize the paths
	//    e.g.: unoconv: file `/unoconv/uploadfiles-prod/conversion/courseProgram-36_1579633977683/UPL150-1579633984450.docx' does not exist.
	const command = `unoconv --format=${outputFormat} --doctype=${inputFormat} --server ${unoconvConfig.host} --port ${unoconvConfig.port} --output '${outputFilePath}' '${inputFilePath}' 2> /dev/null`

	return execAsync(command)
		.then(resolve)
		.catch(reject)
})

export const libreOffice = {
	wordToPdf: (inputFileName, outputFileName, conversionFolder) => {
		return _libreOffice('document', inputFileName, 'pdf', outputFileName, conversionFolder)
	},
}
