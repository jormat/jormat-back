'use strict'

var model = require('./model');


function getWarehouses(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getWarehouses(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getWarehousesUser(req, res) {

    var query = [];
    var userId = req.query['userId'];
    query.push(model._getWarehousesUser(userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getTransportation(req, res) {

    var query = [];
    let transportationId = req.param('transportationId')
    
    query.push(model._getTransportation(transportationId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAddress(req, res) {

    var query = [];
    let id = req.param('id')
    
    query.push(model._getAddress(id))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function transportationCreate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._transportationCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the _transportation is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function transportationUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._transportationUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the t is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getWarehouses: getWarehouses,
    getWarehousesUser:getWarehousesUser,
    getTransportation:getTransportation,
    getAddress:getAddress,
    transportationCreate:transportationCreate,
    transportationUpdate:transportationUpdate,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}