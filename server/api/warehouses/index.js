var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/list', service.getWarehouses);
router.get('/transportation', service.getTransportation);
router.post('/transportation', service.transportationCreate);
router.put('/transportation', service.transportationUpdate);
router.get('/listByUser', service.getWarehousesUser);
router.get('/address', service.getAddress);

module.exports = router;