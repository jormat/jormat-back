'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');


function _getWarehouses(userId){

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                    id as "warehouseId",
                    dsName as "warehouseName",
                    dsCode as  "warehouseCode",
                    branchOffice as  "officeName",
                    rut as "rut",
                    clientId as  "clientId",
                    providerId as  "providerId",
                    clientDescription as  "providerName",
                    clientDescription as  "fullName"
                    FROM warehouse`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getWarehousesUser(userId){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT w.id as "warehouseId",
                                    w.dsName as "warehouseName",
                                    w.dsCode as  "warehouseCode",
                                    w.rut as "rut",
                                    w.clientId as  "clientId",
                                    w.clientDescription as  "fullName"
                    FROM            userwarehouse uw
                    INNER JOIN  users u
                    ON          u.usId = uw.usId
                    LEFT JOIN   warehouse w
                    ON          w.id = uw.warehouseId 
                    WHERE u.usId = :userId AND inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getTransportation(transportationId){

    var deferred = Q.defer()

    let query = ''
    if (transportationId == undefined){
        query = 'WHERE inStatus = 1'
    }else{
        query = 'WHERE id = :transportationId'
    }

    let sql = `		SELECT DISTINCT
                    id as "transportationId",
                    dsName as "transportName",
                    dsCode as  "transportCode",
                    schedule as  "schedule",
                    phone as  "phone",
                    trackinglink as  "link",
                    comment as "comment"
                    FROM transportcompany 
                    `+ query +``

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            transportationId:transportationId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getAddress(id){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                    id AS "id",
                    addressName as "addressName",
                    phone as  "phone",
                    city as  "city",
                    regionId as  "regionId"
                    FROM address 
                    WHERE transportationId = :id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            id:id
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _transportationCreate(data){

    var deferred = Q.defer()
    if(data.phone==undefined) data.phone=null
    if(data.link==undefined) data.link=null
    if(data.schedule==undefined) data.schedule=null
    
    let sql = ` INSERT INTO transportcompany (
                                dsName,
                                dsCode,
                                userUpdate,
                                phone,
                                trackingLink,
                                schedule,
                                inStatus) 
                      VALUES (  :name,
                                :code,
                                :userId,
                                :phone,
                                :link,
                                :schedule,
                                 1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId: data.userId,
            name:data.name,
            code: data.code,
            phone: data.phone,
            link: data.link,
            schedule: data.schedule

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise

}

function _transportationUpdate(data){
    var deferred = Q.defer()
    if(data.phone==undefined) data.phone=null
    if(data.link==undefined) data.link=null
    if(data.schedule==undefined) data.schedule=null
    

    let sql = `update  transportcompany set dsName=:name,
                                dsCode=:code,
                                userUpdate=:userId,
                                phone=:phone,
                                trackingLink=:link,
                                schedule=:schedule
               where  id=:transportationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            transportationId: data.transportationId,
            userId: data.userId,
            name:data.name,
            code: data.code,
            phone: data.phone,
            link: data.link,
            schedule: data.schedule
          
        }
    }).then(function (itemId) {
        deferred.resolve(itemId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getWarehouses: _getWarehouses,
    _getWarehousesUser:_getWarehousesUser,
    _getTransportation:_getTransportation,
    _getAddress:_getAddress,
    _transportationCreate:_transportationCreate,
    _transportationUpdate:_transportationUpdate,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}