'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

function _getFoliosList(userId){

    var deferred = Q.defer()

    let sql = `		SELECT   f.loadId AS loadId,
                                    f.folios AS folios,
                                    DATE_FORMAT(f.date, '%d/%m/%y') as date,
                                    u.usName as userName,
                                    f.format as format,
                                    f.path as path

                                    from folios f
                                    inner join users u
                                    on u.usId=f.userId
                                    order BY date DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCertificates(userId){

    var deferred = Q.defer()

    let sql = `		SELECT      c.id AS id,
                                c.file AS file,
                                DATE_FORMAT(c.date, '%d/%m/%y') as date,
                                u.usName as userName,
                                c.password as password,
                                c.serialNumber as serialNumber,
                                DATE_FORMAT(c.expiration, '%d/%m/%y %T') as expirationDate,
                                c.owner as owner,
                                c.path as path

                                from certificates c
                                inner join users u
                                on u.usId=c.usId
                                order BY date DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCompanyData(userId){

    console.log('entra _getCompanyData')

    var deferred = Q.defer()

    let sql = `		SELECT      c.id AS id,
                                c.businessName AS businessName,
                                c.address AS address,
                                c.cityName AS cityName,
                                c.country AS country,
                                c.serialNumber AS serialNumber,
                                c.type AS type,
                                c.description AS description,
                                c.currency AS currency,
                                c.phone AS phone,
                                c.cellPhone AS cellPhone,
                                c.email AS email,
                                c.web AS web,
                                c.dteEmail AS dteEmail,
                                c.environmentServices AS environmentServices,
                                c.generalOffice AS generalOffice,
                                c.resolutionNumber AS resolutionNumber,
                                DATE_FORMAT(c.resolutionDate, '%d/%m/%y %T') as resolutionDate

                                from company c
                                WHERE id=1
                                order BY c.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getEconomicActivities(userId){

    var deferred = Q.defer()

    let sql = `		SELECT      a.id AS id,
                                a.code AS code,
                                a.descriptionName AS descriptionName

                                from economicActivities a
                                order BY a.id DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getMailServers(userId){

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                                    m.id as "id",
                                    m.description as "description",
                                    m.type as "type",
                                    if(m.serverDteIs=1, 'SI','NO') AS "serverDteIs",
                                    m.port as  "port",
                                    m.serverName as  "serverName",
                                    if(m.certification=1, 'SI','NO') AS "certification",
                                    m.userName as  "userName",
                                    m.password as  "password",
                                    DATE_FORMAT(m.date, '%d/%m/%y') as date,
                                    DATE_FORMAT(m.searchDate, '%d/%m/%y %T') as searchDate,
                                    u.usName as  "userCreate"

                        FROM mailServers m
                                    left JOIN  users u
                                    ON  u.usId = m.usId
                        order BY m.id asc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

module.exports = {
    _getFoliosList: _getFoliosList,
    _getCertificates:_getCertificates,
    _getCompanyData:_getCompanyData,
    _getEconomicActivities:_getEconomicActivities,
    _getMailServers:_getMailServers,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}