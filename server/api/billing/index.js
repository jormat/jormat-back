var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/folios/list', service.getFoliosList);
router.get('/certificates/list', service.getCertificates);
router.get('/company', service.getCompanyData);
router.get('/company/activities', service.getEconomicActivities);
router.get('/mails-servers/list', service.getMailServers);

module.exports = router;