'use strict'

var model = require('./model');


function getFoliosList(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getFoliosList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCertificates(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getCertificates(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCompanyData(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getCompanyData(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getEconomicActivities(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getEconomicActivities(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getMailServers(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getMailServers(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


module.exports = {
    getFoliosList: getFoliosList,
    getCertificates:getCertificates,
    getCompanyData:getCompanyData,
    getEconomicActivities:getEconomicActivities,
    getMailServers:getMailServers,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}