'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');


function _getBanks(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT id AS bankId,
                               code,
                               dsName AS description
                FROM           banks WHERE status = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise 
}

function _getBanksDetails(bankId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT id AS bankId,
                               code,
                               dsName AS description
                FROM           banks WHERE id = :bankId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            bankId:bankId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise 
}

function _createBank(data){

    var deferred = Q.defer()
 
    let sql = `   INSERT INTO banks (dsName,code) 
                         VALUES        (:bankName,:bankCode)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            bankName:data.bankName,
            bankCode:data.bankCode

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _updateBank(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE  banks SET  code=:bankCode,dsName=:bankName 
                                       WHERE id=:bankId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            bankId:data.bankId,
            bankName:data.bankName,
            bankCode:data.bankCode

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

// client Payments
function _getPayments(){

    var deferred = Q.defer()

    let sql = `SELECT distinct invoices.invoiceId,
                               DATE_FORMAT(invoices.date, '%d/%m/%y') as date,
                               invoices.total AS total,
                               s.otherName AS "statusName",
                               s.style AS "style",
                               IFNULL(invoices.total - SUM(payments.total),invoices.total) AS pending,
                               clients.dsName as clientName,
                               clients.id as clientId,
                               warehouse.dsCode AS "origin",
                               invoices.isCredit AS isCredit,
                               IFNULL(paymentMethod.dsName, 'No hay pagos') AS "paymentForm"
                FROM           invoices 
                left  JOIN      payments 
                ON             invoices.invoiceId=payments.invoiceId
                left  JOIN      paymentMethod  
                ON             paymentMethod.paymentMethodId=payments.paymentMethodId 
                INNER JOIN     warehouse 
                ON             invoices.origin = warehouse.id
                INNER JOIN     clients 
                ON             invoices.clientId=clients.id
                left JOIN      status s
                ON             invoices.status = s.id 
                WHERE          invoices.inStatus=1 
                GROUP BY       invoices.invoiceId
                order BY       invoices.invoiceId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPaymentFormsList(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT paymentMethodId AS paymentFormId,
                               dsName AS description
                FROM           paymentmethod WHERE inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise 
}

function _getPaymentsDetails(invoiceId){

    var deferred = Q.defer()

    let sql = `SELECT    p.id AS paymentsId,
                         DATE_FORMAT(p.date, '%d/%m/%y') as date,
                         p.total,
                         p.observation AS comment,
                         u.usName AS userCreation,
                         p.docNumber AS document,
                         pm.dsName AS paymentForm
                         
                FROM payments p
                inner JOIN  users u
                ON  u.usId = p.usId
                INNER JOIN  paymentMethod  pm 
                ON pm.paymentMethodId = p.paymentMethodId 
                WHERE invoiceId = :invoiceId AND p.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createPayment(data){

    var deferred = Q.defer()
    if(data.numberDocument == undefined) data.numberDocument = 0
 
    let sql = `   INSERT INTO payments (invoiceId,clientId,total,paymentMethodId,docNumber,date,observation,usId) 
                         VALUES        (:invoiceId,:clientId,:total,:paymentFormId,:numberDocument,NOW(),:comment,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:data.invoiceId,
            clientId:data.clientId,
            total:data.total,
            paymentFormId:data.paymentFormId,
            numberDocument:data.numberDocument,
            date:data.date,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateStatusInvoice(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  invoices SET  status = 2
                   WHERE invoiceId = :invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            invoiceId:data.invoiceId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledPayment(data){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM payments where id = :paymentId;
                 UPDATE  invoices SET  status = 1 WHERE invoiceId = :invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: data.paymentId,
            invoiceId: data.invoiceId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPaymentsReport(startDate,endDate){

    var deferred = Q.defer()

    let sql = ` SELECT distinct 
                               invoices.invoiceId,
                               DATE_FORMAT(invoices.date, '%d/%m/%y') as date,
                               invoices.total AS total,
                               s.otherName AS "statusName",
                               s.style AS "style",
                               IFNULL(invoices.total - SUM(payments.total),invoices.total) AS pending,
                               clients.dsName as clientName,
                               clients.id as clientId,
                               warehouse.dsCode AS "origin",
                               IFNULL(paymentMethod.dsName, 'No hay pagos') AS "paymentForm"
                FROM           invoices 
                left JOIN      payments 
                ON             invoices.invoiceId=payments.invoiceId
                left JOIN      paymentMethod  
                ON             paymentMethod.paymentMethodId=payments.paymentMethodId 
                INNER JOIN     warehouse 
                ON             invoices.origin = warehouse.id
                INNER JOIN     clients 
                ON             invoices.clientId=clients.id
                left JOIN      status s
                ON             invoices.status = s.id 
                WHERE          invoices.date BETWEEN '`+ startDate + `' AND '`+ endDate + `' AND invoices.inStatus = 1
                GROUP BY       invoices.invoiceId
                order BY       invoices.invoiceId DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getOfficePayments(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        o.paymentId as "paymentId",
                        o.total,
                        w.dsName AS "origin",
                        DATE_FORMAT(o.date, '%d/%m/%y') as date,
                        GROUP_CONCAT(distinct opi.documentId) as "documents",
                        u.usLastName AS "userCreation",
                        s.name AS "statusName",
                        s.style AS "style"
                        
                    FROM officepayments o
                    inner JOIN officepaymentsitems opi
                    ON  o.paymentId = opi.paymentId
                    inner JOIN  warehouse w
                    ON  o.origin = w.id
                    inner JOIN  users u
                    ON  o.usId = u.usId
                    inner JOIN  status s
                    ON  o.status = s.id
                    WHERE o.inStatus = 1
                    GROUP BY o.paymentId
                    order BY o.paymentId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getOfficePaymentsDetails(paymentId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        op.paymentId as "paymentId",
                        op.total,
                        ROUND(op.total/1.19,0) as "netPrice",
                        op.total - ROUND(op.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        DATE_FORMAT(op.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userUpdate",
                        s.name AS "statusName",
                        op.commentary AS "comment"
                        
                    FROM officepayments op
                    left JOIN  warehouse w
                    ON  op.origin = w.id
                    left JOIN  users u
                    ON  op.usId = u.usId
                    left JOIN  users us
                    ON  op.usModifierId = us.usId
                    left JOIN  status s
                    ON  op.status = s.id
                    WHERE op.paymentId = :paymentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            paymentId:paymentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getOfficePaymentsItems(paymentId){

    var deferred = Q.defer()

    let sql = `SELECT  DISTINCT
                        opi.id,
                        opi.documentId AS documentId,
                        i.clientId AS clientId,
                        c.dsName AS clientName,
                        td.name AS type,
                        opi.date as date,
                        pm.dsName AS paymentForm,
                        opi.paymentForm AS paymentFormId,
                        opi.pay,
                        opi.total,
                        IFNULL(opi.total - SUM(p.total),opi.total) AS pending,
                        if(i.status=1, 1,0) AS "status",
                        opi.isClick,
                        'false' as disabled
                        
                        FROM officepaymentsitems opi
                        INNER JOIN typedocument td
                        ON  td.id = opi.type
                        INNER JOIN invoices i
                        ON  i.invoiceId = opi.documentId
                        left JOIN payments p
                        ON  i.invoiceId = p.invoiceId
                        INNER JOIN clients c
                        ON  c.id = i.clientId
                        INNER JOIN paymentmethod pm
                        ON  pm.paymentMethodId = opi.paymentForm
                        WHERE opi.paymentId = :paymentId
                        GROUP BY       opi.id
                        
                    UNION ALL
                                    
                    SELECT  DISTINCT 
                        opi.id,
                        opi.documentId AS documentId,
                        b.clientId AS clientId,
                        c.dsName AS clientName,
                        td.name AS type,
                        opi.date as date,
                        pm.dsName AS paymentForm,
                        opi.paymentForm AS paymentFormId,
                        opi.pay,
                        opi.total,
                        opi.total AS pending,
                        if(b.status=1, 1,0) AS "status",
                        opi.isClick,
                        'false' as disabled
                                
                        FROM officepaymentsitems opi
                        INNER JOIN typedocument td
                        ON  td.id = opi.type
                        INNER JOIN ballots b
                        ON  b.ballotId = opi.documentId
                        INNER JOIN clients c
                        ON  c.id = b.clientId
                        INNER JOIN paymentmethod pm
                        ON  pm.paymentMethodId = opi.paymentForm
                        WHERE opi.paymentId = :paymentId
                        GROUP BY    opi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            paymentId:paymentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _getSalesByWarehouse(warehouseId){

    var deferred = Q.defer()

    let sql = `SELECT  documentId,clientName,total,totalDocument,date,statusName,typeId,type 
                FROM (SELECT DISTINCT
                                    i.invoiceId as "documentId",
                                    c.dsName AS "clientName",
                                    i.total as totalDocument,
                                    IFNULL(i.total - SUM(p.total),i.total) AS total,
                                    DATE_FORMAT(i.date, '%d/%m/%y') as date,
                                    s.otherName AS "statusName",
                                    1 AS "typeId",
                                    'Factura' AS type 
                                        
                                    FROM invoices i
                                    left JOIN payments p
                                    ON  i.invoiceId = p.invoiceId
                                    inner JOIN  clients c
                                    ON  i.clientId = c.id
                                    left JOIN  status s
                                    ON  i.status = s.id
                                    WHERE i.inStatus = 1 AND origin = :warehouseId AND status != 2
                                    GROUP BY       i.invoiceId
                                    

                UNION ALL

                SELECT DISTINCT
                                    b.ballotId as "documentId",
                                    c.dsName AS "clientName",
                                    b.total as totalDocument,
                                    b.total AS total,
                                    DATE_FORMAT(b.date, '%d/%m/%y') as date,
                                    if(b.status=1, 'Sin Pagar','Pagado') AS "statusName",
                                    6 AS "typeId",
                                    'Boleta' AS type 
                                        
                                    FROM ballots b
                                    inner JOIN  clients c
                                    ON  b.clientId = c.id
                                    WHERE b.inStatus = 1 AND origin = :warehouseId AND status = 1
                                    )consulta
                 order BY date DESC  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _paymentOfficesCreate(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"

    let sql = `INSERT INTO officepayments(date,status,total,inStatus,origin,commentary,usId)
                      VALUES (NOW(),1,:total,1,:originId,:observation,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        replacements:{
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            observation:data.comment
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _paymentOfficesItemsCreate(items,paymentId){

    var deferred = Q.defer()
    let sql = `   INSERT INTO officepaymentsitems (paymentId,
                                            documentId, 
                                            pay,
                                            total,
                                            type,
                                            date,
                                            paymentForm) 
                                    VALUES (:paymentId,
                                            :documentId,
                                            :pay,
                                            :totalDocument,
                                            :typeId,
                                            :date,
                                            :paymentMethod)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            paymentId:paymentId,
            documentId:items.documentId,
            totalDocument:items.totalDocument,
            pay:items.pay,
            date:items.date,
            typeId:items.typeId,
            paymentMethod:parseInt(items.paymentMethod)
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _statusClickUpdate(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE officepaymentsitems SET isClick=1, usModifierId =:userId WHERE id=:paymentItemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            paymentItemId:data.paymentItemId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _paymentOfficesUpdate(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE officepayments SET status=:status, usModifierId =:userId WHERE paymentId=:officesPaymentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            status:data.statusValue,
            userId:data.userId,
            officesPaymentId:data.officesPaymentId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _ballotUpdate(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  ballots SET  status = 2,
                   usModifierId = :userId,
                   paymentFormId = :paymentMethodId,
                   datePayment = NOW()
                   WHERE ballotId = :ballotId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            ballotId:data.ballotId,
            paymentMethodId:data.paymentMethodId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getNotePayments(noteId){

    var deferred = Q.defer()

    let sql = `SELECT    p.id AS paymentId,
                         DATE_FORMAT(p.date, '%d/%m/%y') as date,
                         p.total,
                         p.observation AS comment,
                         u.usName AS userCreation,
                         p.docNumber AS document
                         
                FROM paymentsnotes p
                inner JOIN  users u
                ON  u.usId = p.usId
                WHERE noteId = :noteId AND p.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            noteId:noteId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _updateStatusNotes(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  creditnotes SET  status = 1,
                   usModifierId =:userId
                   WHERE noteId = :noteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            noteId:data.noteId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createNotesPayment(data){

    var deferred = Q.defer()
 
    let sql = `   INSERT INTO paymentsnotes (noteId,total,paymentMethodId,docNumber,date,observation,usId) 
                         VALUES        (:noteId,:total,10,:numberDocument,NOW(),:comment,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            noteId:data.noteId,
            total:data.total,
            numberDocument:data.numberDocument,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledNotesPayment(data){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM paymentsnotes where id = :paymentId;
                 UPDATE  creditnotes SET  status = 0,
                   usModifierId =:userId
                   WHERE noteId = :noteId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: data.paymentId,
            userId: data.userId,
            noteId: data.noteId
        }

    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getChecks(){

    var deferred = Q.defer()

    let sql = `SELECT    c.id AS checkId,
                         DATE_FORMAT(c.date, '%d/%m/%y') as date, 
                         w.dsCode AS origin,
                         c.serialNumber AS serialNumber,
                         c.account AS account,
                         c.associatedName AS associatedName,
                         DATE_FORMAT(c.depositDate, '%d/%m/%y') as depositDate,
                         DATE_FORMAT(c.day, '%d/%m/%y') as day,
                         c.shareDetails AS shareDetails,
                         c.shareValue AS shareValue,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
                         cl.dsName AS clientassociated,
                         GROUP_CONCAT(distinct i.invoiceId) as "invoices",
                         s.code AS statusName,
                         s.style AS styleStatus,
                         b.code AS bankName
                         
                FROM checks c
                left JOIN  warehouse w
                ON  c.origin = w.id
                left JOIN  banks b
                ON  c.bankId = b.id
                left JOIN  users u
                ON  c.usId = u.usId
                left JOIN  users us
                ON  c.usModifierId = us.usId
                left JOIN  clients cl
                ON  cl.id = c.clientId
                left JOIN  status s
                ON  c.statusId = s.id
                inner JOIN  invoicescheck i
                ON          i.checkId = c.id
                GROUP BY    c.id
                order BY c.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getCheckDetails(checkId){

    var deferred = Q.defer()

    let sql = `SELECT    c.id AS checkId,
                         DATE_FORMAT(c.date, '%d/%m/%y') as date,
                         DATE_FORMAT(c.day, '%d/%m/%y') as day,
                         w.branchOffice AS origin,
                         c.serialNumber AS serialNumber,
                         c.account AS account,
                         c.associatedName AS associatedName,
                         DATE_FORMAT(c.depositDate, '%d/%m/%y') as depositDate,
                         DATE_FORMAT(c.day, '%d/%m/%y') as day,
                         c.shareDetails AS shareDetails,
                         c.shareValue AS shareValue,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
                         cl.dsName AS associatedClient,
                         GROUP_CONCAT(distinct i.invoiceId) as "invoices",
                         s.maleName AS statusName,
                         s.style AS styleStatus,
                         c.phone AS phone,
                         b.dsName AS bankName
                         
                FROM checks c
                left JOIN  warehouse w
                ON  c.origin = w.id
                left JOIN  banks b
                ON  c.bankId = b.id
                left JOIN  users u
                ON  c.usId = u.usId
                left JOIN  users us
                ON  c.usModifierId = us.usId
                left JOIN  clients cl
                ON  cl.id = c.clientId
                left JOIN  status s
                ON  c.statusId = s.id
                INNER JOIN  invoicescheck i
                ON          i.checkId = c.id
                WHERE i.checkId = :checkId
                GROUP BY    c.id
                order BY c.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            checkId:checkId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _checkCreate(data){

    var deferred = Q.defer()

    if(data.phone==undefined) data.phone='';
    if(data.shareDetails == undefined) data.shareDetails = ''
    if(data.depositDate==undefined) data.depositDate='';
    if(data.clientId == undefined) data.clientId = 4934
 
    let sql = `   INSERT INTO checks (serialNumber,account,associatedName,clientId,day,date,origin,usId,shareValue,shareDetails,bankId,phone,depositDate) 
                         VALUES      (:serialNumber,:account,:associatedName,:clientId,:day,NOW(),:origin,:userId,:shareValue,:shareDetails,:bankId,:phone,:depositDate)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            serialNumber:data.serialNumber,
            account:data.account,
            associatedName:data.associatedName,
            clientId:data.clientId,
            origin:data.origin,
            userId:data.userId,
            shareValue:data.shareValue,
            shareDetails:data.shareDetails,
            bankId:data.bankId,
            day:data.day,
            phone:data.phone,
            depositDate:data.depositDate

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _invoicesChecksAssociated (invoiceId,checkId){

    var deferred = Q.defer()
 
    let sql = ` INSERT INTO invoicescheck (invoiceId,checkId,createdAt) VALUES (:invoiceId,:checkId,NOW())`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:invoiceId,
            checkId:checkId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateStatusChecks(data){

    var deferred = Q.defer()

    let intoDay = ''
    if (data.day !=''){
        intoDay = 'depositDate = :day,'
    }
 
    let sql = `  UPDATE  checks SET  statusId = :value,
                   `+ intoDay +`
                   usModifierId =:userId
                   WHERE id = :checkId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            checkId:data.checkId,
            userId:data.userId,
            value:data.value,
            day:data.day

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _checkUpdate(data){

    var deferred = Q.defer()

    if(data.phone==undefined) data.phone='';
    if(data.shareDetails == undefined) data.shareDetails = ''

    let sql = `  DELETE FROM invoicescheck WHERE checkId = :checkId;

                 UPDATE  checks SET  associatedName = :associatedName,
                   serialNumber = :serialNumber,
                   shareValue = :shareValue,
                   account = :account,
                   phone = :phone,
                   shareDetails = :shareDetails,
                   usModifierId =:userId
                   WHERE id = :checkId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            checkId:data.checkId,
            userId:data.userId,
            associatedName:data.associatedName,
            serialNumber:data.serialNumber,
            shareValue:data.shareValue,
            account:data.account,
            phone:data.phone,
            shareDetails:data.shareDetails

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _deleteInvoicesChecks(checkId){


    var deferred = Q.defer()
    let sql = `   DELETE FROM invoicescheck WHERE checkId = :checkId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            checkId:checkId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getHistoricChecks(startDate,endDate){

    var deferred = Q.defer()
    
    let sql = `SELECT    c.id AS checkId,
                         c.date as date, 
                         w.dsCode AS origin,
                         c.serialNumber AS serialNumber,
                         c.account AS account,
                         c.associatedName AS associatedName,
                         DATE_FORMAT(c.depositDate, '%d/%m/%y') as depositDate,
                         DATE_FORMAT(c.day, '%d/%m/%y') as day,
                         c.shareDetails AS shareDetails,
                         c.shareValue AS shareValue,
                         u.usName AS userCreation,
                         us.usName AS userUpdate,
                         cl.dsName AS clientassociated,
                         GROUP_CONCAT(distinct i.invoiceId) as "invoices",
                         s.code AS statusName,
                         s.style AS styleStatus,
                         b.code AS bankName
                         
                FROM checks c
                left JOIN  warehouse w
                ON  c.origin = w.id
                left JOIN  banks b
                ON  c.bankId = b.id
                left JOIN  users u
                ON  c.usId = u.usId
                left JOIN  users us
                ON  c.usModifierId = us.usId
                left JOIN  clients cl
                ON  cl.id = c.clientId
                left JOIN  status s
                ON  c.statusId = s.id
                INNER JOIN  invoicescheck i
                ON          i.checkId = c.id
                 WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                GROUP BY    c.id
                order BY c.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getNnPayments(documentId){

    var deferred = Q.defer()

    let sql = `SELECT    p.id AS paymentId,
                         DATE_FORMAT(p.date, '%d/%m/%y') as date,
                         p.total,
                         p.observation AS comment,
                         u.usName AS userCreation,
                         pm.dsName AS paymentForm,
                         p.docNumber AS document
                         
                FROM paymentsnn p
                inner JOIN  users u
                ON  u.usId = p.usId
                inner JOIN  paymentMethod pm 
                ON pm.paymentMethodId=p.paymentMethodId 
                WHERE documentId = :documentId AND p.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            documentId:documentId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}


function _createNnPayment(data){

    var deferred = Q.defer()
 
    let sql = `   INSERT INTO paymentsnn (documentId,total,paymentMethodId,docNumber,date,observation,usId) 
                         VALUES        (:documentId,:total,:paymentFormId,:numberDocument,NOW(),:comment,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            documentId:data.documentId,
            total:data.total,
            numberDocument:data.numberDocument,
            paymentFormId:data.paymentFormId,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _disabledNnPayment(data){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM paymentsnn where id = :paymentId;
                 UPDATE  documentsnn SET  status = 0,
                   usModifierId =:userId
                   WHERE documentId = :documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: data.paymentId,
            userId: data.userId,
            documentId: data.documentId
        }

    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _updateNnStatus(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  documentsnn SET  status = 1,
                   usModifierId =:userId
                   WHERE documentId = :documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            documentId:data.documentId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getBallotPayments(ballotId){

    var deferred = Q.defer()

    let sql = `SELECT    p.id AS paymentId,
                         DATE_FORMAT(p.date, '%d/%m/%y') as date,
                         p.total,
                         p.observation AS comment,
                         u.usName AS userCreation,
                         pm.dsName AS paymentForm,
                         p.docNumber AS document
                         
                FROM paymentsballots p
                inner JOIN  users u
                ON  u.usId = p.usId
                inner JOIN  paymentMethod pm 
                ON pm.paymentMethodId=p.paymentMethodId 
                WHERE ballotId = :ballotId AND p.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            ballotId:ballotId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createBallotPayment(data){

    var deferred = Q.defer()
 
    let sql = `   INSERT INTO paymentsballots (ballotId,total,paymentMethodId,docNumber,date,observation,usId) 
                         VALUES        (:ballotId,:total,:paymentFormId,:numberDocument,NOW(),:comment,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            ballotId:data.ballotId,
            total:data.total,
            numberDocument:data.numberDocument,
            paymentFormId:data.paymentFormId,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateBallotStatus(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  ballots SET  status = 2,
                   usModifierId =:userId
                   WHERE ballotId = :ballotId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            ballotId:data.ballotId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledBallotPayment(data){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM paymentsballots where id = :paymentId;
                 UPDATE  ballots SET  status = 1,
                   usModifierId =:userId
                   WHERE ballotId = :ballotId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: data.paymentId,
            userId: data.userId,
            ballotId: data.ballotId
        }

    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getProvidersPayments(invoiceId){

    var deferred = Q.defer()

    let sql = `SELECT    p.id AS paymentId,
                         DATE_FORMAT(p.date, '%d/%m/%y') as date,
                         p.total,
                         p.observation AS comment,
                         u.usName AS userCreation,
                         pm.dsName AS paymentForm,
                         p.docNumber AS document
                         
                FROM paymentsproviders p
                inner JOIN  users u
                ON  u.usId = p.usId
                inner JOIN  paymentmethod pm 
                ON pm.paymentMethodId=p.paymentMethodId 
                WHERE invoiceId = :invoiceId AND p.inStatus = 1  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            invoiceId:invoiceId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _disabledProviderPayment(data){

    var deferred = Q.defer()
 
    let sql = `  DELETE FROM paymentsproviders where id = :paymentId;
                 UPDATE  providerinvoices SET  status = 0,
                   usModifierId =:userId
                   WHERE id = :invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            paymentId: data.paymentId,
            userId: data.userId,
            invoiceId: data.invoiceId
        }

    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createProviderPayment(data){

    var deferred = Q.defer()

    if(data.numberDocument==undefined) data.numberDocument='s/n'
 
    let sql = `   INSERT INTO paymentsproviders (invoiceId,total,paymentMethodId,docNumber,date,observation,usId) 
                         VALUES        (:invoiceId,:total,:paymentFormId,:numberDocument,NOW(),:comment,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            invoiceId:data.invoiceId,
            total:data.total,
            numberDocument:data.numberDocument,
            paymentFormId:data.paymentFormId,
            comment:data.comment,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateProviderInvoiceStatus(data){

    var deferred = Q.defer()
 
    let sql = `  UPDATE  providerinvoices SET  status = 1,
                   usModifierId =:userId
                   WHERE id = :invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            invoiceId:data.invoiceId,
            userId:data.userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPaymentsPages(clientName, filters, offset, limit, count){
	let whereCount = ` 	WHERE i.inStatus = 1 `
	let where = ` 	WHERE i.inStatus = 1 `
	let sql = ``
	const conditionLimit = limit ? `LIMIT :limit OFFSET :offset` : ''
	// let conditionRut = (typeof dsCode !== 'undefined' && typeof dsCode !== '') ? ` AND c.dsCode = '${dsCode}' ` : ``
	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'invoiceId':
					where += "	AND i.invoiceId like '%" + filter[i] + "%'"
					whereCount += "	AND i.invoiceId like '%" + filter[i] + "%'"
					break
				case 'clientName':
					where += "	AND c.dsName like '%" + filter[i] + "%'"
					whereCount += "	AND c.dsName like '%" + filter[i] + "%'"
					break
				case 'clientId':
					where += "	AND c.id like '%" + filter[i] + "%'"
					whereCount += "	AND c.id like '%" + filter[i] + "%'"
					break
				case 'total':
					where += "	AND i.total like '%" + filter[i] + "%'"
					whereCount += "	AND i.total like '%" + filter[i] + "%'"
					break
				case 'origin':
					where += "	AND w.dsCode like '%" + filter[i] + "%'"
					whereCount += "	AND w.dsCode like '%" + filter[i] + "%'"
					break
				case 'date':
					where += "	AND DATE_FORMAT(i.date, '%d/%m/%y') like '%" + filter[i] + "%'"
					whereCount += "	AND DATE_FORMAT(i.date, '%d/%m/%y') like '%" + filter[i] + "%'"
					break
				case 'statusName':
					where += "	AND s.otherName like '%" + filter[i] + "%'"
					whereCount += "	AND s.otherName like '%" + filter[i] + "%'"
					break
                case 'paymentForm':
                    where+= " AND pm.dsName like '%" + filter[i] + "%'"
                    whereCount += " AND pm.dsName like '%" + filter[i] + "%'"
			}
		}
	}

	if (clientName){
		where += "	AND c.dsName like '%" + clientName + "%'"
		whereCount += "	AND c.dsName like '%" + clientName + "%'"
	}
   

	if (!count) {
		sql += `	SELECT distinct i.invoiceId,
                                DATE_FORMAT(i.date, '%d/%m/%y') as date,
                                i.total AS total,
                                s.otherName AS "statusName",
                                s.style AS "style",
                                IFNULL(i.total - SUM(p.total),i.total) AS pending,
                                c.dsName as clientName,
                                c.id as clientId,
                                p.paymentMethodId as paymentForm,
                                c.dsCode as rut,
                                w.dsCode AS "origin",
                                i.isCredit AS isCredit,
                                IFNULL(pm.dsName, 'No hay pagos') AS "paymentForm"
                        FROM           invoices i 
                        left  JOIN      payments p
                        ON             i.invoiceId=p.invoiceId
                        left  JOIN      paymentMethod pm
                        ON             pm.paymentMethodId=p.paymentMethodId 
                        INNER JOIN     warehouse w
                        ON             i.origin = w.id
                        INNER JOIN     clients c 
                        ON             i.clientId=c.id
                        left JOIN      status s
                        ON             i.status = s.id 
					${where}
					GROUP BY       i.invoiceId
                order BY       i.invoiceId DESC
					${conditionLimit} `
	} else {
		sql += `	SELECT 	COUNT(DISTINCT i.invoiceId) AS total
                        FROM           invoices i 
                        left  JOIN      payments p
                        ON             i.invoiceId=p.invoiceId
                        left  JOIN      paymentMethod pm
                        ON             pm.paymentMethodId=p.paymentMethodId 
                        INNER JOIN     warehouse w
                        ON             i.origin = w.id
                        INNER JOIN     clients c 
                        ON             i.clientId=c.id
                        left JOIN      status s
                        ON             i.status = s.id 
					${where} `
	}

	return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		},
	})
}





module.exports = {
    _getBanks:_getBanks,
    _getBanksDetails:_getBanksDetails,
    _createBank:_createBank,
    _updateBank:_updateBank,
    _getPayments: _getPayments,
    _getPaymentsPages:_getPaymentsPages,
    _getPaymentFormsList:_getPaymentFormsList,
    _getPaymentsDetails:_getPaymentsDetails,
    _createPayment:_createPayment,
    _updateStatusInvoice:_updateStatusInvoice,
    _disabledPayment:_disabledPayment,
    _getPaymentsReport:_getPaymentsReport,
    _getOfficePayments:_getOfficePayments,
    _getOfficePaymentsDetails:_getOfficePaymentsDetails,
    _getOfficePaymentsItems:_getOfficePaymentsItems,
    _getSalesByWarehouse:_getSalesByWarehouse,
    _paymentOfficesCreate:_paymentOfficesCreate,
    _paymentOfficesItemsCreate:_paymentOfficesItemsCreate,
    _statusClickUpdate:_statusClickUpdate,
    _paymentOfficesUpdate:_paymentOfficesUpdate,
    _ballotUpdate:_ballotUpdate,
    _getNotePayments:_getNotePayments,
    _updateStatusNotes:_updateStatusNotes,
    _createNotesPayment:_createNotesPayment,
    _disabledNotesPayment:_disabledNotesPayment,
    _getChecks:_getChecks,
    _getCheckDetails:_getCheckDetails,
    _checkCreate:_checkCreate,
    _invoicesChecksAssociated:_invoicesChecksAssociated,
    _updateStatusChecks:_updateStatusChecks,
    _checkUpdate:_checkUpdate,
    _deleteInvoicesChecks:_deleteInvoicesChecks,
    _getHistoricChecks:_getHistoricChecks,
    _getNnPayments:_getNnPayments,
    _createNnPayment:_createNnPayment,
    _disabledNnPayment:_disabledNnPayment,
    _updateNnStatus:_updateNnStatus,
    _getBallotPayments:_getBallotPayments,
    _createBallotPayment:_createBallotPayment,
    _updateBallotStatus:_updateBallotStatus,
    _disabledBallotPayment:_disabledBallotPayment,
    _getProvidersPayments:_getProvidersPayments,
    _disabledProviderPayment:_disabledProviderPayment,
    _createProviderPayment:_createProviderPayment,
    _updateProviderInvoiceStatus:_updateProviderInvoiceStatus,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}