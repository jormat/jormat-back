var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

//banks
router.get('/banks/list', service.getBanks); 
router.get('/banks', service.getBanksDetails);
router.post('/banks', service.createBank);
router.put('/banks', service.updateBank);

//client Payments 
router.get('/list', service.getPayments);
router.get('/pagination', service.getPaymentsPages);
router.get('/', service.getPaymentsDetails);
router.post('/', service.createPayment);
router.put('/', service.updateStatusInvoice);
router.put('/ballot', service.ballotUpdate);
router.put('/delete', service.disabledPayment);
router.get('/forms/list', service.getPaymentFormsList);
router.get('/report', service.getPaymentsReport);
router.get('/notes', service.getNotePayments);
router.put('/notes', service.updateStatusNotes);
router.post('/notes', service.createNotesPayment);
router.put('/notes/delete', service.disabledNotesPayment);
router.get('/nn', service.getNnPayments);
router.post('/nn', service.createNnPayment);
router.put('/nn/delete', service.disabledNnPayment);
router.put('/nn', service.updateNnStatus);
router.get('/ballot', service.getBallotPayments);
router.post('/ballot', service.createBallotPayment);
router.put('/ballot-status', service.updateBallotStatus);
router.put('/ballot/delete', service.disabledBallotPayment);

router.post('/providers', service.createProviderPayment);
router.get('/providers', service.getProvidersPayments);
router.put('/providers/delete', service.disabledProviderPayment);
router.put('/providers-invoices-status', service.updateProviderInvoiceStatus);

//office Payments 
router.get('/offices/list', service.getOfficePayments);
router.get('/offices', service.getOfficePaymentsDetails);
router.post('/offices', service.paymentOfficesCreate);
router.put('/offices/validate', service.paymentOfficesUpdate);
router.get('/offices/documents', service.getOfficePaymentsItems);
router.put('/offices/documents-click', service.statusClickUpdate);
router.get('/offices/sales', service.getSalesByWarehouse);

//checks
router.get('/checks/list', service.getChecks);
router.get('/checks', service.getChecksDetails);
router.post('/checks', service.checkCreate);
router.put('/checks', service.checkUpdate);
router.put('/checks/status', service.updateStatusChecks); 
router.get('/checks/historic', service.getHistoricChecks);




module.exports = router;