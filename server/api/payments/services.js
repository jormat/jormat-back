'use strict'

var model = require('./model');

//Payments clients

function getBanks(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getBanks(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBanksDetails(req, res) {

    var query = [];
    var bankId = req.query['bankId'];
    query.push(model._getBanksDetails(bankId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createBank(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createBank(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the bank is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateBank(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateBank(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the bank is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPayments(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getPayments(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPaymentFormsList(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getPaymentFormsList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPaymentsDetails(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._getPaymentsDetails(invoiceId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the payment is associated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateStatusInvoice(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateStatusInvoice(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the invoice is payment succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._disabledPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPaymentsReport(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getPaymentsReport(startDate,endDate))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOfficePayments(req, res) {

    var query = [];
    query.push(model._getOfficePayments())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOfficePaymentsDetails(req, res) {

    var query = [];
    var paymentId = req.query['paymentId'];
    query.push(model._getOfficePaymentsDetails(paymentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOfficePaymentsItems(req, res) {

    var query = [];
    var paymentId = req.query['paymentId'];
    query.push(model._getOfficePaymentsItems(paymentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSalesByWarehouse(req, res) {

    var query = [];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getSalesByWarehouse(warehouseId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function paymentOfficesCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._paymentOfficesCreate(data)]).then((result) => {
            query=[]
            for(var i in data.paymentsItems){
                query.push(model._paymentOfficesItemsCreate(data.paymentsItems[i],result[0]))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services createInvoiceProvider",
            status: 0
        });
    }
}

function paymentOfficesUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._paymentOfficesUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function statusClickUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._statusClickUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function ballotUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._ballotUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the ballot is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNotePayments(req, res) {

    var query = [];
    var noteId = req.query['noteId'];
    query.push(model._getNotePayments(noteId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateStatusNotes(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateStatusNotes(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the note is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createNotesPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createNotesPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the notes payment is associated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledNotesPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._disabledNotesPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the notes payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getChecks(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getChecks())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getChecksDetails(req, res) {

    var query = [];
    var checkId = req.query['checkId'];
    query.push(model._getCheckDetails(checkId))

    Promise.all(query).then((result) => {
        var objArray=[]
        var temp={};
        var arr = []

        for(var i in result[0]){
            var obj={}
            obj=result[0][i]
            
            var temp= {
                "invoices": obj.invoices.split(',')
             }
            
             obj.invoices=temp['invoices']

             // delete obj.invoices

             objArray.push(obj)
        }
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function checkCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._checkCreate(data)]).then((result) => {
            query=[]
            for(var  i in data.invoices){
                query.push(model._invoicesChecksAssociated(data.invoices[i].title,result[0]))
                }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"error in services checks create",
            status: 0
        });
    }
}


function updateStatusChecks(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateStatusChecks(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the checks is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function checkUpdate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._checkUpdate(data)]).then((result) => {
            query=[]
            // query.push(model._deleteInvoicesChecks(data.checkId))
            for(var  i in data.invoices){
              query.push(model._invoicesChecksAssociated(data.invoices[i],data.checkId))
            }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"error in services checks update",
            status: 0
        });
    }
}

function getHistoricChecks(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getHistoricChecks(startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNnPayments(req, res) {

    var query = [];
    var documentId = req.query['documentId'];
    query.push(model._getNnPayments(documentId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createNnPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createNnPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the nn payment is associated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledNnPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._disabledNnPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the nn payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateNnStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateNnStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the nn is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBallotPayments(req, res) {

    var query = [];
    var ballotId = req.query['ballotId'];
    query.push(model._getBallotPayments(ballotId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createBallotPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createBallotPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the ballot payment is associated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledBallotPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._disabledBallotPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the ballot payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateBallotStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateBallotStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the ballot is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getProvidersPayments(req, res) {

    var query = [];
    var invoiceId = req.query['invoiceId'];
    query.push(model._getProvidersPayments(invoiceId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledProviderPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._disabledProviderPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the provider payment is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createProviderPayment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createProviderPayment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the provider payment is associated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateProviderInvoiceStatus(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateProviderInvoiceStatus(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "the invoice is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getPaymentsPages(req, res) {
    console.log("paramss", req.query['newPage'],req.query['newPage'])
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = req.query['pageSize'] ? parseInt(req.query['pageSize']) : false
    const clientName = req.query.searchText
	const {
		filters = {},
	} = req.query

	const promises = [
		model._getPaymentsPages(clientName,filters, offset, limit, false),
		model._getPaymentsPages(clientName,filters, offset, limit, true)
	]

	Promise.all(promises).then((result) => {
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}

module.exports = {
    getBanks:getBanks,
    getBanksDetails:getBanksDetails,
    createBank:createBank,
    updateBank:updateBank,
    getPayments: getPayments,
    getPaymentsDetails:getPaymentsDetails,
    createPayment:createPayment,
    updateStatusInvoice:updateStatusInvoice,
    disabledPayment:disabledPayment,
    getPaymentFormsList:getPaymentFormsList,
    getPaymentsReport:getPaymentsReport,
    getOfficePayments:getOfficePayments,
    getOfficePaymentsDetails:getOfficePaymentsDetails,
    getOfficePaymentsItems:getOfficePaymentsItems,
    getSalesByWarehouse:getSalesByWarehouse,
    paymentOfficesCreate:paymentOfficesCreate,
    paymentOfficesUpdate:paymentOfficesUpdate,
    statusClickUpdate:statusClickUpdate,
    ballotUpdate:ballotUpdate,
    getNotePayments:getNotePayments,
    updateStatusNotes:updateStatusNotes,
    createNotesPayment:createNotesPayment,
    disabledNotesPayment:disabledNotesPayment,
    getChecks:getChecks,
    getChecksDetails:getChecksDetails,
    checkCreate:checkCreate,
    updateStatusChecks:updateStatusChecks,
    checkUpdate:checkUpdate,
    getHistoricChecks:getHistoricChecks,
    getNnPayments:getNnPayments,
    createNnPayment:createNnPayment,
    disabledNnPayment:disabledNnPayment,
    updateNnStatus:updateNnStatus,
    getBallotPayments:getBallotPayments,
    createBallotPayment:createBallotPayment,
    updateBallotStatus:updateBallotStatus,
    disabledBallotPayment:disabledBallotPayment,
    getProvidersPayments:getProvidersPayments,
    disabledProviderPayment:disabledProviderPayment,
    createProviderPayment:createProviderPayment,
    updateProviderInvoiceStatus:updateProviderInvoiceStatus,
    getPaymentsPages:getPaymentsPages,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}