'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//Reports
function _getItemsSold(){
    var deferred = Q.defer()
    let sql = `SELECT   distinct STB.*
                            FROM (SELECT

                                @i := @i + 1 AS id,
                                it.id AS itemId,
                                it.dsName AS itemDescription,
                                i.clientId AS clientId,
                                'FACT' AS documentType,
                                i.invoiceId AS documentId,
                                DATE_FORMAT(i.date, '%d/%m/%y') as date,
                                i.date AS dateDocument,
                                i.origin AS "origin",
                                w.dsCode AS "dsCode",
                                ii.nmUnities AS "quantityItems",
                                ii.nmPrice AS price



                                FROM invoices i
                                INNER JOIN invoicesitems ii
                                ON  i.invoiceId = ii.invoiceId
                                INNER JOIN items it
                                ON  ii.itemId = it.id
                                left JOIN  warehouse w
                                ON  i.origin = w.id
                                cross join (select @i := 0) r
                                where i.inStatus =  1
                                GROUP BY    ii.id

                            UNION ALL

                            SELECT

                                @i := @i + 1 AS id,
                                it.id AS itemId,
                                it.dsName AS itemDescription,
                                i.clientId AS clientId,
                                'BOL' AS documentType,
                                i.ballotId AS documentId,
                                DATE_FORMAT(i.date, '%d/%m/%y') as date,
                                i.date AS dateDocument,
                                i.origin AS "origin",
                                w.dsCode AS "dsCode",
                                ii.nmUnities AS "quantityItems",
                                ii.nmPrice AS price

                                FROM ballots i
                                INNER JOIN ballotsitems ii
                                ON  i.ballotId = ii.ballotId
                                INNER JOIN items it
                                ON  ii.itemId = it.id
                                left JOIN  warehouse w
                                ON  i.origin = w.id
                                cross join (select @i := 0) r
                                where i.inStatus =  1
                                GROUP BY    ii.id

                            UNION ALL

                            SELECT

                                @i := @i + 1 AS id,
                                it.id AS itemId,
                                it.dsName AS itemDescription,
                                i.clientId AS clientId,
                                'NN' AS documentType,
                                i.documentId AS documentId,
                                DATE_FORMAT(i.date, '%d/%m/%y') as date,
                                i.date AS dateDocument,
                                i.origin AS "origin",
                                w.dsCode AS "dsCode",
                                ii.nmUnities AS "quantityItems",
                                ii.nmPrice AS price

                                FROM documentsnn i
                                INNER JOIN documentsnnitems ii
                                ON  i.documentId = ii.documentId
                                INNER JOIN items it
                                ON  ii.itemId = it.id
                                left JOIN  warehouse w
                                ON  i.origin = w.id
                                cross join (select @i := 0) r
                                where i.inStatus =  1
                                GROUP BY    ii.id

                            )STB
                                GROUP BY    STB.id
                                order BY dateDocument DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getItemsSolMain(filters, offset, limit, searchText){
	let where = ` WHERE i.inStatus =  1 `
	let whereItems = ` 	WHERE 1=1 `
	let whereClients = ` WHERE 1=1 `
	let sql = ``
	let itemsFilter = false
	let clientsFilter = false
	let documentType = null

	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'itemId':
					itemsFilter = true
					whereItems += "	AND  id LIKE '%" + filter[i] + "%' "
					break
				case 'itemDescription':
					itemsFilter = true
					whereItems += "	AND  dsname LIKE '%" + filter[i] + "%' "
					break
				case 'referencias':
					itemsFilter = true
					whereItems += "	AND  id IN ( SELECT itemId FROM   itemreferences WHERE dsreference LIKE '%" + filter[i] + "%' ) "
					break
				case 'clientName':
					clientsFilter = true
					whereClients += " AND dsname like '%" + filter[i] + "%'"
					break
				case 'clientId':
					clientsFilter = true
					whereClients += " AND id like '%" + filter[i] + "%'"
					break
				case 'documentId':
					where += "	AND invoiceId LIKE '%" + filter[i] + "%'"
					break
				case 'documentType':
					documentType = filter[i]
					break
				case 'date':
					where += "	AND DATE_FORMAT(i.date, '%d/%m/%y') LIKE '%" + filter[i] + "%' "
					break
				case 'dsCode':
					where += "	AND i.origin IN ( SELECT id FROM  warehouse WHERE dsCode LIKE '%" + filter[i] + "%' ) "
					break

			}
		}

		if (itemsFilter) {
			where +=  `	AND ii.itemId IN (SELECT id FROM items ${whereItems} ) `
		}

		if (clientsFilter) {
			where +=  `	AND c.id IN (SELECT id FROM clients ${whereClients} ) `
		}
	}

	sql += ` 		WITH cte AS (
					SELECT 			DISTINCT ii.id,
									'FACT' AS documentType
					FROM 			invoices i
					INNER JOIN  invoicesitems ii
					ON 			i.invoiceId = ii.invoiceId
					INNER JOIN  items it
					ON 			ii.itemId = it.id
					INNER JOIN  users u
					ON 			i.usId= u.usId
					INNER JOIN  clients c
					ON 			i.clientId=c.id
					INNER JOIN  itemwarehouse ITW
					ON 			ITW.itemId = it.id
					${where}
					UNION ALL
					SELECT 		DISTINCT  ii.id,'BOL' AS documentType
					FROM  		ballots i
					INNER JOIN ballotsitems ii
					ON  		i.ballotId = ii.ballotId
					INNER JOIN items it
					ON  		ii.itemId = it.id
					INNER JOIN users u
					ON  		i.usId= u.usId
					INNER JOIN clients c
					ON  		i.clientId=c.id
					INNER JOIN  itemwarehouse ITW
					ON  		ITW.itemId = it.id
					${where}
					UNION ALL
					SELECT        DISTINCT ii.id,
									'NN' AS documentType
					FROM          documentsnn i
					INNER JOIN documentsnnitems ii
					ON              i.documentId = ii.documentId
					INNER JOIN items it
					ON              ii.itemId = it.id
					INNER JOIN users u
					ON              i.usId= u.usId
					INNER JOIN clients c
					ON              i.clientId=c.id
					INNER JOIN  itemwarehouse ITW
					ON               ITW.itemId = it.id
					${where}
					ORDER BY    id DESC
					)
					SELECT  SQL_CALC_FOUND_ROWS
									id,documentType,
									CASE   documentType
															WHEN 'FACT'
											THEN (  SELECT        JSON_OBJECT  ("itemId", it.id ,
																		"itemDescription", it.dsName,
																		"referencias",GROUP_CONCAT(DISTINCT ITR.dsReference),
																		"locations", GROUP_CONCAT(DISTINCT L.dsName) ,
																		"clientId", i.clientId ,
																		"clientName", c.dsName,
																		"documentId",i.invoiceId,
																		"warehouseStock",IW.itemQty,
																		"date",DATE_FORMAT(i.date, '%d/%m/%y'),
																		"dateDocument",i.date,
																		"origin",i.origin,
																		"dsCode",w.dsCode,
																		"quantityItems",ii.nmUnities,
																		"price",ii.nmPrice
																		)
													FROM           invoices i
													INNER JOIN  invoicesitems ii
													ON               i.invoiceId = ii.invoiceId
													INNER JOIN  items it
													ON               ii.itemId = it.id
													LEFT JOIN     itemreferences ITR
													ON               ITR.itemId = it.id
													LEFT JOIN     warehouse w
													ON               i.origin = w.id
													INNER JOIN  users u
													ON               i.usId= u.usId
													INNER JOIN  clients c
													ON               i.clientId=c.id
													INNER JOIN  itemwarehouselocation IWL
													ON               IWL.itemId = it.id
													INNER JOIN  location L
													ON               L.id = IWL.locationId
													AND             IWL.warehouseId = i.origin
													INNER JOIN  itemwarehouse IW
													ON               IW.itemId = it.id
													AND             IW.warehouseId = i.origin
													WHERE          ii.id = cte.id)
														WHEN 'BOL' THEN (
													SELECT  JSON_OBJECT  ("itemId",it.id,
																				"itemDescription",it.dsName ,
																				"referencias",GROUP_CONCAT(DISTINCT ITR.dsReference) ,
																				"locations",GROUP_CONCAT(DISTINCT L.dsName),
																						"clientId",i.clientId ,
																						"clientName",c.dsName,
																						"documentId",i.ballotId ,
																						"warehouseStock",IW.itemQty,
																						"date",DATE_FORMAT(i.date, '%d/%m/%y'),
																						"dateDocument",i.date,
																						"origin",i.origin,
																						"dsCode",w.dsCode,
																						"quantityItems",ii.nmUnities,
																						"price",ii.nmPrice )
														FROM          ballots i
														INNER JOIN ballotsitems ii
													ON              i.ballotId = ii.ballotId
														INNER JOIN items it
														ON              ii.itemId = it.id
															LEFT JOIN    itemreferences ITR
														ON              ITR.itemId = it.id
														LEFT JOIN    warehouse w
														ON              i.origin = w.id
														INNER JOIN users u
														ON              i.usId= u.usId
														INNER JOIN clients c
														ON              i.clientId=c.id
															INNER JOIN itemwarehouselocation IWL
														ON              IWL.itemId = it.id
														INNER JOIN location L
														ON              L.id = IWL.locationId AND IWL.warehouseId = i.origin
														INNER JOIN itemwarehouse IW
													ON              IW.itemId = it.id AND IW.warehouseId = i.origin
														WHERE        ii.id = cte.id
														GROUP BY   ii.id )
								WHEN 'NN' THEN
																				( SELECT          JSON_OBJECT( "itemId",it.id ,
																														"itemDescription",it.dsName,
																														"referencias",GROUP_CONCAT(DISTINCT ITR.dsReference),
																														"locations",GROUP_CONCAT(DISTINCT L.dsName),
																															"clientId",i.clientId ,
																															"clientName",c.dsName,
																														"documentId",i.documentId,
																														"warehouseStock",IW.itemQty,
																														"date",DATE_FORMAT(i.date, '%d/%m/%y'),
																															"dateDocument",i.date,
																														"origin",i.origin,
																														"dsCode",w.dsCode,
																														"quantityItems",ii.nmUnities,
																														"price",ii.nmPrice  )
																					FROM           documentsnn i
																					INNER JOIN  documentsnnitems ii
													ON               i.documentId = ii.documentId
													INNER JOIN  items it
													ON  	  	    ii.itemId = it.id
													LEFT JOIN     itemreferences ITR
													ON 		    ITR.itemId = it.id
													LEFT JOIN     warehouse w
													ON               i.origin = w.id
													INNER JOIN  users u
													ON               i.usId= u.usId
													INNER JOIN  clients c
													ON               i.clientId=c.id
													INNER JOIN  itemwarehouselocation IWL
													ON               IWL.itemId = it.id
													INNER JOIN  location L
													ON 		    L.id = IWL.locationId AND IWL.warehouseId = i.origin
													INNER JOIN  itemwarehouse IW
													ON               IW.itemId = it.id
													AND             IW.warehouseId = i.origin
													WHERE          ii.id = cte.id)
					END  AS ds_data
					FROM   cte
					WHERE 1 =1
					LIMIT :limit OFFSET :offset ;
					SELECT FOUND_ROWS() AS total;
			`

	return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		},
	})
}

function _getItemsSoldHistoric(startDate,endDate){
    var deferred = Q.defer()

    let sql = `SELECT     distinct STB.*,
                        SUM(ITW.itemQty) as "generalStock"
                FROM (SELECT

                        @i := @i + 1 AS id,
                        it.id AS itemId,
                        it.dsName AS itemDescription,
                        group_concat(distinct ITR.dsReference) AS "referencias",
                        GROUP_CONCAT(distinct L.dsName) as "locations",
                        i.clientId AS clientId,
                        c.dsName as clientName,
                        'FACT' AS documentType,
                        i.invoiceId AS documentId,
                        IW.itemQty as "warehouseStock",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.date AS dateDocument,
                        i.date AS dateFilter,
                        i.origin AS "origin",
                        w.dsCode AS "dsCode",
                        ii.nmUnities AS "quantityItems",
                        ii.nmPrice AS price



                        FROM invoices i
                        INNER JOIN invoicesitems ii
                        ON  i.invoiceId = ii.invoiceId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  itemreferences ITR
                        ON ITR.itemId = it.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join clients c
                        on i.clientId=c.id
                        INNER JOIN  itemwarehouselocation IWL
                        ON IWL.itemId = it.id
                        inner JOIN   location L
                        ON L.id = IWL.locationId AND IWL.warehouseId = i.origin
                        INNER JOIN  itemwarehouse IW
                        ON IW.itemId = it.id AND IW.warehouseId = i.origin
                        cross join (select @i := 0) r
                        where i.inStatus =  1
                        GROUP BY    ii.id

                UNION ALL

                SELECT

                        @i := @i + 1 AS id,
                        it.id AS itemId,
                        it.dsName AS itemDescription,
                        group_concat(distinct ITR.dsReference) AS "referencias",
                        GROUP_CONCAT(distinct L.dsName) as "locations",
                        i.clientId AS clientId,
                        c.dsName as clientName,
                        'BOL' AS documentType,
                        i.ballotId AS documentId,
                        IW.itemQty as "warehouseStock",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.date AS dateDocument,
                        i.date AS dateFilter,
                        i.origin AS "origin",
                        w.dsCode AS "dsCode",
                        ii.nmUnities AS "quantityItems",
                        ii.nmPrice AS price

                        FROM ballots i
                        INNER JOIN ballotsitems ii
                        ON  i.ballotId = ii.ballotId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  itemreferences ITR
                        ON ITR.itemId = it.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join clients c
                        on i.clientId=c.id
                        INNER JOIN  itemwarehouselocation IWL
                        ON IWL.itemId = it.id
                        inner JOIN   location L
                        ON L.id = IWL.locationId AND IWL.warehouseId = i.origin
                        INNER JOIN  itemwarehouse IW
                        ON IW.itemId = it.id AND IW.warehouseId = i.origin
                        cross join (select @i := 0) r
                        where i.inStatus =  1
                        GROUP BY    ii.id

                UNION ALL

                SELECT

                        @i := @i + 1 AS id,
                        it.id AS itemId,
                        it.dsName AS itemDescription,
                        group_concat(distinct ITR.dsReference) AS "referencias",
                        GROUP_CONCAT(distinct L.dsName) as "locations",
                        i.clientId AS clientId,
                        c.dsName as clientName,
                        'NN' AS documentType,
                        i.documentId AS documentId,
                        IW.itemQty as "warehouseStock",
                        DATE_FORMAT(i.date, '%d/%m/%y') as date,
                        i.date AS dateDocument,
                        i.date AS dateFilter,
                        i.origin AS "origin",
                        w.dsCode AS "dsCode",
                        ii.nmUnities AS "quantityItems",
                        ii.nmPrice AS price

                        FROM documentsnn i
                        INNER JOIN documentsnnitems ii
                        ON  i.documentId = ii.documentId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  itemreferences ITR
                        ON ITR.itemId = it.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join clients c
                        on i.clientId=c.id
                        INNER JOIN  itemwarehouselocation IWL
                        ON IWL.itemId = it.id
                        inner JOIN   location L
                        ON L.id = IWL.locationId AND IWL.warehouseId = i.origin
                        INNER JOIN  itemwarehouse IW
                        ON IW.itemId = it.id AND IW.warehouseId = i.origin
                        cross join (select @i := 0) r
                        where i.inStatus =  1
                        GROUP BY    ii.id

                    )STB
                    INNER JOIN  itemwarehouse ITW
                        ON ITW.itemId =  STB.itemId
                     WHERE dateFilter BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                     GROUP BY    STB.id
                     order BY dateDocument DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getItemsOutputs(){

    var deferred = Q.defer()

    let sql = `SELECT items.id AS itemId,
                       items.dsName AS itemDescription,
                       group_concat(distinct itemreferences.dsReference) AS "references",
                         clients.dsName AS clientName,
                         guides.id AS guideId,
                         guidesitems.nmUnities AS quantityItems,
                         DATE_FORMAT(guides.date, '%d/%m/%y') as date,
                         guides.origin,
                         guides.destination

                FROM items, clients, guides, guidesitems , itemreferences
                WHERE guides.id = guidesitems.guideId
                AND itemreferences.itemId = items.id
                AND guidesitems.itemId = items.id
                AND guides.clientId = clients.id
                AND guides.inStatus = 1
                GROUP BY  guidesitems.id
                ORDER BY guides.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getItemsPurchased(invoiceId ){

    var deferred = Q.defer()

    let sql = `SELECT    items.id AS itemId,
                         items.dsName AS itemDescription,
                         group_concat(distinct itemreferences.dsReference) AS "references",
                         provider.dsName AS providerName,
                         providerinvoices.id AS invoiceId,
                         providerinvoices.dsCode AS invoiceCode,
                         providerinvoicesitems.nmUnities AS quantityItems,
                         providerinvoicesitems.id AS id,
                         providerinvoicesitems.nmPrice AS price,
                         DATE_FORMAT(providerinvoices.date, '%d/%m/%y') as date

                FROM items, provider, providerinvoices, providerinvoicesitems, itemreferences
                WHERE providerinvoices.id = providerinvoicesitems.invoiceId
                AND itemreferences.itemId = items.id
                AND providerinvoicesitems.itemId = items.id AND providerinvoicesitems.draft = 0
                AND providerinvoices.providerId = provider.id
                AND providerinvoices.inStatus = 1
                GROUP BY    providerinvoicesitems.id
                ORDER BY providerinvoices.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInventoryItems(locationId,warehouseId){

    var deferred = Q.defer()

    let sql = ` SELECT itemId, itemDescription, brand, maxDiscount, referencias,providers,netPrice,vatPrice,itemQty,locations
                FROM (      SELECT i.id AS itemId,
                                   i.dsName as "itemDescription",
                                   i.brand,
                                   i.dsMaxDiscount as maxDiscount,
                                   GROUP_CONCAT(distinct ITR.dsReference) as "referencias",
                                   GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
                                   i.nmNet as "netPrice",
                                   ROUND(i.nmNet*1.19,0) as "vatPrice",
                                   IW.itemQty,
                                   GROUP_CONCAT(DISTINCT L.dsName) AS locations

                            FROM itemwarehouselocation IWL
                            INNER JOIN items i
                            ON  i.id = IWL.itemId
                            INNER JOIN  itemreferences ITR
                            ON ITR.itemId = i.id
                            INNER JOIN  itemwarehouse IW
                            ON IW.itemId = i.id AND IW.warehouseId = IWL.warehouseId
                            inner JOIN   itemproviders ITP
                            ON          ITP.itemId = i.id
                            INNER JOIN   provider PRO
                            ON          PRO.id = ITP.providerId
                            inner JOIN   location L
                            ON           L.id = IWL.locationId
                            WHERE  IWL.warehouseId = :warehouseId
                            GROUP BY    i.id ) consulta
                            WHERE locations LIKE '%`+ locationId + `%'
                            GROUP BY    itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            locationId:locationId,
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getDailyInventory(warehouseId,day){

    var deferred = Q.defer()

    let sql = `
            SELECT itemId, itemDescription, reference, brand, documentId, codeDoc, dateDocument,outputs,stock,locations
            FROM (      SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        it.brand as "brand",
                        i.invoiceId AS documentId,
                        t.code AS codeDoc,
                        DATE_FORMAT(i.date, '%d/%m/%y') as dateDocument,
                        GROUP_CONCAT(distinct ITR.dsReference) as "reference",
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        ii.nmUnities AS "outputs",
                        iw.itemQty AS stock


                        FROM invoices i
                        INNER JOIN invoicesitems ii
                        ON  i.invoiceId = ii.invoiceId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        INNER JOIN  itemreferences ITR
                        ON it.id = ITR.itemId
                        INNER JOIN  itemwarehouse iw
                        ON it.id = iw.itemId and iw.warehouseId = :warehouseId
                        INNER JOIN  itemwarehouselocation iwl
                        ON it.id = iwl.itemId and iwl.warehouseId = :warehouseId
                        INNER JOIN  location l
                        ON l.id = iwl.locationId
                        left JOIN  typeDocument t
                        ON  t.id = i.typeDoc
                        WHERE i.origin = :warehouseId
                        GROUP BY  ii.id

            UNION ALL
                        SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        it.brand as "brand",
                        b.ballotId AS documentId,
                        t.code AS codeDoc,
                        DATE_FORMAT(b.date, '%d/%m/%y') as dateDocument,
                        GROUP_CONCAT(distinct ITR.dsReference) as "reference",
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        bi.nmUnities AS "outputs",
                        iw.itemQty AS stock


                        FROM ballots b
                        INNER JOIN ballotsitems bi
                        ON  b.ballotId = bi.ballotId
                        INNER JOIN items it
                        ON  bi.itemId = it.id
                        INNER JOIN  itemreferences ITR
                        ON it.id = ITR.itemId
                        INNER JOIN  itemwarehouse iw
                        ON it.id = iw.itemId and iw.warehouseId = :warehouseId
                        INNER JOIN  itemwarehouselocation iwl
                        ON it.id = iwl.itemId and iwl.warehouseId = :warehouseId
                        INNER JOIN  location l
                        ON l.id = iwl.locationId
                        left JOIN  typeDocument t
                        ON  t.id = b.typeDoc
                        WHERE b.origin = :warehouseId
                        GROUP BY  bi.id
            UNION ALL
                        SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        it.brand as "brand",
                        d.documentId AS documentId,
                        t.code AS codeDoc,
                        DATE_FORMAT(d.date, '%d/%m/%y') as dateDocument,
                        GROUP_CONCAT(distinct ITR.dsReference) as "reference",
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        di.nmUnities AS "outputs",
                        iw.itemQty AS stock


                        FROM documentsnn d
                        INNER JOIN documentsnnitems di
                        ON  d.documentId = di.documentId
                        INNER JOIN items it
                        ON  di.itemId = it.id
                        INNER JOIN  itemreferences ITR
                        ON it.id = ITR.itemId
                        INNER JOIN  itemwarehouse iw
                        ON it.id = iw.itemId and iw.warehouseId = :warehouseId
                        INNER JOIN  itemwarehouselocation iwl
                        ON it.id = iwl.itemId and iwl.warehouseId = :warehouseId
                        INNER JOIN  location l
                        ON l.id = iwl.locationId
                        left JOIN  typeDocument t
                        ON  t.id = d.typeDoc
                        WHERE d.origin = :warehouseId
                        GROUP BY  di.id

            UNION ALL
                        SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        it.brand as "brand",
                        g.id AS documentId,
                        t.code AS codeDoc,
                        DATE_FORMAT(g.date, '%d/%m/%y') as dateDocument,
                        GROUP_CONCAT(distinct ITR.dsReference) as "reference",
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        gi.nmUnities AS "outputs",
                        iw.itemQty AS stock


                        FROM guides g
                        INNER JOIN guidesitems gi
                        ON  g.id = gi.guideId
                        INNER JOIN items it
                        ON  gi.itemId = it.id
                        INNER JOIN  itemreferences ITR
                        ON it.id = ITR.itemId
                        INNER JOIN  itemwarehouse iw
                        ON it.id = iw.itemId and iw.warehouseId = :warehouseId
                        INNER JOIN  itemwarehouselocation iwl
                        ON it.id = iwl.itemId and iwl.warehouseId = :warehouseId
                        INNER JOIN  location l
                        ON l.id = iwl.locationId
                        left JOIN  typeDocument t
                        ON  t.id = g.typeDoc
                        WHERE g.origin = :warehouseId
                        GROUP BY  gi.id
            ) documents
            WHERE dateDocument = '`+ day + `'
            order BY dateDocument DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            day:day
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getItemsInventoryByWarehouse(warehouseId, mayorCero = false){
	console.log('mayorCero :: ', mayorCero)
    var deferred = Q.defer()
	const AND = (mayorCero === 'true') ? ' AND IW.itemQty > 0 ' : ''
	console.log('AND :: ', AND)

    let sql = ` SELECT itemId, itemDescription, brand, maxDiscount, referencias,providers,netPrice,vatPrice,itemQty,locations
                FROM (      SELECT i.id AS itemId,
                                   i.dsName as "itemDescription",
                                   i.brand,
                                   i.dsMaxDiscount as maxDiscount,
                                   GROUP_CONCAT(distinct ITR.dsReference) as "referencias",
                                   GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
                                   i.nmNet as "netPrice",
                                   ROUND(i.nmNet*1.19,0) as "vatPrice",
                                   IW.itemQty,
                                   GROUP_CONCAT(DISTINCT L.dsName) AS locations

                            FROM itemwarehouselocation IWL
                            INNER JOIN items i
                            ON  i.id = IWL.itemId
                            INNER JOIN  itemreferences ITR
                            ON ITR.itemId = i.id
                            INNER JOIN  itemwarehouse IW
                            ON IW.itemId = i.id AND IW.warehouseId = IWL.warehouseId
                            inner JOIN   itemproviders ITP
                            ON          ITP.itemId = i.id
                            INNER JOIN   provider PRO
                            ON          PRO.id = ITP.providerId
                            inner JOIN   location L
                            ON           L.id = IWL.locationId
                            WHERE  IWL.warehouseId = :warehouseId
                            ` + AND + `
							GROUP BY    i.id ) consulta
                            GROUP BY    itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getUserSales(startDate,endDate,userId){

    var deferred = Q.defer()

    let sql = `SELECT documentId, total, status,clientName, statusName, origin, type, DATE_FORMAT(date, '%d/%m/%y') as date
                FROM ( SELECT
                            i.invoiceId as documentId,
                            i.total as total,
                            i.status,
                            s.otherName AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            DATE(i.date) as date

                            from users u
                            inner join invoices i
                            on i.usId=u.usId
                            inner join clients c
                            on i.clientId=c.id
                            left JOIN  warehouse w
                            ON  i.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            left JOIN  status s
                            ON  i.status = s.id
                            where u.usId= :userId AND i.inStatus = 1

                    UNION ALL

                    SELECT
                            d.documentId as documentId,
                            d.total as total,
                            d.status,
                            if(d.status=1, 'Pagado','Sin Pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            DATE(d.date) as date

                            from users u
                            inner join documentsnn d
                            on d.usId=u.usId
                            inner join clients c
                            on d.clientId=c.id
                            left JOIN  warehouse w
                            ON  d.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = d.typeDoc
                            where u.usId= :userId AND d.inStatus = 1

                    UNION ALL

                    SELECT
                            b.ballotId as documentId,
                            b.total as total,
                            b.status,
                            if(b.status=1, 'Cancelada','Sin pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            DATE(b.date) as date

                            from users u
                            inner join ballots b
                            on b.usId=u.usId
                            inner join clients c
                            on b.clientId=c.id
                            left JOIN  warehouse w
                            ON  b.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = b.typeDoc
                            where u.usId= :userId AND b.inStatus = 1
                            )sales
            WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
            order BY date DESC  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getWarehouseSales(startDate,endDate,warehouseId){

    var deferred = Q.defer()

    let sql = `SELECT documentId, total,iva, CAST(net AS SIGNED) AS net,status, clientName,statusName, origin, userName , type, DATE_FORMAT(date, '%d/%m/%y') as date
            FROM (
                    SELECT

                            i.invoiceId as documentId,
                            i.total as total,
                            i.total as iva,
                            (i.total/1.19 ) as net,
                            i.status,
                            s.otherName AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(i.date) as date

                            from warehouse w
                            inner join invoices i
                            on i.origin = w.id
                            inner join clients c
                            on i.clientId=c.id
                            inner join users u
                            on i.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            left JOIN  status s
                            ON  i.status = s.id
                            where w.id= :warehouseId AND i.inStatus = 1

                    UNION ALL

                    SELECT
                            d.documentId as documentId,
                            d.total as total,
                            0 as iva,
                            d.total as net,
                            d.status,
                            if(d.status=1, 'Pagado','Sin Pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(d.date) as date

                            from warehouse w
                            inner join documentsnn d
                            on d.origin = w.id
                            inner join clients c
                            on d.clientId=c.id
                            inner join users u
                            on d.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = d.typeDoc
                            where w.id= :warehouseId AND d.inStatus = 1

                    UNION ALL

                    SELECT
                            b.ballotId as documentId,
                            b.total as total,
                            b.total as iva,
                            (b.total/1.19 ) as net,
                            b.status,
                            if(b.status=1, 'Cancelada','Sin pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(b.date) as date

                            from warehouse w
                            inner join ballots b
                            on b.origin = w.id
                            inner join clients c
                            on b.clientId=c.id
                            inner join users u
                            on b.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = b.typeDoc
                            where w.id= :warehouseId AND b.inStatus = 1

                    UNION ALL

                    SELECT
                            cn.noteId as documentId,
                            -(cn.total) AS total,
                            -(cn.total) AS iva,
                            - cn.total/1.19 as net,
                            cn.status,
                            if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(cn.date) as date

                            from warehouse w
                            inner join creditnotes cn
                            on cn.origin = w.id
                            inner join clients c
                            on cn.clientId=c.id
                            inner join users u
                            on cn.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = cn.typeDoc
                            where w.id= :warehouseId AND cn.inStatus = 1
                            )sales
            WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
            order BY date DESC  `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCardexItems(itemId){

    var deferred = Q.defer()

    let sql = `SELECT documentId, codeDoc, type, DATE_FORMAT(dateDocument, '%d/%m/%y') as fecha,  if(DATE_FORMAT(dateDocument, '%H:%i:%S') ='00:00:00', '--',DATE_FORMAT(dateDocument, '%H:%i:%S')) AS time,outputs, origin, destination,userName,clientName
               FROM(

                    SELECT

                        i.invoiceId AS documentId,
                        t.code AS codeDoc,
                        'Salida -' AS type,
                        i.date as dateDocument,
                        ii.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM invoices i
                        INNER JOIN invoicesitems ii
                        ON  i.invoiceId = ii.invoiceId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = i.typeDoc
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join clients c
                        on i.clientId=c.id
                        WHERE it.id = :itemId

                    UNION ALL

                    SELECT

                        b.ballotId AS documentId,
                        t.code AS codeDoc,
                        'Salida -' AS type,
                        b.date as dateDocument,
                        bi.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        c.dsName as clientName

                        FROM ballots b
                        INNER JOIN ballotsitems bi
                        ON  b.ballotId = bi.ballotId
                        INNER JOIN items it
                        ON  bi.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = b.typeDoc
                        left JOIN  warehouse w
                        ON  b.origin = w.id
                        inner join users u
                        on b.usId= u.usId
                        inner join clients c
                        on b.clientId=c.id
                        WHERE it.id = :itemId

                    UNION ALL

                    SELECT

                        d.documentId AS documentId,
                        t.code AS codeDoc,
                        'Salida -' AS type,
                        d.date as dateDocument,
                        di.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM documentsnn d
                        INNER JOIN documentsnnitems di
                        ON  d.documentId = di.documentId
                        INNER JOIN items it
                        ON  di.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = d.typeDoc
                        left JOIN  warehouse w
                        ON  d.origin = w.id
                        inner join users u
                        on d.usId= u.usId
                        inner join clients c
                        on d.clientId=c.id
                        WHERE it.id = :itemId

                    UNION ALL

                    SELECT

                        g.id AS documentId,
                        t.code AS codeDoc,
                        'Traslado' AS type,
                        g.date as dateDocument,
                        gi.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        wd.dsCode AS "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM guides g
                        INNER JOIN guidesitems gi
                        ON  g.id = gi.guideId
                        INNER JOIN items it
                        ON  gi.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = g.typeDoc
                        left JOIN  warehouse w
                        ON  g.origin = w.id
                        left JOIN  warehouse wd
                        ON  g.destination = wd.id
                        inner join users u
                        on g.usId= u.usId
                        inner join clients c
                        on g.clientId=c.id
                        WHERE it.id = :itemId AND g.type = 0 AND g.status = 2

                    UNION ALL

                    SELECT

                        s.id AS documentId,
                        t.code AS codeDoc,
                        if(s.type=0, 'Sobrante +','Merma -') AS "type",
                        s.date as dateDocument,
                        si.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        NULL as "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM stockadjustment s
                        INNER JOIN stockadjustmentitems si
                        ON  s.id = si.guideId
                        INNER JOIN items it
                        ON  si.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = s.typeDoc
                        left JOIN  warehouse w
                        ON  s.origin = w.id
                        inner join users u
                        on s.usId= u.usId
                        inner join clients c
                        on s.clientId=c.id
                        WHERE it.id = :itemId AND s.status = 2


                    UNION ALL

                    SELECT

                        i.id AS documentId,
                        'FACT-PROV' AS codeDoc,
                        'Entrada +' AS type,
                        i.date as dateDocument,
                        ii.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        p.dsName as clientName


                        FROM providerinvoices i
                        INNER JOIN providerinvoicesitems ii
                        ON  i.id = ii.invoiceId and ii.draft = 0
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join provider p
                        on i.providerId=p.id
                        WHERE it.id = :itemId

                    UNION ALL

                    SELECT

                        i.noteId AS documentId,
                        t.code AS codeDoc,
                        'Devolución +' AS type,
                        i.date as dateDocument,
                        ii.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM creditnotes i
                        INNER JOIN creditnotesitems ii
                        ON  i.noteId = ii.noteId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = i.typeDoc
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join clients c
                        on i.clientId=c.id
                        WHERE it.id = :itemId


                    UNION ALL

                    SELECT

                        i.returnId AS documentId,
                        t.code AS codeDoc,
                        'Devolución -' AS type,
                        i.date as dateDocument,
                        ii.nmUnities AS "outputs",
                        w.dsCode AS "origin",
                        null AS "destination",
                        u.usName as userName,
                        c.dsName as clientName


                        FROM supplierreturn i
                        INNER JOIN supplierreturnitems ii
                        ON  i.returnId = ii.returnId
                        INNER JOIN items it
                        ON  ii.itemId = it.id
                        left JOIN  typeDocument t
                        ON  t.id = i.typeDoc
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        inner join users u
                        on i.usId= u.usId
                        inner join provider c
                        on i.providerId=c.id
                        WHERE it.id = :itemId
                        ) documents

            order BY dateDocument desc `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getItemsImportsHistoric(startDate,endDate){
    var deferred = Q.defer()
    let where = ''
    let sign = "'"
    if (startDate!='' && endDate!=''){
        where = ' AND DATE(imports.date) BETWEEN '+ sign+startDate+sign +' AND '+ sign+endDate+sign + ''
    }

    let sql = `SELECT itemId, itemDescription, referencias,locations, providerId, providerName,importId,DATE_FORMAT(date,'%d/%m/%y') as date,origin,dsCode,quantityItems,price
                FROM ( SELECT   items.id AS itemId,
                        items.dsName AS itemDescription,
                        group_concat(distinct itemreferences.dsReference) AS "referencias",
                        GROUP_CONCAT(distinct location.dsName) as "locations",
                        imports.providerId,
                        provider.dsName AS providerName,
                        imports.importId AS importId,
                        DATE(imports.date) as date,
                        imports.origin,
                        warehouse.dsCode,
                        importsitems.nmUnities AS quantityItems,
                        importsitems.nmPrice AS price

                FROM    items, provider, imports, importsitems ,itemreferences,itemwarehouselocation,location,warehouse
                WHERE   imports.importId = importsitems.importId
                AND     itemreferences.itemId = items.id
                AND     itemwarehouselocation.itemId = items.id
                AND     location.id =  itemwarehouselocation.locationId
                AND     importsitems.itemId = items.id
                AND     imports.providerId = provider.id
                AND     imports.origin = warehouse.id
                AND     imports.origin = itemwarehouselocation.warehouseId
                AND     imports.inStatus =  1
                 ` + where + `
                GROUP BY    importsitems.id
                order BY date DESC
                            )sales`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getItemstransitImports(startDate,endDate){
    var deferred = Q.defer()
    let where = ''
    let sign = "'"
    if (startDate!='' && endDate!=''){
        where = ' AND DATE(imports.date) BETWEEN '+ sign+startDate+sign +' AND '+ sign+endDate+sign + ''
    }

    let sql = `SELECT itemId, itemDescription, referencias,locations, providerId, providerName,importId,DATE_FORMAT(date,'%d/%m/%y') as date,origin,dsCode,quantityItems,price
                FROM ( SELECT   items.id AS itemId,
                        items.dsName AS itemDescription,
                        group_concat(distinct itemreferences.dsReference) AS "referencias",
                        GROUP_CONCAT(distinct location.dsName) as "locations",
                        imports.providerId,
                        provider.dsName AS providerName,
                        imports.importId AS importId,
                        DATE(imports.date) as date,
                        imports.origin,
                        warehouse.dsCode,
                        importsitems.nmUnities AS quantityItems,
                        importsitems.nmPrice AS price

                FROM    items, provider, imports, importsitems ,itemreferences,itemwarehouselocation,location,warehouse
                WHERE   imports.importId = importsitems.importId
                AND     itemreferences.itemId = items.id
                AND     itemwarehouselocation.itemId = items.id
                AND     location.id =  itemwarehouselocation.locationId
                AND     importsitems.itemId = items.id
                AND     imports.providerId = provider.id
                AND     imports.origin = warehouse.id
                AND     imports.origin = itemwarehouselocation.warehouseId
                AND     imports.inStatus =  1 AND imports.status =  1
                 ` + where + `
                GROUP BY    importsitems.id
                order BY date DESC
                            )sales`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getNetSales(warehouseId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT clientName, DATE_FORMAT(date, '%d/%m/%y') as date , total, sum(total) AS suma
                    FROM (

                           SELECT

                            i.invoiceId as documentId,
                            (i.total/1.19) AS total,
                            i.status,
                            if(i.status=1, 'Pendiente','Pagada') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(i.date) as date

                            from warehouse w
                            inner join invoices i
                            on i.origin = w.id
                            inner join clients c
                            on i.clientId=c.id
                            inner join users u
                            on i.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            where w.id= :warehouseId AND i.inStatus = 1

                    UNION ALL

                    SELECT
                            d.documentId as documentId,
                            d.total as total,
                            d.status,
                            if(d.status=1, 'Pagado','Sin Pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(d.date) as date

                            from warehouse w
                            inner join documentsnn d
                            on d.origin = w.id
                            inner join clients c
                            on d.clientId=c.id
                            inner join users u
                            on d.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = d.typeDoc
                            where w.id= :warehouseId AND d.inStatus = 1

                    UNION ALL

                    SELECT
                            b.ballotId as documentId,
                            (b.total/1.19) AS total,
                            b.status,
                            if(b.status=1, 'Cancelada','Sin pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(b.date) as date

                            from warehouse w
                            inner join ballots b
                            on b.origin = w.id
                            inner join clients c
                            on b.clientId=c.id
                            inner join users u
                            on b.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = b.typeDoc
                            where w.id= :warehouseId AND b.inStatus = 1

                    UNION ALL

                    SELECT
                            cn.noteId as documentId,
                            -(cn.total/1.19) AS total,
                            cn.status,
                            if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(cn.date) as date

                            from warehouse w
                            inner join creditnotes cn
                            on cn.origin = w.id
                            inner join clients c
                            on cn.clientId=c.id
                            inner join users u
                            on cn.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = cn.typeDoc
                            where w.id= :warehouseId AND cn.inStatus = 1

                            )consulta

                     WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getSellingCosts(warehouseId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT DATE_FORMAT(date, '%d/%m/%y') as date, total, sum(total) AS suma
                    FROM (
                            SELECT

                               i.invoiceId as documentId,
                               i.netTotal as total,
                               i.status,
                               if(i.status=1, 'Pendiente','Pagada') AS "statusName",
                               w.dsCode AS "origin",
                               t.name as type,
                               c.dsName as clientName,
                               u.usName as userName,
                               DATE(i.date) as date

                            from warehouse w
                            inner join invoices i
                            on i.origin = w.id
                            inner join clients c
                            on i.clientId=c.id
                            inner join users u
                            on i.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            where w.id= :warehouseId AND i.inStatus = 1

                            )consulta

                     WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getOtherExpenses(warehouseId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT DATE_FORMAT(date, '%d/%m/%y') as date , sum(shareValue) AS suma
                    FROM (

                           SELECT    ec.id AS expenseControlId,
                                     ec.documentId AS documentId,
                                     ec.date,
                                     w.dsCode AS origin,
                                     (ec.shareValue/1.19) AS shareValue

                            FROM expensecontrol ec
                            left JOIN  warehouse w
                            ON  ec.origin = w.id
                            WHERE ec.origin= :warehouseId AND  ec.inStatus = 1
                            order BY ec.id DESC

                            )consulta

                     WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                     order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getOtherExpensesList(warehouseId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `SELECT dsName,DATE_FORMAT(minDate, '%d/%m/%y') as minDate ,DATE_FORMAT(maxDate, '%d/%m/%y') as maxDate, suma
                    FROM (
                            SELECT  acc.dsName,
                                    min(ex.date) AS minDate,
                                    max(ex.date) AS maxDate,
                                    SUM(ex.netValue) AS suma
                            FROM    accounts acc
                            inner JOIN expensecontrol ex
                            ON ex.accountId = acc.id
                            left JOIN  warehouse w
                            ON  ex.origin = w.id
                            WHERE ex.origin= :warehouseId AND ex.inStatus= 1 AND ex.date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                            GROUP BY acc.dsName

                            )consulta
                     order BY minDate asc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getFaultyItemsList(){
    var deferred = Q.defer()

    let sql = `SELECT   items.id AS itemId,
                        items.dsName AS itemDescription,
                        group_concat(distinct itemreferences.dsReference) AS "references",
                        aftersale.clientId,
                        aftersale.afterSaleId,
                        DATE_FORMAT(aftersale.date, '%d/%m/%y') as date,
                        aftersale.afterSaleId as documentId,
                        warehouse.dsCode as origin,
                        aftersaleitems.nmUnities AS quantityItems,
                        aftersaleitems.nmPrice AS price

                FROM    items, clients, aftersale, aftersaleitems ,itemreferences,itemwarehouselocation,location,warehouse
                WHERE   aftersale.afterSaleId = aftersaleitems.afterSaleId
                AND     itemreferences.itemId = items.id
                AND     itemwarehouselocation.itemId = items.id
                AND     location.id =  itemwarehouselocation.locationId
                AND     aftersaleitems.itemId = items.id
                AND     aftersale.clientId = clients.id
                AND     aftersale.origin = warehouse.id
                AND     aftersale.origin = itemwarehouselocation.warehouseId
                AND     aftersale.inStatus =  1
                GROUP BY    aftersaleitems.id
                ORDER BY aftersale.afterSaleId DESC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getNetSalesUser(userId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT clientName, DATE_FORMAT(date, '%d/%m/%y') as date , total, sum(total) AS suma
                    FROM (

                           SELECT

                            i.invoiceId as documentId,
                            (i.total/1.19) AS total,
                            i.status,
                            if(i.status=1, 'Pendiente','Pagada') AS "statusName",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(i.date) as date

                            from users u
                            inner join invoices i
                            on i.usId = u.usId
                            inner join clients c
                            on i.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            where u.usId = :userId AND i.inStatus = 1

                    UNION ALL

                    SELECT
                            d.documentId as documentId,
                            d.total as total,
                            d.status,
                            if(d.status=1, 'Pagado','Sin Pagar') AS "statusName",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(d.date) as date

                            from users u
                            inner join documentsnn d
                            on d.usId = u.usId
                            inner join clients c
                            on d.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = d.typeDoc
                            where u.usId = :userId AND d.inStatus = 1

                    UNION ALL

                    SELECT
                            b.ballotId as documentId,
                            (b.total/1.19) AS total,
                            b.status,
                            if(b.status=1, 'Cancelada','Sin pagar') AS "statusName",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(b.date) as date

                            from users u
                            inner join ballots b
                            on b.usId= u.usId
                            inner join clients c
                            on b.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = b.typeDoc
                            where u.usId= :userId AND b.inStatus = 1

                    UNION ALL

                    SELECT
                            cn.noteId as documentId,
                            -(cn.total/1.19) AS total,
                            cn.status,
                            if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                            t.name as type,
                            c.dsName as clientName,
                            u.usName as userName,
                            DATE(cn.date) as date

                            from users u
                            inner join creditnotes cn
                            on cn.usId= u.usId
                            inner join clients c
                            on cn.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = cn.typeDoc
                            where u.usId= :userId AND cn.inStatus = 1

                            )consulta

                    WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getSellingCostsUser(userId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT DATE_FORMAT(date, '%d/%m/%y') as date, total, sum(total) AS suma
                    FROM (
                            SELECT

                               i.invoiceId as documentId,
                               i.netTotal as total,
                               i.status,
                               if(i.status=1, 'Pendiente','Pagada') AS "statusName",
                               w.dsCode AS "origin",
                               t.name as type,
                               c.dsName as clientName,
                               u.usName as userName,
                               DATE(i.date) as date

                            from warehouse w
                            inner join invoices i
                            on i.origin = w.id
                            inner join clients c
                            on i.clientId=c.id
                            inner join users u
                            on i.usId= u.usId
                            left JOIN  typeDocument t
                            ON  t.id = i.typeDoc
                            where  u.usId= :userId AND i.inStatus = 1

                            )consulta

                    WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

module.exports = {
    _getItemsSold: _getItemsSold,
    _getItemsSoldHistoric:_getItemsSoldHistoric,
    _getItemsOutputs:_getItemsOutputs,
    _getItemsPurchased:_getItemsPurchased,
    _getInventoryItems:_getInventoryItems,
    _getDailyInventory:_getDailyInventory,
    _getItemsInventoryByWarehouse:_getItemsInventoryByWarehouse,
    _getUserSales:_getUserSales,
    _getWarehouseSales:_getWarehouseSales,
    _getCardexItems : _getCardexItems,
    _getItemsImportsHistoric:_getItemsImportsHistoric,
    _getItemstransitImports:_getItemstransitImports,
    _getNetSales:_getNetSales,
    _getSellingCosts:_getSellingCosts,
    _getOtherExpenses:_getOtherExpenses,
    _getOtherExpensesList:_getOtherExpensesList,
    _getFaultyItemsList:_getFaultyItemsList,
    _getNetSalesUser:_getNetSalesUser,
    _getSellingCostsUser:_getSellingCostsUser,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
	_getItemsSolMain
}
