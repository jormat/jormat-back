'use strict'

const { async } = require('q');
var model = require('./model');

// reports

function getItemsSold(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getItemsSold(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

const parsedData = async (data) => {
    const returnData = []
    for(const item in data) {
		const row = (data[item].ds_data !== undefined || data[item].ds_data !== null) ? JSON.parse(data[item].ds_data) : { warehouseStock: 0 }
        returnData.push({
            id: data[item].id,
            documentType: data[item].documentType,
			generalStock: row.warehouseStock,
			...row
        })
	}

    return returnData
}

const getItemsSoldPaginated = async (req, res) => {
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = parseInt(req.query['pageSize'])
	const {
		filters = {},
		searchText = null,
	} = req.query

	model._getItemsSolMain(filters, offset, limit, searchText).then(async (result) => {
		const data = result[0] != undefined ? await parsedData(result[0]) : []
		const count = result[1] != undefined ? result[1][0].total : 0

		res.json({
			data: data,
			count,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}

function getItemsSoldHistoric(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getItemsSoldHistoric(startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsOutputs(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getItemsOutputs(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsPurchased(req, res) {

    var query = [];
    query.push(model._getItemsPurchased())

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInventoryItems(req, res) {

    var query = [];
    var locationId = req.query['locationId'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getInventoryItems(locationId,warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function getDailyInventory(req, res) {

    var query = [];
    var warehouseId = req.query['warehouseId'];
    var day = req.query['day'];
    query.push(model._getDailyInventory(warehouseId,day))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsInventoryByWarehouse(req, res) {

    let query = [];
    const warehouseId = req.query['warehouseId'];
	const mayorCero = req.query['mayorCero'] ? req.query['mayorCero'] : false
    query.push(model._getItemsInventoryByWarehouse(warehouseId, mayorCero))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getUserSales(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var userId = req.query['userId'];
    query.push(model._getUserSales(startDate,endDate,userId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getWarehouseSales(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getWarehouseSales(startDate,endDate,warehouseId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCardexItems(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getCardexItems(itemId))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsImportsHistoric(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getItemsImportsHistoric(startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemstransitImports(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getItemstransitImports(startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNetSales(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getNetSales(warehouseId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSellingCosts(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getSellingCosts(warehouseId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOtherExpenses(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getOtherExpenses(warehouseId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOtherExpenses(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getOtherExpenses(warehouseId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getOtherExpensesList(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getOtherExpensesList(warehouseId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getFaultyItemsList(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getFaultyItemsList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNetSalesUser(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var userId = req.query['userId'];
    query.push(model._getNetSalesUser(userId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getSellingCostsUser(req, res) {

    var query = [];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    var userId = req.query['userId'];
    query.push(model._getSellingCostsUser(userId,startDate,endDate))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getItemsSold: getItemsSold,
    getItemsSoldHistoric:getItemsSoldHistoric,
    getItemsOutputs:getItemsOutputs,
    getItemsPurchased:getItemsPurchased,
    getInventoryItems:getInventoryItems,
    getDailyInventory:getDailyInventory,
    getItemsInventoryByWarehouse:getItemsInventoryByWarehouse,
    getUserSales:getUserSales,
    getWarehouseSales:getWarehouseSales,
    getCardexItems:getCardexItems,
    getItemsImportsHistoric:getItemsImportsHistoric,
    getItemstransitImports:getItemstransitImports,
    getNetSales:getNetSales,
    getSellingCosts:getSellingCosts,
    getOtherExpenses:getOtherExpenses,
    getOtherExpensesList:getOtherExpensesList,
    getFaultyItemsList:getFaultyItemsList,
    getNetSalesUser:getNetSalesUser,
    getSellingCostsUser:getSellingCostsUser,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
	getItemsSoldPaginated
}
