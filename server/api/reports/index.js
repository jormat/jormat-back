var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');


//reports services
router.get('/items-sold', service.getItemsSold);
router.get('/items-sold-paginated', service.getItemsSoldPaginated);
router.get('/items-sold-historic', service.getItemsSoldHistoric)
router.get('/items-outputs', service.getItemsOutputs);
router.get('/items-purchased', service.getItemsPurchased);
router.get('/inventory', service.getInventoryItems);
router.get('/daily-inventory', service.getDailyInventory);
router.get('/warehouse-inventory', service.getItemsInventoryByWarehouse);
router.get('/user-sales', service.getUserSales);
router.get('/warehouse-sales', service.getWarehouseSales);
router.get('/items-cardex', service.getCardexItems);
router.get('/items-imports', service.getItemsImportsHistoric);
router.get('/items-transit-imports', service.getItemstransitImports);
router.get('/net-sales', service.getNetSales);
router.get('/selling-costs', service.getSellingCosts);
router.get('/other-expenses', service.getOtherExpenses);
router.get('/other-expenses-list', service.getOtherExpensesList);
router.get('/faulty-items', service.getFaultyItemsList);

//reports rentability user
router.get('/net-sales-user', service.getNetSalesUser);
router.get('/selling-costs-user', service.getSellingCostsUser);


module.exports = router;
