'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//transfer Guides 
function _getGuides(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        g.id as "transferGuideId",
                        c.dsName AS "clientName",
                        g.total,
                        w.dsCode AS "origin",
                        wd.dsCode AS "destination",
                        DATE_FORMAT(g.date, '%d/%m/%y') as date,
                        s.name AS "statusName",
                        u.usName AS "userCreation",
                        g.orderId,
                        if(g.type=0, 'Traslado','Venta') AS "type"
                        
                    FROM guides g
                    inner JOIN  clients c
                    ON  g.clientId = c.id
                    left JOIN  warehouse w
                    ON  g.origin = w.id
                    left JOIN  warehouse wd
                    ON  g.destination = wd.id
                    left JOIN  users u
                    ON  g.usId = u.usId
                    left JOIN  status s
                    ON  g.status = s.id
                    order BY g.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getGuidesDetails(transferGuideId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        g.id as "transferGuideId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        g.total,
                        ROUND(g.total/1.19,0) as "netPrice",
                        g.total - ROUND(g.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        w.id AS "originId",
                        wd.dsCode AS "destination",
                        g.destination AS "destinationId",
                        DATE_FORMAT(g.date, '%d/%m/%y') as date,
                        DATE_FORMAT(g.date, '%H:%i:%S') as time,
                        s.name AS "statusName",
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(g.type=0, 'Traslado','Venta') AS "type",
                        c.dsPhoneNumber AS "phone",
                        c.dsAddress AS "address",
                        g.commentary AS "comment",
                        g.rejection AS "rejection"
                        
                    FROM guides g
                    inner JOIN  clients c
                    ON  g.clientId = c.id
                    left JOIN  warehouse w
                    ON  g.origin = w.id
                    left JOIN  warehouse wd
                    ON  g.destination = wd.id
                    left JOIN  status s
                    ON  g.status = s.id
                    left JOIN  users u
                    ON  g.usId = u.usId
                    left JOIN  users us
                    ON  g.usModifierId = us.usId
                    WHERE g.id = :transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            transferGuideId:transferGuideId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _createGuide(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.destinationId==undefined) data.destinationId=0
    if(data.orderId==undefined) data.orderId=null  
    

    let sql = `INSERT INTO guides(date,type,clientId,status,total,inStatus,origin,destination,commentary,usId,orderId)
                      VALUES (NOW(),:type,:clientId,1,:total,1,:originId,:destinationId,:observation,:userId,:orderId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            type:data.type,
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            orderId:data.orderId,
            destinationId:data.destinationId,
            originId:data.originId,
            observation:data.comment
        }
    }).then(function (guideId) {
        deferred.resolve(guideId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
   
}

function _createItemsGuideList(items,guideId,originId){

    var deferred = Q.defer()

    if(items.disscount == undefined) items.disscount=0

        let sql = `INSERT INTO guidesitems (guideId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:guideId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));

                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            guideId:guideId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsGuideListSales(items,guideId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
        
        let sql = `INSERT INTO guidesitems (guideId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:guideId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            guideId:guideId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _validateGuide(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE guides SET status=2, usModifierId =:userId WHERE id=:transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            userId:data.userId,
            transferGuideId:data.transferGuideId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _rejectedGuide(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE guides SET status=3, usModifierId =:userId WHERE id=:transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            userId:data.userId,
            transferGuideId:data.transferGuideId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateStock(items,destination){

    var deferred = Q.defer()
        let sql = ` UPDATE itemwarehouse SET itemQty=(itemQty+:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:destination`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            destination:destination,
            itemId:items.itemId,
            quantity:items.quantityItems
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })
    return deferred.promise
}

function _updateGuide(model){

    var data = [
       {
          "messagge": "the Guide is update succesfully",
        }
    ]

    return data;
}

function _disabledGuide(model){

    var data = [
       {
          "messagge": "the Guide is disabled succesfully",
        }
    ]

    return data;
}

function _getGuidesItems(transferGuideId){

    var deferred = Q.defer()

    let sql = `SELECT                  i.id AS itemId,
                        i.dsName AS itemDescription,
                        i.webType AS webType,
                        i.brandCode AS brandCode,
                        ITR.dsReference as referenceKey,
                        gi.nmUnities as quantityItems,
                        gi.nmPrice AS price,
                        gi.total
                FROM guidesitems gi
                INNER JOIN items i
                ON  i.id = gi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                WHERE guideId = :transferGuideId
                GROUP BY    gi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            transferGuideId:transferGuideId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

//stock adjustment guide 
function _getStockAdjustmentGuides(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        g.id as "transferGuideId",
                        c.dsName AS "clientName",
                        g.total,
                        w.dsCode AS "origin",
                        DATE_FORMAT(g.date, '%d/%m/%y') as date,
                        s.name AS "statusName",
                        u.usName AS "userCreation",
                        if(g.type=0, 'Sobrante +','Merma -') AS "type"
                        
                    FROM stockadjustment g
                    inner JOIN  clients c
                    ON  g.clientId = c.id
                    left JOIN  warehouse w
                    ON  g.origin = w.id
                    left JOIN  status s
                    ON  g.status = s.id
                    left JOIN  users u
                    ON  g.usId = u.usId
                    order BY g.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getStockAdjustmentGuidesDetails(guideId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        g.id as "guideId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        g.total,
                        ROUND(g.total/1.19,0) as "netPrice",
                        g.total - ROUND(g.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        g.origin AS "originId",
                        DATE_FORMAT(g.date, '%d/%m/%y') as date,
                        DATE_FORMAT(g.date, '%H:%i:%S') as time,
                        s.name AS "statusName",
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        if(g.type=0, 'Sobrante +','Merma -') AS "type",
                        g.commentary AS "comment"
                        
                    FROM stockadjustment g
                    inner JOIN  clients c
                    ON  g.clientId = c.id
                    left JOIN  warehouse w
                    ON  g.origin = w.id
                    left JOIN  status s
                    ON  g.status = s.id
                    left JOIN  users u
                    ON  g.usId = u.usId
                    left JOIN  users us
                    ON  g.usModifierId = us.usId
                    WHERE g.id =:guideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            guideId:guideId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createStockAdjustment(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    

    let sql = `INSERT INTO stockadjustment(date,type,clientId,status,total,inStatus,origin,commentary,usId)
                      VALUES (NOW(),:type,:clientId,1,:total,1,:originId,:observation,:userId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            type:data.type,
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            observation:data.comment
        }
    }).then(function (guideId) {
        deferred.resolve(guideId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsStockAdjustment(items,guideId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0
        
        let sql = `INSERT INTO stockadjustmentitems (guideId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:guideId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            guideId:guideId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _validateStockAdjustment(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE stockadjustment SET status=2, usModifierId =:userId WHERE id=:transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            userId:data.userId,
            transferGuideId:data.transferGuideId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _rejectedStockAdjustment(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE stockadjustment SET status=3, usModifierId =:userId WHERE id=:transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            userId:data.userId,
            transferGuideId:data.transferGuideId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateStockGuideAdjustment(items,origin){
    

    var deferred = Q.defer()
        let sql = ` UPDATE itemwarehouse SET itemQty=(itemQty+:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:origin`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            origin:origin,
            itemId:items.itemId,
            quantity:items.quantityItems
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })
    return deferred.promise
}

function _updateStockGuideAdjustmentDecrease(items,origin){
    

    var deferred = Q.defer()
        let sql = ` UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:origin`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            origin:origin,
            itemId:items.itemId,
            quantity:items.quantityItems
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })
    return deferred.promise
}

function _updateStockAdjustment(model){

    var data = [
       {
          "messagge": "the stock adjustment guide is update succesfully",
        }
    ]

    return data;
}

function _disabledStockAdjustment(model){

    var data = [
       {
          "messagge": "the stock adjustment guide is disabled succesfully",
        }
    ]

    return data;
}

function _getStockAdjustmentItems(transferGuideId){

    var deferred = Q.defer()

    let sql = `SELECT   itemId,
                        i.dsName AS itemDescription,
                        i.webType as webType,
                        gi.nmUnities as quantityItems,
                        gi.nmPrice AS price,
                        gi.total
                FROM stockadjustmentitems gi
                INNER JOIN items i
                ON  i.id = gi.itemId
                WHERE guideId =:transferGuideId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            transferGuideId:transferGuideId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}


function _getGuidesItemsLocation(transferGuideId,destination,origin){

    var deferred = Q.defer()

    let sql = `SELECT   i.id AS itemId,
                        i.dsName AS itemDescription,
                        i.webType AS webType,
                        ITR.dsReference as referenceKey,
                        gi.nmUnities as quantityItems,
                        GROUP_CONCAT(distinct lo.dsName) as "originLocation",
                        GROUP_CONCAT(distinct l.dsName) as "destinationLocation",
                        i.brandCode AS brandCode,
                        gi.nmPrice AS price,
                        gi.total
                FROM guidesitems gi
                INNER JOIN items i
                ON  i.id = gi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                INNER JOIN  itemwarehouselocation iwl
                ON i.id = iwl.itemId and iwl.warehouseId = :destination
                left JOIN  location l
                ON l.id = iwl.locationId
                INNER JOIN  itemwarehouselocation iwlo
                ON i.id = iwlo.itemId and iwlo.warehouseId = :origin
                left JOIN  location lo
                ON lo.id = iwlo.locationId
                WHERE guideId = :transferGuideId
                GROUP BY    gi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            transferGuideId:transferGuideId,
            origin:origin,
            destination:destination
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}


module.exports = {
    _getGuides: _getGuides,
    _getGuidesDetails:_getGuidesDetails,
    _createGuide:_createGuide,
    _createItemsGuideList:_createItemsGuideList,
    _createItemsGuideListSales:_createItemsGuideListSales,
    _updateStock:_updateStock,
    _validateGuide:_validateGuide,
    _rejectedGuide:_rejectedGuide,
    _updateGuide:_updateGuide,
    _disabledGuide:_disabledGuide,
    _getGuidesItems:_getGuidesItems,
    _getStockAdjustmentGuides: _getStockAdjustmentGuides,
    _getStockAdjustmentGuidesDetails:_getStockAdjustmentGuidesDetails,
    _createStockAdjustment:_createStockAdjustment,
    _createItemsStockAdjustment:_createItemsStockAdjustment,
    _validateStockAdjustment:_validateStockAdjustment,
    _updateStockGuideAdjustment:_updateStockGuideAdjustment,
    _updateStockGuideAdjustmentDecrease:_updateStockGuideAdjustmentDecrease,
    _rejectedStockAdjustment:_rejectedStockAdjustment,
    _updateStockAdjustment:_updateStockAdjustment,
    _disabledStockAdjustment:_disabledStockAdjustment,
    _getStockAdjustmentItems:_getStockAdjustmentItems,
    _getGuidesItemsLocation:_getGuidesItemsLocation,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}