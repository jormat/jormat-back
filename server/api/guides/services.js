'use strict'

var model = require('./model');
const fs = require('fs')
const csv = require('csv-express')
const Parse = require('csv-parse')

//transfer Guides 

function uploadGuides(req, res) {
    const fileUpload = req.file

    if (!fileUpload) {
        const error = {
            parent : { errno: 1 },
            message: 'archivo omitido',
            sql    : 'no sql',
        }

        res.json({
            data: [],
            status: 0,
            error
        });

        return
    }

    const folderPath = fileUpload.path.toString()
    const source = fs.createReadStream(folderPath)
    const parser = Parse({ delimiter: ';', columns: true })
    let linesRead = 0
    let output = []

    parser.on('readable', function () {
        let record = parser.read()

        while (record) {
            if ((Object.keys(record).length == 1)) {
                if (record[Object.keys(record)[0]].length != 0) {
                    errorLectura = "Delimitador debe ser ';' "

                    return false
                }
            } else {
                linesRead++
                if(record.codigo != undefined && record.codigo != null && record.codigo != '') {
                    output.push({
                        itemId : record.codigo,
                        itemDescription : record.nombre,
                        quantity : (record.cantidad == undefined || record.cantidad == null || record.cantidad == '') ? 0 : record.cantidad,
                        disscount : (record.desc == undefined || record.desc == null || record.desc == '') ? 0 : record.desc,
                        netPrice : (record.precio == undefined || record.precio == null || record.precio == '') ? 0 : record.precio,
                        referenceKey : (record.referencia == undefined || record.referencia == null || record.referencia == '') ? 0 : record.referencia
                    })
                }
            }

            record = parser.read()
        }
    })

    parser.on('end', function () {
        if (linesRead == 0) {
            res.status(500).json({ status: false, error: 'Archivo Vacio' })

            return false
        }

        res.json({
            data    : output,
            status     : 1,
        })
    })

    parser.on('error', function () {
        res.status(500).json({ status: false, error: 'Error de Formato' })

        return false
    })

    source.pipe(parser)
}

function getGuides(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getGuides(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGuidesDetails(req, res) {

    var query = [];
    var transferGuideId = req.query['transferGuideId'];
    query.push(model._getGuidesDetails(transferGuideId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createGuide(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createGuide(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsGuideList(data.itemsInvoices[i],result[0],data.originId))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create Guide",
            status: 0
        });
    }
}

function createGuideSales(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createGuide(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsGuideListSales(data.itemsInvoices[i],result[0],data.originId))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create Guide",
            status: 0
        });
    }
}

// function validateGuide(req, res) {

//     var query = [];
//     var data = req.body;
//     if(req.body){
//         Promise.all([model._validateGuide(data)]).then((result) => {
//             query=[]
//             for(var i in data.itemsInvoices){
//                 query.push(model._updateStock(data.itemsInvoices[i],data.destination))
//             }

//             Promise.all(query).then((results) => {

//                 res.json({
//                     data: result[0],
//                     status: 1
//                 });

//             }).catch(function(error) {
//                 res.json({
//                     data: error,
//                     status: 0
//                 });
//             })
    
//         }).catch(function(error) {
//             res.json({
//                 data: error,
//                 status: 0
//             });
//         })

//     }else{
//         res.json({
//             data: [],
//             messagge:"not data this services validate Guide",
//             status: 0
//         });
//     }
// }

function validateGuide(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._validateGuide(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateGuideStock(req, res) {

    var query = [];
    var data = req.body;
    for(var i in data.itemsInvoices){
                query.push(model._updateStock(data.itemsInvoices[i],data.destination))
            }

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function rejectedGuide(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._rejectedGuide(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._updateStock(data.itemsInvoices[i],data.origin))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services validate Guide",
            status: 0
        });
    }
}

function guideDecline(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._rejectedGuide(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateGuide(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._updateGuide(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledGuide(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledGuide(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGuidesItems(req, res) {

    var query = [];
    var transferGuideId = req.query['transferGuideId'];
    query.push(model._getGuidesItems(transferGuideId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

//Stock  Adjustment Guides 

function getStockAdjustmentGuides(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getStockAdjustmentGuides(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getStockAdjustmentGuidesDetails(req, res) {

    var query = [];
    var guideId = req.query['transferGuideId'];
    query.push(model._getStockAdjustmentGuidesDetails(guideId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function createStockAdjustment(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createStockAdjustment(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsStockAdjustment(data.itemsInvoices[i],result[0],data.originId))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create Guide",
            status: 0
        });
    }
}

function validateStockAdjustment(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._validateStockAdjustment(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._updateStockGuideAdjustment(data.itemsInvoices[i],data.origin))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services validate Guide",
            status: 0
        });
    }
}

function validateStockAdjustmentDecrease(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._validateStockAdjustment(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._updateStockGuideAdjustmentDecrease(data.itemsInvoices[i],data.origin))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services validate Guide",
            status: 0
        });
    }
}

function updateStockAdjustment(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._updateStockAdjustment(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function rejectedStockAdjustment(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._rejectedStockAdjustment(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledStockAdjustment(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledStockAdjustment(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getStockAdjustmentItems(req, res) {

    var query = [];
    var transferGuideId = req.query['transferGuideId'];
    query.push(model._getStockAdjustmentItems(transferGuideId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGuidesItemsLocation(req, res) {

    var query = [];
    var transferGuideId = req.query['transferGuideId'];
    var destination = req.query['destination'];
    var origin = req.query['origin'];
    query.push(model._getGuidesItemsLocation(transferGuideId,destination,origin))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getGuides: getGuides,
    getGuidesDetails:getGuidesDetails,
    createGuide:createGuide,
    createGuideSales:createGuideSales,
    validateGuide:validateGuide,
    updateGuideStock:updateGuideStock,
    rejectedGuide:rejectedGuide,
    guideDecline:guideDecline,
    updateGuide:updateGuide,
    disabledGuide:disabledGuide,
    getGuidesItems:getGuidesItems,
    getStockAdjustmentGuides:getStockAdjustmentGuides,
    getStockAdjustmentGuidesDetails:getStockAdjustmentGuidesDetails,
    createStockAdjustment:createStockAdjustment,
    validateStockAdjustment:validateStockAdjustment,
    updateStockAdjustment:updateStockAdjustment,
    validateStockAdjustmentDecrease:validateStockAdjustmentDecrease,
    rejectedStockAdjustment:rejectedStockAdjustment,
    disabledStockAdjustment:disabledStockAdjustment,
    getStockAdjustmentItems:getStockAdjustmentItems,
    getGuidesItemsLocation:getGuidesItemsLocation,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
    uploadGuides
}