var express = require('express');
const multer = require('multer')

// view engine setup
var router = express.Router();
var service = require('./services');
const upload = multer({ dest: 'files/' })


//transfer guides services
router.get('/transfer/list', service.getGuides);
router.get('/transfer', service.getGuidesDetails);
router.post('/transfer', service.createGuide);
router.put('/transfer', service.updateGuide);
router.delete('/transfer', service.disabledGuide);
router.put('/transfer/validate', service.validateGuide);
router.put('/transfer/stock', service.updateGuideStock);
router.put('/transfer/rejected', service.rejectedGuide);
router.put('/transfer/decline', service.guideDecline);
router.post('/transfer/sales', service.createGuideSales);
router.get('/transfer/items', service.getGuidesItems);
router.get('/transfer/items-locations', service.getGuidesItemsLocation);
router.post('/uploadGuides', upload.single('uploadGuides'), service.uploadGuides);

//stock adjustment guides services
router.get('/stock-adjustment/list', service.getStockAdjustmentGuides);
router.get('/stock-adjustment/', service.getStockAdjustmentGuidesDetails);
router.post('/stock-adjustment/', service.createStockAdjustment);
router.put('/stock-adjustment/', service.updateStockAdjustment);
router.delete('/stock-adjustment', service.disabledStockAdjustment);
router.get('/stock-adjustment/items', service.getStockAdjustmentItems);
router.put('/stock-adjustment/validate', service.validateStockAdjustment);
router.put('/stock-adjustment/decrease', service.validateStockAdjustmentDecrease);
router.put('/stock-adjustment/rejected', service.rejectedStockAdjustment);


module.exports = router;