'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var dict = {"á":"a", "Á":"A", "é":"e", "É":"E","ó":"o", "Ó":"O","í":"i", "Í":"I","ñ":"n", "Ñ":"N","ü":"u"};

var Q = require('q');

/**
 * Busca las modalidades a partir de un queryvalue
 *
 * @param {string} queryValue Valor buscado
 * @since 2018/07/08
 * @see creación
 * @author Merge&Deploy.
 */



function _getList(userId){

    var deferred = Q.defer()

    let sql = ` SELECT * FROM items where status=1 `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getpermissions(userId){
	var deferred = Q.defer()

	sequelize.query(`  SELECT 		fet.ds_name,
	                                 rsp.ds_permission,
									sfp.ds_url as url_path
						FROM  		role sr
						inner join 	rolpermission rsp on sr.roId=rsp.id_role and rsp.is_active=1
						inner join 	permission sfp on sfp.id_feature=rsp.id_feature
						inner join  users sru on sru.usRole=sr.roId
						inner join 	features fet on fet.id_feature=rsp.id_feature
						where 		sru.usId=:id and sr.roVisible = 1
						group by 	rsp.id_feature,rsp.ds_permission,sfp.ds_url;`, {

		replacements: {
			id: userId
		},

		type: sequelize.QueryTypes.SELECT
	}).then(function (permissions) {

		var obj=[],obj1={},name1='';

		for(var i in permissions){
			var spliter=permissions[i].ds_permission ? permissions[i].ds_permission.split(',') : permissions[i].ds_permission




			for(var j in spliter){
				name1=permissions[i].ds_name.replace(/[^\w ]/g, function(char) {return dict[char] || char;});

				name1= name1.toLowerCase()+' '+spliter[j].toLowerCase();
				name1= name1.replace(/ /g,"-");
				obj1[name1]=true;
			}

		}

		obj.push(obj1)

		deferred.resolve(obj);
	}).catch(function (error){
		deferred.reject(error);
	});

	return deferred.promise;
}


function _getpermissionsDetailsUrl(userId){

	var deferred = Q.defer()

	sequelize.query(`select sfp.id_feature, sfp.ds_url
							FROM features fet
							inner join (
								select id_feature, n, substring_index(substring_index(ds_url, ',', n),',', -1) as ds_url
								from (select 1 n union all
									select 2 union all select 3 union all
									select 4 union all select 5 union all
									select 6 union all select 7 union all
									select 8 union all select 9 union all
									select 10 union all select 11 union all
									select 12 union all select 13 union all
									select 14 union all select 15) as numbers
								inner JOIN permission
								on char_length(ds_url) - char_length(replace(ds_url, ',', ''))>= n - 1
								order by id_feature, n
							) sfp on sfp.id_feature=fet.id_feature
							inner join rolpermission srp on srp.id_feature= sfp.id_feature and srp.is_active=1
							inner join  users sru on sru.usRole=srp.id_role
							where sru.usId=:id
							group by sfp.ds_url order by sfp.id_feature;`,{
		replacements: {
			id: userId
		},

		type: sequelize.QueryTypes.SELECT

	}).then(function (permissionsUrl) {

		var permUrl=[];

		for(var i in permissionsUrl){
			permUrl.push(permissionsUrl[i].ds_url);
		}

		deferred.resolve(permUrl);
	}).catch(function (error){
		deferred.reject(error);
	});

	return deferred.promise;
}


function _getItemsOutputs(){

    var deferred = Q.defer()

    let sql = `SELECT items.id AS itemId,
                       items.dsName AS itemDescription,
                       group_concat(distinct itemreferences.dsReference) AS "references",
                         clients.dsName AS clientName,
                         guides.id AS guideId,
                         guidesitems.nmUnities AS quantityItems,
                         DATE_FORMAT(guides.date, '%d/%m/%y') as date,
                         guides.origin,
                         guides.destination

                FROM items, clients, guides, guidesitems , itemreferences
                WHERE guides.id = guidesitems.guideId
                AND itemreferences.itemId = items.id
                AND guidesitems.itemId = items.id
                AND guides.clientId = clients.id
                AND guides.inStatus = 1
                GROUP BY  guidesitems.id
                ORDER BY guides.id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getUsersList(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    u.usId as "id",
                    u.rut as "rut",
                    u.usName as "userName",
                    u.usLastName as  "fullName",
                    CONCAT(u.day,'/',u.month,'/',u.year) AS birthday_date,
                    u.phone as  "phone",
                    u.usMail as  "email",
                    r.roName as  "rol",
                    IF(u.is_active=1, 'Active','Inactive') as active

                    FROM users u
                    inner JOIN  role r
                    ON  u.usRole = r.roId
                    where usVisible = 1
                    order by usId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getUsersDetails(userId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    u.usId as "id",
                    u.rut as "rut",
                    u.usPass as "password",
                    u.usName as "userName",
                    u.usLastName as  "fullName",
                    CONCAT(u.day,'/',u.month,'/',u.year) AS birthday_date,
                    u.phone as  "phone",
                    u.usMail as  "email",
                    u.address as  "address",
                    u.cityName,
                    u.day as  "day",
                    u.month as  "month",
                    u.year as  "year",
                    r.roName as  "rol",
                    u.gender as "genderId",
                    if(u.gender=1, 'Masculino','Femenino') AS "gender",
                    u.nationality as "nationality",
                    DATE_FORMAT(u.startDate, '%Y-%m-%d') as startDate,
                    DATE_FORMAT(u.finishDate, '%Y-%m-%d') as finishDate,
                    if(u.contractType=1, 'Indefinido','Plazo Fijo') AS "contractTypeName",
                    u.contractType as "contractType",
                    u.baseSalary as baseSalary,
                    u.costCenter as costCenterId,
                    w.dsCode as costCenterName,
                    re.name as "retirementName",
                    u.retirement as "retirementId",
                    h.name as "healthSystemName",
                    u.healthSystem as "healthSystemId",
                    i.name as "isapreName",
                    u.isapreId as "isapreId",
                    u.percentagePlan as "percentagePlan",
                    c.name as "compensationBoxName",
                    u.compensationBox as "compensationBoxId",
                    u.bank as "bank",
                    b.dsName as "bankName",
                    u.bankAccountType as "bankAccountType",
                    u.accountNumber as "accountNumber",
                    u.maritalStatus as "maritalStatus",
                    u.numberOfChildren as "numberOfChildren",
                    u.shirtSize as "shirtSize",
                    u.shoeSize as "shoeSize",
                    u.lastEmployer as "lastEmployer",
                    u.scholarShip as "scholarShip",
                    u.profession as "profession",
                    u.observation as "observation",
                    if(u.is_active=1, 'Activo','Inactivo') AS "isActive"

                    FROM users u
                    left JOIN  role r
                    ON  u.usRole = r.roId
                    left JOIN  retirement re
                    ON  u.retirement = re.id
                    left JOIN  healthsystem h
                    ON  u.healthSystem = h.id
                    left JOIN  isapres i
                    ON  u.isapreId = i.id
                    left JOIN  compensationbox c
                    ON  u.compensationBox = c.id
                    left JOIN  banks b
                    ON  u.bank = b.id
                    left JOIN  warehouse w
                    ON  u.costCenter = w.id
                    WHERE u.usId = :userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getUserWarehouses(userId){

    var deferred = Q.defer()

    let sql = `
                    SELECT DISTINCT
                    w.id as  "id",
                    w.dsName as  "nameDescription",
                    w.dsCode as  "code"
                    FROM userwarehouse u
                    inner JOIN  warehouse w
                    ON  u.warehouseid = w.id
                    WHERE u.usId = :userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _userCreate(data){

    var deferred = Q.defer()
    if(data.cityName==undefined) data.cityName="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.gender==undefined) data.gender=null
    if(data.nationality==undefined) data.nationality='no definida'
    if(data.startDate==undefined) data.startDate=null
    if(data.finishDate==undefined) data.finishDate=null
    if(data.documentType==undefined) data.documentType=null
    if(data.retirementId==undefined) data.retirementId=null
    if(data.healthSystemId==undefined) data.healthSystemId=null
    if(data.isapreId==undefined) data.isapreId=null
    if(data.percentagePlan==undefined) data.percentagePlan=null
    if(data.compensationBoxId==undefined) data.compensationBoxId=null
    if(data.bankId==undefined) data.bankId=null
    if(data.bankAccountType==undefined) data.bankAccountType='no definido'
    if(data.baseSalary==undefined) data.baseSalary=380000
    if(data.accountNumber==undefined) data.accountNumber='por definir'
    if(data.maritalStatus==undefined) data.maritalStatus='indefinido'
    if(data.numberOfChildren==undefined) data.numberOfChildren=0
    if(data.warehouseId==undefined) data.warehouseId=7
    if(data.shirtSize==undefined) data.shirtSize=null
    if(data.shoeSize==undefined) data.shoeSize=null
    if(data.lastEmployer==undefined) data.lastEmployer="no definido"
    if(data.scholarShip==undefined) data.scholarShip='indefinido'
    if(data.profession==undefined) data.profession='indefinido'

    let sql = ` INSERT INTO users (
                                usLastName,
                                usName,
                                usPass,
                                usMail,
                                usCreate,
                                usRole,
                                is_active,
                                birthday_date,
                                rut,
                                address,
                                phone,
                                cityName,
                                day,
                                month,
                                year,
                                gender,
                                nationality,
                                startDate,
                                finishDate,
                                contractType,
                                retirement,
                                healthSystem,
                                isapreId,
                                percentagePlan,
                                compensationBox,
                                bank,
                                bankAccountType,
                                accountNumber,
                                maritalStatus,
                                numberOfChildren,
                                shirtSize,
                                shoeSize,
                                costCenter,
                                baseSalary,
                                lastEmployer,
                                scholarShip,
                                profession)
                      VALUES (  :fullName,
                                :userName,
                                :password,
                                :email,
                                NOW(),
                                :rolId,
                                1,
                                :birthdayDate,
                                :rut,
                                :address,
                                :phone,
                                :cityName,
                                :day,
                                :month,
                                :year,
                                :gender,
                                :nationality,
                                :startDate,
                                :finishDate,
                                :documentType,
                                :retirementId,
                                :healthSystemId,
                                :isapreId,
                                :percentagePlan,
                                :compensationBoxId,
                                :bankId,
                                :bankAccountType,
                                :accountNumber,
                                :maritalStatus,
                                :numberOfChildren,
                                :shirtSize,
                                :shoeSize,
                                :warehouseId,
                                :baseSalary,
                                :lastEmployer,
                                :scholarShip,
                                :profession)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            fullName: data.fullName,
            userName: data.userName,
            password: data.password,
            email: data.email,
            rolId: data.rolId,
            birthdayDate: data.birthdayDate,
            rut: data.rut,
            address: data.address,
            phone: data.phone,
            cityName: data.cityName,
            day: data.day,
            month: data.month,
            year: data.year,
            gender:data.gender,
            nationality:data.nationality,
            startDate:data.startDate,
            finishDate:data.finishDate,
            documentType:data.documentType,
            retirementId:data.retirementId,
            healthSystemId:data.healthSystemId,
            isapreId:data.isapreId,
            percentagePlan:data.percentagePlan,
            compensationBoxId:data.compensationBoxId,
            bankId:data.bankId,
            bankAccountType:data.bankAccountType,
            accountNumber:data.accountNumber,
            maritalStatus:data.maritalStatus,
            numberOfChildren:data.numberOfChildren,
            shirtSize:data.shirtSize,
            shoeSize:data.shoeSize,
            warehouseId:data.warehouseId,
            baseSalary:data.baseSalary,
            lastEmployer:data.lastEmployer,
            scholarShip:data.scholarShip,
            profession:data.profession
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _userCreateMobile(data){
    if(data.cityName==undefined) data.cityName="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.gender==undefined) data.gender= null
    if(data.nationality==undefined) data.nationality='no definida'
    if(data.startDate==undefined) data.startDate= null
    if(data.finishDate==undefined) data.finishDate= null
    if(data.documentType==undefined) data.documentType= null
    if(data.retirementId==undefined) data.retirementId= null
    if(data.healthSystemId==undefined) data.healthSystemId= null
    if(data.isapreId==undefined) data.isapreId= null
    if(data.percentagePlan==undefined) data.percentagePlan= null
    if(data.compensationBoxId==undefined) data.compensationBoxId= null
    if(data.bankId==undefined) data.bankId= null
    if(data.bankAccountType==undefined) data.bankAccountType='no definido'
    if(data.baseSalary==undefined) data.baseSalary=380000
    if(data.accountNumber==undefined) data.accountNumber='por definir'
    if(data.maritalStatus==undefined) data.maritalStatus='indefinido'
    if(data.numberOfChildren==undefined) data.numberOfChildren=0
    if(data.warehouseId==undefined) data.warehouseId=7
    if(data.shirtSize==undefined) data.shirtSize= null
    if(data.shoeSize==undefined) data.shoeSize= null
    if(data.lastEmployer==undefined) data.lastEmployer="no definido"
    if(data.scholarShip==undefined) data.scholarShip='indefinido'
    if(data.profession==undefined) data.profession='indefinido'
    if(data.birthdayDate==undefined) data.birthdayDate= null
    if(data.rolId==undefined) data.rolId= 15
    if(data.day==undefined) data.day= null
    if(data.month==undefined) data.month= null
    if(data.year==undefined) data.year= null
    if(data.phone==undefined) data.phone= null


    const sql = ` INSERT INTO users (
                                usLastName,
                                usName,
                                usPass,
                                usMail,
                                usCreate,
                                usRole,
                                is_active,
                                birthday_date,
                                rut,
                                address,
                                phone,
                                cityName,
                                day,
                                month,
                                year,
                                gender,
                                nationality,
                                startDate,
                                finishDate,
                                contractType,
                                retirement,
                                healthSystem,
                                isapreId,
                                percentagePlan,
                                compensationBox,
                                bank,
                                bankAccountType,
                                accountNumber,
                                maritalStatus,
                                numberOfChildren,
                                shirtSize,
                                shoeSize,
                                costCenter,
                                baseSalary,
                                lastEmployer,
                                scholarShip,
                                profession)
                      VALUES (  :fullName,
                                :userName,
                                :password,
                                :email,
                                NOW(),
                                :rolId,
                                1,
                                :birthdayDate,
                                :rut,
                                :address,
                                :phone,
                                :cityName,
                                :day,
                                :month,
                                :year,
                                :gender,
                                :nationality,
                                NOW(),
                                :finishDate,
                                :documentType,
                                :retirementId,
                                :healthSystemId,
                                :isapreId,
                                :percentagePlan,
                                :compensationBoxId,
                                :bankId,
                                :bankAccountType,
                                :accountNumber,
                                :maritalStatus,
                                :numberOfChildren,
                                :shirtSize,
                                :shoeSize,
                                :warehouseId,
                                :baseSalary,
                                :lastEmployer,
                                :scholarShip,
                                :profession)`

	return sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            fullName: data.fullName,
            userName: data.userName,
            password: data.password,
            email: data.email,
            rolId: data.rolId,
            birthdayDate: data.birthdayDate,
            rut: data.rut,
            address: data.address,
            phone: data.phone,
            cityName: data.cityName,
            day: data.day,
            month: data.month,
            year: data.year,
            gender:data.gender,
            nationality:data.nationality,
            startDate:data.startDate,
            finishDate:data.finishDate,
            documentType:data.documentType,
            retirementId:data.retirementId,
            healthSystemId:data.healthSystemId,
            isapreId:data.isapreId,
            percentagePlan:data.percentagePlan,
            compensationBoxId:data.compensationBoxId,
            bankId:data.bankId,
            bankAccountType:data.bankAccountType,
            accountNumber:data.accountNumber,
            maritalStatus:data.maritalStatus,
            numberOfChildren:data.numberOfChildren,
            shirtSize:data.shirtSize,
            shoeSize:data.shoeSize,
            warehouseId:data.warehouseId,
            baseSalary:data.baseSalary,
            lastEmployer:data.lastEmployer,
            scholarShip:data.scholarShip,
            profession:data.profession
        }
    })
}

function _userWarehouseAssociate (warehouseId,userId){

    var deferred = Q.defer()

    let sql = ` INSERT INTO userwarehouse (usId,warehouseId,inStatus) VALUES (:userId,:warehouseId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:userId,
            warehouseId:warehouseId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _deleteUserWarehouse (userId){

    var deferred = Q.defer()

    let sql = ` DELETE FROM userwarehouse WHERE usId=:userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:userId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateInfoServices(data){

    var deferred = Q.defer()
    if(data.cityName==undefined) data.cityName="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.finishDate==undefined) data.finishDate=null

    let sql = `UPDATE  users SET
                                 usLastName=:fullName,
                                 rut=:rut,
                                 address=:address,
                                 phone=:phone,
                                 usMail=:email,
                                 birthday_date=:birthdayDate,
                                 cityName=:cityName,
                                 day=:day,
                                 month=:month,
                                 year=:year,
                                 gender=:gender,
                                 nationality=:nationality,
                                 startDate=:startDate,
                                 finishDate=:finishDate

                          WHERE  usId=:userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId: data.userId,
            fullName: data.fullName,
            rut: data.rut,
            address: data.address,
            phone: data.phone,
            email: data.email,
            birthdayDate: data.birthdayDate,
            day: data.day,
            month: data.month,
            year: data.year,
            cityName: data.cityName,
            gender: data.gender,
            nationality: data.nationality,
            startDate: data.startDate,
            finishDate: data.finishDate

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateUserRole(data){

    var deferred = Q.defer()

    let sql = `UPDATE users SET usRole=:rolId WHERE usId=:userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId: data.userId,
            rolId: data.rolId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _contractTypeUpdate(data){

    var deferred = Q.defer()

    let sql = `UPDATE users SET contractType=:contractType WHERE usId=:userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId: data.userId,
            contractType: data.contractType
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateAccessServices(data){

    var deferred = Q.defer()

    let sql = `UPDATE  users SET
                                 usName=:userName,
                                 usPass=:password
                          WHERE  usId=:userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId: data.userId,
            userName: data.userName,
            password: data.password

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getRetirements(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        r.id as "id",
                        r.name as "name",
                        r.code as "code",
                        r.rate as  "rate",
                        r.sis as  "sis",
                        r.value as  "value"

                        FROM retirement r
                        order by id asc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getIsapresList(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        i.id as "id",
                        i.name as "name",
                        i.code as "code",
                        i.rate as  "rate",
                        i.percentage as  "percentage",
                        i.value as  "value"

                        FROM isapres i
                        WHERE id != 0
                        order by id asc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCompensationBoxList(){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        c.id as "id",
                        c.name as "name",
                        c.code as "code",
                        c.rate as  "rate",
                        c.percentage as  "percentage",
                        c.value as  "value"

                        FROM compensationBox c
                        order by id asc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

    function _retirementsCreate(data){

        var deferred = Q.defer()

        let sql = ` INSERT INTO retirement (name,code,rate,sis,date)
                        VALUES           (:name,:code,:rate,:sis,NOW())`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                name: data.name,
                code: data.code,
                rate: data.rate,
                sis: data.sis

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }


    function _retirementsUpdate(data){

        var deferred = Q.defer()

        let sql = `UPDATE  retirement SET
                                     name=:name,
                                     code=:code,
                                     rate=:rate,
                                     sis=:sis,
                                     updateDate=NOW()
                              WHERE  id=:id`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                id: data.id,
                name: data.name,
                code: data.code,
                rate: data.rate,
                sis: data.sis

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }

    function _isapresCreate(data){

        var deferred = Q.defer()

        let sql = ` INSERT INTO isapres (name,code,rate,date)
                        VALUES           (:name,:code,:rate,NOW())`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                name: data.name,
                code: data.code,
                rate: data.rate

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }

    function _isapresUpdate(data){

        var deferred = Q.defer()

        let sql = `UPDATE  isapres SET
                                     name=:name,
                                     code=:code,
                                     rate=:rate,
                                     updateDate=NOW()
                              WHERE  id=:id`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                id: data.id,
                name: data.name,
                code: data.code,
                rate: data.rate

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }

    function _compensationBoxCreate(data){

        var deferred = Q.defer()

        let sql = ` INSERT INTO compensationbox (name,code,rate,date)
                        VALUES           (:name,:code,:rate,NOW())`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                name: data.name,
                code: data.code,
                rate: data.rate

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }


    function _compensationBoxUpdate(data){

        var deferred = Q.defer()

        let sql = `UPDATE  compensationbox SET
                                     name=:name,
                                     code=:code,
                                     rate=:rate,
                                     updateDate=NOW()
                              WHERE  id=:id`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                id: data.id,
                name: data.name,
                code: data.code,
                rate: data.rate

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }


    function _statusUserUpdate(data){

        var deferred = Q.defer()

        let sql = `UPDATE users SET is_active=:value WHERE usId=:userId`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                userId: data.userId,
                value: data.value
            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }

    function _updateUserInfoInstitutions(data){

        var deferred = Q.defer()
        if(data.isapreId==undefined) data.isapreId=null
        if(data.percentagePlan==undefined) data.percentagePlan=0
        if(data.baseSalary==undefined) data.baseSalary=380000

        let sql = `UPDATE  users SET
                                     retirement=:retirementId,
                                     healthSystem=:healthSystemId,
                                     isapreId=:isapreId,
                                     percentagePlan=:percentagePlan,
                                     compensationBox=:compensationBoxId,
                                     baseSalary=:baseSalary,
                                     bank=:bankId,
                                     accountNumber=:accountNumber,
                                     bankAccountType=:bankAccountTypeId

                              WHERE  usId=:userId`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                    userId: data.userId,
                    retirementId: data.retirementId,
                    healthSystemId:  data.healthSystemId,
                    isapreId: data.isapreId,
                    percentagePlan: data.percentagePlan,
                    compensationBoxId:  data.compensationBoxId,
                    baseSalary:  data.baseSalary,
                    bankId: data.bankId,
                    accountNumber: data.accountNumber,
                    bankAccountTypeId: data.bankAccountTypeId

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }


    function _updateUserBlockPersonalInfo(data){

        var deferred = Q.defer()
        if(data.observation==undefined) data.observation=""
        if(data.shirtSize==undefined) data.shirtSize=null
        if(data.shoeSize==undefined) data.shoeSize=null
        if(data.lastEmployer==undefined) data.lastEmployer='no definido'
        if(data.numberOfChildren==undefined) data.numberOfChildren=0
        if(data.scholarShip==undefined) data.scholarShip='no definido'
        if(data.profession==undefined) data.profession='no definido'
        if(data.warehouseId==undefined) data.warehouseId=7

        let sql = `UPDATE  users SET
                                     maritalStatus=:maritalStatus,
                                     numberOfChildren=:numberOfChildren,
                                     shirtSize=:shirtSize,
                                     shoeSize=:shoeSize,
                                     lastEmployer=:lastEmployer,
                                     scholarShip=:scholarShip,
                                     costCenter=:warehouseId,
                                     profession=:profession,
                                     observation=:observation

                              WHERE  usId=:userId`

        sequelize.query(sql, {
            type: Sequelize.QueryTypes.INSERT,
            replacements:{
                userId: data.userId,
                maritalStatus: data.maritalStatus,
                numberOfChildren:  data.numberOfChildren,
                shirtSize: data.shirtSize,
                shoeSize: data.shoeSize,
                lastEmployer:  data.lastEmployer,
                scholarShip: data.scholarShip,
                warehouseId: data.warehouseId,
                profession: data.profession,
                observation: data.observation

            }
        }).then(function (result) {
            deferred.resolve(result)
        }).catch(function (error){
            deferred.reject(error)
        })

        return deferred.promise
    }


function _keyValidate(userId,key){

    var deferred = Q.defer()

    let sql = `
                    SELECT
                    u.usId,
                    u.usName as  "userName",
                    u.usMail  "usMail",
                    u.key AS  "key"
                    FROM users u
                    WHERE usId = :userId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:userId,
            key:key
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _userValidate(username){
	const sql = `SELECT * FROM users WHERE usName=:username `

    return sequelize.query(sql, {
				type: Sequelize.QueryTypes.SELECT,
				replacements:{
					username:username
				}
			})
}

function _getUser(userId){
	const sql = `SELECT * FROM users WHERE usId=:userId  `

    return sequelize.query(sql, {
				type: Sequelize.QueryTypes.SELECT,
				replacements:{
					userId : userId
				}
			})
}

module.exports = {
	_getList: _getList,
	_getpermissions:_getpermissions,
	_getpermissionsDetailsUrl:_getpermissionsDetailsUrl,
	_getUsersList:_getUsersList,
	_getUsersDetails:_getUsersDetails,
    _getUserWarehouses:_getUserWarehouses,
    _userCreate:_userCreate,
    _userWarehouseAssociate:_userWarehouseAssociate,
    _updateInfoServices:_updateInfoServices,
    _updateUserRole:_updateUserRole,
    _updateAccessServices:_updateAccessServices,
    _deleteUserWarehouse:_deleteUserWarehouse,
    _getRetirements:_getRetirements,
    _getIsapresList:_getIsapresList,
    _getCompensationBoxList:_getCompensationBoxList,
    _retirementsCreate:_retirementsCreate,
    _retirementsUpdate:_retirementsUpdate,
    _isapresCreate:_isapresCreate,
    _isapresUpdate:_isapresUpdate,
    _compensationBoxCreate:_compensationBoxCreate,
    _compensationBoxUpdate:_compensationBoxUpdate,
    _statusUserUpdate:_statusUserUpdate,
    _contractTypeUpdate:_contractTypeUpdate,
    _updateUserInfoInstitutions:_updateUserInfoInstitutions,
    _updateUserBlockPersonalInfo:_updateUserBlockPersonalInfo,
    _keyValidate:_keyValidate,
	_userCreateMobile,
	_userValidate,
	_getUser,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}
