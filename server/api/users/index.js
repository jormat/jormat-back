var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');
var loginService = require('./serviceLogin');
const MAX_SIZE = 10 * 1024 * 1024;  // 10 MB
const multer = require('multer')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (file.size > MAX_SIZE) {
            cb(new Error('El archivo debe pesar a lo mas 10 MB'));
        } else {
            cb(null, 'files/users/');
        }

    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
}),
    upload = multer({
        storage,
        limit: {
            fileSize: MAX_SIZE
        }
    })

router.get('/getUserDetails', service.getUserDetailById);
router.get('/getUsers', service.getUsers);
router.get('/getRole', service.getRole);
router.get('/getGroups', service.getGroups);
router.get('/getAllPermissionsByrole', service.getAllPermissionsByrole);
router.get('/validateUser', service.validateUser);
router.get('/listallpermissions', service.listallpermissions);
router.post('/insertUserInfo', service.insertUserInfo);
router.post('/deactiveUsers', service.deactiveUsers);
router.post('/updateUserInfo', service.updateUserInfo);
router.post('/insertRoles', service.insertRoles);
router.post('/updateRole', service.updateRole);
router.post('/insertGroups', service.insertGroups);
router.post('/updateGroup', service.updateGroup);
router.post('/insertRolePermission', service.insertRolePermission);
router.post('/deactiveRole', service.deactiveRole);
router.post('/deactiveGroups', service.deactiveGroups);
router.get('/getUsersByName', service.getUsersByName);
router.get('/getModuleByProduct', service.getModuleByProduct);
router.post('/insertFeatureByName', service.insertFeatureByName);
router.get('/getAllfeaturePermissions', service.getpermissions);
router.post('/deletefeaturePermissions', service.deletefeaturePermissions);

router.post('/login',loginService.loginUsers);

//staff new services

router.get('/list', service.getUsersList);
router.get('/', service.getUsersDetails);
router.get('/warehouses', service.getUserWarehouses);
router.post('/',service.userCreate);
router.post('/userCreateMobile',service.userCreateMobile);
router.put('/info', service.updateInfoServices);
router.put('/status', service.statusUserUpdate);
router.put('/organization', service.updateOrganizationServices);
router.put('/institutions', service.updateUserInfoInstitutions);
router.put('/personal-info', service.updateUserBlockPersonalInfo);
router.put('/access', service.updateAccessServices);
router.get('/retirements', service.getRetirements);
router.get('/isapres', service.getIsapresList);
router.put('/contract-type', service.contractTypeUpdate);
router.get('/compensation-box', service.getCompensationBoxList);
router.post('/institutions/retirements',service.retirementsCreate);
router.put('/institutions/retirements',service.retirementsUpdate);
router.post('/institutions/isapres',service.isapresCreate);
router.put('/institutions/isapres',service.isapresUpdate);
router.post('/institutions/compensation-box',service.compensationBoxCreate);
router.put('/institutions/compensation-box',service.compensationBoxUpdate);
router.post('/uploadFiles/:userId', upload.array('files', 4),service.uploadFiles);
router.get('/uploadFiles/:userId', service.getFiles);
router.get('/file/:id', service.getFileUrl);
router.delete('/file', service.deleteFile)
router.get('/key-validate', service.keyValidate);

module.exports = router;
