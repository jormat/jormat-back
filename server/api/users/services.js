'use strict';
import sqldb from '../../sqldb';
var sequelize = sqldb.sequelize;
var Sequelize = require('sequelize');
var model = require('./model');
const serviceLogin = require('./serviceLogin');
const fs = require('fs');
const path = require('path')
const localenv = require('../../config/local.env.js');

    var dict = { "á": "a", "Á": "A", "é": "e", "É": "E", "ó": "o", "Ó": "O", "í": "i", "Í": "I", "ñ": "n", "Ñ": "N", "ü": "u" };


    var getUserDetailById = function (req, res) {
        var userId = req.query['id'];
        var object = { roles: []};

        var sqlUserAttr = ` SELECT
                            usr.usId AS userid
                            ,usr.usName AS username
                            ,usr.usName AS fullname
                            ,usr.usMail AS mail
                            ,usr.usPass AS pass
                            ,usr.ds_picture AS picture
                            ,IF(usr.is_active=1, 'Active','Inactive') AS is_active
                            ,usr.usCreate
                            ,usr.usPass AS prim_keys
                            ,DATE_FORMAT(usr.usLastLogin, '%Y-%m-%d %hh:%mm') AS lastLogingAt
                            ,usr.usRole
                            FROM users usr
                            WHERE usr.usId = :id -- AND usr.is_active = 0
                            GROUP BY usr.usId;`;
        sequelize.query(sqlUserAttr, { replacements: { id: userId }, type: sequelize.QueryTypes.SELECT })
            .then(function (users) {
                console.log( users[0]);
                if (typeof users[0] !== 'undefined') {
                    object.userid = users[0].userid;
                    object.username = users[0].username;
                    object.fullname = users[0].fullname;
                    object.mail = users[0].mail;
                    object.picture = users[0].picture;
                    object.is_active = users[0].is_active;
                    object.createdAt = users[0].createdAt;
                    object.prim_keys = users[0].prim_keys;
                    object.lastLogin = users[0].lastLogingAt;
                    object.ds_value = users[0].ds_value;
                    var sqlUser1 = `SELECT sr.roId as roleid,sr.roName as rolename
                                FROM role sr
                                WHERE sr.roId=:role and sr.roVisible = 1;`;
                    sequelize.query(sqlUser1, { replacements: {role:users[0].usRole }, type: sequelize.QueryTypes.SELECT })
                        .then(function (roles) {
                            object.roles = roles;
                                    res.json({ data: object, status: 0 });
                        });
                } else {
                    res.json({ data: [], status: 1 });

                }
            });
    }



    var getUsers = function (req, res) {
        var textSearch = req.query['txt'];
        var offset = (req.query['page']) ? ((req.query['page'] - 1) * 500) : 0;
        var sqlExtra = '';
        if (textSearch !== undefined && textSearch !== '') {
            sqlExtra = "AND usr.usName like '%" + textSearch + "%'";
        }
        sequelize.query(`select usr.usId as id,usr.usName as name , usr.usMail as email , usr.usLastName as lastName ,usr.usPass as pass ,
                            usr.ds_picture as picture,IF(usr.is_active=1, 'Active','Inactive') as active
                            from users usr
                            where usVisible=1 ` + sqlExtra + `
                            group by usr.usId order by usr.usId desc limit 500 offset `+ offset + `;`,
            { type: sequelize.QueryTypes.SELECT })
            .then(function (users) {
                res.json({ data: users, status: 0 });

            });
    }

    var validateUser = function (req, res) {
        var email = req.query['email'];
        var name = req.query['name'];
        sequelize.query("select * from users where ds_mail='" + email + "' or ds_name='" + name + "'",
            { type: sequelize.QueryTypes.SELECT })
            .then(function (userCheck) {
                if (!userCheck[0]) {
                    res.json({ data: userCheck, status: 0 });
                } else {
                    res.json({ data: userCheck, status: 1 });
                }
            });
    }


    var insertUserInfo = function (req, res) {
        var userInfo = req.body;
        var valuesRole = '';
        var valuesGroup = '';



        //userInfo.ds_picture
        var sqlMail = `SELECT * FROM users WHERE ds_mail = '` + userInfo.email + `'`;
        sequelize.query(sqlMail, { type: sequelize.QueryTypes.SELECT })
            .then(function (userCheck) {
                if (!userCheck[0]) {
                    // console.log(userInfo);
                    var sqlInsUser = `INSERT INTO users
                                            (usName,usPass,usMail,usCreate,usUpdate,is_active,usLastName,is_visible,usRole)
                                            VALUES(:ds_name,:ds_pass,:ds_mail,now(),now(),:is_active,:ds_fullname,1,:role);`
                    sequelize.query(sqlInsUser, {
                        replacements: {
                            ds_name: userInfo.username, ds_pass: userInfo.password, ds_mail: userInfo.email, is_active: 1,
                             ds_fullname: userInfo.username,role:userInfo.roles[0].roleid
                        }, type: sequelize.QueryTypes.INSERT
                    })
                        .then(function (userid) {
                                        res.json({ data: userid, status: 0 });
                                });

                } else {
                    res.json({ data: 'Email Exists', status: 1 });
                }

            });

    }

    var updateUserInfo = function (req, res) {
        var userInfo = req.body;
        var valuesRole = '', valuesGroup = '';
        var sqlUpInfo = `UPDATE  users
                         SET usName = :ds_name,usMail = :ds_mail,usUpdate = NOW(),
                            is_active = :is_active,usLastName = :ds_fullname,usRole = :roleId
                            `+ ((userInfo.password) ? ',usPass = :ds_pass' : '') + `
                        WHERE usId = :id`

        sequelize.query(sqlUpInfo, {
            replacements: {
                id: userInfo.user_id, ds_name: userInfo.username, ds_pass: userInfo.password,
                ds_mail: userInfo.email, is_active: userInfo.status,
                ds_fullname: userInfo.username,roleId:userInfo.roles[0].roleid
            }, type: sequelize.QueryTypes.UPDATE
        })
            .then(function (userid) {



                     res.json({ data: userInfo.roles[0].roleid, status: 0 });
            });
    }

    var deactiveUsers = function (req, res) {
        var user = req.body;

        sequelize.query(`UPDATE users SET is_active=:status,is_visible=:status where id_code=:id`, { replacements: { id: user.id_code, status: user.status }, type: sequelize.QueryTypes.UPDATE })
            .then(function (id) {
                res.json({ data: id, status: 0 });
            });
    }

    var getRole = function (req, res) {
        var rid = req.query['id'];
        var USER_ID = req.query['USER_ID'];
        var whereClause = ''
        console.log(rid);

        if (typeof rid !== 'undefined') {
            whereClause = ' where sr.roId=' + rid
        }

        if (typeof USER_ID !== 'undefined') {
            if (USER_ID == 1) {
                whereClause = ' where roVisible=1 '
            } else {
                whereClause = ' where roVisible in (1)'
            }

            if (typeof rid !== 'undefined') {
                whereClause += ' and  sr.roId=' + rid
            }
        }


        if (typeof rid !== 'undefined') {
            whereClause = ' where sr.roId=' + rid
        }
        sequelize.query(`select sr.roId as roleid,sr.roName as rolename,IF(sr.roVisible=1, 'Active','Inactive') as roVisible \n\
                                from  role sr `+ whereClause + ` order by sr.roId desc`,
            { type: sequelize.QueryTypes.SELECT })
            .then(function (roles) {
                res.json({ data: roles, status: 0 });
            });
    }

    var insertRoles = function (req, res) {
        var role = req.body;
        sequelize.query(`INSERT INTO role (roName, roDateCreate, roDateUpdate,
                            roVisible,roUserCreator,roUserModifier)
                            VALUES (:name, now(), now(), 1,:user_id,:user_id);`,
            { replacements: { name: role.name, user_id: role.user_id }, type: sequelize.QueryTypes.INSERT })
            .then(function (roles) {
                res.json({ data: roles, status: 0 });
            });
    }

    var updateRole = function (req, res) {
        var role = req.body;
        sequelize.query("UPDATE role SET roName=:desc,roDateUpdate=now(),roVisible=:status where roId=:id ",
            { replacements: { desc: role.name, id: role.roId, status: role.status }, type: sequelize.QueryTypes.UPDATE })
            .then(function (roles) {
                res.json({ data: roles, status: 0 });
            });
    }

    var getGroups = function (req, res) {
        var gid = req.query['id'];
        var whereClause = ''
        if (typeof gid !== 'undefined') {
            whereClause = ' where sg.id_code=' + gid
        }

        sequelize.query("select sg.id_code as groupid,sg.ds_description as groupname,IF(sg.is_active=1, 'Active','Inactive') as active\n\
                            from sec_groups sg "+ whereClause + ' order by sg.id_code desc', { type: sequelize.QueryTypes.SELECT })
            .then(function (groups) {
                res.json({ data: groups, status: 0 });
            });
    }

    var insertGroups = function (req, res) {
        var group = req.body;
        sequelize.query("INSERT INTO sec_groups (ds_description, createdAt, updatedAt, is_active,id_user_creator,id_user_modifier) VALUES (:id, now(), now(), 1 ,:user_id,:user_id);",
            { replacements: { id: group.name, user_id: group.user_id }, type: sequelize.QueryTypes.INSERT })
            .then(function (id) {
                res.json({ data: id, status: 0 });
            });
    }

    var updateGroup = function (req, res) {
        var group = req.body;
        sequelize.query("UPDATE sec_groups SET ds_description=:desc,updatedAt=now(),is_active=:status where id_code=:id ",
            { replacements: { desc: group.name, id: group.id_code, status: group.status }, type: sequelize.QueryTypes.UPDATE })
            .then(function (groups) {
                res.json({ data: groups, status: 0 });
            });
    }



    var deactiveRole = function (req, res) {
        var role = req.body;
        sequelize.query("UPDATE role SET roVisible=:status where roId=:id ",
            { replacements: { id: role.id_code, status: role.status }, type: sequelize.QueryTypes.UPDATE })
            .then(function (roles) {
                res.json({ data: roles, status: 1 });
            });
    }

    var deactiveGroups = function (req, res) {
        var group = req.body;
        sequelize.query("UPDATE sec_groups SET is_active=:status where id_code=:id ",
            { replacements: { id: group.id_code, status: group.status }, type: sequelize.QueryTypes.UPDATE })
            .then(function (groups) {
                res.json({ data: groups, status: 0 });
            });
    }


    var getAllPermissionsByrole = function (req, res) {


        var rid = req.query['id'];
        var whereClause = ' where rsp.id_role=null'
        var leftjoin = '';
        var select = '';
        if (typeof rid !== 'undefined') {
            whereClause = ' where  rsp.id_role=' + rid;
        }

        var obj = {}, obj1 = [];

        sequelize.query(`SELECT          sfp.ds_permissions,
                                        sf.ds_name as fname,
                                        sf.id_feature as fid,
                                        sm.id_module as mid,
                                        sm.ds_name as mname,
                                        '' as roleid,
                                        '' as rolename
                            FROM            modules sm
                            INNER JOIN      features sf
                            ON                      sf.id_module = sm.id_module
                            AND             sf.is_active = 1
                            INNER JOIN      permission sfp
                            ON                      sfp.id_feature = sf.id_feature
                            GROUP BY        sm.id_module,
                            sfp.id_feature
                            ORDER BY        sm.id_module,
                            sfp.id_feature desc`,
            {
                type: sequelize.QueryTypes.SELECT
            }).then(function (role) {

                sequelize.query(`SELECT     rsp.id_feature,
                                        rsp.ds_permission,
                                        sr.roName as rolename,
                                        sr.roId as roleid
                            FROM        role sr
                            INNER JOIN  rolpermission rsp
                            ON          sr.roId = rsp.id_role `+ whereClause,
                    {
                        type: sequelize.QueryTypes.SELECT
                    }).then(function (values) {

                        for (var i in role) {
                            var str_array = role[i].ds_permissions.split(',');

                            for (var j in str_array) {
                                var obj = {};
                                obj.ds_permissions = str_array[j];
                                obj.fname = role[i].fname;
                                obj.fid = role[i].fid;
                                obj.mid = role[i].mid;
                                obj.mname = role[i].mname;
                                obj.hasPermission = false;
                                obj.roleid = role[i].roleid;
                                obj.rolename = role[i].rolename;

                                for (var k in values) {
                                    // console.log('values[k].ds_permission,str_array[j],role[i].fid,values[k].id_feature',values[k].ds_permission,str_array[j],role[i].fid,values[k].id_feature)
                                    if (values[k].ds_permission == str_array[j] && role[i].fid == values[k].id_feature) {
                                        obj.hasPermission = true;
                                        obj.roleid = values[k].roleid;
                                        obj.rolename = values[k].rolename;
                                    }
                                }

                                obj1.push(obj);
                            }
                        }

                        res.json({
                            data: obj1,
                            status: 0
                        });
                    });
            });
    }


    var insertRolePermission = function (req, res) {
        var roleinfo = req.body;
        var value = '';
        // roleinfo={ "roleid": 13, "values": [ { "fid": 3, "mid": 1, "permission": "Edit" }, { "fid": 4, "mid": 2, "permission": "View" }, { "fid": 4, "mid": 2, "permission": "Edit" } ] };
        sequelize.query("delete from rolpermission where id_role=" + roleinfo.roleid, { type: sequelize.QueryTypes.DELETE })
            .then(function (del) {
                if (roleinfo.values.length > 0) {
                    for (var i in roleinfo.values) {
                        value += "(" + roleinfo.roleid + ",'" + roleinfo.values[i].permission + "'," + roleinfo.values[i].fid + ",1,1,1),"
                    }
                    var values = value.substring(0, value.length - 1);
                    sequelize.query("INSERT INTO rolpermission \n\
                        (id_role, ds_permission, id_feature,is_active,id_user_creator,id_user_modifier) VALUES" + values,
                        { type: sequelize.QueryTypes.INSERT })
                        .then(function (id) {
                            res.json({ data: id, status: 0 });
                        });
                } else {
                    res.json({ data: 0, status: 0 });
                }

            });

    }

    var dict = { "á": "a", "Á": "A", "é": "e", "É": "E", "ó": "o", "Ó": "O", "í": "i", "Í": "I", "ñ": "n", "Ñ": "N", "ü": "u" };
    var listallpermissions = function (req, res) {


        sequelize.query(`select sfp.ds_permissions,fet.ds_name,sfp.ds_url
                                    from  features fet
                                    inner join permission sfp on sfp.id_feature=fet.id_feature`,
            { type: sequelize.QueryTypes.SELECT })
            .then(function (permissions) {

                var obj = [], obj1 = {}, name1 = '';
                for (var i in permissions) {
                    name1 = permissions[i].ds_name.replace(/[^\w ]/g, function (char) { return dict[char] || char; });
                    name1 = name1.toLowerCase();
                    name1 = name1.replace(/ /g, "-");
                    obj1[name1] = { value: permissions[i] };
                }
                obj.push(obj1)

                res.jsonp({ data: obj });

            });



    }

    var getUsersByName = function (req, res) {
        var name = req.query['name'];
        sequelize.query(`select  usr.usId as id,usr.usName as name , usr.usMail as username ,usr.usPass as pass ,
                                usr.ds_picture as picture,IF(usr.is_active=1, 'Active','Inactive') as active
                                from users usr
                                where usVisible=1 and  (usr.usName like '%`+ name + `%' || usr.usMail like '%` + name + `%' ) and usr.usMail!=''
                                group by usr.usId order by usr.usId desc;`,
            { replacements: { name: name }, type: sequelize.QueryTypes.SELECT })
            .then(function (users) {
                res.json({ data: users, status: true });

            });
    }

    var getModuleByProduct = function (req, res) {

        sequelize.query(`SELECT ds_name,id_module,ds_product_name
                                                                            FROM modules
                                                                            group by id_module,ds_product_name
                                                                            order by ds_product_name `,
            { type: sequelize.QueryTypes.SELECT })
            .then(function (product) {

                if (product[0] != '') {

                    var obj = {}
                    for (var i in product) {

                        if (typeof obj[product[i].ds_product_name] === 'undefined') {
                            obj[product[i].ds_product_name] = []
                        }
                        obj[product[i].ds_product_name].push(product[i]);
                    }


                    res.json({ data: { dataObject: obj, product: product }, status: true });

                } else {
                    res.json({ data: null, status: true });
                }

            });
    }

    var insertFeatureByName = function (req, res) {
        var feature = req.body;


        if (typeof feature.featurePermissionId === 'undefined') {
            sequelize.query(`INSERT INTO features (ds_name,id_module, createdAt, updatedAt,
                            is_active,id_user_creator,id_user_modifier)
                            VALUES (:name,:product, now(), now(), 1,:user_id,:user_id);`,
                {
                    replacements: {
                        name: feature.feature,
                        product: feature.moduleId,
                        user_id: feature.user_id
                    }, type: sequelize.QueryTypes.INSERT
                })
                .then(function (featureId) {
                    sequelize.query(`INSERT INTO permission (id_feature,
                                                ds_permissions,
                                                                                                ds_url,
                                                                                                createdAt,
                                                                                                updatedAt,
                                                                                                is_active,
                                                                                                id_user_creator,
                                                                                                id_user_modifier)
                                                                                                VALUES     (:id_features,
                                                                                                                        :featurePermission,
                                                                                                                        :featureUrl,
                                                                                                                        now(),
                                                                                                                        now(), 1,
                                                                                                                        :user_id,
                                                                                                                        :user_id);`,
                        {
                            replacements: {
                                id_features: featureId,
                                featurePermission: feature.featurePermission,
                                featureUrl: feature.featureUrl,
                                user_id: feature.user_id
                            }, type: sequelize.QueryTypes.INSERT
                        })
                        .then(function (permissionId) {
                            res.json({ data: permissionId, status: 0 });

                        });
                });
        } else {

            sequelize.query(`UPDATE  features set ds_name=:name
                                where id_feature=:id_feature`,
                {
                    replacements: {
                        name: feature.feature,
                        id_feature: feature.featureId
                    }, type: sequelize.QueryTypes.UPDATE
                })
                .then(function (featureId) {
                    sequelize.query(`UPDATE permission
                                                            set  ds_permissions=:featurePermission,
                                                                ds_url=:featureUrl
                                                            where id_code=:id_code
                                                                            `,
                        {
                            replacements: {
                                id_code: feature.featurePermissionId,
                                featurePermission: feature.featurePermission,
                                featureUrl: feature.featureUrl
                            }, type: sequelize.QueryTypes.INSERT
                        })
                        .then(function (permissionId) {
                            res.json({ data: permissionId, status: 0 });

                        });
                });

        }


    }

    var dict = { "á": "a", "Á": "A", "é": "e", "É": "E", "ó": "o", "Ó": "O", "í": "i", "Í": "I", "ñ": "n", "Ñ": "N", "ü": "u" };
    // function getpermissions(req,res){
    var getpermissions = function (req, res) {
        // var deferred = Q.defer()
        sequelize.query(`select fet.ds_name,sfp.ds_permissions,sfp.ds_url as url_path,module.ds_product_name,
                                    '' as url,sfp.id_feature,sfp.id_code,module.id_module as module_id,module.ds_name as moduleName
                                    from   permission sfp
                                    inner join features fet on fet.id_feature=sfp.id_feature
                                    inner join modules module on module.id_module=fet.id_module
                                    where sfp.ds_url!=''
                                    group by sfp.id_feature,sfp.ds_permissions,sfp.ds_url;`,
            { replacements: { is_Active: 1 }, type: sequelize.QueryTypes.SELECT })
            .then(function (permissions) {
                var obj = [], obj1 = {}, name1 = '';
                for (var i in permissions) {
                    name1 = permissions[i].ds_name.replace(/[^\w ]/g, function (char) { return dict[char] || char; });
                    name1 = name1.toLowerCase() + ' ' + permissions[i].ds_permissions.toLowerCase();
                    name1 = name1.replace(/ /g, "-");
                    permissions[i].url = name1
                    //obj1[name1]=true;
                }
                // obj.push(obj1)
                res.json({ data: permissions, status: 0 });
            }).catch(function (error) {
                res.json({ data: error, status: 1 });
            });
        // return deferred.promise;
    }

    var deletefeaturePermissions = function (req, res) {

        var feature = req.body;
        sequelize.query(`delete  sfp,fet  FROM  permission sfp
                                                        inner join features fet on fet.id_feature=sfp.id_feature
                                                            where sfp.id_code=:id_code`,
            { replacements: { id_code: feature.id_code }, type: sequelize.QueryTypes.UPDATE })
            .then(function (featureId) {
                res.json({ data: featureId, status: 0 });
            });
    }


    var userAuth = function () {

        return {
            AttLogin: {
                AttLoginSoap: {
                    Authenticate: function (args, callback) {

                        var type = 'WEBSERVICE_DB1';

                        if (typeof args.usuario != 'undefined' && typeof args.clave != 'undefined' && typeof args.XmlString != 'undefined') {

                            sequelize.query("SELECT *  FROM users  WHERE  ds_mail= :email",
                                { replacements: { email: args.usuario }, type: sequelize.QueryTypes.SELECT })
                                .then(function (taskList) {
                                    if (taskList[0] != '') {
                                        var value = {
                                            name: taskList[0].ds_name,
                                            givenName: taskList[0].ds_fullname,
                                            mail: taskList[0].ds_mail
                                        }
                                    } else {
                                        var value = null
                                    }


                                    callback({ salida: value });
                                });


                        } else {
                            callback({ salida: '' })
                        }

                    }

                }
            }
        }
    }


    function getUsersList(req, res) {

        var query = [];
        var queryValue = req.query['queryValue'];
        query.push(model._getUsersList())

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function getUsersDetails(req, res){

        var query = [];
        var userId = req.query['userId'];
        query.push(model._getUsersDetails(userId))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function getUserWarehouses(req, res){

        var query = [];
        var userId = req.query['userId'];
        query.push(model._getUserWarehouses(userId))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function userCreate(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){

        Promise.all([model._userCreate(data)]).then((result) => {
            query=[]
            for(var  i in data.warehouses){
                query.push(model._userWarehouseAssociate(data.warehouses[i].warehouseId,result[0]))
            }

            Promise.all(query).then((results) => {

                res.json({
                    data: result[0],
                    status: 1
                });

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            status: 0
        });
    }

    }

	async function userCreateMobile(req, res){
		const data = req.body
		const fullName = data.fullName || false
		const rut = data.rut || false
		const email = data.email || false
		const userName = data.userName || false
		const password = data.password || false

		if (!fullName || !rut || !email || !userName || !password) {
			res.json({
				data: [],
				status: 0,
				message: 'User no found',
				success: false
			});
		}

		const userExits = await model._userValidate(userName,password)

		if (userExits.length) {
			res.json({
				data: [],
				status: 0,
				message: 'User exists',
				success: false
			});
		} else {
			model._userCreateMobile(data).then(async (result) => {
				const user = await model._getUser(result)
				const userId = user[0]['usId']

				Promise.all([
					model._getpermissions(userId),
					model._getpermissionsDetailsUrl(userId),

				]).then((result) => {
					const userInfo = {
						users: {
							email: user[0]['usMail'],
							name : user[0]['usName'],
						},
						type : 'web',
					}
					const { jwt } = serviceLogin.generateTokens(userInfo)

					const response = {
						user: {
							username:user[0].usName,
							name:user[0].usName,
							email:user[0].usMail,
							usLastName:user[0].usLastName,
							id:user[0].usId,
							profilestatus:'Admin',
							picture:'',
							permissions:result[0],
							url:result[1],
							role:user[0].usRole,
						},
						token:jwt,
						status:true
					};

					const jsontype = JSON.stringify(response)
					const jsonSaml = new Buffer(jsontype).toString('base64')
					res.jsonp(jsonSaml);
				});
			}).catch(function(error) {
				res.json({
					data: error,
					status: 0
				});
			})
		}

	}


    function updateInfoServices(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._updateInfoServices(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function updateOrganizationServices(req, res) {

        var query = [];
        var data = req.body;
        if(req.body){

            Promise.all([model._updateUserRole(data)]).then((result) =>{
                query=[]
                query.push(model._deleteUserWarehouse(data.userId))

                for(var  i in data.warehouses){
                    query.push(model._userWarehouseAssociate(data.warehouses[i].warehouseId,data.userId))
                }

                Promise.all(query).then((results) => {

                    res.json({
                        data: result[0],
                        status: 1
                    });

                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })

            }).catch(function(error) {
                res.json({
                    data: error,
                    status: 0
                });
            })

        }else{
            res.json({
                data: [],
                status: 0
            });
        }

    }

    function updateAccessServices(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._updateAccessServices(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function contractTypeUpdate(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._contractTypeUpdate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }


    function getRetirements(req, res){

        var query = [];
        var userId = req.query['userId'];
        query.push(model._getRetirements(userId))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function getIsapresList(req, res){

        var query = [];
        var userId = req.query['userId'];
        query.push(model._getIsapresList(userId))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }


    function getCompensationBoxList(req, res){

        var query = [];
        var userId = req.query['userId'];
        query.push(model._getCompensationBoxList(userId))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function retirementsCreate(req, res) {

        var query = [];
        var queryValue = req.query['queryValue'];
        var data = req.body;
        query.push(model._retirementsCreate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                msg: "La AFP ha sido creada correctamente",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }

    function retirementsUpdate(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._retirementsUpdate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the AFP is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function isapresCreate(req, res) {

        var query = [];
        var queryValue = req.query['queryValue'];
        var data = req.body;
        query.push(model._isapresCreate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                msg: "La isapre ha sido creada correctamente",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }


    function isapresUpdate(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._isapresUpdate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the ISAPRE is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function compensationBoxCreate(req, res) {

        var query = [];
        var queryValue = req.query['queryValue'];
        var data = req.body;
        query.push(model._compensationBoxCreate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                msg: "La C.C ha sido creada correctamente",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }

    function compensationBoxUpdate(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._compensationBoxUpdate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the C.C is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }


    function statusUserUpdate(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._statusUserUpdate(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }


    function updateAccessServices(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._updateAccessServices(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function updateUserInfoInstitutions(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._updateUserInfoInstitutions(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

    function updateUserBlockPersonalInfo(req, res) {

        var query = [];
        var data = req.body;
        query.push(model._updateUserBlockPersonalInfo(data))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                messagge: "the user is updated succesfully",
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }

	async function uploadFiles(req, res) {
		try {
			const userId = req.params.userId;
			const files = req.files;
			const dir = `files/users/${userId}`;
			if (!fs.existsSync(dir)) fs.mkdirSync(dir);

			files.map(f => {
				const name = `${f.originalname}`;
				const dest = `${dir}/${name}`;
				if (fs.existsSync(dest)) {
				} else {
				fs.renameSync(f.path, dest);
				}
			});

			res.status(200).send({ status:1 })
		} catch (error) {
			res.status(500).send(err);
		}
	}

	async function getFiles(req, res) {
		try {
			const files = fs.readdirSync(`files/users/${req.params.userId}`)
			const fileUrls = files.map(file => ({
				name: file,
				url: path.join(__dirname, `../../../files/users/${req.params.userId}/`, file),
				url2: localenv.DOMAIN + `/api/users/file/${req.params.userId}?fileName=${file}`
			}));
			res.json(fileUrls)
		} catch (error) {
			res.status(500).send(err);
		}
	}

	async function getFileUrl(req, res) {
		const fileName = req.query.fileName;
		const id = req.params.id;
		const filePath = await getFilePath(fileName, id);
		if (filePath) {
			res.sendFile(filePath);
			res.status(200);
		} else {
			console.log('File no existe');
			res.json({
				data: [],
				status: 0,
			});
			res.status(400);
			return
		}
	}

	async function getFilePath(fileName, id) {
		const filePath = path.join(__dirname, `../../../files/users/${id}/`, fileName)
		return filePath;
	}

	async function deleteFile(req, res) {
		const fileName = req.query.fileName;
		const id = req.query.id;
		const filePath = await getFilePath(fileName, id);
		if (filePath) {
			fs.unlink(filePath, (err) => {
				if (err && err.errno) {
					res.status(500).send('Error al eliminar el archivo.');
				}

				res.status(200).send('Archivo eliminado correctamente.');
			}).catch((error) => {
				res.status(400).send(error);
			})
		} else {
			res.status(400).send('Archivo no encontrado.');
		}
	}


     function keyValidate(req, res){

        var query = [];
        var userId = req.query['userId'];
        var key = req.query['key'];
        query.push(model._keyValidate(userId,key))

        Promise.all(query).then((result) => {
            res.json({
                data: result[0],
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }


    module.exports = {
        getUserDetailById: getUserDetailById,
        getUsers: getUsers,
        getRole: getRole,
        getGroups: getGroups,
        getAllPermissionsByrole: getAllPermissionsByrole,
        insertUserInfo: insertUserInfo,
        updateUserInfo: updateUserInfo,
        statusUserUpdate:statusUserUpdate,
        insertRoles: insertRoles,
        updateRole: updateRole,
        insertGroups: insertGroups,
        updateGroup: updateGroup,
        insertRolePermission: insertRolePermission,
        deactiveRole: deactiveRole,
        deactiveGroups: deactiveGroups,
        deactiveUsers: deactiveUsers,
        validateUser: validateUser,
        listallpermissions: listallpermissions,
        getUsersByName: getUsersByName,
        getModuleByProduct: getModuleByProduct,
        insertFeatureByName: insertFeatureByName,
        getpermissions: getpermissions,
        deletefeaturePermissions: deletefeaturePermissions,
        userAuth: userAuth,
        getUsersList:getUsersList,
        getUsersDetails:getUsersDetails,
        getUserWarehouses:getUserWarehouses,
        userCreate:userCreate,
        updateInfoServices:updateInfoServices,
        updateOrganizationServices:updateOrganizationServices,
        updateAccessServices:updateAccessServices,
        getRetirements:getRetirements,
        getIsapresList:getIsapresList,
        getCompensationBoxList:getCompensationBoxList,
        retirementsCreate:retirementsCreate,
        retirementsUpdate:retirementsUpdate,
        isapresCreate:isapresCreate,
        isapresUpdate:isapresUpdate,
        compensationBoxCreate:compensationBoxCreate,
        compensationBoxUpdate:compensationBoxUpdate,
        contractTypeUpdate:contractTypeUpdate,
        updateUserInfoInstitutions:updateUserInfoInstitutions,
        updateUserBlockPersonalInfo:updateUserBlockPersonalInfo,
        keyValidate:keyValidate,
        poolSize: 10000,
        poolIdleTimeout: 30000000,
		uploadFiles,
		getFiles,
		getFileUrl,
		deleteFile,
		userCreateMobile
    }
