'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;
var model=require('./model')
var Q = require('q');
const nJwt = require('njwt')
const JwtSecret = 'JormatKey2023'

/**
 * vignesh
 * @param {string} queryValue Valor buscado
 * @since 2018/07/08
 * @see creación
 * @author Merge&Deploy.
 */

const generateTokens = (claims) => {
    claims.iat = new Date()
    // claims.jti = process.env.JWT_TOKEN_JTI_SECRET

    const JWT = nJwt.create(claims, JwtSecret, 'HS256')
    // const RFT = nJwt.create(claims, JwtSecret, 'HS256')
    /**
     * Taking the expiration time from config, by default is 24 hours
     */
    const expirationTime = (60 * (60 * 24) * 40000)

    const jwtTime = new Date().getTime() + expirationTime // Expiration time for the token
    // const rftTime = jwtTime + (expirationTime * 0.20) // Add 20% of time to the refresh token

    JWT.setExpiration(jwtTime)
    // RFT.setExpiration(rftTime)

    return {
        jwt: JWT.compact(),
        // rft: RFT.compact(),
    }
}

const  loginUsers = function (req, res) {
    var username = req.body.name;
    var password = req.body.password;


    sequelize.query(`SELECT * FROM users WHERE usName=:username AND usPass=:password `,
        {  replacements: {
            username:username,
            password:password
        },type: sequelize.QueryTypes.SELECT })
        .then(function (users) {
            if(users.length>0){
          		var userId=users[0]['usId']

				Promise.all([
					model._getpermissions(userId),
					model._getpermissionsDetailsUrl(userId),

				]).then((result) => {
					const userInfo = {
						users: {
							email: users[0]['usMail'],
							name : users[0]['usName'],
						},
						type : 'web',
					}
					const { jwt } = generateTokens(userInfo)

					const response = {
						user: {
							username:users[0].usName,
							name:users[0].usName,
							email:users[0].usMail,
							usLastName:users[0].usLastName,
							id:users[0].usId,
							profilestatus:'Admin',
							picture:'',
							permissions:result[0],
							url:result[1],
							role:users[0].usRole,
						},
						token:jwt,
						status:true
					};

					const jsontype = JSON.stringify(response)
					const jsonSaml = new Buffer(jsontype).toString('base64')
					res.jsonp(jsonSaml);
				});
            }else{
                var response = {
                    user: {

                    },
                    token:'tokenNew',
                    status:false
                };
                var jsontype = JSON.stringify(response)
                var jsonSaml = new Buffer(jsontype).toString('base64')
                res.jsonp(jsonSaml);
            }
        });
}


module.exports = {
    loginUsers: loginUsers,
	generateTokens,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}
