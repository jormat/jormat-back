const express = require('express')
const router = express.Router()
const service = require('./services')

router.get('/list', service.getWarehouseDeliveries)
router.get('/listPaginated', service.getWarehouseDeliveriesPaginated)
router.post('/update', service.setWarehouseDeliveries)


module.exports = router;
