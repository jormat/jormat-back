'use strict'

var model = require('./model');

const getWarehouseDeliveries = (req, res) => {
	var query = [];
	query.push(model._getWarehouseDeliveries())

	Promise.all(query).then((result) => {
		res.json({
			data: result[0],
			status: 1
		});
	}).catch((error) => {
		res.json({
			data: error,
			status: 0
		});
	})
}

const getWarehouseDeliveriesPaginated = (req, res) => {
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = parseInt(req.query['pageSize'])
	const {
		filters = {}
	} = req.query

	const promises = [
		model._getWarehouseDeliveriesPaginated(filters, offset, limit, false),
		model._getWarehouseDeliveriesPaginated(filters, offset, limit, true)
	]

	Promise.all(promises).then((result) => {
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
	}).catch((error) => {
		res.json({
			data: [],
			message: error,
			status: 0
		});
	})
}

const setWarehouseDeliveries = (req, res) => {
	const data = req.body;
	var query = [];
	query.push(model._setWarehouseDeliveries(data))
	if(req.body){
		Promise.all(query).then((result) => {
			res.json({
				data: result[0][1][0],
				status: 1
			});
		}).catch((error) => {
			res.json({
				data: error,
				status: 0
			});
		})
	}else{
		res.json({
			data: [],
			messagge:"not data this services setWarehouseDeliveries",
			status: 0
		});
	}
}

module.exports = {
	getWarehouseDeliveries,
	setWarehouseDeliveries,
	poolSize: 10000,
	poolIdleTimeout: 30000000,
	getWarehouseDeliveriesPaginated
}
