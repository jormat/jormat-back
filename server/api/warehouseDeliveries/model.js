'use strict'

import sqldb from '../../sqldb'
const Sequelize = require('sequelize');
const sequelize = sqldb.sequelize;
const Q = require('q');


const _getWarehouseDeliveries = (userId) => {

	const deferred = Q.defer()

let sql = `	SELECT documentId, clientId, clientName, typeName, typeId,total, origin, DATE,dateDoc,dateOut,statusName,dsOut,styleOut,isOut,usName,userCreation,commentaryOut,packOff
FROM(
			SELECT		DISTINCT
						i.invoiceId AS documentId,
						c.id AS clientId,
						c.dsName AS clientName,
						td.code AS typeName,
						td.id AS typeId,
						i.total,
						w.dsCode AS "origin",
						i.date AS date,
						DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
						DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
						if(i.status=1, 'Pendiente','OK') AS statusName,
						CASE
							WHEN i.isOut IS NULL THEN 'Pendiente'
							WHEN i.isOut = 1 THEN 'Entregado'
							WHEN i.isOut = 2 THEN 'Incompleto'
							WHEN i.isOut = 3 THEN 'Mixto'
							WHEN i.isOut = 4 THEN 'Delivery'
							WHEN i.isOut = 5 THEN 'Mesón'
							WHEN i.isOut = 6 THEN 'Despacho'
							WHEN i.isOut = 7 THEN 'Entrega Rapida'
						END AS dsOut,
						CASE
							WHEN i.isOut IS NULL THEN 'danger'
							WHEN i.isOut = 1 THEN 'primary'
							WHEN i.isOut = 2 THEN 'warning'
							WHEN i.isOut = 3 THEN 'primary'
							WHEN i.isOut = 4 THEN 'primary'
							WHEN i.isOut = 5 THEN 'primary'
							WHEN i.isOut = 6 THEN 'primary'
							WHEN i.isOut = 7 THEN 'primary'

						END AS styleOut,
						i.isOut,
						u.usName,
						us.usName AS "userCreation",
						i.commentaryOut,
						i.packOff
			FROM 		invoices i
			INNER JOIN	clients c
			ON			i.clientId = c.id
			LEFT JOIN	users u
			ON			i.userOutId = u.usId
			LEFT JOIN	typedocument td
			ON			td.id = i.typeDoc
			left JOIN  warehouse w
         ON  i.origin = w.id
         LEFT  JOIN  users us
         ON  i.usId = us.usId
			WHERE		i.inStatus = 1
			UNION ALL
			SELECT		DISTINCT
						i.documentId AS documentId,
						c.id AS clientId,
						c.dsName AS clientName,
						td.code AS typeName,
						td.id AS typeId,
						i.total,
						w.dsCode AS "origin",
						i.date,
						DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
						DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
						if(i.status=1, 'OK','Pendiente') AS statusName,
						CASE
							WHEN i.isOut IS NULL THEN 'Pendiente'
							WHEN i.isOut = 1 THEN 'Entregado'
							WHEN i.isOut = 2 THEN 'Incompleto'
							WHEN i.isOut = 3 THEN 'Mixto'
							WHEN i.isOut = 4 THEN 'Delivery'
							WHEN i.isOut = 5 THEN 'Mesón'
							WHEN i.isOut = 6 THEN 'Despacho'
							WHEN i.isOut = 7 THEN 'Entrega Rapida'
						END AS dsOut,
						CASE
							WHEN i.isOut IS NULL THEN 'danger'
							WHEN i.isOut = 1 THEN 'primary'
							WHEN i.isOut = 2 THEN 'warning'
							WHEN i.isOut = 3 THEN 'primary'
							WHEN i.isOut = 4 THEN 'primary'
							WHEN i.isOut = 5 THEN 'primary'
							WHEN i.isOut = 6 THEN 'primary'
							WHEN i.isOut = 7 THEN 'primary'

						END AS styleOut,
						i.isOut,
						u.usName,
						us.usName AS "userCreation",
						i.commentaryOut,
						0 as packOff
			FROM		documentsnn i
			INNER JOIN  clients c
			ON			i.clientId = c.id
			LEFT JOIN	warehouse w
			ON			i.origin = w.id
			left JOIN  users us
            ON         i.usId = us.usId
			LEFT JOIN	users u
			ON			i.userOutId = u.usId
			LEFT JOIN	typedocument td
			ON			td.id = i.typeDoc
			WHERE		i.inStatus = 1
			UNION ALL
			SELECT		DISTINCT
						b.ballotId AS documentId,
						c.id AS clientId,
						c.dsName AS clientName,
						td.code AS typeName,
						td.id AS typeId,
						b.total,
						w.dsCode AS "origin",
						b.date,
						DATE_FORMAT(b.date, '%d/%m/%Y') AS dateDoc,
						DATE_FORMAT(b.dateOut, '%d/%m/%Y %T') AS dateOut,
						if(b.status=1, 'Pendiente','OK') AS statusName,
						CASE
							WHEN b.isOut IS NULL THEN 'Pendiente'
							WHEN b.isOut = 1 THEN 'Entregado'
							WHEN b.isOut = 2 THEN 'Incompleto'
							WHEN b.isOut = 3 THEN 'Mixto'
							WHEN b.isOut = 4 THEN 'Delivery'
							WHEN b.isOut = 5 THEN 'Mesón'
							WHEN b.isOut = 6 THEN 'Despacho'
							WHEN b.isOut = 7 THEN 'Entrega Rapida'
						END AS dsOut,
						CASE
							WHEN b.isOut IS NULL THEN 'danger'
							WHEN b.isOut = 1 THEN 'primary'
							WHEN b.isOut = 2 THEN 'warning'
							WHEN b.isOut = 3 THEN 'primary'
							WHEN b.isOut = 4 THEN 'primary'
							WHEN b.isOut = 5 THEN 'primary'
							WHEN b.isOut = 6 THEN 'primary'
							WHEN b.isOut = 7 THEN 'primary'

						END AS styleOut,
						b.isOut,
						u.usName,
						us.usName AS "userCreation",
						b.commentaryOut,
						b.packOff
			FROM		ballots b
			INNER JOIN	clients c
			ON			b.clientId = c.id
			LEFT JOIN	warehouse w
			ON			b.origin = w.id
			left JOIN   users us
            ON          b.usId = us.usId
			LEFT JOIN	users u
			ON			b.userOutId = u.usId
			LEFT JOIN	typedocument td
			ON			td.id = b.typeDoc
			WHERE		b.inStatus = 1
			UNION ALL
			SELECT		DISTINCT
						sn.saleNoteId AS documentId,
						c.id AS clientId,
						c.dsName AS clientName,
						td.code AS typeName,
						td.id AS typeId,
						sn.total,
						w.dsCode AS "origin",
						sn.date,
						DATE_FORMAT(sn.date, '%d/%m/%Y') AS dateDoc,
						DATE_FORMAT(sn.dateOut, '%d/%m/%Y %T') AS dateOut,
						if(sn.status=1, 'Pendiente','OK') AS statusName,
						CASE
							WHEN sn.isOut IS NULL THEN 'Pendiente'
							WHEN sn.isOut = 1 THEN 'Entregado'
							WHEN sn.isOut = 2 THEN 'Incompleto'
							WHEN sn.isOut = 3 THEN 'Mixto'
							WHEN sn.isOut = 4 THEN 'Delivery'
							WHEN sn.isOut = 5 THEN 'Mesón'
							WHEN sn.isOut = 6 THEN 'Despacho'
							WHEN sn.isOut = 7 THEN 'Entrega Rapida'
						END AS dsOut,
						CASE
							WHEN sn.isOut IS NULL THEN 'danger'
							WHEN sn.isOut = 1 THEN 'primary'
							WHEN sn.isOut = 2 THEN 'warning'
							WHEN sn.isOut = 3 THEN 'primary'
							WHEN sn.isOut = 4 THEN 'primary'
							WHEN sn.isOut = 5 THEN 'primary'
							WHEN sn.isOut = 6 THEN 'primary'
							WHEN sn.isOut = 7 THEN 'primary'

						END AS styleOut,
						sn.isOut,
						u.usName,
						us.usName AS "userCreation",
						sn.commentaryOut,
						0 as packOff
			FROM		salenote sn
			LEFT JOIN	clients c
			ON			sn.clientId = c.id
			LEFT JOIN	warehouse w
			ON			sn.origin = w.id
			left JOIN   users us
            ON          sn.usId = us.usId
			LEFT JOIN  	users u
			ON			sn.userOutId = u.usId
			LEFT JOIN	typedocument td
			ON			td.id = sn.typeDoc
			WHERE		sn.inStatus = 1
			) documents

            order BY date DESC `

	sequelize.query(sql, {
		type: Sequelize.QueryTypes.SELECT,
		replacements:{
			userId:1
		}
	}).then((result) => {
		deferred.resolve(result)
	}).catch((error) => {
		deferred.reject(error)
	})

	return deferred.promise
}

const _getWarehouseDeliveriesPaginated = (filters, offset, limit, count) => {
	let whereCount = ` 	WHERE 1=1 `
	let where = ` 	WHERE 1=1 `
	let sql = ``

	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'documentId':
					where += "	AND documentId =" + parseInt(filter[i])
					whereCount += "	AND documentId =" + parseInt(filter[i])
					break
				case 'clientName':
					where += "	AND clientName like '%" + filter[i] + "%'"
					whereCount += "	AND clientName like '%" + filter[i] + "%'"
					break
				case 'typeName':
					where += "	AND typeName like '%" + filter[i] + "%'"
					whereCount += "	AND typeName like '%" + filter[i] + "%'"
					break
				case 'origin':
					where += "	AND origin like '%" + filter[i] + "%'"
					whereCount += "	AND origin like '%" + filter[i] + "%'"
					break
				case 'dateDoc':
					where += "	AND dateDoc like '%" + filter[i] + "%'"
					whereCount += "	AND dateDoc like '%" + filter[i] + "%'"
					break
				case 'statusName':
					where += "	AND statusName like '%" + filter[i] + "%'"
					whereCount += "	AND statusName like '%" + filter[i] + "%'"
					break
				case 'dsOut':
					where += "	AND dsOut like '%" + filter[i] + "%'"
					whereCount += "	AND dsOut like '%" + filter[i] + "%'"
					break
				case 'dateOut':
					where += "	AND dateOut like '%" + filter[i] + "%'"
					whereCount += "	AND dateOut like '%" + filter[i] + "%'"
					break
				case 'usName':
					where += "	AND usName like '%" + filter[i] + "%'"
					whereCount += "	AND usName like '%" + filter[i] + "%'"
					break

			}
		}
	}

	if (!count) {
		sql += `	SELECT documentId, clientId, clientName, typeName, typeId,total, origin, DATE,dateDoc,dateOut,statusName,dsOut,styleOut,isOut,usName,userCreation,commentaryOut,packOff
					FROM(
						SELECT DISTINCT i.invoiceId AS documentId,
										c.id AS clientId,
										c.dsName AS clientName,
										td.code AS typeName,
										td.id AS typeId,
										i.total,
										w.dsCode AS "origin",
										i.date AS date,
										DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
										DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
										if(i.status=1, 'Pendiente','OK') AS statusName,
										CASE
											WHEN i.isOut IS NULL THEN 'Pendiente'
											WHEN i.isOut = 1 THEN 'Entregado'
											WHEN i.isOut = 2 THEN 'Incompleto'
											WHEN i.isOut = 3 THEN 'Mixto'
											WHEN i.isOut = 4 THEN 'Delivery'
											WHEN i.isOut = 5 THEN 'Mesón'
											WHEN i.isOut = 6 THEN 'Despacho'
											WHEN i.isOut = 7 THEN 'Entrega Rapida'
										END AS dsOut,
										CASE
											WHEN i.isOut IS NULL THEN 'danger'
											WHEN i.isOut = 1 THEN 'primary'
											WHEN i.isOut = 2 THEN 'warning'
											WHEN i.isOut = 3 THEN 'primary'
											WHEN i.isOut = 4 THEN 'primary'
											WHEN i.isOut = 5 THEN 'primary'
											WHEN i.isOut = 6 THEN 'primary'
											WHEN i.isOut = 7 THEN 'primary'

										END AS styleOut,
										i.isOut,
										u.usName,
										us.usName AS "userCreation",
										i.commentaryOut,
										i.packOff
						FROM 			invoices i
						INNER JOIN	clients c
						ON			i.clientId = c.id
						LEFT JOIN	users u
						ON			i.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = i.typeDoc
						LEFT JOIN   warehouse w
						ON  		i.origin = w.id
						LEFT JOIN   users us
						ON  		i.usId = us.usId
						WHERE		i.inStatus = 1
						UNION ALL
						SELECT		DISTINCT i.documentId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									i.total,
									w.dsCode AS "origin",
									i.date,
									DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(i.status=1, 'OK','Pendiente') AS statusName,
									CASE
											WHEN i.isOut IS NULL THEN 'Pendiente'
											WHEN i.isOut = 1 THEN 'Entregado'
											WHEN i.isOut = 2 THEN 'Incompleto'
											WHEN i.isOut = 3 THEN 'Mixto'
											WHEN i.isOut = 4 THEN 'Delivery'
											WHEN i.isOut = 5 THEN 'Mesón'
											WHEN i.isOut = 6 THEN 'Despacho'
											WHEN i.isOut = 7 THEN 'Entrega Rapida'
										END AS dsOut,
										CASE
											WHEN i.isOut IS NULL THEN 'danger'
											WHEN i.isOut = 1 THEN 'primary'
											WHEN i.isOut = 2 THEN 'warning'
											WHEN i.isOut = 3 THEN 'primary'
											WHEN i.isOut = 4 THEN 'primary'
											WHEN i.isOut = 5 THEN 'primary'
											WHEN i.isOut = 6 THEN 'primary'
											WHEN i.isOut = 7 THEN 'primary'

									END AS styleOut,
									i.isOut,
									u.usName,
									us.usName AS "userCreation",
									i.commentaryOut,
									0 as packOff
						FROM		documentsnn i
						INNER JOIN  clients c
						ON			i.clientId = c.id
						LEFT JOIN	warehouse w
						ON			i.origin = w.id
						LEFT JOIN  	users us
						ON         	i.usId = us.usId
						LEFT JOIN	users u
						ON			i.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = i.typeDoc
						WHERE		i.inStatus = 1
						UNION ALL
						SELECT		DISTINCT b.ballotId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									b.total,
									w.dsCode AS "origin",
									b.date,
									DATE_FORMAT(b.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(b.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(b.status=1, 'Pendiente','OK') AS statusName,
									CASE
										WHEN b.isOut IS NULL THEN 'Pendiente'
										WHEN b.isOut = 1 THEN 'Entregado'
										WHEN b.isOut = 2 THEN 'Incompleto'
										WHEN b.isOut = 3 THEN 'Mixto'
										WHEN b.isOut = 4 THEN 'Delivery'
										WHEN b.isOut = 5 THEN 'Mesón'
										WHEN b.isOut = 6 THEN 'Despacho'
										WHEN b.isOut = 7 THEN 'Entrega Rapida'
									END AS dsOut,
									CASE
										WHEN b.isOut IS NULL THEN 'danger'
										WHEN b.isOut = 1 THEN 'primary'
										WHEN b.isOut = 2 THEN 'warning'
										WHEN b.isOut = 3 THEN 'primary'
										WHEN b.isOut = 4 THEN 'primary'
										WHEN b.isOut = 5 THEN 'primary'
										WHEN b.isOut = 6 THEN 'primary'
										WHEN b.isOut = 7 THEN 'primary'

									END AS styleOut,
									b.isOut,
									u.usName,
									us.usName AS "userCreation",
									b.commentaryOut,
									b.packOff
						FROM		ballots b
						INNER JOIN	clients c
						ON			b.clientId = c.id
						LEFT JOIN	warehouse w
						ON			b.origin = w.id
						left JOIN   users us
						ON          b.usId = us.usId
						LEFT JOIN	users u
						ON			b.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = b.typeDoc
						WHERE		b.inStatus = 1
						UNION ALL
						SELECT		DISTINCT sn.saleNoteId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									sn.total,
									w.dsCode AS "origin",
									sn.date,
									DATE_FORMAT(sn.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(sn.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(sn.status=1, 'Pendiente','OK') AS statusName,
									CASE
										WHEN sn.isOut IS NULL THEN 'Pendiente'
										WHEN sn.isOut = 1 THEN 'Entregado'
										WHEN sn.isOut = 2 THEN 'Incompleto'
										WHEN sn.isOut = 3 THEN 'Mixto'
										WHEN sn.isOut = 4 THEN 'Delivery'
										WHEN sn.isOut = 5 THEN 'Mesón'
										WHEN sn.isOut = 6 THEN 'Despacho'
										WHEN sn.isOut = 7 THEN 'Entrega Rapida'
									END AS dsOut,
									CASE
										WHEN sn.isOut IS NULL THEN 'danger'
										WHEN sn.isOut = 1 THEN 'primary'
										WHEN sn.isOut = 2 THEN 'warning'
										WHEN sn.isOut = 3 THEN 'primary'
										WHEN sn.isOut = 4 THEN 'primary'
										WHEN sn.isOut = 5 THEN 'primary'
										WHEN sn.isOut = 6 THEN 'primary'
										WHEN sn.isOut = 7 THEN 'primary'

									END AS styleOut,
									sn.isOut,
									u.usName,
									us.usName AS "userCreation",
									sn.commentaryOut,
									0 as packOff
						FROM		salenote sn
						LEFT JOIN	clients c
						ON			sn.clientId = c.id
						LEFT JOIN	warehouse w
						ON			sn.origin = w.id
						left JOIN   users us
						ON          sn.usId = us.usId
						LEFT JOIN  	users u
						ON			sn.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = sn.typeDoc
						WHERE		sn.inStatus = 1
					) documents
					${where}
					order BY date DESC
					LIMIT :limit OFFSET :offset ;`
	} else {
		sql += `	SELECT COUNT(documentId) AS total
					FROM(
						SELECT DISTINCT i.invoiceId AS documentId,
										c.id AS clientId,
										c.dsName AS clientName,
										td.code AS typeName,
										td.id AS typeId,
										i.total,
										w.dsCode AS "origin",
										i.date AS date,
										DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
										DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
										if(i.status=1, 'Pendiente','OK') AS statusName,
										CASE
											WHEN i.isOut IS NULL THEN 'Pendiente'
											WHEN i.isOut = 1 THEN 'Entregado'
											WHEN i.isOut = 2 THEN 'Incompleto'
											WHEN i.isOut = 3 THEN 'Mixto'
											WHEN i.isOut = 4 THEN 'Delivery'
											WHEN i.isOut = 5 THEN 'Mesón'
											WHEN i.isOut = 6 THEN 'Despacho'
											WHEN i.isOut = 7 THEN 'Entrega Rapida'
										END AS dsOut,
										CASE
											WHEN i.isOut IS NULL THEN 'danger'
											WHEN i.isOut = 1 THEN 'primary'
											WHEN i.isOut = 2 THEN 'warning'
											WHEN i.isOut = 3 THEN 'primary'
											WHEN i.isOut = 4 THEN 'primary'
											WHEN i.isOut = 5 THEN 'primary'
											WHEN i.isOut = 6 THEN 'primary'
											WHEN i.isOut = 7 THEN 'primary'

										END AS styleOut,
										i.isOut,
										u.usName,
										us.usName AS "userCreation",
										i.commentaryOut,
										i.packOff
						FROM 			invoices i
						INNER JOIN	clients c
						ON			i.clientId = c.id
						LEFT JOIN	users u
						ON			i.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = i.typeDoc
						LEFT JOIN   warehouse w
						ON  		i.origin = w.id
						LEFT JOIN   users us
						ON  		i.usId = us.usId
						WHERE		i.inStatus = 1
						UNION ALL
						SELECT		DISTINCT i.documentId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									i.total,
									w.dsCode AS "origin",
									i.date,
									DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(i.status=1, 'OK','Pendiente') AS statusName,
									CASE
											WHEN i.isOut IS NULL THEN 'Pendiente'
											WHEN i.isOut = 1 THEN 'Entregado'
											WHEN i.isOut = 2 THEN 'Incompleto'
											WHEN i.isOut = 3 THEN 'Mixto'
											WHEN i.isOut = 4 THEN 'Delivery'
											WHEN i.isOut = 5 THEN 'Mesón'
											WHEN i.isOut = 6 THEN 'Despacho'
											WHEN i.isOut = 7 THEN 'Entrega Rapida'
										END AS dsOut,
										CASE
											WHEN i.isOut IS NULL THEN 'danger'
											WHEN i.isOut = 1 THEN 'primary'
											WHEN i.isOut = 2 THEN 'warning'
											WHEN i.isOut = 3 THEN 'primary'
											WHEN i.isOut = 4 THEN 'primary'
											WHEN i.isOut = 5 THEN 'primary'
											WHEN i.isOut = 6 THEN 'primary'
											WHEN i.isOut = 7 THEN 'primary'

									END AS styleOut,
									i.isOut,
									u.usName,
									us.usName AS "userCreation",
									i.commentaryOut,
									0 as packOff
						FROM		documentsnn i
						INNER JOIN  clients c
						ON			i.clientId = c.id
						LEFT JOIN	warehouse w
						ON			i.origin = w.id
						LEFT JOIN  	users us
						ON         	i.usId = us.usId
						LEFT JOIN	users u
						ON			i.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = i.typeDoc
						WHERE		i.inStatus = 1
						UNION ALL
						SELECT		DISTINCT b.ballotId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									b.total,
									w.dsCode AS "origin",
									b.date,
									DATE_FORMAT(b.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(b.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(b.status=1, 'Pendiente','OK') AS statusName,
									CASE
										WHEN b.isOut IS NULL THEN 'Pendiente'
										WHEN b.isOut = 1 THEN 'Entregado'
										WHEN b.isOut = 2 THEN 'Incompleto'
										WHEN b.isOut = 3 THEN 'Mixto'
										WHEN b.isOut = 4 THEN 'Delivery'
										WHEN b.isOut = 5 THEN 'Mesón'
										WHEN b.isOut = 6 THEN 'Despacho'
										WHEN b.isOut = 7 THEN 'Entrega Rapida'
									END AS dsOut,
									CASE
										WHEN b.isOut IS NULL THEN 'danger'
										WHEN b.isOut = 1 THEN 'primary'
										WHEN b.isOut = 2 THEN 'warning'
										WHEN b.isOut = 3 THEN 'primary'
										WHEN b.isOut = 4 THEN 'primary'
										WHEN b.isOut = 5 THEN 'primary'
										WHEN b.isOut = 6 THEN 'primary'
										WHEN b.isOut = 7 THEN 'primary'

									END AS styleOut,
									b.isOut,
									u.usName,
									us.usName AS "userCreation",
									b.commentaryOut,
									b.packOff
						FROM		ballots b
						INNER JOIN	clients c
						ON			b.clientId = c.id
						LEFT JOIN	warehouse w
						ON			b.origin = w.id
						left JOIN   users us
						ON          b.usId = us.usId
						LEFT JOIN	users u
						ON			b.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = b.typeDoc
						WHERE		b.inStatus = 1
						UNION ALL
						SELECT		DISTINCT sn.saleNoteId AS documentId,
									c.id AS clientId,
									c.dsName AS clientName,
									td.code AS typeName,
									td.id AS typeId,
									sn.total,
									w.dsCode AS "origin",
									sn.date,
									DATE_FORMAT(sn.date, '%d/%m/%Y') AS dateDoc,
									DATE_FORMAT(sn.dateOut, '%d/%m/%Y %T') AS dateOut,
									if(sn.status=1, 'Pendiente','OK') AS statusName,
									CASE
										WHEN sn.isOut IS NULL THEN 'Pendiente'
										WHEN sn.isOut = 1 THEN 'Entregado'
										WHEN sn.isOut = 2 THEN 'Incompleto'
										WHEN sn.isOut = 3 THEN 'Mixto'
										WHEN sn.isOut = 4 THEN 'Delivery'
										WHEN sn.isOut = 5 THEN 'Mesón'
										WHEN sn.isOut = 6 THEN 'Despacho'
										WHEN sn.isOut = 7 THEN 'Entrega Rapida'
									END AS dsOut,
									CASE
										WHEN sn.isOut IS NULL THEN 'danger'
										WHEN sn.isOut = 1 THEN 'primary'
										WHEN sn.isOut = 2 THEN 'warning'
										WHEN sn.isOut = 3 THEN 'primary'
										WHEN sn.isOut = 4 THEN 'primary'
										WHEN sn.isOut = 5 THEN 'primary'
										WHEN sn.isOut = 6 THEN 'primary'
										WHEN sn.isOut = 7 THEN 'primary'

									END AS styleOut,
									sn.isOut,
									u.usName,
									us.usName AS "userCreation",
									sn.commentaryOut,
									0 as packOff
						FROM		salenote sn
						LEFT JOIN	clients c
						ON			sn.clientId = c.id
						LEFT JOIN	warehouse w
						ON			sn.origin = w.id
						left JOIN   users us
						ON          sn.usId = us.usId
						LEFT JOIN  	users u
						ON			sn.userOutId = u.usId
						LEFT JOIN	typedocument td
						ON			td.id = sn.typeDoc
						WHERE		sn.inStatus = 1
						) documents
						${whereCount};`
	}

	return sequelize.query(sql, {
		type: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		}
	})
}

const _setWarehouseDeliveries = (data) => {
	const setTable = setTypeTable(data.typeDocumentId);
	const setId = setTypeId(data.typeDocumentId)

	const deferred = Q.defer()

	let sql = `	UPDATE	${setTable}
				SET		isOut = ${data.option},
						dateOut = NOW(),
						userOutId = ${data.userId},
						commentaryOut = '${data.commentary}'
				WHERE 	${setId} = ${data.documentId};
				SELECT		DISTINCT
							i.${setId} AS documentId,
							c.id AS clientId,
							c.dsName AS clientName,
							td.code AS typeName,
							td.id AS typeId,
							i.total,
							w.dsCode AS "origin",
							i.date,
						    DATE_FORMAT(i.date, '%d/%m/%Y') AS dateDoc,
							DATE_FORMAT(i.dateOut, '%d/%m/%Y %T') AS dateOut,
							if(i.status=1, 'Pendiente','OK') AS statusName,
							CASE
								WHEN i.isOut IS NULL THEN 'Pendiente'
								WHEN i.isOut = 1 THEN 'Entregado'
								WHEN i.isOut = 2 THEN 'Incompleto'
								WHEN i.isOut = 3 THEN 'Mixto'
								WHEN i.isOut = 4 THEN 'Delivery'
								WHEN i.isOut = 5 THEN 'Mesón'
								WHEN i.isOut = 6 THEN 'Despacho'
								WHEN i.isOut = 7 THEN 'Entrega Rapida'
							END AS dsOut,
							CASE
								WHEN i.isOut IS NULL THEN 'danger'
								WHEN i.isOut = 1 THEN 'primary'
								WHEN i.isOut = 2 THEN 'warning'
								WHEN i.isOut = 3 THEN 'primary'
								WHEN i.isOut = 4 THEN 'primary'
								WHEN i.isOut = 5 THEN 'primary'
								WHEN i.isOut = 6 THEN 'primary'
								WHEN i.isOut = 7 THEN 'primary'
							END AS styleOut,
							i.isOut,
							u.usName,
							i.commentaryOut
				FROM 		${setTable} i
				INNER JOIN	clients c
				ON			i.clientId = c.id
				LEFT JOIN	users u
				ON			i.userOutId = u.usId
				LEFT JOIN	warehouse w
			    ON			i.origin = w.id
				LEFT JOIN	typedocument td
				ON			td.id = i.typeDoc
				WHERE		i.inStatus = 1
				AND			i.${setId} = ${data.documentId}`
	sequelize.query(sql, {
		type: Sequelize.QueryTypes.SELECT,
	}).then((result) => {
		deferred.resolve(result)
	}).catch((error) => {
		deferred.reject(error)
	})

	return deferred.promise
}

function setTypeTable(id){
	switch(id){
		case 1:
			return 'invoices'
			breack;
		case 4:
			return 'documentsnn'
			breack;
		case 6:
			return 'ballots'
			breack;
		case 14:
			return 'salenote'
			breack;
	}
}

function setTypeId(id){
	switch(id){
		case 1:
			return 'invoiceId'
			breack;
		case 4:
			return 'documentId'
			breack;
		case 6:
			return 'ballotId'
			breack;
		case 14:
			return 'saleNoteId'
			breack;
	}
}

module.exports = {
	_getWarehouseDeliveries,
	_setWarehouseDeliveries,
	poolSize: 10000,
	poolIdleTimeout: 30000000,
	_getWarehouseDeliveriesPaginated
}
