'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

function _getLocationsList(warehouseId){

    var query = (warehouseId !== undefined) ? ' AND l.warehouseId = :warehouseId  ':''

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                        l.id as "locationId",
                        l.dsName as "locationName",
                        l.dsCode as  "locationCode",
                        w.dsName as "warehouseName"
                    FROM location l
                    INNER JOIN  warehouse w
                    ON  l.warehouseId = w.id `+ query +`
                    WHERE l.inStatus = 1`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getLocationsDetails(){

    var data = [
       {
          "locationId": 1,
          "locationCode": "L1",
          "locationName": "Ubicación 1",
          "warehouseName": "Bodega 1"
        }
    ]

    return data;
}

function _createLocations(data){

    var deferred = Q.defer()
    
    let sql = ` INSERT INTO location (dsName,dsCode,warehouseId,inStatus) VALUES (:locationName,:locationCode,:warehouseId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            locationName:data.locationName,
            locationCode:data.locationCode,
            warehouseId:data.warehouseId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateLocations(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE  location SET  dsCode=:locationCode,
                                     dsName=:locationName 
                                WHERE id=:locationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            locationId:data.locationId,
            locationName:data.locationName,
            locationCode:data.locationCode
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledLocations(locationId){

    var deferred = Q.defer()
    
    let sql = `UPDATE location SET inStatus=0 WHERE id=:locationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            locationId:locationId
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

module.exports = {
    _getLocationsList: _getLocationsList,
    _getLocationsDetails:_getLocationsDetails,
    _createLocations:_createLocations,
    _updateLocations:_updateLocations,
    _disabledLocations:_disabledLocations,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}