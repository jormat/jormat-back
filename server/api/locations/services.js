'use strict'

var model = require('./model');


function getLocations(req, res) {

    var query = [];
    let warehouseId = req.param('warehouseId')
    
    query.push(model._getLocationsList(warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getLocationsDetails(req, res) {

    var query = [];
    let locationId = req.param('locationId')
    
    query.push(model._getLocationsDetails(locationId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createLocations(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createLocations(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the location is created succesfully",
            status: 1

        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateLocations(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateLocations(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the location is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledLocations(req, res) {

    var query = [];
    var locationId  = req.query['locationId'];
    var userId = req.query['userId'];
    query.push(model._disabledLocations(locationId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the location is inactive succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getLocations: getLocations,
    createLocations:createLocations,
    getLocationsDetails:getLocationsDetails,
    updateLocations:updateLocations,
    disabledLocations:disabledLocations,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}