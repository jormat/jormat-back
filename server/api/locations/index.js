var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/list', service.getLocations)
router.get('/', service.getLocationsDetails)
router.post('/', service.createLocations)
router.put('/', service.updateLocations)
router.delete('/', service.disabledLocations)

module.exports = router;