'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

function _getCategoriesList(userId){

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                    id as "categoryId",
                    dsName as "categoryName",
                    dsCode as  "categoryCode"
                    FROM category where inStatus = 1 
                    order by id ASC `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createCategories(data){

    var deferred = Q.defer()
    
    let sql = ` INSERT INTO category (dsName,dsCode,inStatus) VALUES (:categoryName,:categoryCode,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            categoryName:data.categoryName,
            categoryCode:data.categoryCode

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _viewCategorieDetails(model){

    var data = [
       {
          "categoryId": 1,
          "categoryCode": "Lubricantes",
          "categoryName": "LUB"
        }
    ]

    return data;
}

function _updateCategories(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE  category SET  dsCode=:categoryCode,dsName=:categoryName 
                                       WHERE id=:categoryId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            categoryId:data.categoryId,
            categoryCode:data.categoryCode,
            categoryName:data.categoryName

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledCategories(categoryId,userId){

    var deferred = Q.defer()
    
    let sql = `UPDATE category SET inStatus=0 WHERE id=:categoryId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            categoryId:categoryId
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getApplicationsList(userId){

    var deferred = Q.defer()

    let sql = `		SELECT DISTINCT
                    id as "applicationId",
                    dsName as "applicationName",
                    dsCode as  "applicationCode"
                    FROM applications where inStatus = 1 
                    order by id desc `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _applicationsCreate(data){

    var deferred = Q.defer()
    
    let sql = ` INSERT INTO applications (dsName,dsCode,inStatus) VALUES (:descriptionName,:code,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            descriptionName:data.descriptionName,
            code:data.code

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getApplicationsDetails(applicationId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                    id as "applicationId",
                    dsName as "applicationName",
                    dsCode as  "applicationCode"
                    FROM applications where id = :applicationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            applicationId:applicationId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _updateApplications(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE  applications SET  dsCode=:applicationCode,
                                         dsName=:applicationName 
                                       WHERE id=:applicationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            applicationId:data.applicationId,
            applicationCode:data.applicationCode,
            applicationName:data.applicationName

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

module.exports = {
    _getCategoriesList: _getCategoriesList,
    _createCategories:_createCategories,
    _viewCategorieDetails:_viewCategorieDetails,
    _updateCategories:_updateCategories,
    _disabledCategories:_disabledCategories,
    _getApplicationsList:_getApplicationsList,
    _applicationsCreate:_applicationsCreate,
    _getApplicationsDetails:_getApplicationsDetails,
    _updateApplications:_updateApplications,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}