var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/list', service.getCategories);
router.post('/', service.createCategories);
router.get('/', service.viewCategorieDetails);
router.put('/', service.updateCategories);
router.delete('/', service.disabledCategories);

router.get('/applications', service.getApplicationsList);
router.post('/applications', service.applicationsCreate);
router.get('/applications-details', service.getApplicationsDetails);
router.put('/applications', service.updateApplications);

module.exports = router;