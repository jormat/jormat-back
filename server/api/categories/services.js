'use strict'

var model = require('./model');


function getCategories(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getCategoriesList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createCategories(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._createCategories(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "La categoría ha sido creada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function viewCategorieDetails(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._viewCategorieDetails(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateCategories(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._updateCategories(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "La categoría ha sido actualizada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledCategories(req, res) {

    var query = [];
    var categoryId = req.query['categoryId'];
    var userId = req.query['userId'];
    query.push(model._disabledCategories(categoryId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "La categoría ha sido deshabilitada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getApplicationsList(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getApplicationsList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function applicationsCreate(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._applicationsCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "La aplicación ha sido creada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getApplicationsDetails(req, res) {

    var query = [];
    var applicationId = req.query['applicationId'];
    query.push(model._getApplicationsDetails(applicationId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateApplications(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    var data = req.body;
    query.push(model._updateApplications(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "La aplicación ha sido actualizada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getCategories: getCategories,
    createCategories:createCategories,
    viewCategorieDetails:viewCategorieDetails,
    updateCategories:updateCategories,
    disabledCategories:disabledCategories,
    getApplicationsList:getApplicationsList,
    applicationsCreate:applicationsCreate,
    getApplicationsDetails:getApplicationsDetails,
    updateApplications:updateApplications,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}