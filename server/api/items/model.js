'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

function _getListMain(filters, offset, limit, count, searchText){
	let whereCount = ` 	WHERE 1=1 `
	let where = ` 	WHERE 1=1 `
	let sql = ``

	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'itemId':
					where += "	AND STB.itemId =" + parseInt(filter[i])
					whereCount += "	AND item.id =" + parseInt(filter[i])
					break
				case 'itemDescription':
					where += "	AND STB.itemDescription like '%" + filter[i] + "%'"
					whereCount += "	AND item.dsName like '%" + filter[i] + "%'"
					break
				case 'references':
					where += "	AND STB.references like '%" + filter[i] + "%'"
					whereCount += "	AND ITR.dsReference like '%" + filter[i] + "%'"
					break
				case 'brand':
					where += "	AND STB.brand like '%" + filter[i] + "%'"
					whereCount += "	AND item.brand like '%" + filter[i] + "%'"
					break

			}
		}
	}

	if (!count) {
		sql += `		SELECT      STB.*,
								SUM(ITW.itemQty) as "generalStock"
						FROM        (SELECT DISTINCT    item.id as "itemId",
														item.dsName as "itemDescription",
														item.brand,
														item.failure,
														item.priority,
														item.dsMaxDiscount as maxDiscount,
														item.brandCode as brandCode,
                                                        item.oil as oil,
														GROUP_CONCAT(distinct ITR.dsReference) as "references",
														ITR.dsReference as referenceKey,
														GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
														item.nmNet as "netPrice",
														item.netPrice as netPurchaseValue,
														ROUND(item.nmNet*1.19,0) as "vatPrice",
														item.criticalStock as "criticalStock"
										FROM        items item
										INNER JOIN  itemreferences ITR
										ON          ITR.itemId = item.id
										left JOIN   itemproviders IP
										ON          IP.itemId=item.id
										LEFT JOIN   provider PRO
										ON          PRO.id = IP.providerId
										AND         item.inStatus = 1
										GROUP BY    item.id) STB
							INNER JOIN   itemwarehouse ITW
							ON          ITW.itemId = STB.itemId
							${where}
							GROUP BY    STB.itemId
							ORDER BY    STB.itemId DESC
							LIMIT :limit OFFSET :offset ; `
	} else {
		sql += `	SELECT      COUNT(DISTINCT item.id) AS total
					FROM        items item
					INNER JOIN  itemreferences ITR
					ON          ITR.itemId = item.id
					left JOIN   itemproviders IP
					ON          IP.itemId=item.id
					LEFT JOIN   provider PRO
					ON          PRO.id = IP.providerId
					AND         item.inStatus = 1
					INNER JOIN   itemwarehouse ITW
					ON          ITW.itemId = item.id
					${whereCount} `
	}

    return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{
			offset,
			limit,
		},
	})
}

function _getListItemsRelations(itemId){
	const sql = `		SELECT      STB.*,
							SUM(ITW.itemQty) as "generalStock"
					FROM        (SELECT DISTINCT    item.id as "itemId",
												item.dsName as "itemDescription",
												item.brand,
												item.failure,
												item.priority,
												item.dsMaxDiscount as maxDiscount,
												item.brandCode as brandCode,
													item.oil as oil,
												GROUP_CONCAT(distinct ITR.dsReference) as "references",
												ITR.dsReference as referenceKey,
												GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
												item.nmNet as "netPrice",
												item.netPrice as netPurchaseValue,
												ROUND(item.nmNet*1.19,0) as "vatPrice",
												item.criticalStock as "criticalStock"
								FROM        items item
								INNER JOIN  itemreferences ITR
								ON          ITR.itemId = item.id
								left JOIN   itemproviders IP
								ON          IP.itemId=item.id
								LEFT JOIN   provider PRO
								ON          PRO.id = IP.providerId
								AND         item.inStatus = 1
								WHERE 		item.id IN (SELECT idItemAssociated FROM relateditems WHERE itemId = :itemId)
								GROUP BY    item.id) STB
					INNER JOIN   itemwarehouse ITW
					ON          ITW.itemId = STB.itemId
					GROUP BY    STB.itemId
					ORDER BY    STB.itemId DESC; `
	return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{
			itemId,
		},
	})
}

function _getList(){
	let sql = `		SELECT      STB.*,
                                SUM(ITW.itemQty) as "generalStock"
                    FROM        (SELECT DISTINCT    item.id as "itemId",
                                                    item.dsName as "itemDescription",
                                                    item.brand,
                                                    item.failure,
                                                    item.priority,
                                                    item.dsMaxDiscount as maxDiscount,
                                                    item.brandCode as brandCode,
                                                    item.webType as webType,
                                                    item.oil as oil,
                                                    GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                    ITR.dsReference as referenceKey,
                                                    GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
                                                    item.nmNet as "netPrice",
                                                    item.netPrice as netPurchaseValue,
                                                    ROUND(item.nmNet*1.19,0) as "vatPrice",
                                                    item.criticalStock as "criticalStock"
                                FROM        items item
                                INNER JOIN  itemreferences ITR
                                ON          ITR.itemId = item.id
                                left JOIN   itemproviders IP
                                ON          IP.itemId=item.id
                                LEFT JOIN   provider PRO
                                ON          PRO.id = IP.providerId
                                AND         item.inStatus = 1
                                GROUP BY    item.id) STB
                                INNER JOIN   itemwarehouse ITW
                                ON          ITW.itemId = STB.itemId
                                GROUP BY    STB.itemId
                                ORDER BY    STB.itemId DESC`


    return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{},
	})
}

function _getListQuickView(){
	let sql = `		SELECT DISTINCT    item.id as "itemId",
                                                    item.dsName as "itemDescription",
                                                    item.brand,
                                                    item.failure,
                                                    item.brandCode as brandCode,
                                                    GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                    ITR.dsReference as referenceKey,
                                                    item.nmNet as "netPrice",
                                                    item.netPrice as netPurchaseValue
                                FROM        items item
                                INNER JOIN  itemreferences ITR
                                ON          ITR.itemId = item.id
                                AND         item.inStatus = 1
                                GROUP BY    item.id
                                ORDER BY item.id DESC`

    return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{},
	})
}

function _getItemsFreeMarket(){
    let sql = `     SELECT      STB.*,
                                SUM(ITW.itemQty) as "generalStock"
                    FROM        (SELECT DISTINCT    item.id as "itemId",
                                                    item.dsName as "itemDescription",
                                                    item.brand,
                                                    item.failure,
                                                    item.priority,
                                                    item.freeMarket,
                                                    item.brandCode as brandCode,
                                                    item.webType as webType,
                                                    item.oil as oil,
                                                    GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                    ITR.dsReference as referenceKey,
                                                    item.nmNet as "netPrice",
                                                    item.netPrice as netPurchaseValue,
                                                    ROUND(item.nmNet*1.19,0) as "vatPrice",
                                                    item.criticalStock as "criticalStock"
                                FROM        items item
                                INNER JOIN  itemreferences ITR
                                ON          ITR.itemId = item.id
                                WHERE       item.freeMarket LIKE  '%mercadolibre%'
                                GROUP BY    item.id) STB
                                INNER JOIN   itemwarehouse ITW
                                ON          ITW.itemId = STB.itemId
                                GROUP BY    STB.itemId
                                ORDER BY    generalStock asc`

    return sequelize.query(sql, {
        type        : sequelize.QueryTypes.SELECT,
        replacements:{},
    })
}

function _getListDetails(itemId){

    var deferred = Q.defer()

    let sql = ` SELECT      ITE.id AS itemId,ITE.dsName AS itemName,group_concat(distinct ITR.dsReference) AS reference,
                ITE.nmPrice as purchasePrice,
                ITE.nmNet as  netPrice,
                ITE.nmNet*1.19 as  vatPrice,
                ITE.netPrice as netPurchaseValue,
                ITE.mayorPrice as mayorPrice,
                ITE.dsMaxDiscount as  maxDiscount,
                ITE.dsDesc as   competitionPrice,
                ITE.dsComments as observation,
                ITE.inStatus AS 'status',
                ITE.isKit AS 'isKit',
                ITE.offerIs AS 'offerIs',
                ITE.freeMarketCode AS 'freeMarketCode',
                ITE.priority AS 'priority',
                ITE.createdAt as createdDate,
                if(ITE.failure=1, 'Con Falla','Sin Falla') AS "failure",
                ITE.oil as oil,
                ITE.observationFails as observationFails,
                ITE.brand as brand,
                DATE_FORMAT(ITE.updatedAt, '%d/%m/%Y %T') AS updateDate,
                u.usName as updateUser,
                ITE.criticalStock as "criticalStock",
                ITE.brandCode as brandCode,
                ITE.type AS "type",
                ITE.currency as "currency",
                ITE.webType as "webType",
                ITE.web as "web",
                ITE.freeMarket as "freeMarket",
                ITE.driveLink as "driveLink",
                ITE.image as "image",
                ITE.dataWeb as "dataWeb",
                ITE.currencyValue as currencyValue,
                ITR.dsReference  AS 'REFERENCES',
                l.dsName  AS locationName,
                ITS.locationId  AS locationId,
                l.dsCode AS locationCode,
                CATT.id as  'categoryCode',
                CATT.dsName as  'categoryName',
                PRO.id    AS  providerCode,
                PRO.dsName AS providerName,
                GROUP_CONCAT(DISTINCT CATT.id,'-',CATT.dsName) AS categoryP,
                GROUP_CONCAT(DISTINCT PRO.id,'-',PRO.dsName) AS providerP,
                GROUP_CONCAT(DISTINCT AP.id,'-',AP.dsName) AS applicationP,
                GROUP_CONCAT(DISTINCT w.dsName,'#',w.dsCode) AS warehousesP

                FROM        items ITE
                INNER JOIN  itemreferences ITR
                ON          ITR.itemId = ITE.id
                AND         ITE.inStatus = 1
                LEFT JOIN   itemproviders ITP
                ON          ITP.itemId = ITE.id
                AND         ITP.inStatus = 1
                LEFT JOIN   provider PRO
                ON          PRO.id = ITP.providerId
                AND         PRO.inStatus = 1
                LEFT JOIN   itemcategory CAT
                ON          CAT.itemId=ITE.id
                LEFT JOIN   category CATT
                ON      CAT.categoryId=CATT.id
                INNER JOIN   itemwarehouse ITS
                ON          ITS.itemId=ITE.id
                INNER JOIN   location l
                ON          l.id=ITS.locationId

                LEFT JOIN   applicationsitems API
                ON          API.itemId = ITE.id
                LEFT JOIN   applications AP
                ON          AP.id = API.applicationId

                INNER JOIN   warehouse w
                ON          w.id=ITS.warehouseId
                left JOIN   users u
                ON          u.usId=ITE.usModifierId
                WHERE       ITE.id=:itemId;
    `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getWarehouseByItem(itemId){
    var deferred = Q.defer()

    let sql = ` SELECT      W.id  AS warehouseId,
                            W.dsName AS  warehouseName,
                            W.dsCode AS warehouseCode,
                            L.id AS locationId,
                            L.dsName  AS locationName,
                            L.dsCode AS locationCode,
                            ITS.itemQty as 'quantity',
                            GROUP_CONCAT(DISTINCT L.dsName) AS locationP
                FROM        items ITE
                LEFT JOIN   itemwarehouse ITS
                ON          ITS.itemId = ITE.id
                LEFT JOIN   warehouse W
                ON          W.id=ITS.warehouseId
                LEFT JOIN   location L
                ON          L.warehouseId = W.id
                LEFT JOIN   itemwarehouselocation WL
                ON          WL.itemId = ITE.id
                AND         WL.locationId = L.id
                AND         WL.warehouseId = W.id
                WHERE       WL.itemId = :itemId
                GROUP BY    WL.itemId,WL.locationId,WL.warehouseId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItem(data){
    var deferred = Q.defer()
    if(data.maxDiscount==undefined) data.maxDiscount=0;
    if(data.competitionPrice==undefined) data.competitionPrice=0
    if(data.netPrice==undefined) data.netPrice=0
    if(data.purchasePrice==undefined) data.purchasePrice=0
    if(data.mayorPrice==undefined) data.mayorPrice=null
    if(data.observation==undefined) data.observation=0
    if(data.criticalStock==undefined) data.criticalStock=0
    if(data.netPurchaseValue==undefined) data.netPurchaseValue=data.purchasePrice/1.19
    if(data.brand==undefined) data.brand="sin marca"
    if(data.oil==undefined) data.oil=0
    if(data.type==undefined) data.type="Nacional"
    if(data.currency==undefined) data.currency="Peso Chileno"
    if(data.webType==undefined) data.webType="NO"
    if(data.web==undefined) data.web="Sin Link"
    if(data.freeMarket==undefined) data.freeMarket="Sin Link"
    if(data.driveLink==undefined) data.driveLink="Sin Link"
    if(data.currencyValue==undefined) data.currencyValue=data.purchasePrice
    if(data.isKit==undefined) data.isKit=0
    if(data.userId==undefined) data.userId=0
    if(data.offerIs==undefined) data.offerIs=0
    if(data.freeMarketCode==undefined) data.freeMarketCode=null

    let sql = `INSERT INTO items (dsName,nmPrice,nmNet,netPrice,dsMaxDiscount,dsDesc,dsComments,isKit,brand,brandCode,criticalStock,inStatus,usModifierId,updatedAt,oil,currency,currencyValue,type,web,freeMarket,driveLink,mayorPrice,webType,offerIs,freeMarketCode)
                 VALUES (:itemDescription,:purchasePrice,:netPrice,:netPurchaseValue,:maxDiscount,:competitionPrice,:observation,:isKit,:brand,:brandCode,:criticalStock,1,:userId,NOW(),:oil,:currency,:currencyValue,:type,:web,:freeMarket,:driveLink,:mayorPrice,:webType,:offerIs,:freeMarketCode)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            itemDescription:data.itemDescription,
            purchasePrice:data.purchasePrice,
            netPrice:data.netPrice,
            netPurchaseValue:data.netPurchaseValue,
            mayorPrice:data.mayorPrice,
            maxDiscount:data.maxDiscount,
            competitionPrice:data.competitionPrice,
            observation:data.observation,
            isKit:data.isKit,
            brand:data.brand,
            criticalStock:data.criticalStock,
            brandCode:data.brandCode,
            oil:data.oil,
            type:data.type,
            currency:data.currency,
            currencyValue:data.currencyValue,
            webType:data.webType,
            web:data.web,
            driveLink:data.driveLink,
            freeMarket:data.freeMarket,
            offerIs:data.offerIs,
            freeMarketCode:data.freeMarketCode
        }
    }).then(function (itemId) {
        deferred.resolve(itemId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemReferences(references,itemId){

    var deferred = Q.defer()
    let sql = `   INSERT INTO itemreferences (itemId,dsReference,inStatus) VALUES (:itemId,:dsReference,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            dsReference:references
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemWarehouse(warehouseId,locationId,itemId){

    var deferred = Q.defer()


    let sql = `  INSERT INTO itemwarehouse (itemId,warehouseId,locationId,itemQty,inStatus) VALUES (:itemId,:warehouseId,:locationId,:itemQty,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId,
            locationId:locationId,
            itemQty:0

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemWarehouselocation(warehouseId,locationId,itemId){

    var deferred = Q.defer()


    let sql = `  INSERT INTO itemwarehouselocation (itemId,warehouseId,locationId,inStatus) VALUES (:itemId,:warehouseId,:locationId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId,
            locationId:locationId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemCategory (categoryId,itemId){

    var deferred = Q.defer()

    let sql = ` INSERT INTO itemcategory (itemId,categoryId,inStatus) VALUES (:itemId,:categoryId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            categoryId:categoryId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemApplications (applicationId,itemId){

    var deferred = Q.defer()

    let sql = ` INSERT INTO applicationsitems (itemId,applicationId,inStatus) VALUES (:itemId,:applicationId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            applicationId:applicationId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemProvider(providerId,itemId){

    var deferred = Q.defer()

    let sql = `   INSERT INTO itemproviders (itemId,providerId,inStatus) VALUES(:itemId,:providerId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId,
            providerId:providerId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItem(data){
    var deferred = Q.defer()
    if(data.maxDiscount=='') data.maxDiscount=0;
    if(data.competitionPrice=='') data.competitionPrice=0
    if(data.netPrice=='') data.netPrice=0
    if(data.netPurchaseValue=='') data.netPurchaseValue=data.purchasePrice/1.19
    if(data.criticalStock==undefined) data.criticalStock=0
    if(data.oil==undefined) data.oil=0
    if(data.purchasePrice=='') data.purchasePrice=0
    if(data.type==undefined) data.type="Nacional"
    if(data.currency==undefined) data.currency="Peso Chileno"
    if(data.web==undefined) data.web="Sin Link"
    if(data.freeMarket==undefined) data.freeMarket="Sin Link"
    if(data.driveLink==undefined) data.driveLink="Sin Link"
    if(data.currencyValue==undefined) data.currencyValue=data.purchasePrice
    if(data.mayorPrice==undefined) data.mayorPrice=null
    if(data.webType==undefined) data.webType="NO"
    if(data.offerIs==undefined) data.offerIs=0
    if(data.freeMarketCode==undefined) data.freeMarketCode=null

    let sql = `update  items set dsName=:itemDescription,
                                 nmPrice=:purchasePrice,
                                 nmNet=:netPrice,
                                 netPrice=:netPurchaseValue,
                                 mayorPrice=:mayorPrice,
                                 dsMaxDiscount=:maxDiscount,
                                 dsDesc=:competitionPrice,
                                 dsComments=:observation,
                                 brand=:brand,
                                 brandCode=:brandCode,
                                 oil=:oil,
                                 criticalStock=:criticalStock,
                                 usModifierId=:userId,
                                 type=:type,
                                 currency=:currency,
                                 currencyValue=:currencyValue,
                                 updatedAt= NOW(),
                                 isKit=:isKit,
                                 driveLink=:driveLink,
                                 web=:web,
                                 freeMarket=:freeMarket,
                                 offerIs=:offerIs,
                                 freeMarketCode=:freeMarketCode,
                                 webType=:webType
               where  items.id=:item`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            item:data.itemId,
            userId:data.userId,
            itemDescription:data.itemDescription,
            purchasePrice:data.purchasePrice,
            netPurchaseValue:data.netPurchaseValue,
            netPrice:data.netPrice,
            maxDiscount:data.maxDiscount,
            competitionPrice:data.competitionPrice,
            observation:data.observation,
            brand:data.brand,
            brandCode:data.brandCode,
            criticalStock:data.criticalStock,
            oil:data.oil,
            type:data.type,
            currency:data.currency,
            currencyValue:data.currencyValue,
            isKit:data.isKit,
            mayorPrice:data.mayorPrice,
            webType:data.webType,
            web:data.web,
            driveLink:data.driveLink,
            freeMarket:data.freeMarket,
            offerIs:data.offerIs,
            freeMarketCode:data.freeMarketCode
        }
    }).then(function (itemId) {
        deferred.resolve(itemId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _deleteItemReferences(itemId){


    var deferred = Q.defer()
    let sql = `   DELETE FROM itemreferences WHERE itemId = :itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItemReferences(references,itemId){

    var deferred = Q.defer()
    let upd = ``
    var values = []


    let sql = ` INSERT INTO itemreferences (itemId,dsReference,inStatus,createdAt,updatedAt)
                VALUES`

    for(var i in references) {
        values.push(`( :itemId, '`+ references[i] +`', 1, NOW(), NOW())`)
    }
    sql += values.join(', ')

    upd += ` DELETE FROM itemreferences
             WHERE   itemId = :itemId`

    sequelize.query(upd, {
        replacements: {
            itemId:itemId
        },
        type: Sequelize.QueryTypes.DELETE
    }).then(function (result) {
        if (references.length > 0) {
            sequelize.query(sql, {
                replacements: {
                    itemId:itemId
                },
                type: Sequelize.QueryTypes.INSERT
            }).then(function (result) {
                deferred.resolve(result)
            }).catch(function (error){
                deferred.reject(error)
            })
        }
        deferred.resolve(result)

    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItemWarehouse(warehouseId,locationId,itemId,itemQty){

    var deferred = Q.defer()


    let sql = `  UPDATE  itemwarehouse SET  locationId=:locationId,
                                            itemQty=:itemQty
                                            WHERE itemId=:itemId AND warehouseId=:warehouseId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId,
            locationId:locationId,
            itemQty:itemQty

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItemCategory (categoryIds,itemId){

    var deferred = Q.defer()

    let upd = ``
    var values = []


    let sql = ` INSERT INTO itemcategory (itemId,categoryId,inStatus,createdAt,updatedAt)
                VALUES`

    for(var i in categoryIds) {
        values.push(`( :itemId, `+ categoryIds[i] +`, 1, NOW(), NOW())`)
    }
    sql += values.join(', ')

    upd += ` DELETE FROM itemcategory
             WHERE   itemId = :itemId`

    sequelize.query(upd, {
        replacements: {
            itemId:itemId
        },
        type: Sequelize.QueryTypes.DELETE
    }).then(function (result) {
        if (categoryIds.length > 0) {
            sequelize.query(sql, {
                replacements: {
                    itemId:itemId
                },
                type: Sequelize.QueryTypes.INSERT
            }).then(function (result) {
                deferred.resolve(result)
            }).catch(function (error){
                deferred.reject(error)
            })
        }
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItemProvider(providerIds,itemId){

    var deferred = Q.defer()
    let upd = ``
    var values = []


    let sql = ` INSERT INTO itemproviders (itemId,providerId,inStatus,createdAt,updatedAt)
                VALUES`

    for(var i in providerIds) {
        values.push(`( :itemId, `+ providerIds[i] +`, 1, NOW(), NOW())`)
    }
    sql += values.join(', ')

    upd += ` DELETE FROM itemproviders
             WHERE   itemId = :itemId`

    sequelize.query(upd, {
        replacements: {
            itemId:itemId
        },
        type: Sequelize.QueryTypes.DELETE
    }).then(function (result) {
        if (providerIds.length > 0) {
            sequelize.query(sql, {
                replacements: {
                    itemId:itemId
                },
                type: Sequelize.QueryTypes.INSERT
            }).then(function (result) {
                deferred.resolve(result)
            }).catch(function (error){
                deferred.reject(error)
            })
        }
        deferred.resolve(result)

    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateItemApplications(applicationsIds,itemId){

    var deferred = Q.defer()
    let upd = ``
    var values = []


    let sql = ` INSERT INTO applicationsitems (itemId,applicationId,inStatus,createdAt,updatedAt)
                VALUES`

    for(var i in applicationsIds) {
        values.push(`( :itemId, `+ applicationsIds[i] +`, 1, NOW(), NOW())`)
    }
    sql += values.join(', ')

    upd += ` DELETE FROM applicationsitems
             WHERE   itemId = :itemId`

    sequelize.query(upd, {
        replacements: {
            itemId:itemId
        },
        type: Sequelize.QueryTypes.DELETE
    }).then(function (result) {
        if (applicationsIds.length > 0) {
            sequelize.query(sql, {
                replacements: {
                    itemId:itemId
                },
                type: Sequelize.QueryTypes.INSERT
            }).then(function (result) {
                deferred.resolve(result)
            }).catch(function (error){
                deferred.reject(error)
            })
        }
        deferred.resolve(result)

    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledItem(itemId,status){


    var deferred = Q.defer()


    let sql = ` UPDATE items SET inStatus=0 where id=:itemId;
                UPDATE itemreferences SET inStatus=0 where itemId=:itemId;
                UPDATE itemwarehouse SET inStatus=0 where itemId=:itemId;
                UPDATE itemwarehouselocation SET inStatus=0 where itemId=:itemId;
                UPDATE itemcategory SET inStatus=0 where itemId=:itemId;
                UPDATE itemproviders SET inStatus=0 where itemId=:itemId;`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            itemId:itemId,
            status:status

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getStockItem(itemId,warehouseId){

    var deferred = Q.defer()

    let sql = ` SELECT    itemQty AS stock
                FROM itemwarehouse
                WHERE itemId = :itemId and warehouseId = :warehouseId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getGeneralStockWeb(itemId){

    var deferred = Q.defer()

    let sql = ` SELECT      STB.*,
                            SUM(ITW.itemQty) as "generalStock"
                            FROM        (SELECT DISTINCT     i.id AS itemId,
                                                            i.dsName AS itemDescription
                                        FROM        items i
                                        WHERE       i.id = :itemId
                                        GROUP BY    i.id) STB
                                        LEFT JOIN   itemwarehouse ITW
                                        ON          ITW.itemId = STB.itemId
                                        GROUP BY    STB.itemId
                                        ORDER BY    STB.itemId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getLocationsItemByWarehouse(itemId,warehouseId){

    var deferred = Q.defer()

    let sql = ` SELECT      l.id AS locationId,
                            l.dsName AS locationName,
                            l.dsCode AS locationCode,
                            ite.itemQty AS stock
                FROM        itemwarehouse ite
                INNER JOIN  warehouse w
                on          w.id = ite.warehouseId
                INNER JOIN  location l
                ON          l.warehouseId = w.id
                LEFT JOIN   itemwarehouselocation itl
                ON          itl.warehouseId = w.id
                AND         itl.locationId = l.id
                AND         itl.itemId = ite.itemId
                WHERE       itl.itemId = :itemId
                AND         itl.warehouseId = :warehouseId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateStock(stock,warehouseId,itemId,userId){
    var deferred = Q.defer()
    if(stock=='') stock=0
    let sql = `update  itemwarehouse set itemQty=:stock
               where  itemId=:itemId AND warehouseId=:warehouseId;

               UPDATE items SET usModifierId=:userId,updatedAt= NOW() where id=:itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            stock:stock,
            warehouseId:warehouseId,
            itemId:itemId,
            userId:userId
        }
    }).then(function (itemId) {
        deferred.resolve(itemId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateLocation(locations,warehouseId,itemId){

    var deferred = Q.defer()
    let upd = ``
    var values = []


    let sql = ` INSERT INTO itemwarehouselocation (itemId,warehouseId,locationId,inStatus,createdAt,updatedAt)
                VALUES`

    for(var i in locations) {
        values.push(`( :itemId, `+ `:warehouseId, `+ locations[i].locationId +`, 1, NOW(), NOW())`)
    }
    sql += values.join(', ')

    upd += ` DELETE FROM itemwarehouselocation
             WHERE   itemId = :itemId AND warehouseId = :warehouseId`

    sequelize.query(upd, {
        replacements: {
            itemId:itemId,
            warehouseId:warehouseId
        },
        type: Sequelize.QueryTypes.DELETE
    }).then(function (result) {

        if (locations.length > 0) {

            sequelize.query(sql, {
                replacements: {
                    itemId:itemId,
                    warehouseId:warehouseId
                },
                type: Sequelize.QueryTypes.INSERT
            }).then(function (result) {
                deferred.resolve(result)
            }).catch(function (error){
                deferred.reject(error)
            })
        }
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getObservationsItem(itemId,warehouseId){

    var deferred = Q.defer()

    let sql = ` SELECT    comment AS observation
                FROM itemcomment
                WHERE itemId = :itemId and warehouseId = :warehouseId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _associateObservationItems(data){

    var deferred = Q.defer()
    let sql = `DELETE FROM itemcomment WHERE  itemId = :itemId AND warehouseId = :warehouseId;
               INSERT INTO itemcomment (comment,itemId,warehouseId) VALUES(:comment,:itemId,:warehouseId);
               UPDATE items SET usModifierId=:userId,updatedAt= NOW() where id=:itemId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:data.itemId,
            userId:data.userId,
            comment:data.comment,
            warehouseId:data.warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getGeneralStock(warehouseId){

    var deferred = Q.defer()

    let sql = `SELECT      STB.*,
                           SUM(ITW.itemQty) as "generalStock"
                           FROM        (SELECT DISTINCT     i.id AS itemId,
                                                            i.dsName AS itemDescription,
                                                            ITR.dsReference as referenceKey,
                                                            GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                            i.brand AS brand,
                                                            iw.itemQty AS stock,
                                                            i.nmNet as "netPrice",
                                                            ROUND(i.nmNet*1.19,0) as "vatPrice"
                                        FROM        items i
                                        INNER JOIN  itemreferences ITR
                                        ON          ITR.itemId = i.id
                                        INNER JOIN itemwarehouse iw
                                        ON          i.id = iw.itemId
                                        WHERE       iw.warehouseId = :warehouseId
                                        GROUP BY    i.id) STB
                                        LEFT JOIN   itemwarehouse ITW
                                        ON          ITW.itemId = STB.itemId
                                        GROUP BY    STB.itemId
                                        ORDER BY    STB.itemId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getGeneralStockByItemId(warehouseId,itemId){

    var deferred = Q.defer()

    let sql = `SELECT      STB.*,
                           SUM(ITW.itemQty) as "generalStock"
                           FROM        (SELECT DISTINCT     i.id AS itemId,
                                                            i.dsName AS itemDescription,
                                                            ITR.dsReference as referenceKey,
                                                            GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                            i.brand AS brand,
                                                            iw.itemQty AS stock,
                                                            i.nmNet as "netPrice",
                                                            ROUND(i.nmNet*1.19,0) as "vatPrice"
                                        FROM        items i
                                        INNER JOIN  itemreferences ITR
                                        ON          ITR.itemId = i.id
                                        INNER JOIN itemwarehouse iw
                                        ON          i.id = iw.itemId
                                        WHERE       iw.warehouseId = :warehouseId and i.id = :itemId
                                        GROUP BY    i.id) STB
                                        LEFT JOIN   itemwarehouse ITW
                                        ON          ITW.itemId = STB.itemId
                                        GROUP BY    STB.itemId
                                        ORDER BY    STB.itemId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            warehouseId:warehouseId,
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getItemsTransit(itemId){

    var deferred = Q.defer()

    let sql = `SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        g.id AS documentId,
                        c.dsName AS "clientName",
                        DATE_FORMAT(g.date, '%d/%m/%y') as dateDocument,
                        w.dsCode AS "origin",
                        wd.dsCode AS "destination",
                        gi.nmUnities AS "outputs"


                        FROM guides g
                        INNER JOIN guidesitems gi
                        ON  g.id = gi.guideId
                        inner JOIN  clients c
                        ON  g.clientId = c.id
                        INNER JOIN items it
                        ON  gi.itemId = :itemId
                        left JOIN  warehouse w
                        ON  g.origin = w.id
                        left JOIN  warehouse wd
                        ON  g.destination = wd.id
                        WHERE it.id = :itemId AND g.status = 1
                        order BY  gi.id desc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getImportInTransitItems(itemId){

    var deferred = Q.defer()

    let sql = `SELECT
                        it.id as "itemId",
                        it.dsName as "itemDescription",
                        g.importId AS documentId,
                        c.dsName AS "clientName",
                        DATE_FORMAT(g.date, '%d/%m/%y') as dateDocument,
                        w.dsCode AS "origin",
                        gi.nmUnities AS "outputs"


                        FROM imports g
                        INNER JOIN importsitems gi
                        ON  g.importId = gi.importId
                        inner JOIN  provider c
                        ON  g.providerId = c.id
                        INNER JOIN items it
                        ON  gi.itemId = :itemId
                        left JOIN  warehouse w
                        ON  g.origin = w.id
                        WHERE it.id = :itemId AND g.status = 1
                        order BY  gi.importId desc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _statusFailUpdate(data){

    var deferred = Q.defer()
    let sql = `UPDATE  items SET failure=:value,observationFails=:observation,usModifierId=:userId,updatedAt=NOW()
               where  id=:itemId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            itemId:data.itemId,
            observation:data.observation,
            value:data.value,
            userId:data.userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _priorityUpdate(data){

    var deferred = Q.defer()
    let sql = `UPDATE  items SET priority=:value,usModifierId=:userId,updatedAt=NOW()
               where  id=:itemId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            userId:data.userId,
            itemId:data.itemId,
            value:data.value
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getTrucks(){

    var deferred = Q.defer()

    let sql = ` SELECT
                    t.id as "id",
                    t.patent as "patent",
                    t.brand AS brand,
                    c.dsName AS "clientName",
                    c.dsCode AS "rut",
                    t.year AS "year",
                    DATE_FORMAT(t.date, '%d/%m/%y') as date,
                    t.model AS "model",
                    t.motor_number AS "motor_number",
                    t.chassis AS "chassis",
                    if(t.isActive=1, 'Activo','Baja') AS "isActive"


                    FROM trucks t
                    inner JOIN  clients c
                    ON  t.clientId = c.id
                    WHERE       t.status=1
                    order BY t.id desc`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _trucksCreate(data){
    var deferred = Q.defer()
    if(data.motorNumber==undefined) data.motorNumber="------"

    let sql = `INSERT INTO trucks (userId,clientId,patent,brand,model,chassis,year,motor_number,date)
                 VALUES (:userId,:clientId,:patent,:brand,:model,:chassis,:year,:motorNumber,NOW())`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            clientId:data.clientId,
            patent:data.patent,
            brand:data.brand,
            model:data.model,
            chassis:data.chassis,
            year:data.year,
            motorNumber:data.motorNumber
        }
    }).then(function (truckId) {
        deferred.resolve(truckId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getTruckDetails(truckId){

    var deferred = Q.defer()

    let sql = `SELECT
                    t.id as "id",
                    t.patent as "patent",
                    t.brand AS brand,
                    c.dsName AS "clientName",
                    DATE_FORMAT(t.date, '%d/%m/%y') as date,
                    t.model AS "model",
                    t.year AS "year",
                    t.motor_number AS "motor_number",
                    t.chassis AS "chassis",
                    t.isActive AS "isActive"


                    FROM trucks t
                    inner JOIN  clients c
                    ON  t.clientId = c.id
                    WHERE t.id = :truckId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            truckId:truckId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _truckUpdate(data){
    var deferred = Q.defer()
    if(data.motor_number=='') data.motor_number=' ------- ';
    if(data.isActive==null) data.isActive=1

    let sql = `update  trucks set patent=:patent,
                                 chassis=:chassis,
                                 year=:year,
                                 motor_number=:motor_number,
                                 model=:model,
                                 brand=:brand,
                                 isActive=:isActive,
                                 userUpdate=:userId,
                                 updateDate= NOW()
               where  trucks.id=:truckId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            truckId:data.truckId,
            userId:data.userId,
            isActive:data.isActive,
            patent:data.patent,
            model:data.model,
            brand:data.brand,
            year:data.year,
            motor_number:data.motor_number,
            chassis:data.chassis
        }
    }).then(function (truckId) {
        deferred.resolve(truckId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _truckDisabled(truckId,userId){

    var deferred = Q.defer()

    let sql = `UPDATE trucks SET status=0, userUpdate=:userId WHERE id=:truckId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            truckId:truckId,
            userId:userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getWishList(){

    var deferred = Q.defer()

    let sql = ` SELECT DISTINCT                     w.wishListId as "wishListId",
                                                    w.jormatId as "jormatId",
                                                    w.brandCode as brandCode,
                                                    w.dsName as "itemDescription",
                                                    w.brand,
                                                    w.dsComments AS comments,
                                                    GROUP_CONCAT(distinct wr.dsReference) as "references",
                                                    DATE_FORMAT(w.createdAt, '%d/%m/%Y') AS date,
                                                    if(w.type=1, 'Nacional','Internacional') AS "type",
                                                    w.quantities as quantities,
                                                    p.dsName AS "providerName",
                                                    u.usName as userCreate,
                                                    w.granted
                                FROM        wishlist w
                                INNER JOIN  wishlistreferences wr
                                ON          wr.wishListId = w.wishListId
                                left JOIN  users u
                                ON          u.usId=w.userId
                                INNER JOIN  provider p
                                ON  w.providerId = p.id
                                WHERE w.granted = 0 and w.status = 1
                                GROUP BY    w.wishListId
                                order BY w.wishListId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getWishlistItems(references){

    var deferred = Q.defer()

    let sql = ` SELECT itemId, itemName, reference, brand, brandCode
                        FROM (
                                SELECT          ITE.id AS itemId,
                                                (UPPER(ITE.dsName)) AS itemName,
                                                group_concat(distinct ITR.dsReference) AS reference,
                                                ITE.inStatus AS 'status',
                                                (UPPER(ITE.brand)) AS brand,
                                                ITE.brandCode as brandCode,
                                                ITR.dsReference  AS 'REFERENCES'
                                                FROM        items ITE
                                                INNER JOIN  itemreferences ITR
                                                ON          ITR.itemId = ITE.id
                                                AND         ITE.inStatus = 1


                                                GROUP BY    ITE.id
                                                order BY ITE.id DESC
                                                )consulta

                                WHERE reference  LIKE  '%`+ references + `%'
                                LIMIT 50`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            references:references
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _wishlistAddItems(data){
    var deferred = Q.defer()
    if(data.itemId==undefined) data.itemId='------';
    if(data.comments==undefined) data.comments="------"
    if(data.brand==undefined) data.brand='no definido'

    let sql = `INSERT INTO wishlist (jormatId,brandCode,dsName,brand,quantities,dsComments,userId,createdAt,providerId,type)
                 VALUES (:itemId,:brandCode,:itemName,:brand,:quantities,:comments,:userId,NOW(),:providerId,:type)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            itemId : data.itemId ,
            itemName : data.itemName,
            brand : data.brand,
            brandCode : data.brandCode,
            providerId : data.providerId,
            quantities: data.quantities,
            type : data.type,
            comments : data.comments
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _wishListAddReferences(data){

    var deferred = Q.defer()
    let sql = `   INSERT INTO wishlistreferences (wishlistId,dsReference,inStatus) VALUES (:wishlistId,:reference,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            wishlistId:data.wishlistId,
            reference:data.reference
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _wishlistDisabled(wishListId,userId){

    var deferred = Q.defer()

    let sql = `UPDATE wishlist SET status=0, userUpdate=:userId WHERE wishListId=:wishListId;
               UPDATE wishlistreferences SET inStatus=0 WHERE wishListId=:wishListId;`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            wishListId:wishListId,
            userId:userId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _wishlistUpdate(data){

    var deferred = Q.defer()

    if(data.itemDescription==undefined) data.itemDescription="no definido"
    if(data.brandCode==undefined) data.brandCode="no definido"
    if(data.brand==undefined) data.brand="no definido"

    let sql = `UPDATE  wishlist SET brandCode=:brandCode,
                                     dsName=:itemDescription,
                                     brand=:brand,
                                     quantities=:quantities,
                                     userUpdate=:userId,
                                     updatedAt=NOW()
               where  wishListId=:wishListId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            wishListId: data.wishListId,
            userId : data.userId,
            brandCode: data.brandCode,
            brand: data.brand,
            itemDescription: data.itemDescription,
            quantities: data.quantities
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _wishlistUpdateId(data){

    var deferred = Q.defer()

    let sql = `UPDATE  wishlist SET jormatId=:itemId
               where  wishListId=:wishListId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            wishListId: data.wishListId,
            itemId: data.itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCatalogs(filters){
	let where = ` 	WHERE 1=1 `
	let aplication = []
	let category = []
    let provider = []

	if (JSON.stringify(filters) !== '{}') {
		filters.replace('"', "'")
		const filter = JSON.parse(filters)
		for (let i in filter) {
			switch (i) {
				case 'description':
					where += "	AND ITE.dsName LIKE '%" + filter[i] + "%'"
					break
				case 'brand':
					where += "AND ITE.brand LIKE '%" + filter[i] + "%'"
					break
                case 'offer':
                    where += "AND ITE.offerIs = 1"
                    break
				case 'aplication':
					aplication = filter[i]
					where += "	AND AP.id IN (:aplication)"
					break
				case 'category':
					category = filter[i]
					where += "	AND CATT.id IN (:category)"
					break
                case 'provider':
                    provider = filter[i]
                    where += "	AND PRO.id IN (:provider)"
                    break

			}
		}
	}

    const sql = `SELECT ITE.id AS itemId,
                        ITE.web as "web",
                        ITE.brand as brand,
                        ITE.dsName AS itemDescription,
                        GROUP_CONCAT(DISTINCT AP.dsName SEPARATOR ' / ') AS applications,
                        GROUP_CONCAT(DISTINCT CATT.dsName) AS categories,
                        GROUP_CONCAT(DISTINCT CATT.dsCode) AS categoryCode
               	FROM     items ITE
				LEFT JOIN   applicationsitems API
				ON    API.itemId = ITE.id
				LEFT JOIN   applications AP
				ON    AP.id = API.applicationId
				LEFT JOIN   itemcategory CAT
				ON    CAT.itemId=ITE.id
				LEFT JOIN   category CATT
				ON    CAT.categoryId=CATT.id
                LEFT JOIN   itemproviders ITP
                ON          ITP.itemId = ITE.id
                LEFT JOIN   provider PRO
                ON          PRO.id = ITP.providerId
				${where}
				GROUP BY    ITE.id`

    return sequelize.query(sql, {
		type: Sequelize.QueryTypes.SELECT,
		replacements:{
			aplication,
			category,
            provider
        }
    })
}


function _permalinkAdd(data){
    var deferred = Q.defer()

    let sql = `update  items set web=:web
               where  items.id=:item`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            item:data.itemId,
            web:data.web
        }
    }).then(function (itemId) {
        deferred.resolve(itemId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}



function _getHistoric(itemId, originId){

    var deferred = Q.defer()

    let sql = `SELECT      STB.*,
                                    SUM(ITW.itemQty) as "generalStock"
                                    FROM        (SELECT DISTINCT     i.id AS itemId,
                                    i.dsName AS itemDescription,
                                    iw.itemQty AS stock,
                                    GROUP_CONCAT(distinct l.dsName) as "locations",
                                    ROUND(i.nmNet*1.19,0) as "vatPrice"
                                    FROM        items i
                                    INNER JOIN itemwarehouse iw
                                    ON          i.id = iw.itemId
                                    INNER JOIN  itemwarehouselocation iwl
                                    ON i.id = iwl.itemId and iwl.warehouseId = :originId
                                    INNER JOIN  location l
                                    ON l.id = iwl.locationId
                                    WHERE     i.id=:itemId
                                    GROUP BY    i.id) STB
                                    LEFT JOIN   itemwarehouse ITW
                                    ON          ITW.itemId = STB.itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _historicalItems(data){


    var deferred = Q.defer()
    if(data.destiny==undefined) data.destiny=null
    if(data.originId==undefined) data.originId=null

    let sign = ''
    if (data.type == 'Salida -'){
        sign = ':previousStock - :quantity,'
    }else if (data.type == 'Entrada +'){
        sign = ':previousStock + :quantity,'
    }else if (data.type == 'Movimiento'){
        sign = ':currentStock,'
    }else if (data.type == 'Creación'){
        sign = ':currentStock,'
    }else if (data.type == 'Modificación'){
        sign = 'null,'
    }

    let sql = `   INSERT INTO itemshistory (        userId,
                                                    date,
                                                    typeDocument,
                                                    documentId,
                                                    idItems,
                                                    previousLocation,
                                                    previousStock,
                                                    currentStock,
                                                    origin,
                                                    type,
                                                    itemDescription,
                                                    destiny,
                                                    prevPrice,
                                                    currentPrice,
                                                    generalStock,
                                                    quantity,
                                                    currentLocation,
                                                    time)
                                            VALUES (:userId,
                                                    now(),
                                                    :document,
                                                    :documentId,
                                                    :itemId,
                                                    :location,
                                                    :previousStock,
                                                     `+ sign +`
                                                    :originId,
                                                    :type,
                                                    :itemDescription,
                                                    :destiny,
                                                    :price,
                                                    :currentprice,
                                                    :generalStock,
                                                    :quantity,
                                                    :currentLocation,
                                                    now()) `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{

            documentId:data.documentId,
            itemId:data.itemId,
            originId:data.originId,
            quantity:data.quantity,
            userId: data.userId,
            previousStock: data.previousStock,
            currentStock:data.currentStock,
            location : data.location,
            itemDescription : data.itemDescription,
            price:data.price,
            document: data.document,
            type: data.type,
            generalStock:data.generalStock,
            currentprice:data.currentprice,
            currentLocation:data.currentLocation,
            destiny:data.destiny


        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getHistoricalTable(itemId){

    var deferred = Q.defer()

    let sql = `SELECT

                        i.documentId AS documentId,
                        i.typeDocument AS typeDocument,
                        i.type AS type,
                        DATE_FORMAT(i.date, '%d/%m/%y') as fecha,
                        if(DATE_FORMAT(i.date, '%H:%i:%S') ='00:00:00', '--',DATE_FORMAT(i.date, '%H:%i:%S')) AS time,
                        i.generalstock,
                        i.previousStock,
                        i.quantity AS "outputs",
                        i.currentStock,
                        w.dsCode AS "origin",
                        wd.dsCode AS "destination",
                        u.usName as userName,
                        i.previouslocation,
                        i.currentLocation,
                        i.prevPrice,
                        i.currentPrice


                        FROM itemshistory i
                        INNER JOIN items it
                        ON  i.idItems = it.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        left JOIN  warehouse wd
                        ON  i.destiny = wd.id
                        inner join users u
                        on i.userId= u.usId
                        WHERE i.idItems = :itemId
                        order BY date desc `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getCurrentInfoItem(itemId,warehouseId){

    var deferred = Q.defer()

    let sql = ` SELECT         STB.*,
                SUM(ITW.itemQty) as "generalStock"
                    FROM (  SELECT    ITE.id  AS itemId,
                                      ITE.dsName  AS dsName,
                                      ITS.itemQty as stockCurrent,
                                      ITE.nmNet AS price,
                                      GROUP_CONCAT(DISTINCT L.dsName) AS locationGroup
                            FROM        items ITE
                            LEFT JOIN   itemwarehouse ITS
                            ON          ITS.itemId = ITE.id
                            LEFT JOIN   warehouse W
                            ON          W.id=ITS.warehouseId
                            LEFT JOIN   location L
                            ON          L.warehouseId = W.id
                            LEFT JOIN   itemwarehouselocation WL
                            ON          WL.itemId = ITE.id
                            AND         WL.locationId = L.id
                            WHERE       WL.itemId = :itemId AND ITS.warehouseId = :warehouseId) STB
                LEFT JOIN   itemwarehouse ITW
                ON          ITW.itemId = STB.itemId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            itemId:itemId,
            warehouseId:warehouseId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _relatedItems(data){

    var deferred = Q.defer()

    let sql = `   INSERT INTO relateditems (itemId,idItemAssociated,inStatus) VALUES(:itemId,:relatedItem,1);
                  INSERT INTO relateditems (itemId,idItemAssociated,inStatus) VALUES(:relatedItem,:itemId,1)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            itemId:data.itemId,
            relatedItem:data.relatedItem

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _relatedItemsDelete(itemId,relatedItem){


    var deferred = Q.defer()


    let sql = ` DELETE FROM relateditems WHERE itemId=:itemId AND idItemAssociated=:relatedItem;
                DELETE FROM relateditems WHERE itemId=:relatedItem AND idItemAssociated=:itemId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.UPDATE,
        replacements:{
            itemId:itemId,
            relatedItem:relatedItem

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getListoffersItems(offset , limit){
	let sql = `		SELECT      STB.*,
                                SUM(ITW.itemQty) as "generalStock"
                    FROM        (SELECT DISTINCT    item.id as "itemId",
                                                    item.dsName as "itemDescription",
                                                    item.brand,
                                                    item.failure,
                                                    item.offerIs,
                                                    item.priority,
                                                    item.dsMaxDiscount as maxDiscount,
                                                    item.brandCode as brandCode,
                                                    item.webType as webType,
                                                    item.oil as oil,
                                                    GROUP_CONCAT(distinct ITR.dsReference) as "references",
                                                    ITR.dsReference as referenceKey,
                                                    GROUP_CONCAT(DISTINCT PRO.dsName) AS providers,
                                                    item.nmNet as "netPrice",
                                                    item.netPrice as netPurchaseValue,
                                                    ROUND(item.nmNet*1.19,0) as "vatPrice",
                                                    item.criticalStock as "criticalStock"
                                FROM        items item
                                INNER JOIN  itemreferences ITR
                                ON          ITR.itemId = item.id
                                left JOIN   itemproviders IP
                                ON          IP.itemId=item.id
                                LEFT JOIN   provider PRO
                                ON          PRO.id = IP.providerId
                                AND         item.inStatus = 1
                                WHERE item.offerIs = 1
                                GROUP BY    item.id) STB
                                INNER JOIN   itemwarehouse ITW
                                ON          ITW.itemId = STB.itemId
                                GROUP BY    STB.itemId
                                ORDER BY    STB.itemId DESC LIMIT :limit OFFSET :offset`;

    return sequelize.query(sql, {
		type		: sequelize.QueryTypes.SELECT,
		replacements:{ offset, limit },
	})
}


module.exports = {
    _getList: _getList,
    _getListQuickView:_getListQuickView,
    _getListDetails:_getListDetails,
    _relatedItems:_relatedItems,
    _getItemsFreeMarket:_getItemsFreeMarket,
    _getWarehouseByItem:_getWarehouseByItem,
    _createItem:_createItem,
    _updateItem:_updateItem,
    _disabledItem:_disabledItem,
    _createItemReferences:_createItemReferences,
    _createItemWarehouse:_createItemWarehouse,
    _createItemApplications:_createItemApplications,
    _createItemWarehouselocation:_createItemWarehouselocation,
    _createItemCategory:_createItemCategory,
    _createItemProvider:_createItemProvider,
    _updateItemProvider:_updateItemProvider,
    _updateItemApplications:_updateItemApplications,
    _updateItemCategory:_updateItemCategory,
    _updateItemWarehouse:_updateItemWarehouse,
    _deleteItemReferences:_deleteItemReferences,
    _updateItemReferences:_updateItemReferences,
    _getStockItem:_getStockItem,
    _getGeneralStockWeb:_getGeneralStockWeb,
    _getLocationsItemByWarehouse:_getLocationsItemByWarehouse,
    _updateStock:_updateStock,
    _updateLocation:_updateLocation,
    _getObservationsItem:_getObservationsItem,
    _associateObservationItems:_associateObservationItems,
    _getGeneralStock:_getGeneralStock,
    _getItemsTransit:_getItemsTransit,
    _getImportInTransitItems:_getImportInTransitItems,
    _statusFailUpdate:_statusFailUpdate,
    _priorityUpdate:_priorityUpdate,
    _getGeneralStockByItemId:_getGeneralStockByItemId,
    _getTrucks:_getTrucks,
    _trucksCreate:_trucksCreate,
    _getTruckDetails:_getTruckDetails,
    _truckUpdate:_truckUpdate,
    _truckDisabled:_truckDisabled,
    _getWishList:_getWishList,
    _getWishlistItems:_getWishlistItems,
    _wishlistAddItems:_wishlistAddItems,
    _wishListAddReferences:_wishListAddReferences,
    _wishlistDisabled:_wishlistDisabled,
    _wishlistUpdate:_wishlistUpdate,
    _wishlistUpdateId:_wishlistUpdateId,
    _getCatalogs:_getCatalogs,
    _permalinkAdd:_permalinkAdd,
    _getHistoric:_getHistoric,
    _historicalItems:_historicalItems,
    _getHistoricalTable:_getHistoricalTable,
    _getCurrentInfoItem:_getCurrentInfoItem,
    _relatedItemsDelete:_relatedItemsDelete,
    _getListoffersItems:_getListoffersItems,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
	_getListMain,
	_getListItemsRelations
}
