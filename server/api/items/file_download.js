const exporter = require('./export.helper')
const pdfmake = require('pdfmake')
const fs = require('fs')
const fonts = require('./fonts')
const path = require('path')

const { resolveCaa } = require('dns')

function downloadItemsPDF(req, res) {

	let response = {}
	const d = new Date()
	response.date = (d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear())
	response.id = req.query.iditems
	response.name = req.query.itemName
	response.bodega = req.query.warehouseName
	response.referencia = req.query.references
	response.location = req.query.location === 'Sin ubicación' ? 'No definido' : req.query.location
	req.query.output = 'docx'


	//Guardamos el contenido en la ubicacion especificada
	const createPdf = async () => {
		const printer = new pdfmake(fonts)


		let document = {
			content: [
				// { text: 'Rotula', style: 'header' },
				{
					style: 'tableExample',
					color: '#444',
					table: {
						widths: [60, 176],
						headerRows: 2,
						// keepWithHeaderRows: 1,
						body: [
							[{ rowSpan: 2, image: 'server/api/items/logo.png', width: 55, height: 55 }, { text: ' ID: ' +response.id, alignment: 'center', style: 'tableColumns' }],
							['', { text: '\n' + response.referencia, alignment: 'center', style: 'reference' }],
							[{ text: response.name, colSpan: 2, alignment: 'center', style: 'subheader' }, {}],
							[
							 {text: response.bodega, colSpan: 1, alignment: 'center', style: 'warehouse'},{ text:response.location, colSpan: 1, alignment: 'center', style: 'foot' }]
						]
					}
				},
			],
			pageSize: {
			    width: 236,
			    height: 'auto'
			  },
			pageMargins: [ 0, 0, 0, 0 ],
			styles: {
				header: {
					fontSize: 21,
					bold: true,
					margin: [0, 0, 0, 0],
					aligment: 'center',
					color: '#000000'
				},
				subheader: {
					fontSize: 16,
					margin: [3, 3, 20, 2],
					bold: true,
				},
				tableExample: {
					margin: [0, 0, 0, 0]
				},
				tableColumns: {
					bold: true,
					fontSize: 20,
					color: 'black'
				},
				reference: {
					bold: true,
					fontSize: 15,
					color: 'black'
				},
				foot: {
					bold: true,
					fontSize: 15,
					color: 'black'
				},
				warehouse: {
					bold: true,
					fontSize: 12,
					color: 'black'
				}
			},
			defaultStyle: {
				// alignment: 'justify'
			}
		}
		let pdfDoc = printer.createPdfKitDocument(document)
		pdfDoc.pipe(fs.createWriteStream('server/api/items/pdfs/'+response.id+'.pdf'))
		pdfDoc.end()
		// await new Promise(resolve => pdfDoc.on('end', resolve));

		// let file = fs.readFileSync('server/api/items/pdfs/pdfTest.pdf')
		// return file;
		await new Promise((resolve, reject) => {
			pdfDoc.on('end', () => {
			  try {
				let file = fs.readFileSync('server/api/items/pdfs/'+response.id+'.pdf')
				resolve(file)
			  } catch (err) {
				reject(err)
			  }
			})
			pdfDoc.on('error', (err) => {
			  reject(err)
			})
		  })
	};


	const downloadPdf = async () => {

		const CONVERSION_FOLDER = 'pdfs'
		const filepath = path.join('./server/api/items', CONVERSION_FOLDER)
		try {
			fs.accessSync(filepath)
		} catch (e) {
			fs.mkdirSync(filepath)
		}
		try {
			try {
				await createPdf();
			} catch (error) {
				res.json({
					data: [],
					error: error,
					message: 'Error al traer archivo de items',
					status: false,
				})
			}
			const output = response.id+'.pdf';

			res.setHeader(
				'Content-Disposition', 'attachment; filename=' + output
			)
			res.setHeader('Content-Type', 'application/pdf')
			// Crear un stream de lectura del archivo
			let fileStream = fs.createReadStream(filepath + '/' + output)
			// Enviar el stream en la respuesta HTTP
			fileStream.pipe(res)

		} catch (e) {
			res.json({
				data: [],
				error: e,
				message: 'Error al traer archivo de items',
				status: false,
			})
		}
	}
	downloadPdf()
}

function deleteLocalFile(file) {
	if (fs.lstatSync(file).isDirectory()) {
		fs.readdirSync(file).forEach(function (cFile) {
			var curPath = file + '/' + cFile

			deleteLocalFile(curPath)
		})

		if (fs.readdirSync(file).length == 0) {
			fs.rmdirSync(file)
		} else {
			setTimeout(deleteLocalFile, 100, file)
		}
	} else {
		fs.unlinkSync(file)
	}
}

module.exports = {
	downloadItemsPDF,
}
