'use strict'

var model = require('./model');
const fs = require('fs')
const csv = require('csv-express')
const Parse = require('csv-parse');
const { parseInt } = require('lodash');
const localenv = require('../../config/local.env.js');
const up_image = require('../up-image/service.js');
const { application } = require('express');
const { networkInterfaces } = require('os');

function uploadImports(req, res) {
	const fileUpload = req.file

	if (!fileUpload) {
		const error = {
			parent : { errno: 1 },
			message: 'archivo omitido',
			sql    : 'no sql',
		}

		res.json({
            data: [],
            status: 0,
			error
        });

		return
	}

	const folderPath = fileUpload.path.toString()
	const source = fs.createReadStream(folderPath)
	const parser = Parse({ delimiter: ';', columns: true })
	let linesRead = 0
	let output = []

	parser.on('readable', function () {
		let record = parser.read()

		while (record) {
			if ((Object.keys(record).length == 1)) {
				if (record[Object.keys(record)[0]].length != 0) {
					errorLectura = "Delimitador debe ser ';' "

					return false
				}
			} else {
				linesRead++
				if(record.codigo != undefined && record.codigo != null && record.codigo != '') {
					output.push({
						itemId : record.codigo,
						itemDescription : record.nombre,
						quantity : (record.cantidad == undefined || record.cantidad == null || record.cantidad == '') ? 0 : record.cantidad,
						disscount : (record.desc == undefined || record.desc == null || record.desc == '') ? 0 : record.desc,
						netPrice : (record.precio == undefined || record.precio == null || record.precio == '') ? 0 : record.precio,
						referenceKey : (record.referencia == undefined || record.referencia == null || record.referencia == '') ? 0 : record.referencia
					})
				}
			}

			record = parser.read()
		}
	})

	parser.on('end', function () {
		if (linesRead == 0) {
			res.status(500).json({ status: false, error: 'Archivo Vacio' })

			return false
		}

		res.json({
			data    : output,
			status     : 1,
		})
	})

	parser.on('error', function () {
		res.status(500).json({ status: false, error: 'Error de Formato' })

		return false
	})

	source.pipe(parser)
}

function getListMain(req, res) {
	const offset = (parseInt(req.query['newPage']) - 1) * parseInt(req.query['pageSize'])
	const limit = parseInt(req.query['pageSize'])
	const {
		filters = {},
		searchText = null,
	} = req.query

	const promises = [
		model._getListMain(filters, offset, limit, false, searchText),
		model._getListMain(filters, offset, limit, true, searchText)
	]

	Promise.all(promises).then(async (result) => {
		result[0] = result[0].length ? await parsedData(result[0]) : result[0]
		res.json({
			data: result[0],
			count: result[1][0].total,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}

function getListItemsRelations(req, res) {
	const itemId = req.query['itemId'] || false;

	if (!itemId) {
		const error = {
			parent : { errno: 1 },
			message: 'Id de item no proporcionado',
			sql    : 'no sql',
		}

		res.json({
            data: [],
            status: 0,
			error
        });

		return
	}

	model._getListItemsRelations(itemId).then(async (result) => {
		result = result.length ? await parsedData(result) : result
		res.json({
			data: result,
			status: 1
		});
    }).catch((error) => {
        res.json({
            data: [],
			message: error,
            status: 0
        });
    })
}

function relatedItems(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._relatedItems(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "Item relacionado correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function parsedData(data) {
	return data.map(item => {
		const val = up_image.getImagePath(item.itemId) ? localenv.DOMAIN + `/api/image/image/${item.itemId}` : null
		return {
			...item,
			urlImage: val
		}
	})
}

function getList(req, res) {
    let query = [];
    query.push(model._getList())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getListQuickView(req, res) {
    let query = [];
    query.push(model._getListQuickView())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsFreeMarket(req, res) {
    let query = [];
    query.push(model._getItemsFreeMarket())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getitemDetails(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getListDetails(itemId))
    query.push(model._getWarehouseByItem(itemId))

    Promise.all(query).then((result) => {
        var objArray=[],providerM=[],applicationM=[],categoryM=[],warehouses=[],providerTemp=''
        var temp={};
        var arr = []

        result[1].forEach(function (warehouse){
            if(typeof arr[warehouse.warehouseId] === 'undefined'){
                arr[warehouse.warehouseId] = warehouse
                arr[warehouse.warehouseId].locations = []
                arr[warehouse.warehouseId].locationP = warehouse.locationP
            }else  arr[warehouse.warehouseId].locationP += ',' + warehouse.locationP

            if (temp[warehouse.warehouseId]==undefined){
                    temp[warehouse.warehouseId]={}
                    temp[warehouse.warehouseId].locations=[]
            }
            temp[warehouse.warehouseId].locations.push( {
                                                    "locationName": warehouse.locationName,
                                                    "locationId": warehouse.locationId,
                                                    "locationCode": warehouse.locationCode
                                                    })
        })
        for(var i in arr){
            if (arr[i].warehouses==undefined){
                arr[i].warehouses={"warehouseid": arr[i].warehouseId,
                                    "warehouseName": arr[i].warehouseName,
                                    "warehouseCode": arr[i].warehouseCode,
                                    "stock": arr[i].quantity,
                                    "locationP": arr[i].locationP,
                                    "locations": temp[i].locations}
            }
            // arr[i].warehouses.locationP =+ ', ' +arr[i].locationP
        }
        for(var i in arr){
            warehouses.push(arr[i].warehouses)
        }

        for(const i in result[0]){
            let obj={}
            obj=result[0][i]
			if(obj.providerP !== null){
				let provider=obj.providerP.split(',')

				for(const j in provider ){
					providerTemp=provider[j].split('-')
					providerM.push({providerName:providerTemp[1],providerCode:providerTemp[0]})
				}
			}

            if(obj.applicationP !== null){
				let application=obj.applicationP.split(',')

				for(const j in application ){
					providerTemp=application[j].split('-')
					applicationM.push({applicationName:providerTemp[1],applicationCode:providerTemp[0]})
				}
			}

            if(obj.categoryP!==null){
				let category=obj.categoryP.split(',')

				for(const j in category ){
					providerTemp=category[j].split('-')
					categoryM.push({categoryName:providerTemp[1],categoryCode:providerTemp[0]})
				}
            }

            var temp= {
                "providers": providerM,
                "applications": applicationM,
                "references": obj.reference.split(','),
                "categories":categoryM,
                "warehouses": warehouses
             }
             obj.providers=temp['providers']
             obj.applications=temp['applications']
             obj.references=temp['references']
             obj.categories=temp['categories']
             obj.warehouses=temp['warehouses']
			 const val = up_image.getImagePath(obj.itemId) ? localenv.DOMAIN + `/api/image/image/${obj.itemId}` : null
			 obj.urlImage = val

             delete obj.providerName
             delete obj.providerCode
             delete obj.applicationName
             delete obj.applicationCode
             delete obj.categoryName
             delete obj.categoryCode
             delete obj.warehouseName
             delete obj.quantity
             delete obj.locationName
             delete obj.locationId
             delete obj.locationCode
             delete obj.REFERENCES
             delete obj.reference
             delete obj.warehouseId
             delete obj.warehouseCode

             objArray.push(obj)
        }


        res.json({
            data: objArray,
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createItem(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){

        // query.push(model._createItem(data))

        Promise.all([model._createItem(data)]).then((result) => {
            query=[]
            for(var  i in data.references){
                query.push(model._createItemReferences(data.references[i],result[0]))
            }

            for(var  i in data.warehouses){
              for(var j in data.warehouses[i].locationsIds){
                query.push(model._createItemWarehouse(data.warehouses[i].warehouseId,data.warehouses[i].locationsIds[j],result[0]))
              }
            }

            for(var  i in data.warehouses){
              for(var j in data.warehouses[i].locationsIds){
                query.push(model._createItemWarehouselocation(data.warehouses[i].warehouseId,data.warehouses[i].locationsIds[j],result[0]))
              }
            }

            for(var i in data.categoriesIds){
                query.push(model._createItemCategory (data.categoriesIds[i],result[0]))
            }

            for(var i in data.applicationsIds){
                query.push(model._createItemApplications (data.applicationsIds[i],result[0]))
            }

            for(var i in data.providersIds){
                query.push(model._createItemProvider(data.providersIds[i],result[0]))
            }

                Promise.all(query).then((results) => {

                    res.json({
                        data: result[0],
                        status: 1
                    });

                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            status: 0
        });
    }

}

function updateItem(req, res) {


    var query = [];
    var data = req.body;
    if(req.body){

        Promise.all([model._updateItem(data)]).then((result) => {
            query=[]
            // query.push(model._deleteItemReferences(data.itemId))
            // for(var  i in data.references){
            //     query.push(model._createItemReferences(data.references[i],data.itemId))
            // }
            query.push(model._updateItemApplications(data.applicationsIds,data.itemId))
            query.push(model._updateItemReferences(data.references,data.itemId))
            query.push(model._updateItemCategory(data.categoriesIds,data.itemId))
            query.push(model._updateItemProvider(data.providersIds,data.itemId))


            Promise.all(query).then((results) => {

                res.json({
                    data: data.itemId,
                    status: 1
                });

            }).catch(function(error) {

                res.json({
                    data: error,
                    status: 0,
                    error: console.log('error 1',error)
                });
            })

        }).catch(function(error) {
            res.json({
                data: error,
                status: 0,
                error: console.log('error 2',error)
            });
        })

    }else{
        res.json({
            data: [],
            error: error,
            status: 0
        });
    }

}

function disabledItem(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var userId = req.query['userId'];
    if(itemId){
        query.push(model._disabledItem(itemId,0))

        Promise.all(query).then((result) => {
            res.json({
                data: itemId,
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }else{
        res.json({
            data: [],
            status: 0
        });
    }

}

function getStockItem(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getStockItem(itemId,warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGeneralStockWeb(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getGeneralStockWeb(itemId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGeneralStock(req, res) {

    var query = [];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getGeneralStock(warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getGeneralStockByItemId(req, res) {

    var query = [];
    var warehouseId = req.query['warehouseId'];
    var itemId = req.query['itemId'];
    query.push(model._getGeneralStockByItemId(warehouseId,itemId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getLocationsItemByWarehouse(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getLocationsItemByWarehouse(itemId,warehouseId))
    query.push(model._getStockItem(itemId,warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            stock: result[1],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function itemWarehouseUpdate(req, res) {

    var query = [];
    var data = req.body;
    var stock = req.body.stock
    var warehouseId = req.body.warehouseId
    var itemId = req.body.itemId
    var locations = req.body.locations
    var userId = req.body.userId

    query.push(model._updateStock(stock,warehouseId,itemId,userId))
    query.push(model._updateLocation(locations,warehouseId,itemId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getObservationsItem(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getObservationsItem(itemId,warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function associateObservationItems(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._associateObservationItems(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "comment is created successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsTransit(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getItemsTransit(itemId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getImportInTransitItems(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getImportInTransitItems(itemId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function statusFailUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._statusFailUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "item is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function priorityUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._priorityUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "item is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getTrucks(req, res) {

    var query = [];
    query.push(model._getTrucks())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function trucksCreate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._trucksCreate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "item is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getTruckDetails(req, res) {

    var query = [];
    var truckId = req.query['truckId'];
    query.push(model._getTruckDetails(truckId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function truckUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._truckUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "trucks is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function truckDisabled(req, res) {

    var query = [];
    var truckId = req.query['truckId'];
    var userId = req.query['userId'];
    query.push(model._truckDisabled(truckId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "El Camión ha sido deshabilitada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getWishList(req, res) {

    var query = [];
    query.push(model._getWishList())

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getWishlistItems(req, res) {

    var query = [];
    var references = req.query['references'];
    query.push(model._getWishlistItems(references))

    Promise.all(query).then((result) => {

        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function wishlistAddItems(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._wishlistAddItems(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "El item fue agregado a lista de deseos exitosamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}



function wishListAddReferences(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._wishListAddReferences(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "references is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function wishlistDisabled(req, res) {

    var query = [];
    var wishListId = req.query['wishListId'];
    var userId = req.query['userId'];
    query.push(model._wishlistDisabled(wishListId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "El item ha sido deshabilitada correctamente",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function wishlistUpdate(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._wishlistUpdate(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "item is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function wishlistUpdateId(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._wishlistUpdateId(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "item wishlist is update successfull",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCatalogs(req, res) {
	const {
		filters = {},
	} = req.query

    model._getCatalogs(filters).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function permalinkAdd(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._permalinkAdd(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            msg: "web is is update successfull in items",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function uploadItems(req, res) {
    const fileUpload = req.file

	if (!fileUpload) {
		const error = {
			parent : { errno: 1 },
			message: 'archivo omitido',
			sql    : 'no sql',
		}

		res.json({
            data: [],
            status: 0,
			error
        });

		return
	}

	const folderPath = fileUpload.path.toString()
	const source = fs.createReadStream(folderPath)
	const parser = Parse({ delimiter: ';', columns: true })
	let linesRead = 0
	let output = []

	parser.on('readable', function () {
		let record = parser.read()

		while (record) {
			if ((Object.keys(record).length == 1)) {
				if (record[Object.keys(record)[0]].length != 0) {
					errorLectura = "Delimitador debe ser ';' "

					return false
				}
			} else {
				linesRead++
				if(record.descripcion != undefined && record.descripcion != null && record.descripcion != '') {
					output.push({
                        itemDescription : record.descripcion,
                        category : (record.categoria == undefined || record.categoria == null || record.categoria == '') ? 0 : record.categoria,
                        aplication : (record.aplicaciones == undefined || record.aplicaciones == null || record.aplicaciones == '') ? 0 : record.aplicaciones,
                        provider : (record.proveedores == undefined || record.proveedores == null || record.proveedores == '') ? 0 : record.proveedores,
                        brand : (record.marca == undefined || record.marca == null || record.marca == '') ? 0 : record.marca,
                        brandCode : (record.codigo_marca == undefined || record.codigo_marca == null || record.codigo_marca == '') ? 0 : record.codigo_marca,
                        references : (record.referencia == undefined || record.referencia == null || record.referencia == '') ? 0 : record.referencia,
                        netPrice : (record.compra_neta == undefined || record.compra_neta == null || record.compra_neta == '') ? 0 : record.compra_neta,
                        purchasePrice : (record.compra == undefined || record.compra == null || record.compra == '') ? 0 : record.compra,
                        netPurchase : (record.neto == undefined || record.neto == null || record.neto == '') ? 0 : record.neto,
                        userId : (record.id_usuario == undefined || record.id_usuario == null || record.id_usuario == '') ? 0 : record.id_usuarioxxxx,
                        warehouses:[{
                            warehouseId: 1,
                            locationsIds: [419]
                        },
                        {
                            warehouseId: 2,
                            locationsIds: [1096]
                        },
                        {
                            warehouseId: 3,
                            locationsIds: [1615]
                        },
                        {
                            warehouseId: 4,
                            locationsIds: [2626]
                        },
                        {
                            warehouseId: 5,
                            locationsIds: [3117]
                        },
                        {
                            warehouseId: 6,
                            locationsIds: [3396]
                        }

                    ]
			})
				}
			}

			record = parser.read()

		}
	})

	parser.on('end', function () {
		if (linesRead == 0) {
			res.status(500).json({ status: false, error: 'Archivo Vacio' })

			return false
		}
        //else if

		res.json({
			data    : output,
			status     : 1,
		})
        for(var i in output){
            insertCsv(output[i])
        }


	})

	parser.on('error', function () {
		res.status(500).json({ status: false, error: 'Error de Formato' })

		return false
	})

	source.pipe(parser)

}


function insertCsv(data) {

    var query = [];
    var data= data;
    var reference = data.references;

    if(data){
        Promise.all([model._createItem(data)]).then((result) => {
            query=[]
                query.push(model._createItemReferences(reference,result))
                query.push(model._createItemCategory (data.category,result))
                query.push(model._createItemApplications (data.aplication,result))
                query.push(model._createItemProvider(data.provider,result))

              for(var  i in data.warehouses){
                for(var j in data.warehouses[i].locationsIds){
                  query.push(model._createItemWarehouselocation(data.warehouses[i].warehouseId,data.warehouses[i].locationsIds[j],result[0]))
                }
              }

              for(var  i in data.warehouses){
              for(var j in data.warehouses[i].locationsIds){
                query.push(model._createItemWarehouse(data.warehouses[i].warehouseId,data.warehouses[i].locationsIds[j],result[0]))
              }
            }



        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })


    }else{
        res.json({
            data: [],
            status: 0
        });
    }

return false
}


function getHistoric(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var originId = req.query['originId'];
    query.push(model._getHistoric(itemId, originId))



    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function historicalItems(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._historicalItems(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "Historical register items succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getHistoricalTable(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    query.push(model._getHistoricalTable(itemId))



    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getCurrentInfoItem(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var warehouseId = req.query['warehouseId'];
    query.push(model._getCurrentInfoItem(itemId,warehouseId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            stock: result[1],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function relatedItemsDelete(req, res) {

    var query = [];
    var itemId = req.query['itemId'];
    var relatedItem = req.query['relatedItem'];
    if(itemId){
        query.push(model._relatedItemsDelete(itemId,relatedItem))

        Promise.all(query).then((result) => {
            res.json({
                data: itemId,
                status: 1
            });
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })
    }else{
        res.json({
            data: [],
            status: 0
        });
    }

}

function getListoffersItems(req, res) {

	const offset = (parseInt(req.query['pageSize']) - 1) * parseInt(req.query['page'])
	const limit = parseInt(req.query['page']) || false

    model._getListoffersItems(offset, limit).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getList: getList,
    getListQuickView:getListQuickView,
    getitemDetails:getitemDetails,
    getItemsFreeMarket:getItemsFreeMarket,
    createItem:createItem,
    updateItem:updateItem,
    disabledItem:disabledItem,
    getStockItem:getStockItem,
    getGeneralStockWeb:getGeneralStockWeb,
    getLocationsItemByWarehouse:getLocationsItemByWarehouse,
    itemWarehouseUpdate:itemWarehouseUpdate,
    getObservationsItem:getObservationsItem,
    associateObservationItems:associateObservationItems,
    getGeneralStock:getGeneralStock,
    getItemsTransit:getItemsTransit,
    getImportInTransitItems:getImportInTransitItems,
    statusFailUpdate:statusFailUpdate,
    priorityUpdate:priorityUpdate,
    getGeneralStockByItemId:getGeneralStockByItemId,
    getTrucks:getTrucks,
    trucksCreate:trucksCreate,
    getTruckDetails:getTruckDetails,
    truckUpdate:truckUpdate,
    truckDisabled:truckDisabled,
    getWishList:getWishList,
    getWishlistItems:getWishlistItems,
    wishlistAddItems:wishlistAddItems,
    wishListAddReferences:wishListAddReferences,
    wishlistDisabled:wishlistDisabled,
    wishlistUpdate:wishlistUpdate,
    wishlistUpdateId:wishlistUpdateId,
    getCatalogs:getCatalogs,
    permalinkAdd:permalinkAdd,
    uploadItems:uploadItems,
    getHistoric:getHistoric,
    historicalItems:historicalItems,
    getHistoricalTable:getHistoricalTable,
    getCurrentInfoItem:getCurrentInfoItem,
    relatedItems:relatedItems,
    relatedItemsDelete:relatedItemsDelete,
    getListoffersItems:getListoffersItems,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
	uploadImports,
	getListMain,
	getListItemsRelations
}
