var express = require('express');
const multer = require('multer')
// view engine setup
var router = express.Router();
var service = require('./services');
const file = require('./file_download')
const upload = multer({ dest: 'files/' })
const uploadPdf = multer({
	dest: 'server/api/items/pdfs/'
})
router.get('/', service.getitemDetails);
router.get('/list-items-relations', service.getListItemsRelations);
router.post('/items-relations', service.relatedItems);
router.delete('/items-relations', service.relatedItemsDelete);
router.post('/', service.createItem);
router.put('/', service.updateItem);
router.put('/itemWarehouseUpdate', service.itemWarehouseUpdate);
router.delete('/', service.disabledItem);
router.get('/list', service.getList);
router.get('/list-quick-view', service.getListQuickView);
router.get('/list-main', service.getListMain);
// router.get('/list-providers', service.getListProviders);
router.get('/stock', service.getStockItem);
router.get('/locations', service.getLocationsItemByWarehouse);
router.get('/current-info', service.getCurrentInfoItem);
router.get('/observations', service.getObservationsItem);
router.put('/observations', service.associateObservationItems);
router.get('/general-stock', service.getGeneralStock);
router.get('/general-stock-web', service.getGeneralStockWeb);
router.get('/general-stock-id', service.getGeneralStockByItemId);
router.get('/transit', service.getItemsTransit);
router.get('/import-transit', service.getImportInTransitItems);
router.get('/free-market', service.getItemsFreeMarket);
router.put('/status-fail', service.statusFailUpdate);
router.put('/priority', service.priorityUpdate);
// router.get('/getLocations',service.getLocations);
router.post('/uploadImports', upload.single('uploadImports'), service.uploadImports);
router.post('/uploadItems', upload.single('uploadItems'), service.uploadItems);
router.get('/wish-list', service.getWishList)
router.get('/wishlist-items', service.getWishlistItems)
router.post('/wish-list', service.wishlistAddItems)
router.post('/wishlist-references', service.wishListAddReferences)
router.delete('/wish-list', service.wishlistDisabled)
router.put('/wish-list', service.wishlistUpdate)
router.put('/wishlist-id', service.wishlistUpdateId)
router.put('/permalink', service.permalinkAdd);

router.get('/trucks', service.getTrucks);
router.post('/trucks', service.trucksCreate);
router.get('/trucks-details', service.getTruckDetails);
router.put('/trucks-update', service.truckUpdate);
router.delete('/trucks', service.truckDisabled);
router.get('/downloadItemsPDF', uploadPdf.single('downloadItemsPDF'), file.downloadItemsPDF)
router.get('/historic', service.getHistoric);
router.post('/historical', service.historicalItems);
router.get('/historical-table', service.getHistoricalTable);
router.get('/catalogs', service.getCatalogs);
router.get('/offers', service.getListoffersItems)

module.exports = router;


