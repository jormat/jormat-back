const mkdirp = require('mkdirp')
const fs = require('fs')
const Docxtemplater = require('docxtemplater')
const JSZip = require('jszip')
let path = require('path')
import { libreOffice } from '../helpers/fileConverter.helper'
let templatePath = false
const CONVERSION_FOLDER = 'conversion'
const SHARED_FOLDER = path.join('./uploadfiles', CONVERSION_FOLDER)

try {
	fs.statSync('./server/api/report-templates/rotula')
	templatePath = `./server/api/report-templates/rotula`
} catch (err) {
	console.log(err)
}

function exportJSON(req, res, data, file) {
	if (!templatePath) {
		templatePath = `./server/api/report-templates/rotula`
	}

	const filepath = SHARED_FOLDER
	mkdirp.sync(filepath)
	if( !fs.existsSync(path.resolve(templatePath, file)) ){
		templatePath = `./server/api/report-templates/rotula`
	}
	console.log('filepath', filepath)

	const content = fs
		.readFileSync(path.resolve(templatePath, file), 'binary')
	const doc = new Docxtemplater()
	const zip = new JSZip(content)
	doc.loadZip(zip)
	doc.setOptions({ parser })
	const json = data
	doc.setData(json)
	console.log('documento', doc)
	try {
		doc.render()
	} catch (error) {
		res.json({ data: 'Error indefinido' })

		return false
	}
	console.log('filepath 3', filepath)
	const buf = doc.getZip()
		.generate({ type: 'nodebuffer' })

	const name = 'rotula'
	const wordPath = path.join(filepath, `${name}output.docx`)
	fs.writeFileSync(path.resolve(wordPath), buf)
	console.log('filepath 4', filepath)
	/* Se debe considerar que libre office no funciona en Windows con consola, se debe realizar prueba final en servidor */
	if (req.query.output == 'pdf') {
		const pdfPath = path.join(filepath, `${name}.pdf`)

		libreOffice.wordToPdf(`${name}output.docx`, `${name}output.pdf`, CONVERSION_FOLDER)
			.then(() => {
				const output = 'rotula.pdf'

				res.setHeader('Content-Disposition', 'attachment; filename=' + output)
				res.download(pdfPath, output)
				setTimeout(deleteLocalFile, 20000, pdfPath)
			})
			.catch((error) => {
				console.error(error)
				res.json({
					data       : [],
					error      : error,
					messageType: 'error',
					message    : 'Error al consultar data',
					status     : false,
				})
			})
	} else {
		const output = 'rotula.docx'

		res.setHeader('Content-Disposition', 'attachment; filename=' + output)
		res.download(wordPath, output)
	}

	setTimeout(deleteLocalFile, 20000, wordPath)
}

function deleteLocalFile(file) {
	if (fs.lstatSync(file).isDirectory()) {
		fs.readdirSync(file).forEach(function (cFile) {
			var curPath = file + '/' + cFile

			deleteLocalFile(curPath)
		})

		if (fs.readdirSync(file).length == 0) {
			fs.rmdirSync(file)
		} else {
			setTimeout(deleteLocalFile, 100, file)
		}
	} else {
		fs.unlinkSync(file)
	}
}

function parser(tag) {
	return {
		get(scope, context) {
			if (tag === '$index') {
				const indexes = context.scopePathItem

				return indexes[indexes.length - 1] + 1
			}

			if (tag === '.') {
				return scope
			}

			return scope[tag]
		},
	}
}

module.exports = {
	exportJSON: exportJSON,
}
