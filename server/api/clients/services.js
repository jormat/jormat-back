'use strict'

var model = require('./model');


function getClients(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getList(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function getClientsByName(req, res) {

    var query = [];
    var name = req.query['name'];
    query.push(model._getClientsByName(name))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientsByNameCategory(req, res) {

    var query = [];
    var name = req.query['name'];
    query.push(model._getClientsByNameCategory(name))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function getClientsByDsName(req, res) {

    var query = [];
    var name = req.query['name'];
    query.push(model._getClientsByDsName(name))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientsDetails(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._getClientsDetails(clientId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createClient(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createClient(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the client is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateClient(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateClient(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Client is updated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function lockedClient(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._lockedClient(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Client is unLocked/locked succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledClient(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    var userId = req.query['userId'];
    query.push(model._disabledClient(clientId,userId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Client is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function activeCredit(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._activeCredit(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Client credit is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function clientHistory(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._clientHistory(clientId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientsRut(req, res) {

    var query = [];
    var rut = req.query['rut'];
    query.push(model._getClientsRut(rut))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientAccount(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    var startDate = req.query['startDate'];
    var endDate = req.query['endDate'];
    query.push(model._getClientAccount(startDate,endDate,clientId))
    query.push(model._getPaymentsTotal(clientId,startDate,endDate))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            total: result[1],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesNotificationClient(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._getInvoicesNotificationClient(clientId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNotesNotificationClient(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._getNotesNotificationClient(clientId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getChecksNotificationClient(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._getChecksNotificationClient(clientId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


function getClientPayments(req, res) {

    var query = [];
    var clientId = req.query['clientId'];
    query.push(model._getClientPayments(clientId))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updateAddress(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateAddress(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the Client address is updated succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getClients: getClients,
    getClientsByName:getClientsByName,
    getClientsByNameCategory:getClientsByNameCategory,
    getClientsByDsName:getClientsByDsName,
    getClientsDetails:getClientsDetails,
    createClient:createClient,
    updateClient:updateClient,
    lockedClient:lockedClient,
    disabledClient:disabledClient,
    clientHistory:clientHistory,
    getClientsRut:getClientsRut,
    getClientAccount : getClientAccount,
    getInvoicesNotificationClient:getInvoicesNotificationClient,
    getNotesNotificationClient:getNotesNotificationClient,
    getChecksNotificationClient:getChecksNotificationClient,
    activeCredit:activeCredit,
    getClientPayments:getClientPayments,
    updateAddress:updateAddress,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}