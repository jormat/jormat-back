'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');


function _getList(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                    id as "clientId",
                    dsName as "fullName",
                    dsCode as  "rut",
                    dsPhoneNumber as "mobile",
                    dsEmail as  "email",
                    if(locked=0, 'Activo','Bloqueado') AS "isLocked",
                    if(isCredit=1, 'Crédito','Sin crédito') AS "isCredit",
                    if(category=0, 'NO','SI') AS "category"
                    FROM clients where inStatus = 1 
                    order by id DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsByName(name){

    var deferred = Q.defer()
    // if(name==undefined) name=1

    let sql = `SELECT   distinct STB.* FROM
                        (SELECT  id as "clientId",
                            (UPPER(dsName)) as "fullName",
                            dsCode as  "rut",
                            locked as "locked",
                            if(isCredit=1, 'Crédito','Sin crédito') AS "isCredit",
                            (UPPER(dsDesc)) as "context",
                            (UPPER(dsAddress)) as "address",
                            (UPPER(address2)) as "address2",
                            (UPPER(dsCity)) as "city",
                            dsPhoneNumber as "phone",
                            category as "category",
                            (UPPER(dsEmail)) as "email"

                        FROM clients WHERE inStatus = 1 and dsCode LIKE  '%`+ name + `%' OR dsName LIKE  '%`+ name + `%' and inStatus = 1 limit 10)STB
                        WHERE category  = 1`
                             

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            name:name
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsByDsName(name){

    var deferred = Q.defer()

    let sql = `     SELECT  id as "clientId",
                            (UPPER(dsName)) as "fullName",
                            dsCode as  "rut",
                            locked as "locked",
                            (UPPER(dsDesc)) as "context",
                            (UPPER(dsAddress)) as "address",
                            (UPPER(address2)) as "address2",
                            (UPPER(dsCity)) as "city",
                            dsPhoneNumber as "phone",
                            (UPPER(dsEmail)) as "email"

                            FROM clients WHERE dsName LIKE  '%`+ name + `%' and inStatus = 1 limit 10 `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            name:name
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsByNameCategory(name){

    var deferred = Q.defer()

    let sql = `SELECT  id as "clientId",
                            (UPPER(dsName)) as "fullName",
                            dsCode as  "rut",
                            locked as "locked",
                            if(isCredit=1, 'Crédito','Sin crédito') AS "isCredit",
                            (UPPER(dsDesc)) as "context",
                            (UPPER(dsAddress)) as "address",
                            (UPPER(address2)) as "address2",
                            (UPPER(dsCity)) as "city",
                            dsPhoneNumber as "phone",
                            category as "category",
                            (UPPER(dsEmail)) as "email"

                        FROM clients WHERE inStatus = 1 and dsCode LIKE  '%`+ name + `%' OR dsName LIKE  '%`+ name + `%' and inStatus = 1 limit 10`
                             

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            name:name
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsDetails(clientId){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                    id as "clientId",
                    dsName as "fullName",
                    dsCode as  "rut",
                    dsPhoneNumber as "phone",
                    dsAddress as "address",
                    address2 as "address2",
                    dsMobileNumber as "mobile",
                    dsEmail as  "email",
                    dsCity as  "cityName",
                    dsDesc as  "commercialBusiness",
                    dsUrl as  "web",
                    blockingReason as  "blockingReason",
                    customerManager as  "customerManager",
                    if(locked=0, 'Activo','Bloqueado') AS "isLocked",
                    locked as  "locked",
                    if(isCredit=1, 'Crédito','Sin crédito') AS "isCredit",
                    if(category=0, 'NO','SI') AS "category",
                    category AS "categoryValue"
                    FROM clients 
                          where id = :clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
    
}

function _createClient(data){

    var deferred = Q.defer()
    if(data.phone==undefined) data.phone="s/n"
    if(data.mobile==undefined) data.mobile="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.web==undefined) data.web="www."
    if(data.commercialBusiness==undefined) data.commercialBusiness="Giro no definido" 
    if(data.customerManager==undefined) data.customerManager="No definido"
    if(data.credit==undefined) data.credit=0
    if(data.address2==undefined) data.address2=null
    if(data.category==undefined) data.category=1
    if(data.region==undefined) data.region=0
    if(data.cityName==undefined) data.cityName=null
    if(data.email==undefined) data.email="sin mail"
    
    let sql = ` INSERT INTO clients (
                                dsName,
                                dsCode,
                                dsAddress,
                                address2,
                                dsPhoneNumber,
                                dsMobileNumber,
                                dsEmail,
                                dsUrl,
                                regionId,
                                dsCity,
                                dsDesc,
                                customerManager,
                                inStatus,
                                locked,
                                isCredit,
                                category) 
                      VALUES (  :fullName,
                                :rut,
                                :address,
                                :address2,
                                :phone,
                                :mobile,
                                :email,
                                :web,
                                :region,
                                :cityName,
                                :commercialBusiness,
                                :customerManager,
                                 1,
                                 0,
                                :credit,
                                :category)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            fullName: data.fullName,
            rut: data.rut,
            address: data.address,
            address2: data.address2,
            phone: data.phone,
            mobile: data.mobile,
            email: data.email,
            web: data.web,
            region: data.region,
            cityName: data.cityName,
            commercialBusiness: data.commercialBusiness,
            customerManager: data.customerManager,
            credit: data.credit,
            category: data.category


        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateClient(data){

    var deferred = Q.defer()
    if(data.mobile== undefined) data.mobile="s/n"
    if(data.address== undefined) data.address='Sin dirección'
    if(data.web== undefined) data.web="www."
    if(data.commercialBusiness== "") data.commercialBusiness="Giro no definido"
    if(data.customerManager==undefined) data.customerManager="No definido"
    if(data.observation== undefined) data.observation='no hay comentarios'
    if(data.address2==undefined) data.address2=null
    
    let sql = `UPDATE  clients SET 
                                 dsName=:clientName,
                                 dsCode=:rut,
                                 dsAddress=:address,
                                 address2=:address2,
                                 dsPhoneNumber=:phone,
                                 dsMobileNumber=:mobile,
                                 dsEmail=:email,
                                 dsUrl=:web,
                                 dsCity=:cityName,
                                 dsDesc=:commercialBusiness,
                                 customerManager=:customerManager,
                                 category=:category,
                                 blockingReason=:observation
                          WHERE  clients.id=:clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId: data.clientId,
            clientName: data.clientName,
            rut: data.rut,
            address: data.address,
            address2: data.address2,
            phone: data.phone,
            mobile: data.mobile,
            email: data.email,
            web: data.web,
            cityName: data.cityName,
            commercialBusiness: data.commercialBusiness,
            customerManager: data.customerManager,
            category: data.category,
            observation: data.observation

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _lockedClient(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE clients SET locked=:value,blockingReason=:blockingReason WHERE id=:clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            value:data.isLocked,
            blockingReason:data.blockingReason
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledClient(clientId,userId){

    var deferred = Q.defer()
    
    let sql = `UPDATE clients SET inStatus=0 WHERE id=:clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:clientId
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _activeCredit(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE clients SET isCredit=:value WHERE id=:clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            value:data.value
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _clientHistory(clientId){

    var deferred = Q.defer()

    let sql = `     SELECT  i.invoiceId as documentId,
                        i.total as total,
                        i.status,
                        s.otherName AS "statusName",
                        w.dsCode AS "origin",
                        t.name as type,
                        DATE_FORMAT(i.date, '%d/%m/%y') as date

                        from clients c
                        inner join invoices i
                        on i.clientId=c.id
                        left JOIN  warehouse w
                        ON  i.origin = w.id
                        left JOIN  typeDocument t
                        ON  t.id = i.typeDoc
                        left JOIN  status s
                        ON  i.status = s.id
                        where c.id= :clientId
        
                    UNION ALL

                    SELECT
                            d.documentId as documentId,
                            d.total as total,
                            d.status,
                            if(d.status=1, 'Pagado','Sin Pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            DATE_FORMAT(d.date, '%d/%m/%y') as date

                            from clients c
                            inner join documentsnn d
                            on d.clientId=c.id
                            left JOIN  warehouse w
                            ON  d.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = d.typeDoc
                            where c.id= :clientId
        
                    UNION ALL

                    SELECT
                            cn.noteId as documentId,
                            cn.total as total,
                            cn.status,
                            if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            DATE_FORMAT(cn.date, '%d/%m/%y') as date

                            from clients c
                            inner join creditnotes cn
                            on cn.clientId=c.id
                            left JOIN  warehouse w
                            ON  cn.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = cn.typeDoc
                            where c.id= :clientId
                            
                    UNION ALL

                    SELECT
                            b.ballotId as documentId,
                            b.total as total,
                            b.status,
                            if(b.status=1, 'Cancelada','Sin pagar') AS "statusName",
                            w.dsCode AS "origin",
                            t.name as type,
                            DATE_FORMAT(b.date, '%d/%m/%y') as date

                            from clients c
                            inner join ballots b
                            on b.clientId=c.id
                            left JOIN  warehouse w
                            ON  b.origin = w.id
                            left JOIN  typeDocument t
                            ON  t.id = b.typeDoc
                            where c.id= :clientId `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsRut(rut){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT id,dsCode,category FROM clients WHERE dsCode = :rut`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            rut:rut
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientAccount(startDate,endDate,clientId){

    var deferred = Q.defer()
    let where = ''
    let sign = "'"
    if (startDate!='' && endDate!=''){
        where = ' WHERE date BETWEEN '+ sign+startDate+sign +' AND '+ sign+endDate+sign + ''
    }

    let sql = `     SELECT documentId, type, origin, DATE_FORMAT(dateFilter, '%d/%m/%y') as dateFilter, date , status, statusName, total, pay, pending, customerManager
                    FROM (

                           SELECT   i.invoiceId as documentId,
                                    i.total as total,
                                    i.status,
                                    s.otherName AS "statusName",
                                    IFNULL(i.total - SUM(p.total),i.total) AS pending,
                                    IFNULL(SUM(p.total),0) AS pay,
                                    w.dsCode AS "origin",
                                    t.name as type,
                                    i.date as dateFilter,
                                    c.customerManager as customerManager,
                                    DATE_FORMAT(i.date, '%y/%m/%d') as date

                                    from clients c
                                    inner join invoices i
                                    on i.clientId=c.id
                                    left JOIN payments p
                                    ON i.invoiceId=p.invoiceId
                                    left JOIN  warehouse w
                                    ON  i.origin = w.id
                                    left JOIN  typeDocument t
                                    ON  t.id = i.typeDoc
                                    left JOIN  status s
                                    ON  i.status = s.id
                                    where c.id= :clientId AND i.status != 2 
                                    GROUP BY i.invoiceId
                    
                                UNION ALL

                            SELECT
                                    cn.noteId as documentId,
                                    cn.total as total,
                                    cn.status,
                                    if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                                    IFNULL(- cn.total + SUM(p.total),- cn.total) AS pending,
                                    IFNULL(SUM(p.total),0) AS pay,
                                    w.dsCode AS "origin",
                                    t.name as type,
                                    cn.date as dateFilter,
                                    c.customerManager as customerManager,
                                    DATE_FORMAT(cn.date, '%y/%m/%d') as date

                                    from clients c
                                    inner join creditnotes cn
                                    on cn.clientId=c.id
                                    left JOIN paymentsnotes p
                                    ON cn.noteId=p.noteId
                                    left JOIN  warehouse w
                                    ON  cn.origin = w.id
                                    left JOIN  typeDocument t
                                    ON  t.id = cn.typeDoc
                                    where c.id= :clientId AND cn.status = 0
                                    GROUP BY cn.noteId

                            )consulta
                    ` + where + `
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getPaymentsTotal(clientId,startDate,endDate){

    var deferred = Q.defer()

    let sql = `   SELECT clientName, rut, DATE_FORMAT(date, '%d/%m/%y') as date , total, sum(pending) AS suma
                    FROM (

                           SELECT   c.dsName AS clientName,
                                    c.dsCode AS rut,
                                    i.total as total,
                                    IFNULL(i.total - SUM(p.total),i.total) AS pending,
                                    i.date

                                    from clients c
                                    inner join invoices i
                                    on i.clientId=c.id
                                    left JOIN payments p
                                    ON i.invoiceId=p.invoiceId
                                    where c.id= :clientId AND i.status != 2
                                    GROUP BY i.invoiceId

                                UNION ALL

                            SELECT
                                    c.dsName AS clientName,
                                    c.dsCode AS rut,
                                    cn.total as total,
                                    - cn.total AS pending,
                                    cn.date

                                    from clients c
                                    inner join creditnotes cn
                                    on cn.clientId=c.id
                                    where c.id= :clientId AND cn.status = 0
                                    GROUP BY cn.noteId

                            )consulta
                     
                     WHERE date BETWEEN '`+ startDate + `' AND '`+ endDate + `'
                    order BY date DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId,
            startDate:startDate,
            endDate:endDate
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getInvoicesNotificationClient(clientId){

    var deferred = Q.defer()

    let sql = `  SELECT   i.invoiceId as documentId,
                                    i.total as total,
                                    i.status,
                                    s.otherName AS "statusName",
                                    t.name as type,
                                    i.date

                                    from clients c
                                    inner join invoices i
                                    on i.clientId=c.id
                                    left JOIN  typeDocument t
                                    ON  t.id = i.typeDoc
                                    left JOIN  status s
                                    ON  i.status = s.id
                                    where c.id= :clientId AND i.status != 2
                                    GROUP BY i.invoiceId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getNotesNotificationClient(clientId){

    var deferred = Q.defer()

    let sql = `  SELECT
                            cn.noteId as documentId,
                            cn.total as total,
                            cn.status,
                            if(cn.status=1, 'Aplicada','No Aplicada') AS "statusName",
                            t.name as type,
                            DATE_FORMAT(cn.date, '%d/%m/%y') as date

                            from clients c
                            inner join creditnotes cn
                            on cn.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = cn.typeDoc
                            where c.id= :clientId AND cn.status = 0`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getChecksNotificationClient(clientId){

    var deferred = Q.defer()

    let sql = `  SELECT     ch.id as documentId,
                            ch.statusId,
                            t.name as type,
                            ch.date

                            from clients c
                            inner join checks ch
                            on ch.clientId=c.id
                            left JOIN  typeDocument t
                            ON  t.id = ch.typeDoc
                            where c.id= :clientId AND ch.statusId = 3
                            GROUP BY ch.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _getClientPayments(clientId){

    var deferred = Q.defer()

    let sql = `     
                SELECT id,documentId, total, dateFilter, DATE_FORMAT(date, '%d/%m/%y') as date, type, paymentmethod, origin, userName
                                    
                FROM (

                SELECT DISTINCT p.id,
                                p.invoiceId AS documentId,
                                p.total,
                                p.date as dateFilter,
                                p.date,
                                'Factura' AS type,
                                pm.dsName AS paymentmethod,
                                w.dsCode AS origin,
                                u.usName AS userName
                        FROM payments p
                                INNER JOIN paymentMethod pm 
                                ON p.paymentMethodId = pm.paymentMethodId
                                inner JOIN  users u
                                ON  p.usId = u.usId
                                inner JOIN  invoices i
                                ON  p.invoiceId = i.invoiceId
                                inner JOIN warehouse w
                                ON  i.origin = w.id
                                WHERE p.clientId = :clientId AND p.inStatus = 1
                                     
                UNION ALL

                SELECT DISTINCT p.id,
                                p.ballotId AS documentId,
                                p.total,
                                p.date as dateFilter,
                                p.date,
                                'Boleta' AS type,
                                pm.dsName AS paymentmethod,
                                w.dsCode AS origin,
                                u.usName AS userName
                        FROM paymentsballots p
                                INNER JOIN paymentMethod pm 
                                ON p.paymentMethodId = pm.paymentMethodId
                                inner JOIN  users u
                                ON  p.usId = u.usId
                                inner JOIN  ballots b
                                ON  p.ballotId = b.ballotId
                                inner JOIN warehouse w
                                ON  b.origin = w.id
                                WHERE b.clientId = :clientId AND p.inStatus = 1
                                     
                UNION ALL

                SELECT DISTINCT p.id,
                                p.documentId AS documentId,
                                p.total,
                                p.date as dateFilter,
                                p.date,
                                'NN' AS type,
                                pm.dsName AS paymentmethod,
                                w.dsCode AS origin,
                                u.usName AS userName
                        FROM paymentsnn p
                                INNER JOIN paymentMethod pm 
                                ON p.paymentMethodId = pm.paymentMethodId
                                inner JOIN  users u
                                ON  p.usId = u.usId
                                inner JOIN  documentsnn d
                                ON  p.documentId = d.documentId
                                inner JOIN warehouse w
                                ON  d.origin = w.id
                                WHERE d.clientId = :clientId AND p.inStatus = 1
                                     
                                )consulta
                                    
                            order BY dateFilter DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            clientId:clientId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateAddress(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE  clients SET 
                                 dsAddress=:address,
                                 dsCity=:cityName
                          WHERE  clients.id=:clientId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId: data.clientId,
            address: data.address,
            cityName: data.city

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getList: _getList,
    _getClientsByName:_getClientsByName,
    _getClientsByNameCategory:_getClientsByNameCategory,
    _getClientsByDsName:_getClientsByDsName,
    _getClientsDetails:_getClientsDetails,
    _createClient:_createClient,
    _updateClient:_updateClient,
    _disabledClient:_disabledClient,
    _lockedClient:_lockedClient,
    _clientHistory:_clientHistory,
    _getClientsRut:_getClientsRut,
    _getClientAccount:_getClientAccount,
    _getPaymentsTotal:_getPaymentsTotal,
    _getInvoicesNotificationClient:_getInvoicesNotificationClient,
    _getNotesNotificationClient:_getNotesNotificationClient,
    _getChecksNotificationClient:_getChecksNotificationClient,
    _activeCredit:_activeCredit,
    _getClientPayments:_getClientPayments,
    _updateAddress: _updateAddress,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}




