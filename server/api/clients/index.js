var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');

router.get('/list', service.getClients);
router.get('/name', service.getClientsByName);
router.get('/dsname', service.getClientsByDsName);
router.get('/category', service.getClientsByNameCategory);
router.get('/', service.getClientsDetails);
router.post('/', service.createClient);
router.put('/', service.updateClient);
router.delete('/', service.disabledClient);
router.get('/history', service.clientHistory);
router.get('/client-payments', service.getClientPayments);
router.put('/locked', service.lockedClient);
router.get('/rut', service.getClientsRut);
router.get('/account', service.getClientAccount); 
router.get('/invoices-notification', service.getInvoicesNotificationClient);
router.get('/notes-notification', service.getNotesNotificationClient);
router.get('/checks-notification', service.getChecksNotificationClient);
router.put('/credit', service.activeCredit);
router.put('/address', service.updateAddress);


module.exports = router;