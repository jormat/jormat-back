'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//Quotations clients
function _getQuotations(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        b.quotationId as "quotationId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        c.category AS "category",
                        b.invoiced AS "invoiceId",
                        c.dsCode AS "rut",
                        b.total,
                        t.code,
                        w.dsCode AS "origin",
                        w.id AS "warehouseId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        b.invoiced AS "invoiced",
                        b.discount as "discount",
                        if(b.status=1, 'Pagado','Sin pagar') AS "statusName"
                        
                    FROM quotations b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    left JOIN  typeDocument t
                    ON  t.id = b.invoicedDoc
                    WHERE b.inStatus = 1
                    order BY b.quotationId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getQuotationsDetails(quotationId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        b.quotationId as "quotationId",
                        c.dsName AS "clientName",
                        c.id AS "clientId",
                        c.dsCode AS "rut",
                        b.total,
                        t.code,
                        ROUND(b.total/1.19,0) as "netPrice",
                        b.total - ROUND(b.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        w.id AS "warehouseId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        us.usName AS "userModifier",
                        b.invoiced AS "invoiceId",
                        if(b.status=1, 'Pagado','Sin Pagar') AS "statusName",
                        b.discount AS "discount",
                        b.nameDescription,
                        b.phone,
                        b.commentary AS "comment"
                        
                    FROM quotations b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    left JOIN  users us
                    ON  b.usModifierId = us.usId
                    
                    left JOIN  typeDocument t
                    ON  t.id = b.invoicedDoc
                    WHERE b.quotationId =:quotationId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            quotationId:quotationId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getQuotationsItems(quotationId,originId){

    var deferred = Q.defer()

    let sql = `SELECT       i.id AS itemId,
                         i.dsName AS itemDescription,
                         ITR.dsReference as referenceKey,
                         i.brand AS brand,
                         i.oil AS oil,
                         i.dsMaxDiscount AS maxDiscount,
                         i.brandCode AS brandCode,
                         i.webType as webType,
                         qi.nmUnities as quantityItems,
                         qi.nmPrice AS price,
                         GROUP_CONCAT(distinct l.dsName) as "locations",
                         qi.discount AS discount,
                         qi.total
                 FROM quotationsitems qi
                 INNER JOIN items i
                 ON  i.id = qi.itemId
                 INNER JOIN  itemreferences ITR
                 ON ITR.itemId = i.id
                 INNER JOIN  itemwarehouselocation iwl
                 ON i.id = iwl.itemId and iwl.warehouseId = :originId
                 INNER JOIN  location l
                 ON l.id = iwl.locationId
                 WHERE quotationId= :quotationId 
                 GROUP BY    qi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            quotationId:quotationId,
            originId:originId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createQuotation(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.clientId == undefined) data.clientId = 4934
    if(data.name == undefined) data.name="s/n"
    if(data.phone == undefined) data.phone = "s/n"

    let sql = `INSERT INTO quotations(date,clientId,status,total,inStatus,origin,commentary,discount,usId,nameDescription,phone)
                      VALUES (NOW(),:clientId,1,:total,1,:originId,:observation,:discount,:userId,:name,:phone)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        replacements:{
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            name:data.name,
            phone:data.phone,
            observation:data.comment
        }
    }).then(function (quotationId) {
        deferred.resolve(quotationId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _updateQuotation(data){

    if(data.comment==undefined) data.comment="Sin Observación"
    if(data.discount == undefined) data.discount=0
    if(data.name == undefined) data.name="s/n"
    if(data.phone == undefined) data.phone = "s/n"

    var deferred = Q.defer()
    let sql = `DELETE FROM quotationsitems WHERE  quotationId = :quotationId;

               UPDATE  quotations SET  total =:total, 
                                       nameDescription =:name, 
                                       phone =:phone, 
                                       usModifierId =:userId, 
                                       commentary =:observation, 
                                       discount =:discount
                                  WHERE quotationId =:quotationId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            quotationId:data.quotationId,
            userId:data.userId,
            discount:data.discount,
            observation:data.comment,
            total:data.total,
            name:data.name,
            phone:data.phone
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


function _createItemsQuotation(items,quotationId){

    var deferred = Q.defer()
    console.log('look 3',quotationId)
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO quotationsitems (quotationId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:quotationId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100))`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            quotationId:quotationId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledQuotation(model){

    var data = [
       {
          "messagge": "the Quotation is disabled succesfully",
        }
    ]

    return data;
}

function _invoicedQuotation(data){

    var deferred = Q.defer()
    let sql = `UPDATE  quotations SET  invoiced =:documentId, 
                                       invoicedDoc =:docTypeId

                                  WHERE quotationId =:quotationId
               `

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            quotationId:data.quotationId,
            documentId:data.documentId,
            docTypeId:data.docTypeId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getClientsRut(rut){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT id,dsCode FROM clients WHERE dsCode = :rut`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            rut:rut
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createClient(data){

    var deferred = Q.defer()
    if(data.mobile==undefined) data.mobile="s/n"
    if(data.address==undefined) data.address='Sin dirección'
    if(data.web==undefined) data.web="www."
    if(data.commercialBusiness==undefined) data.commercialBusiness="Giro no definido" 
    if(data.customerManager==undefined) data.customerManager="No definido"
    if(data.credit==undefined) data.credit=0
    if(data.address2==undefined) data.address2=null
    
    let sql = ` INSERT INTO clients (
                                dsName,
                                dsCode,
                                dsAddress,
                                address2,
                                dsPhoneNumber,
                                dsMobileNumber,
                                dsEmail,
                                dsUrl,
                                regionId,
                                dsCity,
                                dsDesc,
                                customerManager,
                                inStatus,
                                locked,
                                isCredit) 
                      VALUES (  :fullName,
                                :rut,
                                :address,
                                :address2,
                                :phone,
                                :mobile,
                                :email,
                                :web,
                                :region,
                                :cityName,
                                :commercialBusiness,
                                :customerManager,
                                 2,
                                 0,
                                :credit)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            fullName: data.fullName,
            rut: data.rut,
            address: data.address,
            address2: data.address2,
            phone: data.phone,
            mobile: data.mobile,
            email: data.email,
            web: data.web,
            region: data.region,
            cityName: data.cityName,
            commercialBusiness: data.commercialBusiness,
            customerManager: data.customerManager,
            credit: data.credit


        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getQuotations: _getQuotations,
    _getQuotationsDetails:_getQuotationsDetails,
    _getQuotationsItems:_getQuotationsItems,
    _createQuotation:_createQuotation,
    _createItemsQuotation:_createItemsQuotation,
    _updateQuotation:_updateQuotation,
    _disabledQuotation:_disabledQuotation,
    _invoicedQuotation:_invoicedQuotation,
    _getClientsRut:_getClientsRut,
    _createClient:_createClient,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}