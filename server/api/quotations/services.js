'use strict'

var model = require('./model');
const fs = require('fs')
const csv = require('csv-express')
const Parse = require('csv-parse')

//Ballots clients

function uploadQuotations(req, res) {
    const fileUpload = req.file

    if (!fileUpload) {
        const error = {
            parent : { errno: 1 },
            message: 'archivo omitido',
            sql    : 'no sql',
        }

        res.json({
            data: [],
            status: 0,
            error
        });

        return
    }

    const folderPath = fileUpload.path.toString()
    const source = fs.createReadStream(folderPath)
    const parser = Parse({ delimiter: ';', columns: true })
    let linesRead = 0
    let output = []

    parser.on('readable', function () {
        let record = parser.read()

        while (record) {
            if ((Object.keys(record).length == 1)) {
                if (record[Object.keys(record)[0]].length != 0) {
                    errorLectura = "Delimitador debe ser ';' "

                    return false
                }
            } else {
                linesRead++
                if(record.codigo != undefined && record.codigo != null && record.codigo != '') {
                    output.push({
                        itemId : record.codigo,
                        itemDescription : record.nombre,
                        quantity : (record.cantidad == undefined || record.cantidad == null || record.cantidad == '') ? 0 : record.cantidad,
                        disscount : (record.desc == undefined || record.desc == null || record.desc == '') ? 0 : record.desc,
                        netPrice : (record.precio == undefined || record.precio == null || record.precio == '') ? 0 : record.precio,
                        referenceKey : (record.referencia == undefined || record.referencia == null || record.referencia == '') ? 0 : record.referencia
                    })
                }
            }

            record = parser.read()
        }
    })

    parser.on('end', function () {
        if (linesRead == 0) {
            res.status(500).json({ status: false, error: 'Archivo Vacio' })

            return false
        }

        res.json({
            data    : output,
            status     : 1,
        })
    })

    parser.on('error', function () {
        res.status(500).json({ status: false, error: 'Error de Formato' })

        return false
    })

    source.pipe(parser)
}

function getQuotations(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getQuotations(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getQuotationsDetails(req, res) {

    var query = [];
    var quotationId = req.query['quotationId'];
    query.push(model._getQuotationsDetails(quotationId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getQuotationsItems(req, res) {

    var query = [];
    var quotationId = req.query['quotationId']; 
    var originId = req.query['originId']; 
    query.push(model._getQuotationsItems(quotationId,originId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createQuotation(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createQuotation(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsQuotation(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create quotation",
            status: 0
        });
    }
}

function updateQuotation(req, res) {


    var query = [];
    var data = req.body;

    if(req.body){
        Promise.all([model._updateQuotation(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsQuotation(data.itemsInvoices[i],data.quotationId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services update quotation",
            status: 0
        });
    }
    // query.push(model._updateQuotation(data))
    
    // for(var i in data.itemsInvoices){
    //         query.push(model._createItemsQuotation(data.itemsInvoices[i],data.quotationId))
    //       }

    // Promise.all(query).then((result) => {
    //     res.json({
    //         data: result[0],
    //         msg: "quotation is update successfull",
    //         status: 1
    //     });
    // }).catch(function(error) {
    //     res.json({
    //         data: error,
    //         status: 0
    //     });
    // })
}

function disabledQuotation(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledBallot(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function invoicedQuotation(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._invoicedQuotation(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the cotizacion is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientsRut(req, res) {

    var query = [];
    var rut = req.query['rut'];
    console.log("rut...", rut)
    query.push(model._getClientsRut(rut))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createClient(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._createClient(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the client is created succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


module.exports = {
    getQuotations: getQuotations,
    getQuotationsDetails:getQuotationsDetails,
    getQuotationsItems:getQuotationsItems,
    createQuotation:createQuotation,
    updateQuotation:updateQuotation,
    disabledQuotation:disabledQuotation,
    invoicedQuotation:invoicedQuotation,
    getClientsRut:getClientsRut,
    createClient:createClient,
    poolSize: 10000,
    poolIdleTimeout: 30000000,
    uploadQuotations
}