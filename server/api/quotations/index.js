var express = require('express');
const multer = require('multer')
// view engine setup
var router = express.Router();
var service = require('./services');
const upload = multer({ dest: 'files/' })


//quotation clients
router.get('/list', service.getQuotations);
router.get('/', service.getQuotationsDetails);
router.get('/items', service.getQuotationsItems);
router.post('/', service.createQuotation);
router.put('/', service.updateQuotation);
router.put('/invoiced', service.invoicedQuotation);
router.delete('/', service.disabledQuotation);
router.post('/uploadQuotations', upload.single('uploadQuotations'), service.uploadQuotations);
//router.get('/confirm/rut', service.getClientsRut);
router.post('/confirm/', service.createClient);

module.exports = router;