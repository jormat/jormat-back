const fs = require('fs');
const path = require('path');

function getImage(req, res) {
    const imageName = req.params.imageName;
    const imagePath = getImagePath(imageName);
    if (imagePath) {
        console.log('existe');
        res.sendFile(imagePath);
        res.status(200);
    } else {
        console.log('La imagen no existe');
        res.json({
            data: [],
            status: 0,
        });
        res.status(400);
        return
    }
}
async function uploadImage(req, res) {
    if (!req.file) {
        const error = {
            parent: { errno: 1 },
            message: 'archivo omitido',
            sql: 'no sql',
        }
        res.json({
            data: [],
            status: 0,
            error
        });
        return
    }
    const mimeType = req.file.mimetype;
    const extension = req.file.originalname.split('.').pop().toLowerCase();
    // Controlamos que se eliminen los archivos que no son imagen
    if (!mimeType.startsWith('image/') && !['jpg', 'png'].includes(extension)) {
        fs.unlinkSync(req.file.path);
        res.status(400).send({ error: 'El archivo debe ser una imagen (JPG o PNG)' })
    } else {

        existe(req.file.filename);
        
        const imageData = fs.readFileSync(req.file.path);
        res.send({ data: imageData, status: 1, });
        res.status(200);
    }
}


//Reutilizables
function getImagePath(url) {
    const extensions = ['.png', '.jpg', '.jpeg'];
    const imagePath = extensions.map(ext => path.join(__dirname, '../../../files', url + ext)).find(fs.existsSync);
    return imagePath;
}

function existe(filename) {
    const dirPath = path.join(__dirname, "../../../files"); // Ruta a la carpeta donde se buscará el archivo
    const basename = path.parse(filename).name; // Nombre del archivo sin la extensión
    const ext = path.parse(filename).ext; // Extensión del archivo
  
    // Obtener todos los archivos en la carpeta que empiezan con el mismo nombre
    const files = fs.readdirSync(dirPath).filter(file => file.startsWith(basename));
  
    // Eliminar todos los archivos con el mismo nombre pero diferente extensión
    files.filter(file => path.parse(file).ext !== ext)
         .forEach(file => fs.unlinkSync(path.join(dirPath, file)));
}

module.exports = {
    getImage: getImage,
    uploadImage: uploadImage,
    getImagePath: getImagePath,
    existe: existe
}
