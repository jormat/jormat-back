const express = require('express');
var   router  = express.Router();
const service    = require('./service'),
      multer     = require('multer'),
      bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const storage = multer.diskStorage({
  destination: (req, file, cb) => { cb(null, 'files/'); },
  filename: (req, file, cb) => { cb(null, req.params.itemId + '.' + file.originalname.split('.').pop()) }
})
const  upload = multer({ storage })

router.post('/subir/:itemId', upload.single('file'), service.uploadImage);

router.get('/image/:imageName', service.getImage);

module.exports = router;
