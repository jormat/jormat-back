'use strict'
const invoicesModel = require('../invoices/model')
const ballotsModel = require('../ballots/model')
const documentsModel = require('../documents/model')
const warehousesModel = require('../warehouses/model')
const clientsModel = require('../clients/model')
const notesModel = require('../notes/model')
const reportsModel = require('../reports/model')
const itemsModel = require('../items/model')
const financeModel = require('../finance/model')

function getInvoicesList(req, res) {
    const dsRut = req.query.dsCode

    invoicesModel._getInvoices(dsRut).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBallotsList(req, res) {
    ballotsModel._getBallots(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getDocumentsList(req, res) {
    documentsModel._getDocuments(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getWarehousesList(req, res) {
    warehousesModel._getWarehouses(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getClientsList(req, res) {
    clientsModel._getList(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getNotesCreditList(req, res) {
    notesModel._getCreditNotes(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getReportsItemsPurchased(req, res) {
    reportsModel._getItemsPurchased(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getReportsItemsSold(req, res) {
    reportsModel._getItemsSold(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getItemsList(req, res) {
    itemsModel._getList().then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getDocumentsImportList(req, res) {
	documentsModel._getImports().then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getInvoicesProvidersList(req, res) {
	invoicesModel._getInvoicesProvider(1).then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getExpenseControlList(req, res) {
	financeModel._getExpenseControl().then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getAccountsList(req, res) {
	financeModel._getAccounts().then((result) => {
        res.json({
            data: result,
            status: 1
        });
    }).catch((error) => {
        res.json({
            data: error,
            status: 0
        });
    })
}

module.exports = {
    getInvoicesList,
	getBallotsList,
	getDocumentsList,
	getWarehousesList,
	getClientsList,
	getNotesCreditList,
	getReportsItemsPurchased,
	getReportsItemsSold,
	getItemsList,
	getDocumentsImportList,
	getInvoicesProvidersList,
	getExpenseControlList,
	getAccountsList
}
