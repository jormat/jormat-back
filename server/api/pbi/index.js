const express = require('express')
const router = express.Router()
const service = require('./services');
const serviceUsers = require('../users/services');

router.get('/invoices/list', service.getInvoicesList)
router.get('/ballots/list', service.getBallotsList)
router.get('/documents/nn/list', service.getDocumentsList)
router.get('/warehouses/list', service.getWarehousesList)
router.get('/clients/list', service.getClientsList)
router.get('/notes/credit/list', service.getNotesCreditList)
router.get('/reports/items-purchased', service.getReportsItemsPurchased)
router.get('/reports/items-sold', service.getReportsItemsSold)
router.get('/items/list', service.getItemsList)
router.get('/documents/import/list', service.getDocumentsImportList)
router.get('/invoices/providers/list', service.getInvoicesProvidersList)
router.get('/expenseControl/list', service.getExpenseControlList)
router.get('/accounts/list', service.getAccountsList)
router.get('/users/getUsers', serviceUsers.getUsers)

module.exports = router;
