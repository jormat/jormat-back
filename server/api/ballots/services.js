'use strict'

var model = require('./model');

//Ballots clients

function getBallots(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._getBallots(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBallotsDetails(req, res) {

    var query = [];
    var ballotId = req.query['ballotId'];
    query.push(model._getBallotsDetails(ballotId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function getBallotsItems(req, res) {

    var query = [];
    var ballotId = req.query['ballotId'];
    var originId = req.query['originId']; 
    query.push(model._getBallotsItems(ballotId,originId))

    Promise.all(query).then((result) => {
      
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function createBallot(req, res) {

    var query = [];
    var data = req.body;
    if(req.body){
        Promise.all([model._createBallot(data)]).then((result) => {
            query=[]
            for(var i in data.itemsInvoices){
                query.push(model._createItemsBallot(data.itemsInvoices[i],result[0],data.originId))
              }
                Promise.all(query).then((results) => {
    
                    res.json({
                        data: result[0],
                        status: 1
                    });
    
                }).catch(function(error) {
                    res.json({
                        data: error,
                        status: 0
                    });
                })
    
        }).catch(function(error) {
            res.json({
                data: error,
                status: 0
            });
        })

    }else{
        res.json({
            data: [],
            messagge:"not data this services create ballot",
            status: 0
        });
    }
}

function updateBallot(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updateBallot(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the ballot is disabled succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function disabledBallot(req, res) {

    var query = [];
    var queryValue = req.query['queryValue'];
    query.push(model._disabledBallot(1))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}

function updatePackOff(req, res) {

    var query = [];
    var data = req.body;
    query.push(model._updatePackOff(data))

    Promise.all(query).then((result) => {
        res.json({
            data: result[0],
            messagge: "the ballot is update succesfully",
            status: 1
        });
    }).catch(function(error) {
        res.json({
            data: error,
            status: 0
        });
    })
}


module.exports = {
    getBallots: getBallots,
    getBallotsDetails:getBallotsDetails,
    getBallotsItems:getBallotsItems,
    createBallot:createBallot,
    updateBallot:updateBallot,
    disabledBallot:disabledBallot,
    updatePackOff:updatePackOff,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}