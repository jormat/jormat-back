'use strict'

import sqldb from '../../sqldb'
var Sequelize = require('sequelize');
var sequelize = sqldb.sequelize;

var Q = require('q');

//Ballots clients
function _getBallots(){

    var deferred = Q.defer()

    let sql = `     SELECT DISTINCT
                        b.ballotId as "ballotId",
                        c.id AS "clientId",
                        c.dsName AS "clientName",
                        c.dsCode AS "rut",
                        b.total,
                        w.dsCode AS "origin",
                        b.origin AS "originId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        u.usName AS "userCreation",
                        if(b.packOff=0, 'No','Si') AS "packOff",
                        b.isOut,
                        if(b.status=1, 'Sin pagar','Pagado') AS "statusName"
                        
                    FROM ballots b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    WHERE b.inStatus = 1
                    order BY b.ballotId DESC`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            userId:1
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _getBallotsDetails(ballotId){

    var deferred = Q.defer()

    let sql = `SELECT DISTINCT
                        b.ballotId as "ballotId",
                        c.dsName AS "clientName",
                        c.dsPhoneNumber AS "phone",
                        c.dsAddress AS "address",
                        c.dsCity AS "city",
                        c.dsCode AS "rut",
                        b.total,
                        ROUND(b.total/1.19,0) as "netPrice",
                        b.total - ROUND(b.total/1.19)  as "priceVAT",
                        w.dsCode AS "origin",
                        b.origin AS "originId",
                        DATE_FORMAT(b.date, '%d/%m/%y') as date,
                        DATE_FORMAT(b.date, '%H:%i:%S') as time,
                        u.usName AS "userCreation",
                        u2.usName AS "userOut",
                        b.transportationName as "transportationName",
                        b.isOut,
                        us.usName AS "userUpdate",
                        if(b.status=1, 'Sin Pagar','Pagado') AS "statusName",
                        pm.dsName AS paymentForm,
                        DATE_FORMAT(b.datePayment, '%d/%m/%y') as datePayment,
                        b.discount AS "discount",
                        b.commentary AS "comment",
                        if(b.packOff=0, 'No','Si') AS "packOff",
                        td.code AS typeName,
						td.id AS typeId
                        
                    FROM ballots b
                    inner JOIN  clients c
                    ON  b.clientId = c.id
                    left JOIN  warehouse w
                    ON  b.origin = w.id
                    left JOIN  users u
                    ON  b.usId = u.usId
                    left JOIN  users us
                    ON  b.usModifierId = us.usId
                    left JOIN  users u2
                    ON  b.userOutId = u2.usId
                    INNER JOIN paymentmethod pm
                    ON  pm.paymentMethodId = b.paymentFormId
                    LEFT JOIN	typedocument td
			        ON			td.id = b.typeDoc
                    WHERE b.ballotId = :ballotId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            ballotId:ballotId
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise   
}

function _getBallotsItems(ballotId,originId){

    var deferred = Q.defer()
    if(originId == undefined) originId=1

    let sql = `SELECT   i.id as itemId,
                        i.dsName AS itemDescription,
                        i.oil AS oil,
                        i.webType as webType,
                        i.brandCode AS brandCode,
                        pi.nmUnities as quantityItems,
                        ITR.dsReference as referenceKey,
                        pi.nmPrice AS price,
                        GROUP_CONCAT(distinct l.dsName) as "locations",
                        pi.discount AS discount,
                        pi.total
                FROM ballotsitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                INNER JOIN  itemreferences ITR
                ON ITR.itemId = i.id
                INNER JOIN  itemwarehouselocation iwl
                 ON i.id = iwl.itemId and iwl.warehouseId = :originId
                 INNER JOIN  location l
                 ON l.id = iwl.locationId
                WHERE ballotId =:ballotId
                GROUP BY    pi.id`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.SELECT,
        replacements:{
            ballotId:ballotId,
            originId:originId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise  
}

function _createBallot(data){

    var deferred = Q.defer()
    if(data.comment==undefined) data.comment="Sin Observación" 
    if(data.discount == undefined) data.discount=0
    if(data.clientId == undefined) data.clientId = 4934
    if(data.packOff == false) data.packOff=0
    if(data.transportationName == undefined) data.transportationName = null
    if(data.transportationId == undefined) data.transportationId=null

    let sql = `INSERT INTO ballots(date,clientId,status,total,inStatus,origin,commentary,packOff,discount,usId,transportationId,transportationName,paymentFormId)
                      VALUES (NOW(),:clientId,1,:total,1,:originId,:observation,:packOff,:discount,:userId,:transportationId,:transportationName,:paymentmethodId)`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            clientId:data.clientId,
            total:data.total,
            userId:data.userId,
            originId:data.originId,
            discount:data.discount,
            observation:data.comment,
            packOff:  data.packOff,
            transportationName:data.transportationName,
            transportationId:  data.transportationId,
            paymentmethodId: data.paymentmethodId

        }
    }).then(function (ballotId) {
        deferred.resolve(ballotId)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _createItemsBallot(items,ballotId,originId){

    var deferred = Q.defer()
    if(items.disscount == undefined) items.disscount=0

    let sql = `   INSERT INTO ballotsitems (ballotId,
                                            itemId, 
                                            nmPrice,
                                            nmUnities,
                                            discount,
                                            total) 
                                    VALUES (:ballotId,
                                            :itemId,
                                            :price,
                                            :quantity,
                                            :disscount,
                                            (:price * :quantity - (:disscount * (:price * :quantity)) / 100));
                                                    
                  UPDATE itemwarehouse SET itemQty=(itemQty-:quantity) 
                                       WHERE itemId=:itemId 
                                       AND warehouseId=:originId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            ballotId:ballotId,
            originId:originId,
            itemId:items.itemId,
            price:items.price,
            quantity:items.quantity,
            disscount:items.disscount
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _updateBallot(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE ballots SET status=1, 
                                      usModifierId =:userId
                                      WHERE ballotId=:documentId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            userId:data.userId,
            documentId:data.documentId

        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}

function _disabledBallot(model){

    var data = [
       {
          "messagge": "the Ballot is disabled succesfully",
        }
    ]

    return data;
}

function _updatePackOff(data){

    var deferred = Q.defer()
    
    let sql = `UPDATE ballots SET packOff=1 WHERE ballotId=:ballotId`

    sequelize.query(sql, {
        type: Sequelize.QueryTypes.INSERT,
        replacements:{
            ballotId:data.ballotId
            
        }
    }).then(function (result) {
        deferred.resolve(result)
    }).catch(function (error){
        deferred.reject(error)
    })

    return deferred.promise
}


module.exports = {
    _getBallots: _getBallots,
    _getBallotsDetails:_getBallotsDetails,
    _getBallotsItems:_getBallotsItems,
    _createBallot:_createBallot,
    _createItemsBallot:_createItemsBallot,
    _updateBallot:_updateBallot,
    _disabledBallot:_disabledBallot,
    _updatePackOff:_updatePackOff,
    poolSize: 10000,
    poolIdleTimeout: 30000000
}