var express = require('express');

// view engine setup
var router = express.Router();
var service = require('./services');


//Ballots clients
router.get('/list', service.getBallots);
router.get('/', service.getBallotsDetails);
router.get('/items', service.getBallotsItems);
router.post('/', service.createBallot);
router.put('/', service.updateBallot);
router.delete('/', service.disabledBallot);
router.put('/packoff', service.updatePackOff);


module.exports = router;