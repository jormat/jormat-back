'use strict';

var gulp = require('gulp'),
// server     = require( './server/index.js' ),
server = require( 'gulp-develop-server' ),
livereload = require( 'gulp-livereload' ),
env = require('gulp-env'),
localenv = require('./server/config/local.env.js'),
memory = 4096;
// v8.setFlagsFromString('--max_old_space_size=4096');

if(localenv.MAX_OLD_SPACE_SIZE !== 'undefiend'){
	memory = localenv.MAX_OLD_SPACE_SIZE
}

var options = {
	path: './server/index.js',
	execArgv: [ '--max-old-space-size='+memory ]
};

// npm install  gulp  gulp-develop-server  gulp-livereload gulp-env
// netstat -a -o -n
// taskkill /F /PID  1888
// process.env.NODE_OPTIONS= '--max_old_space_size=4096 ',
process.env.NODE_ENV = 'development'

var serverFiles = [
	'./server/*',
	'./server/**/*',
	'./server/**/**/*',
	'./server/**/**/**/*',
	'./server/**/**/**/**/*',
	'./server/**/*.js',
	'./server/api/**/*.js',
	'./server/api/**/**/*.js',
	'./server/api/**/*.json'];

gulp.task( 'server:start', function() {
	server.listen(options, livereload.listen);
});

// If server scripts change, restart the server and then livereload.

gulp.task( 'default', ['server:start'], function() {

	function restart(file) {
		console.log('file changed',file.path)
		server.changed( function(error) {
			if(!error) {
				livereload.changed( file.path );
			}else{
				server.restart( function(error) {});
			}
		});
	}

	var watcher = gulp.watch(serverFiles).on('change', server.restart);
	var timeout = setTimeout(watcher.end, 60*60*1000);

	watcher.on('change', function() {
		console.log('.. we clear the timeout call we created earlier ...')
		clearTimeout(timeout);
		// .. and start it anew ...
		timeout = setTimeout(watcher.end, 60*60*1000);
	});
});